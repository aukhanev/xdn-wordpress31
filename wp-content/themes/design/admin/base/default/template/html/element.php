<?php /* @var $this Xedin_Html_Block_Element */ ?>
<<?php echo $this->getTagName() ?> <?php echo $this->getAttributeString() ?><?php if( $this->isSelfClosing() ): ?> /<?php endif ?>><?php echo $this->getContent() ?><?php if( !$this->isSelfClosing() ): ?>	
</<?php echo $this->getTagName() ?>>
<?php endif ?>
