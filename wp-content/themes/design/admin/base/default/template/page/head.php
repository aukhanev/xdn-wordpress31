<?php /* @var $this Xedin_Page_Block_Head */ ?>
	<head>
		<title><?php echo $this->getTitle() ?></title>
		
		<?php foreach( $this->getTags(Xedin_Page_Block_Head::TAG_TYPE_JS) as $_jsIdx => $_jsUrl ): ?>
		<script type="text/javascript" src="<?php echo $_jsUrl ?>"></script>
		<?php endforeach ?>
		
		<?php foreach( $this->getTags(Xedin_Page_Block_Head::TAG_TYPE_CSS) as $_cssIdx => $_cssUrl ): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $_cssUrl ?>" />
		<?php endforeach ?>
		
		<?php if( $this->hasFavicon() ): ?>
		<link rel="icon" type="image/png" href="<?php echo $this->getFavicon() ?>" />
		<link rel="icon" href="<?php echo $this->getFavicon() ?>" type="image/x-icon"> 
		<link rel="shortcut icon" href="<?php echo $this->getFavicon() ?>" type="image/x-icon"> 
		<?php endif ?>
	</head>