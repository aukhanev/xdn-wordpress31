<?php /* @var $this Xedin_Users_Block_User_Controls */ ?>
<div class="user-controls">
	<?php if( !$this->isUserLoggedIn() ): ?>
	<a href="<?php echo $this->getLoginUrl() ?>" title="Login">Login</a>
	<?php else: ?>
	Welcome, <?php echo $this->getUser()->getUsername() ?>!&nbsp;|&nbsp;<a href="<?php echo $this->getLogoutUrl() ?>">Logout</a>
	<?php endif ?>
</div>