<?php /* @var $this Xedin_Core_Block_Template */ ?>
	<body>
		<div class="wrapper">
			<header class="header-wrapper">
				<div class="header">
					<?php echo $this->getChildHtml('header') ?>
				</div>
			</header>
			<div class="main-wrapper">
				<div class="main">
					<?php echo $this->getChildHtml('main') ?>
				</div>
			</div>
			<footer class="footer-wrapper">
				<div class="footer">
					<?php echo $this->getChildHtml('footer') ?>
				</div>
			</footer>
		</div>
	</body>