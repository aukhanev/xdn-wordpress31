<?php /* @var $this Xedin_Adminhtml_Block_Menu_Models */ ?>
<div class="menu menu-models">
	<?php $models = $this->getModels() ?>
	<?php $modelsCount = count($models) ?>
	<?php if( $modelsCount ): ?>
	<ul>
		<?php $i = 0; foreach( $models as $_modelName => $_modelConfig ): ?>
		<li class="<?php if( !$i ): ?>first<?php endif ?><?php if( $i == ($modelsCount - 1) ): ?> last<?php endif ?><?php if( $_modelName == $this->getCurrentModel() ): ?> current-item<?php endif ?>">
			<div class="module">
				<a href="<?php echo $this->getAdminLinkUrl('*', $_modelName) ?>"><?php echo $_modelConfig['label'] ?></a>
			</div>
		</li>
		<?php $i++; endforeach ?>
	</ul>
	<?php endif ?>
</div>