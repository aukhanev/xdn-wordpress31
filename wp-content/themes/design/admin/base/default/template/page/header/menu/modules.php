<?php /* @var $this Xedin_Adminhtml_Block_Menu_Modules */ ?>
<div class="menu menu-modules">
	<?php $modules = $this->getModules() ?>
	<?php $modulesCount = count($modules) ?>
	<?php if( $modulesCount ): ?>
	<ul>
		<?php $i = 0; foreach( $modules as $_moduleName => $_moduleConfig ): ?>
		<li class="<?php if( !$i ): ?>first<?php endif ?><?php if( $i = $modulesCount - 1 ): ?> last<?php endif ?><?php if( $_moduleName == $this->getCurrentModule() ): ?> current-item<?php endif ?>">
			<div class="module">
				<a href="<?php echo $this->getAdminLinkUrl($_moduleName) ?>"><?php echo $_moduleConfig['label'] ?></a>
			</div>
		</li>
		<?php $i++; endforeach ?>
	</ul>
	<?php endif ?>
</div>
