<?php /* @var $this Xedin_Adminhtml_Block_Grid_List_Controls */ ?>
<?php $pagesAmount = $this->getPagesAmount() ?>
<?php $currentPage = $this->getCurrentPage() ?>
<div class="controls">
	<div class="summary">
		Viewing records <span class="starts-with"><?php echo $this->getStartingRecordNumber() ?></span> to <span class="ends-with"><?php echo $this->getEndingRecordNumber() ?></span> of <span class="total"><?php echo $this->getTotalRecordsAmount() ?></span>
	</div>
	<div class="pagination">
		<?php if( $pagesAmount > 1 ): ?>
		<ul>
			<li class="first">
				<div class="goto goto-first">
					<a href="<?php echo $this->getPageUrl(0) ?>">&laquo;</a>
				</div>
			</li>
			<li>
				<div class="goto goto-previous">
					<?php $previousPage = $this->getPreviousPageNumber() ?>
					<?php if( !is_null($previousPage) ): ?>
					<a href="<?php echo $this->getPageUrl($previousPage) ?>">
					<?php else: ?>
					<span class="no-link">
					<?php endif ?>
						&larr;
					<?php if( !is_null($previousPage) ): ?>
					</a>
					<?php else: ?>
					</span>
					<?php endif ?>
				</div>
			</li>
			<?php for( $i = 0; $i < $pagesAmount; $i++ ): ?>
			<?php $awayFromCurrent = abs($currentPage - $i) ?>
			<?php $allowedDistance = (ceil($this->getVisiblePagesAmount()) / 2) ?>
			<?php $isCurrent = $currentPage == $i ?>
			<?php if( ($awayFromCurrent > $allowedDistance) ) continue ?>
			<li>
				<div class="page-number page-number-<?php echo $i ?>">
					<?php if( !$isCurrent ): ?>
					<a href="<?php echo $this->getPageUrl($i) ?>">
					<?php else: ?>
					<span class="no-link">
					<?php endif ?>
						<?php echo $i + 1 ?>
					<?php if( !$isCurrent ): ?>
					</a>
					<?php else: ?>
					</span>
					<?php endif ?>
				</div>
			</li>
			<?php endfor ?>
			<li>
				<div class="goto goto-next">
					<?php $nextPage = $this->getNextPageNumber() ?>
					<?php if( !is_null($nextPage) ): ?>
					<a href="<?php echo $this->getPageUrl($nextPage) ?>">
					<?php else: ?>
					<span class="no-link">
					<?php endif ?>
						&rarr;
					<?php if( !is_null($nextPage) ): ?>
					</a>
					<?php else: ?>
					</span>
					<?php endif ?>
				</div>
			</li>
			<li class="last">
				<div class="goto goto-last">
					<a href="<?php echo $this->getPageUrl($pagesAmount - 1) ?>">&raquo;</a>
				</div>
			</li>
		</ul>
		<?php endif ?>
	</div>
</div>