<?php /* @var $this Xedin_Adminhtml_Block_Grid_Details */ ?>
<?php $fields = $this->getFields() ?>
<?php $fieldsCount = count($fields) ?>
<?php $model = $this->getCurrentObject() ?>
<div class="grid-list">
	<div class="<?php echo $this->getModelClassName() ?>">
		<div class="model-uri"><h3><?php echo $this->getModelName() ?></h3></div>
		<div class="model-info">
			<?php if( $fieldsCount ): ?>
			<form action="<?php echo $this->getFormActionUrl() ?>" method="post" id="<?php echo $this->getFormName() ?>">
				<input type="hidden" name="<?php echo $this->getFieldName( $model->getIdFieldName() ) ?>" value="<?php echo $model->getId() ?>" />
				<ul>
				<?php $i=0; foreach( $fields as $_colIdx => $_fieldInfo ): ?>
					<?php $fieldName = $_fieldInfo[Xedin_Adminhtml_Block_Grid_Details::COLUMN_INFO_INDEX_NAME] ?>
					<?php $isId = ($fieldName == 'id') ?>
					<?php if( $isId ): continue; endif?>
					<?php $fieldField = $_fieldInfo[Xedin_Adminhtml_Block_Grid_Details::COLUMN_INFO_INDEX_FIELD] ?>
					<?php $fieldLabel = $_fieldInfo[Xedin_Adminhtml_Block_Grid_Details::COLUMN_INFO_INDEX_LABEL] ?>
					<?php $fieldId = $this->getFieldId($fieldField) ?>
					<li class="<?php if( !$i ): ?>first<?php endif ?><?php if( $i == ($fieldsCount - 1) ): ?> last<?php endif ?>">
						<div class="input-field input-field-<?php echo $fieldName ?>">
							<label for="<?php echo $fieldId ?>"><?php echo $fieldLabel ?></label>
							<?php echo $this->getFieldValue($model, $fieldName) ?>
							<!--input type="text" name="<?php echo $this->getFieldName($fieldField) ?>" id="<?php echo $fieldId ?>" value="<?php echo $model->getData($fieldField) ?>" /-->
						</div>
					</li>
				<?php $i++; endforeach ?>
				</ul>
				<?php echo $this->getChildHtml() ?>
			</form>			
			<?php endif ?>
		</div>
	</div>
</div>