<?php /* @var $this Xedin_Adminhtml_Block_Grid_List */ ?>
<?php $collection = $this->getCollection() ?>
<?php $profiler = $collection->getProfiler()->setEnabled(true) ?>
<?php $items = $collection->getItems() ?>
<?php $itemsCount = $collection->count() ?>
<?php $columns = $this->getFields() ?>
<?php $columnsCount = count($columns) ?>
<div class="grid-list">
	<div class="<?php echo $this->getModelClassName() ?>">
		<div class="model-uri"><h3><?php echo $this->getModelUri() ?></h3></div>
		<?php echo $this->getControlsBlock()->toHtml() ?>
		<div class="model-list">
			<?php if( $columnsCount ): ?>
			<table cellpadding="0" cellspacing="0" width="100%">
				<colgroup>
				<?php foreach( $columns as $_colIdx => $_colInfo ): ?>
					<col width="<?php echo $this->getColumnInfo($_colInfo, 'width') ?>" />
				<?php endforeach ?>
				</colgroup>
				<thead>
					<tr class="heading">
					<?php foreach( $columns as $_colIdx => $_colInfo ): ?>
					<th><?php echo $this->getColumnInfo($_colInfo, 'label') ?></th>
					<?php endforeach ?>
					</tr>
				</thead>
				
				<tbody>
					<?php $i = 0; foreach( $items as $_id => $_model ): ?>
					<tr class="model <?php if( !$i ): ?>first<?php endif ?><?php if( $i == ($itemsCount - 1) ): ?> last<?php endif ?>" onclick="top.location = '<?php echo $this->getModelViewUrl($_id) ?>'">
						<?php $k = 0; foreach( $columns as $_colIdx => $_colInfo ): ?>
						<td class="col-<?php echo $this->getColumnInfo($_colInfo, 'name') ?><?php if( !$k ): ?> first<?php endif ?><?php if( $k == ($columnsCount - 1) ): ?> last<?php endif ?><?php if( $k%2 ): ?> even<?php else: ?> odd<?php endif ?>">
							<?php if( $this->getColumnInfo($_colInfo, 'col_type') == 'data' ): ?>
								<?php $fieldValue = $this->getFieldValue( $_model, $this->getColumnInfo($_colInfo, 'field') )?>
								<?php echo !is_null($fieldValue) ? $fieldValue : 'N/A' ?>
							<?php elseif( $this->getColumnInfo($_colInfo, 'col_type') == 'actions' ): ?>
								<a href="<?php echo $this->getModelViewUrl($_id) ?>" title="View entity with ID #<?php echo $_model->getId() ?>">View</a>
								<a href="<?php echo $this->getModelDeleteUrl($_id) ?>" title="Delete entity with ID #<?php echo $_model->getId() ?>">Delete</a>
							<?php endif ?>
						</td>
						<?php $k++; endforeach ?>
					</tr>
				<?php $i++; endforeach ?>
				</tbody>
			</table>
			<?php endif ?>
		</div>
		<?php echo $this->getControlsBlock()->toHtml() ?>
	</div>
</div>