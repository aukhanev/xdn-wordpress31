<?php /* $this Xedin_Adminhtml_Block_Main_Controls */ ?>
<?php $items = $this->getItems() ?>
<?php $itemsCount = count($items) ?>
<div class="main-controls">
	<?php if( $itemsCount ): ?>
	<ul>
		<?php foreach( $items as $_idx => $_item ): ?>
		<li>
			<a href="<?php echo $_item->getUrl() ?>" title="<?php echo $_item->getTitle() ?>"><?php echo $_item->getLabel() ?></a>
		</li>
		<?php endforeach ?>
	</ul>
	<?php endif ?>
</div>