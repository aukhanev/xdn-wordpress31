<?php /* @var $this Xedin_Adminhtml_Block_Login */ ?>
<div class="box box-login">
	<form action="<?php echo $this->getFormActionUrl() ?>" method="post">
		<ul>
			<?php foreach( $this->getFields() as $_fieldName => $_fieldInfo ): ?>
			<li>
				<div class="input-field input-text">
					<label for="<?php echo $this->getUserFieldId($_fieldName) ?>"><?php echo $this->getUserFieldLabel($_fieldName) ?></label>
					<?php $tagName = 'input' ?>
					<?php $type = (isset($_fieldInfo['type']) && $_fieldInfo['type'] == 'password') ? 'password' : 'text' ?>
					<<?php echo $tagName ?> type="<?php echo $type ?>" name="<?php echo $this->getUserFieldName($_fieldName) ?>" id="<?php echo $this->getUserFieldId($_fieldName) ?>"/>
				</div>
			</li>
			<?php endforeach ?>
			<li>
				<div class="buttons">
					<button type="submit">Login</button>
				</div>
			</li>
		</ul>
	</form>
</div>