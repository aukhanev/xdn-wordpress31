<?php /* @var $this Xedin_Competitions_Block_Leaderboard */ ?>
<?php if( !$this->isLiked() || !$this->getFacebook()->getUser() ) return ?>
<div class="leaderboard">
	<?php /* @var $this Xedin_Competitions_Block_Leaderboard */ ?>
	<?php $reportCollection = $this->getReportCollection() ?>
	<?php // $profiler = $reportCollection->getProfiler()->setEnabled(true) ?>
	<?php $profiler = $reportCollection->getProfiler()->setEnabled(true) ?>
	<?php $reportCount = $reportCollection->count() ?>
	<div class="profiles" style="display: none">
		<?php var_dump($profiler->getQueryProfiles()) ?>
	</div>
	<?php // var_dump($profiler->getQueryProfiles()) ?>
	<?php $helper = $this->getNumberHelper() ?>
	<?php /* @var $_report Xedin_Competitions_Model_Participant_Game_Report */ ?><?php if( $reportCount ): ?>
	<div class="main-header"></div>
	<hr class="rainbow" />
	<?php $currentReport = $reportCollection->getItemByField($this->getCurrentParticipant()->getId()) ?>
	<div style="display: none;">
		<?php foreach( $reportCollection as $_idx => $_item ): ?>
		<?php var_dump($_item->getData()) ?>
		<?php endforeach ?>
		<?php var_dump($this->getCurrentParticipant()->getId()) ?>
		<?php var_dump($currentReport) ?>
	</div>
	<?php if( $currentReport ): ?>
	<div class="you">
		<div class="name"><span class="label">You</span><span class="value"><?php echo $currentReport->getFirstName() ?> <?php echo $currentReport->getLastName() ?></span></div>
		<div class="points"><span class="label">Points</span><span class="value"><?php echo number_format($currentReport->getPointTotal(), 0, ".",",") ?></span></div>
		<div class="rank"><span class="label">Rank</span><span class="value"><?php echo $currentReport->getRank() ?></span></div>
	</div>
	<hr />
	<?php endif ?>
	<div class="col-header">
		<div class="rank"><span>Rank</span></div>
		<div class="name"><span>Name</span></div>
		<div class="points"><span>Points</span></div>
		<div class="prize"><span>Prize</span></div>
	</div>
	<ul>
		<?php $currentReport = null ?>
		<?php $i = 0; foreach( $reportCollection as $_id => $_report ): ?>
		<?php if( $i == $this->getLimit() ) break; ?>
		<?php $isCurrentReport = false; ?>
			<?php if( $this->isCurrentReport($_report) ): ?>
			<?php $currentReport = $_report ?>
			<?php $isCurrentReport = true ?>
			<?php endif ?>
			<?php $rank = $_report->getRank() ?>
			<?php $awardMedal = $rank < 4 ?>
		<li class="<?php if( !$i ): ?>first<?php endif ?><?php if( $i == ($reportCount - 1) ): ?> last<?php endif ?><?php if( $isCurrentReport ): ?> current<?php endif ?>">
			<div class="report">
				<div class="rank report-section">
					<?php if( $awardMedal && false ): ?>
					<div class="medal medal-<?php echo $helper->getSpelledOut($rank) ?>">
						<span class="place">
					<?php endif ?>
					<?php echo $rank ?><!--span class="ordinal-suffix"><?php echo $helper->getOrdinalSuffix($rank)  ?></span-->
					<?php if( $awardMedal && false ): ?>
						</span>
					</div>
					<?php endif ?>
				</div>
				<div class="name report-section rounded">
					<span>
						<?php echo $_report->getFirstName() ?> <?php echo $_report->getLastName() ?>
					</span>
				</div>
				<div class="points report-section rounded">
					<span>
						<?php echo number_format($_report->getPointTotal(), 0, ".",",") ?>
					</span>
				</div>
				<div class="prize report-section rounded">
					<?php
						switch( ($i+1) ) {
							case 1:
								$prize = 250;
								break;
							
							case 2:
								$prize = 150;
								break;
							
							case 3:
								$prize = 100;
								break;
							
							case 7:
							case 8:
							case 9:
							case 10:
								$prize = 25;
								break;
							
							default: $prize = 50;
						}
					?>
					&euro;<?php echo $prize ?>
				</div>
			</div></li>
		<?php $i++; endforeach ?>
	</ul>
</div>
	<?php if( !$this->isLiked() ) return ?>
	<?php // echo $this->getChildHtml('tc.pullout') ?>
	<?php return  ?>
	<h2>You</h2>
	<div class="current-report current">
		<div class="report">
			<?php if( !$currentReport ): ?>
			<?php $currentReport = $this->getCurrentReport() ?>
			<?php endif ?>
			<?php $rank = $currentReport->getRank() ?>
			<?php $awardMedal = $rank < 4 ?>
			<div class="rank">
				<?php if( $awardMedal ): ?>
				<div class="medal medal-<?php echo $helper->getSpelledOut($rank) ?>">
					<span class="place">
				<?php endif ?>
				<?php echo $rank ?><span class="ordinal-suffix"><?php echo $helper->getOrdinalSuffix($rank)  ?></span>
				<?php if( $awardMedal ): ?>
					</span>
				</div>
				<?php endif ?>
			</div>
			<div class="name">
				<span>
					<?php echo $currentReport->getFirstName() ?> <?php echo $currentReport->getLastName() ?>
				</span>
			</div>
			<div class="points">
				<span>
					<?php echo $currentReport->getPointTotal() ?>
				</span>
			</div>
		</div>
	</div>
	<?php endif ?>
</div>