<?php /* @var $this Xedin_Page_Block_Head */ ?>
	<head>
		<?php foreach( $this->getMeta() as $_type => $_meta ): ?>
			<?php foreach( $_meta as $_key => $_value ): ?>
		<meta <?php echo $_type ?>="<?php echo $_key ?>" content="<?php echo $_value ?>"/>
			<?php endforeach ?>
		<?php endforeach ?>
		
		<title><?php echo $this->getTitle() ?></title>
		
		<?php foreach( $this->getTags(Xedin_Page_Block_Head::TAG_TYPE_JS) as $_jsIdx => $_jsUrl ): ?>
		<script type="text/javascript" src="<?php echo $_jsUrl ?>"></script>
		<?php endforeach ?>
		
		<?php foreach( $this->getTags(Xedin_Page_Block_Head::TAG_TYPE_CSS) as $_cssIdx => $_cssUrl ): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $_cssUrl ?>" />
		<?php endforeach ?>
		
		<?php echo $this->getChildHtml() ?>
	</head>