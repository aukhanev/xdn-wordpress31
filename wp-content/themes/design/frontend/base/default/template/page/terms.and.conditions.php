<?php /* @var $this Xedin_Core_Block_Template */ ?>
<div class="terms-and-conditions">
<h1 style="padding:20px;">Terms and Conditions</h1>
	<ol>
		<li>This is not a lottery and winners will be asked a skilled question before they can redeem their prize</li>
		<li>In case of a tie, the accumulative value is divided between the number of winners</li>
		<li>Players must be 18yrs+</li>
		<li>Player must be residents in Malta</li>
		<li>No purchase necessary</li>
		<li>By playing this application you agree your contact details will be used for promotional purposes</li>
		<li>All prizes are shopping mall vouchers and are not exchangeable for cash</li>
		<li>Winners have 3 weeks to collect their vouchers</li>
		<li>Only one entry per person</li>
	</ol>
</div>