<?php /* @var $this Xedin_Competitions_Block_Leaderboard */ ?>
<?php $reportCollection = $this->getReportCollection() ?>
<?php $reportCount = $reportCollection->count() ?>
<?php $limit = $this->getLimit() ?>
<?php $helper = $this->getNumberHelper() ?>
<div class="ribbon-yellow-medium">
	<div class="content">
		<div class="text-rule">
			<hr />
			<h4><span>The prize</span></h4>
		</div>
		<h3 class="red">2 tickets to watch the Formula 1 in Monza</h3>  
		<div class="text-rule">
			<hr />
			<h4><span>Between the 7<sup>th</sup> &amp; 9<sup>th</sup> September, including flights!</span></h4>
		</div>
	</div>
</div>
<?php echo $this->getChildHtml('tc.pullout') ?>
<div class="leaderboard">
	<div class="leaderboard-heading"></div>
	<!--h2>Top <?php echo $this->getLimit() ?></h2-->
	<?php if( $reportCount ): ?>
	<ul>
		<?php $currentReport = null ?>
		<?php $i = 0; foreach( $reportCollection->getItems() as $_id => $_report ): ?>
		<?php $isCurrentReport = false; ?>
			<?php if( $this->isCurrentReport($_report) ): ?>
			<?php $currentReport = $_report ?>
			<?php $isCurrentReport = true ?>
			<?php endif ?>
			<?php $rank = $_report->getRank() ?>
			<?php $awardMedal = $rank < 4 ?>
		<li class="<?php if( !$i ): ?>first<?php endif ?><?php if( $i == ($reportCount - 1) ): ?> last<?php endif ?><?php if( $isCurrentReport ): ?> current<?php endif ?>">
			<div class="report">
				<div class="rank">
					<?php if( $awardMedal ): ?>
					<div class="medal medal-<?php echo $helper->getSpelledOut($rank) ?>">
						<span class="place">
					<?php endif ?>
					<?php echo $rank ?><span class="ordinal-suffix"><?php echo $helper->getOrdinalSuffix($rank)  ?></span>
					<?php if( $awardMedal ): ?>
						</span>
					</div>
					<?php endif ?>
				</div>
				<div class="name">
					<span>
						<?php echo $_report->getFirstName() ?> <?php echo $_report->getLastName() ?>
					</span>
				</div>
				<div class="points">
					<span>
						<?php echo number_format($_report->getPointTotal(), 0, ".",",") ?>
					</span>
				</div>
			</div>
			<hr />
		</li>
		<?php $i++; endforeach ?>
	</ul>
	<?php return  ?>
	<h2>You</h2>
	<div class="current-report current">
		<div class="report">
			<?php if( !$currentReport ): ?>
			<?php $currentReport = $this->getCurrentReport() ?>
			<?php endif ?>
			<?php $rank = $currentReport->getRank() ?>
			<?php $awardMedal = $rank < 4 ?>
			<div class="rank">
				<?php if( $awardMedal ): ?>
				<div class="medal medal-<?php echo $helper->getSpelledOut($rank) ?>">
					<span class="place">
				<?php endif ?>
				<?php echo $rank ?><span class="ordinal-suffix"><?php echo $helper->getOrdinalSuffix($rank)  ?></span>
				<?php if( $awardMedal ): ?>
					</span>
				</div>
				<?php endif ?>
			</div>
			<div class="name">
				<span>
					<?php echo $currentReport->getFirstName() ?> <?php echo $currentReport->getLastName() ?>
				</span>
			</div>
			<div class="points">
				<span>
					<?php echo $currentReport->getPointTotal() ?>
				</span>
			</div>
		</div>
	</div>
	<?php endif ?>
</div>