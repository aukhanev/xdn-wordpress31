<?php /* @var $this Xedin_Competitions_Block_Template */ ?>
<div class="section section-not-liked">
	<div class="like-structure">
		<?php if( !$this->isLiked() ): ?>
		<?php echo $this->getChildHtml('facebook.like') ?>
		<?php endif ?>
		<div class="like-text">
			Like the page to participate
		</div>
	</div>
	<div class="blurrred blurred-1"></div>
	<div class="blurrred blurred-2"></div>
	<div class="blurrred blurred-3"></div>
	<div class="blurrred blurred-4"></div>
	<div class="blurrred blurred-5"></div>
</div>