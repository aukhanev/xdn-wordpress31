<?php /* @var $this Xedin_Core_Block_Template */ ?>
<div class="prizes">
	<div class="heading"></div>
	<ul>
		<li class="first">
			<div class="prize prize-first">
				<div class="heading"></div>
				<div class="picture"></div>
				<div class="description">LG 42'' LED Full HD Smart TV</div>
			</div>
		</li>
		<li>
			<div class="prize prize-second">
				<div class="heading"></div>
				<div class="picture"></div>
				<div class="description">MPMAN 10&Prime; Internet Tablet</div>
			</div>
		</li>
		<li class="last">
			<div class="prize prize-third">
				<div class="heading"></div>
				<div class="picture"></div>
				<div class="description">Sony Bloggie HD Camcorder</div>
			</div>
		</li>
	</ul>
</div>