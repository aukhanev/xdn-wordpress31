<?php /* @var $this Xedin_Core_Block_Template */ ?>
<div class="pull-out pull-out-horizontal" id="<?php echo $this->getPulloutId() ?>">
	<div class="container">
		<?php echo $this->getChildHtml() ?>
	</div>
	<div class="handle">
		<?php echo $this->getHandleTitle() ?>
	</div>
</div>
<script type="text/javascript">
	(function($) {
		var isOut = false;
		var $this = $('#<?php echo $this->getPulloutId() ?>');
		var $container = $this.find('.container');
		var collapsedHeight = $container.height();
		var expandedHeight = $container.css('height', 'auto').height();
		$container.css('height', collapsedHeight + 'px');
		$('#<?php echo $this->getPulloutId() ?> .handle').bind('click', function(){
			if( !isOut ) {
				$container.animate({
					'height': expandedHeight
				},
				1000, function() {
					isOut = true;
				});
			}
			else {
				$container.animate({
					height: collapsedHeight
				},
				1000, function() {
					isOut = false;
					FB.Canvas.setSize();
				});
			}
		});
	}(jQuery));
</script>