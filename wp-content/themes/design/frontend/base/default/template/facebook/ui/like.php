<?php /* @var $this Xedin_Facebook_Block_Ui_Like */ ?>
<fb:like href="<?php echo $this->getFacebookPageUrl() ?>" data-send="<?php echo $this->getData('data-send') ?>" layout="<?php echo $this->getData('layout') ?>" width="50" show_faces="<?php echo $this->getData('show_faces') ?>" font="<?php echo $this->getData('font') ?>" class="<?php echo $this->getData('class') ?>"></fb:like>
<script type="text/javascript">
	(function($) {
		$(document).bind("fb-ready", function() {
			FB.Event.subscribe("edge.create", function(response) {
				top.location.href = '<?php echo $this->getFacebookPageTabUrl() ?>';
			});
		})
	}(jQuery));
</script>