<?php /* @var $this Xedin_Core_Block_Modules */ ?>
<div class="xdn">

	<h5 class="xdn-modules xdn-section-heading" id="xdn-modules"><span class="section-toggle">+</span>Installed Modules</h5>
	<div class="xdn-modules-section xdn-section" id="xdn-modules-section">
		<ul>
			<?php foreach( $this->getModulesCollection()->getItems() as $_moduleName => $_module ): ?>
			<li>
				<ul>
					<li class="module-name"><span><?php echo $_moduleName ?></span></li>
					<li class="module-version"><span>v<span class="version-number"><?php echo $_module->getVersion() ?></span></span></li>
					<?php $statusClassName = $_module->getData('isActive') ? 'enabled' : 'disabled' ?>
					<li class="module-status"><span class="<?php echo $statusClassName ?>"><?php if( $_module->getData('isActive')): ?>Enabled<?php else: ?>Disabled<?php endif ?></span>
				</ul>
			<li>
			<?php endforeach ?>
		</ul>
	</div>
	<script type="text/javascript">
		(function($) {
			$('.xdn-section-heading').click(function() {
				var headingId = $(this).attr('id');
				var $section = $('#' + headingId + '-section');
				if( $section.css('display') == 'none' ) {
					$section.css('display', 'block');
					$(this).find('.section-toggle').html('-');
				}
				else {
					$section.css('display', 'none');
					$(this).find('.section-toggle').html('+');
				}
			});
		}(jQuery));
	</script>
	<style type="text/css">
		.xdn-section-heading {
			cursor: pointer;
		}
		
		.xdn-section-heading:hover {
			background-color: #f1f1f1;
		}

		.xdn-section {
			display: none;
		}

		.xdn-section li li {
			float: left;
			margin-left: 3px;
			width: 150px;
		}

		.xdn-section-heading .section-toggle {
			display: inline-block;
			padding: 1px 2px;
			width: 8px;
			border: 1px solid #2b2b2b;
			background-color: #ffffff;
			margin-right: 3px;
			line-height: 9px;
			vertical-align: baseline;
			text-align: center;
		}

		.xdn-modules-section .module-status .enabled {
			color: #009900;
		}

		.xdn-modules-section .module-status .disabled {
			color: #990000;
		}

		.xdn ul:after,
		.xdn ul li:after {
			/*display:block; content:"."; clear:both; font-size:0; line-height:0; height:0; overflow:hidden;*/
			content: "";
			clear: both;
			display: table;
		}
		
	</style>
</div>