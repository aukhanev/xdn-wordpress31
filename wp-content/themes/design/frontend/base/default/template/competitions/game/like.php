<?php /* @var $this Xedin_Competitions_Block_Game_Like */ ?>
<?php $isLiked = $this->isLiked() ?>
<div class="area-dark">
	<div class="container like-structure">
		<div class="<?php if( $isLiked ): ?>liked<?php endif ?>">
			<div class="hemi">
				<?php if( $isLiked): ?>
				<h4>You're in!</h4>
					<?php else: ?>
				<fb:like href="<?php echo $this->getFacebookPageUri() ?>" data-send="false" layout="button_count" width="90" show_faces="false" font="lucida grande" class="btn-fb-like"></fb:like>
				<?php endif ?>
			</div>
			<div class="heading-yellow">
				<h3>
					<?php if( $isLiked ): ?>
					Get ready and start racing
					<?php else: ?>
					Start racing and gain miles
					<?php endif ?>
				</h3>
			</div>
			<div class="heading-gray">
				<h3>
					<span>
						<?php if( $isLiked ): ?>
						Gain more miles with<br/>one of the options above
						<?php else: ?>
						By liking our page
						<?php endif ?>
					</span>
				</h3>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
(function($) {
	
	
	$(document).bind("fb-ready", function() {
		FB.Event.subscribe("edge.create", function(response) {
			top.location.href = '<?php $this->getFacebookPageTabUrl() ?>';
		})
}(jQuery));
				
</script>


<?php // var_dump($this->getFacebook()->getSignedRequest()) ?>