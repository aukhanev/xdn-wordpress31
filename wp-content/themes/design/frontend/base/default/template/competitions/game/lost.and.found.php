<?php /* @var $this Xedin_Competitions_Block_Game_Bonus_Code */ ?>
<?php $game = $this->getCurrentGame() ?>
<?php $helper = $this->getUnitsHelper() ?>
<?php $formHelper = $this->getFormHelper() ?>
<?php $totalCodesAmount = $this->getTotalCodesAmount() ?>
<?php $currentCodesAmount = $this->getUsedCodesAmount() ?>
<?php $isCorrectAnswer = $this->isAnsweredCorrect() ?>
<div class="lost-and-found">
	<div class="area-dark">
		<div class="container">
			<div class="heading"></div>
			<h4 class="small-yellow">
				<div class="btn btn-available btn-extra-small">
					<?php $pointsPerScore = $game->getPointsPerScore() ?>
					<span class="amount"><?php echo $helper->getExplicitSign($pointsPerScore) ?><?php echo abs($pointsPerScore) ?></span><br />
					<span class="unit"><?php echo $helper->getGrammaticalNumber($pointsPerScore, 'mile', 'miles') ?></span>
				</div>
				<span>For each code found at our showroom</span>
			</h4>
			<div class="form-box">
				<?php if( $this->getFailureMessage() ): ?>
				<div class="message-incorrect"></div>
				<div class="message failure-message">
					<?php echo $this->getFailureMessage() ?>
				</div>
				<?php endif ?>
				<?php if( $isCorrectAnswer ): ?>
					<div class="message-congratulations"></div>
					<?php $points = $game->getPointsPerScore() ?>
					<p>
						You have found another item<br />
						You have been awarded <span><?php $helper->getExplicitSign($points) ?><?php echo abs($points) ?> miles</span>
					</p>
				<?php else: ?>
				<h2 class="day">Day <?php echo abs($this->getCurrentCompetition()->getDaysSinceStart()) ?></h2>
				<h2>Enter todays code</h2>
				<form action="<?php echo $this->getFormActionUrl() ?>" method="post">
					<ul>
						<li class="first">
							<div class="input-field input-text">
								<input type="text" class="code-input" name="<?php echo $formHelper->getNameFromPath('bonus_code/code') ?>" />
								<button type="submit" id="btn-submit" class="btn-submit">Enter</button>
							</div>
						</li>
					</ul>
				</form>
				<?php endif ?>
				<div class="small-box">
					<div class="side left">
						<div class="btn btn-found">
							<span class="amount">
								<?php echo $currentCodesAmount ?>
							</span><br />
							<span class="unit">
								found
							</span>
						</div>
					</div>
					<div class="found-froma"></div>
					<div class="side right">
						<div class="btn btn-total">
							<span class="amount">
								<?php echo $totalCodesAmount ?>
							</span><br />
							<span class="unit">
								items
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div style="padding-top:80px;">[ Find these codes placed in items around our<br />showroom. Miles will be given out once every 24hrs. ] </div>
	</div>
</div>