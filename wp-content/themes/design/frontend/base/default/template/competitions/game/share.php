<?php /* @var $this Xedin_Competitions_Block_Game_Share */ ?>
<?php $game = $this->getCurrentGame(); ?>
<?php $helper = $this->getUnitsHelper() ?>
<div class="area-dark">
	<div class="container">
		<div class="share-widget btn-fb-share" id="btn-fb-share"></div>
		<?php if( $this->getSuccessMessage() ): ?>
		<div class="message success-message">
			<?php echo $this->getSuccessMessage() ?>
		</div>
		<?php endif ?>
		<?php if( $this->getFailureMessage() ): ?>
		<div class="message failure-message">
			<?php echo $this->getFailureMessage() ?>
		</div>
		<?php endif ?>
		<h3 class="var-size">
			<span class="large">Share this on</span>
			<span class="small">your timeline and get</span>
			<div class="btn btn-available btn-available-extra-large btn-extra-large btn-game-<?php echo $game->getCode() ?>">
				<?php $pointsPerScore = $game->getPointsPerScore() ?>
				<span class="amount"><?php echo $helper->getExplicitSign($pointsPerScore) ?><?php echo abs($pointsPerScore) ?></span><br />
				<span class="unit"><?php echo $helper->getGrammaticalNumber($pointsPerScore, 'mile', 'miles') ?></span>
			</div>
		</h3>
		<div class="yellow">Get an extra mile if one of your friends clicks on your link</div>
	</div>
    <div style="padding-top:40px;">[ Miles will only be awarded once every 24hrs. ]</div>
</div>
<?php $fbApp = $this->getFacebook()->getAppModel() ?>
<script type="text/javascript">
	(function($) {
		$('#btn-fb-share').bind('click', function() {
			var competition = competition || Xedin_Competition_Abstract.init("#<?php echo $this->getMainSelector() ?>").setBaseUrl("<?php echo $this->getGameActionUrl() ?>").setKey('<?php echo $this->getNonce() ?>');
			competition.takeAuthorisedAction(function() {
				FB.ui({
					method: 'feed',
					link: '<?php echo $this->getFacebookPageUri() ?>',
					picture: '<?php echo $this->getSkinImageUrl($fbApp->getPictureUrl()) ?>',
					name: '<?php echo $fbApp->getCaption() ?>',
					caption: 'Win 2 Tickets to watch the F1 in Monza',
					description: 'Race against your friends to earn miles with the Gauci Miles Competition. The person with the most miles will win 2 tickets to Formula 1 in Monza this September.'
				}, function(response) {
						if( !response['post_id'] ) {
							return false;
						}

						competition.executeGameAction('<?php echo $this->getGameCode() ?>', {
							'fb_response':	response
						});
				});
			});
		})
	}(jQuery));
</script>