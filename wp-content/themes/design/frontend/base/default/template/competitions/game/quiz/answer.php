<?php /* @var $this Xedin_Competitions_Block_Game_Quiz_Answer */ ?>
<?php $question = $this->getQuestion() ?>
<?php $answersCollection = $this->getAnswersCollection() ?>
<?php $answersCount = $answersCollection->count() ?>
<?php $helper = $this->getFormHelper() ?>
<?php $game = $this->getCurrentGame() ?>
<?php $pointsPerScore = $game->getPointsPerScore() ?>
<?php $unitHelper = $this->getUnitsHelper() ?>
<?php $questionsInRow = $this->getCurrentParticipant()->loadMeta()->getQuestionsInRow() ?>
<div class="game-quiz-answer">
	<div class="area-dark">
		<div class="container">
			<?php if ($question): ?>
				<?php if (!$this->isAnswered()): ?>
					<?php if ($question && ($successionAmount = $question->getBonusSuccessionAmount())): ?>
						<div class="questions-in-row">
							<div class="traffic-lights">
								<div class="lights">
									<ul>
										<?php for ($i = 0; $i < $successionAmount; $i++): ?>
											<li class="item-<?php echo $i + 1 ?><?php if (!$i): ?> first<?php endif ?><?php if ($i == ($successionAmount - 1)): ?> last<?php endif ?>">
												<div class="light light-state-<?php if ($i <= ($questionsInRow - 1)): ?>on<?php else: ?>off<?php endif ?>"></div>
											</li>
										<?php endfor ?>
									</ul>
								</div>
							</div>
						</div> 
					<?php endif ?>
					<h5 class="yellow-strip">
						<span>By answering correctly to our daily<br />question you will be awarded</span>
						<div class="btn btn-available btn-regular">
							<span class="amount"><?php echo $unitHelper->getExplicitSign($pointsPerScore) ?><?php echo abs($pointsPerScore) ?></span><br />
							<span class="unit"><?php echo $unitHelper->getGrammaticalNumber($pointsPerScore, 'mile', 'miles') ?></span>
						</div>
					</h5>
					<div class="quiz-question">
						<hr />
						<h4><?php echo $question->getText() ?></h4>
						<hr />
					</div>
					<div class="quiz-answers">
						<?php if ($answersCount): ?>
							<form action="<?php echo $this->getFormActionUrl() ?>" method="post">
								<input type="hidden" name="<?php echo $helper->getNameFromPath('question/id') ?>" value="<?php echo $question->getId() ?>">
								<?php $key = $this->getNonce() ?>
								<?php if ($key): ?>
									<input type="hidden" name="form_key" value="<?php echo $key ?>">
								<?php endif ?>
								<ul>
									<?php $i = 0; foreach ($answersCollection->getItems() as $_id => $_answer):
										?>
										<li class="<?php if( !$i ): ?>first<?php endif ?><?php if ($i == ($answersCount - 1)): ?> last<?php endif ?>">
											<div class="answer">
										<?php $id = $helper->getIdFromPath('answer/' . $_id . '/id') ?>
												<div class="answer-text"><label for="<?php echo $id ?>"><?php echo $_answer->getText() ?></label></div>
												<div class="answer-field"><input type="radio" name="<?php echo $helper->getNameFromPath('answer/id') ?>" id="<?php echo $id ?>" value="<?php echo $_answer->getId() ?>"><span class="number"><?php echo chr($i + 97) ?>.</span></div>
											</div>
										</li>
									<?php $i++; endforeach ?>
								</ul>
								<div class="buttons"><button type="submit" class="submit"></button></div>
							</form>
						<?php endif ?>
					</div>
				<?php else: ?>
						<?php if( $this->getLastAttempt()->getIsCorrect() ): ?>
							<div class="win-message in-row-<?php echo $questionsInRow ?>">
							<div class="answer-correct"></div>
							<?php switch( $questionsInRow ): ?><?php case 1: ?>
								<p>
									You just gained <?php echo $game->getPointsPerScore() ?> miles to your mileage counter.
								</p>
								<p class="white">
									Come back tomorrow to collect more miles.
								</p>
								<?php break; case 2: ?>
								<p>
									You just gained <?php echo $game->getPointsPerScore() ?> miles to your mileage counter.
								</p>
								<p class="white">
									Come back tomorrow to collect more miles.
								</p>
								<?php break; case 3: ?>
								<p>
									You just gained <?php echo $game->getPointsPerScore() ?> miles to your mileage counter
								</p>
								<p class="white">
									You're on winning streak. Come back tomorrow to collect more miles.
								</p>
								<?php break; case 4: ?>
								<p>
									You just gained <?php echo $game->getPointsPerScore() ?> miles to your mileage counter
								</p>
								<p class="white">
									You're on winning streak. Come back tomorrow to collect more miles.
								</p>
								<?php break; case 5: ?>
								<p>
									You just gained <?php echo $game->getPointsPerScore() ?> miles to your mileage counter
								</p>
								<p class="white">
									You're on winning streak. Come back tomorrow to collect more miles.
								</p>
								<?php break ?>
							<?php endswitch ?>
							</div>
						<?php else: ?>
						<?php $wrongAnswer = $game->getGameTypeHandler()->getWrongAnswerModel() ?>
						<div class="answer-incorrect"></div>
						<p>
							Better luck next time. We are giving you <span class="white"><?php echo $wrongAnswer->getPointsPerScore() ?> mile</span>
							for trying it out. Come back tomorrow and try again.
						</p>
						<?php endif ?>
				<?php endif ?>
			<?php else: ?>
			<h3>We are updating the question<br />Please check back later</h3>
			<?php endif ?>
		</div>
		<?php $otherGame = Hub::getModel('competitions/game')->load('quiz-question') ?>
        <div style="padding-top:40px;">[ Get <?php echo $question->getBonusSuccessionAmount() ?> consecutive correct answers and get an extra <?php echo $otherGame->getPointsPerScore() ?> Miles bonus. ]</div>
	</div>
</div>