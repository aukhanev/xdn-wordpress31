<?php /* @var $this Xedin_Competitions_Block_Game_Fortunewheel */ ?>
<div class="game game-<?php echo $this->getGameCode() ?>">
	<?php if( !$this->isLiked() ): ?>
		<div class="no-like">
			<ul>
				<li class="first">
					<div class="slice slice-1"></div>
				</li>
				<li>
					<div class="slice slice-2"></div>
				</li>
				<li>
					<div class="slice slice-3"></div>
				</li>
				<li>
					<div class="slice slice-4">
						<fb:like href="<?php echo $this->getFacebookPageUri() ?>" data-send="false" layout="button_count" width="90" show_faces="false" font="lucida grande" class="btn-fb-like"></fb:like>
						<script type="text/javascript">
						(function($) {


							$(document).bind("fb-ready", function() {
								FB.Event.subscribe("edge.create", function(response) {
									top.location.href = '<?php echo $this->getFacebookPageTabUrl() ?>';
								});
							});
						}(jQuery));

						</script>
					</div>
				</li>
				<li>
					<div class="slice slice-5"></div>
				</li>
				<li class="last">
					<div class="slice slice-6"></div>
				</li>
			</ul>
		</div>
		<?php elseif( !$this->getCurrentParticipant() || !$this->getFacebook()->getUser() ): ?>
		<div class="no-access">
			<ul>
				<li class="first">
					<div class="slice slice-1">
						<button class="btn-grant-permissions"></button>
						<script type="text/javascript">
//								function takeAuthorisedAction(action) {
//									if( !FB ) { return false; }
//
//									var fbPermissions = "email";
//
//									FB.getLoginStatus(function(response) {
//										if( response.status != "connected" && response.status != "not_authorised" ) {
//											FB.login(function(loginResponse) {
//												if( loginResponse.status == "connected" ) {
//													action.call();
//													return;
//												}
//												return;
//											});
//											return;
//										}
//										action.call();
//										return;
//									});
//								};

								$('.no-access').bind('click', function() {
									if( !FB ) {
										return false;
									}

//									FB.getLoginStatus(function(response) {
//										if( response.status != "connected" && response.status != "not_authorised" ) {
											FB.login(function(loginResponse) {
												if( loginResponse.status == "connected" ) {
													top.location.replace('<?php echo $this->getFacebookPageTabUrl() ?>');
//													return;
												}
//												return;
											},
											{'scope': 'email,publish_actions'});
//											return;
//										}
//										top.location.replace('<?php echo $this->getFacebookPageTabUrl() ?>');
//										return;
//									});

								});
						</script>
					</div>
				</li>
				<li>
					<div class="slice slice-2"></div>
				</li>
				<li>
					<div class="slice slice-3"></div>
				</li>
				<li>
					<div class="slice slice-4"></div>
				</li>
				<li class="last">
					<div class="slice slice-5"></div>
				</li>
			</ul>
		</div>
	<?php else: ?>
	<div class="heading-main"></div>
	<div class="wheel-wrapper">
		<?php $countdownId = 'draw-countdown' ?>
		<div class="countdown-wrapper">
			<h5>Winner announced in:</h5>
			<div class="draw-countdown" id="<?php echo $countdownId ?>"></div>
		</div>
		<script type="text/javascript">
			<?php $endsAt = strtotime($this->getCurrentCompetition()->getEndsAt()) ?>
			var until = new Date('<?php echo date( 'Y/m/d H:i:s ', $endsAt ) ?>');
			$('#<?php echo $countdownId ?>').countdown({
				until: until,
				format: 'DHMS'
			});//.countdown('pause');
			$('#<?php echo $countdownId ?>')
		</script>
		<div class="fortune-wheel" id="fortune-wheel" style="background-image: url(<?php echo $this->getSkinUrl('images/wheel.png') ?>); background-position: -10032px 0">
			<?php $buttonId = 'game-' . $this->getGameCode() . '-spin' ?>
			<div class="btn-spin-now" id="<?php echo $buttonId ?>">
			</div>
			<?php $canScore = $this->getCurrentGame()->canScore($this->getCurrentParticipant()) ?>
			<?php
				$timeZone = new DateTimeZone('Europe/Malta');
				$now = new DateTime(date( 'Y/m/d H:i:s' ));
				$now->setTimezone($timeZone);
				$now = $now->format('Y/m/d H:i:s');
			?>
			<?php if( $endsAt+0 < strtotime($now)+0 ): ?>
			<div class="white-circle-overlay"style="display: block">
				<div class="message-box" id="game-message-box">
					<h2 class="info-heading">We are </h2>
					<h3 class="info-subheading">currently drawing this week's winners <br/>and resetting the wheel</h3>
					<p class="info-description">Check back at 2:00pm<br />for another spin</p>
				</div>
			</div>
			<?php elseif( $canScore ): ?>
			<div class="white-circle-overlay">
				<div class="message-box" id="game-message-box">
					<h2 class="info-heading"><span class="points" id="slot-points"></span> points</h2>
					<h3 class="info-subheading">have been added to your score</h3>
					<p class="info-description">Check back tomorrow<br />for another spin</p>
					<div class="btn-facebook-share"></div>
					<script type="text/javascript">
						<?php $fbApp = $this->getFacebook()->getAppModel() ?>
						(function($) {
							$('.btn-facebook-share').bind('click', function() {
									FB.ui({
										method: 'feed',
										link: '<?php echo $this->getFacebookPageUri() ?>',
										picture: '<?php echo $this->getSkinUrl('images/' . $fbApp->getPictureUrl()) ?>',
										name: '<?php echo addslashes($fbApp->getCaption()) ?>',
										caption: 'Spin and Win with The Point\'s Wheel of Fortune.',
										description: '&euro;750 worth of vouchers to be won every week with The Point\'s Wheel of Fortune.'
									});
							});
						}(jQuery));
					</script>
				</div>
			</div>
			<?php else: ?>
			<div class="white-circle-overlay"style="display: block">
				<div class="message-box" id="game-message-box">
					<h2 class="info-heading">Sorry!</h2>
					<h3 class="info-subheading">You have no more spins today</h3>
					<p class="info-description">Check back tomorrow<br />for another spin</p>
					<div class="btn-facebook-share"></div>
					<script type="text/javascript">
						<?php $fbApp = $this->getFacebook()->getAppModel() ?>
						(function($) {
							$('.btn-facebook-share').bind('click', function() {
									FB.ui({
										method: 'feed',
										link: '<?php echo $this->getFacebookPageUri() ?>',
										picture: '<?php echo $this->getSkinUrl('images/' . $fbApp->getPictureUrl()) ?>',
										name: '<?php echo addslashes($fbApp->getCaption()) ?>',
										caption: 'Spin and Win with The Point\'s Wheel of Fortune.',
										description: '&euro;750 worth of vouchers to be won every week with The Point\'s Wheel of Fortune.'
									});
							});
						}(jQuery));
					</script>
				</div>
			</div>
			<?php endif ?>
		</div>
	</div>
	<script type="text/javascript">
		var spinEnabled = true;
		xdnWheel = Xedin_Competition_Fortunewheel.init('<?php echo $this->getGameActionUrl() ?>');
		(function($){
			function addCommas(nStr) {
				nStr += '';
				x = nStr.split('.');
				x1 = x[0];
				x2 = x.length > 1 ? '.' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}
			
			function spinCallback(response) {
				if( response.error ) {
					
				}
				else {
					var points = Math.floor(response.points_gained);
					$('#fortune-wheel #slot-points').html(addCommas(points));
				}
				
				$('.fortune-wheel .white-circle-overlay').css('display', 'block');
//				console.log('response:');
//				console.log(response);
			};
			
			$('#' + '<?php echo $buttonId ?>').bind('click', function() {
				if( !spinEnabled ) {
					return false;
				}
				
				spinEnabled = false;
				$(this).css('background-image', 'url(<?php echo $this->getSkinUrl('images/btn-spin-gone.png') ?>)')
				.css('display', 'block');
				xdnWheel.spin(function(response) {
					setTimeout(function() {
						spinCallback(response);
					}, 1500);
				});
			});
		}(jQuery));
	</script>
	
	<div class="the-rules">
		<div class="rules-wrapper">
			<div class="main-heading"></div>
			<div class="rules">
				<ul><li class="first">
						<div class="rule">1 spin per day</div>
					</li><li>
						<div class="rule">Winner announced weekly on Friday at 1pm</div>
					</li><li>
						<div class="rule">Leaderboard resets every Friday at 2pm</div>
					</li><li class="last">
						<div class="rule">Terms & Conditions apply</div>
					</li>
				</ul>
			</div>
		</div>
		<hr />
	</div>
	<?php endif ?>
</div>