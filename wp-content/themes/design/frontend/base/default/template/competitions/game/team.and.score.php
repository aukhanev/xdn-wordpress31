<?php /* @var $this Xedin_Competitions_Block_Game_Teamandscore */ ?>
<script type="text/javascript">
<?php $formHelper = $this->getFormHelper() ?>
<?php $teams = $this->getTeamsCollection() ?>
<?php $fbApp = $this->getFacebook()->getAppModel() ?>
	jQuery(document).bind('ready', function() {
		jQuery('input[placeholder]').placeholder();
	})

	jQuery(document).bind('ready', function() {
		jQuery(".game-team-and-score form").attr('novalidate', 'novalidate');
	});

	jQuery(document).bind('ready', function() {
		jQuery.validator.addMethod("football_teams_same", function(value, element) {
			return jQuery('.team-a select').val() != jQuery('.team-b select').val()
		}, "The teams must be different");

		var isSharedDialogShown = false;

		jQuery(".game-team-and-score form").validate({
			rules : {
				'<?php echo $formHelper->getIdFromPath('vote/team_a') ?>' : {
					football_teams_same: true
				},
				'<?php echo $formHelper->getIdFromPath('vote/team_b') ?>' : {
					football_teams_same: true
				}
			},

			submitHandler : function(form) {
				FB.ui({
						method: 'feed',
						link: '<?php echo $this->getFacebookPageTabUrl() ?>',
						picture: '<?php echo $this->getSkinImageUrl($fbApp->getPictureUrl()) ?>',
						name: '<?php echo $fbApp->getTitle() ?>',
						caption: '<?php echo $fbApp->getCaption() ?>',
						description: '<?php echo $fbApp->getDescription() ?>'
					}, function(response) {
						form.submit();
				});
			}
		});
	});

</script>

<div class="game game-team-and-score team-and-score">
	<?php if( $successMessage = $this->getSuccessMessage() ): ?>
	<div class="message success-message">
		<?php echo $successMessage ?>
	</div>
	<?php endif ?>
	<?php if( $failureMessage = $this->getFailureMessage() ): ?>
	<div class="message success-message">
		<?php echo $failureMessage ?>
	</div>
	<?php endif ?>
	<?php if( !$this->getCurrentParticipant() && $this->getFacebook()->getUser() ): ?>
	<div class="new-vote">
		<form action="<?php echo $this->getFormActionUrl() ?>" method="post">
			<div class="form vote-form">
				<div class="heading"></div>
				<ul>
					<li class="first">
						<div class="input-field input-select team-a">
							<?php $fieldPath = 'vote/team_a' ?>
							<select name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" class="required football_team football_teams_same">
								<option value="" selected="selected">Select Finalist</option>
								<?php $teamsCount = $teams->count() ?>
								<?php if( $teamsCount ): ?>
								<?php foreach( $teams as $_teamId => $_team ): ?>
								<?php /* @var $_team Xedin_Competitions_Model_Football_Team */ ?>
								<option value="<?php echo $_team->getId() ?>"><?php echo $_team->getLabel() ?></option>
								<?php endforeach ?>
								<?php endif ?>
							</select>
						</div>
						<div class="input-field input-text team-a-score">
							<?php $fieldPath = 'vote/team_a_score' ?>
							<input type="text" name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" maxlength="2" class="required" />
						</div>
					</li>
					<li><div class="vs"></div></li>
					<li class="last">
						<div class="input-field input-select team-b">
							<?php $fieldPath = 'vote/team_b' ?>
							<select name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" class="required football_team football_teams_same">
								<option value="" selected="selected">Select Finalist</option>
								<?php $teamsCount = $teams->count() ?>
								<?php if( $teamsCount ): ?>
								<?php foreach( $teams as $_teamId => $_team ): ?>
								<?php /* @var $_team Xedin_Competitions_Model_Football_Team */ ?>
								<option value="<?php echo $_team->getId() ?>"><?php echo $_team->getLabel() ?></option>
								<?php endforeach ?>
								<?php endif ?>
							</select>
						</div>
						<div class="input-field input-text team-b-score">
							<?php $fieldPath = 'vote/team_b_score' ?>
							<input type="text" name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" maxlength="2" class="required" />
						</div>
					</li>
				</ul>
				<div class="notice notice-small">*Correct Score after any Extra Time and before Penalty shoot-out.</div>
			</div>
			<div class="form participant-form">
				<div class="heading"></div>
				<ul>
					<li class="first">
						<div class="input-field input-text">
							<?php $fieldPath = 'participant/first_name' ?>
							<input name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" placeholder="Name" class="required" />
						</div>
						<div class="input-field input-text last">
							<?php $fieldPath = 'participant/last_name' ?>
							<input name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" placeholder="Surname" class="required" />
						</div>
					</li>
					<li class="last">
						<div class="input-field input-text">
							<?php $fieldPath = 'participant/mobile_phone' ?>
							<input name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" placeholder="Mobile" class="required" />
						</div>
						<div class="input-field input-text last">
							<?php $fieldPath = 'participant/email' ?>
							<input name="<?php echo $formHelper->getNameFromPath($fieldPath) ?>" id="<?php echo $formHelper->getIdFromPath($fieldPath) ?>" placeholder="E-mail" class="required email" />
						</div>
					</li>
				</ul>
				<button class="btn-submit">&nbsp;</button>
			</div>
		</form>
	</div>
	<?php elseif( !$this->getFacebook()->getUser() ): ?>
	<button class="btn-play-now"></button>
	<script type="text/javascript">
			function takeAuthorisedAction(action) {
				if( !FB ) { return false; }

				var fbPermissions = "email";

				FB.getLoginStatus(function(response) {
					if( response.status != "connected" && response.status != "not_authorised" ) {
						FB.login(function(loginResponse) {
							if( loginResponse.status == "connected" ) {
								action.call();
								return;
							}
							return;
						});
						return;
					}
					action.call();
					return;
				});
			};
			
			$('.btn-play-now').bind('click', function() {
				if( !FB ) {
					return false;
				}
				
				FB.getLoginStatus(function(response) {
					if( response.status != "connected" && response.status != "not_authorised" ) {
						FB.login(function(loginResponse) {
							if( loginResponse.status == "connected" ) {
					top.location.replace('<?php echo $this->getFacebookPageTabUrl() ?>');
								return;
							}
							return;
						});
						return;
					}
					top.location.replace('<?php echo $this->getFacebookPageTabUrl() ?>');
					return;
				});
				
			});
	</script>
	<?php else: ?>
	<div class="vote-results">
		<?php $vote = $this->getVote() ?>
		<?php if( $vote ): ?>
		<h3>Your Choice:</h3>
		<ul>
			<li>
				<div class="vote">
					<div class="team">
						<?php $teamA = $this->getTeamModel($vote->getTeamA()) ?>
						<?php echo $teamA->getLabel() ?>
					</div>
					<div class="score">
						&nbsp;-&nbsp;<?php echo $vote->getData('team_a_score') ?>
					</div>
				</div>
			</li>
			<li>
				<div class="vote">
					<div class="team">
						<?php $teamB = $this->getTeamModel($vote->getTeamB()) ?>
						<?php echo $teamB->getLabel() ?>
					</div>
					<div class="score">
						&nbsp;-&nbsp;<?php echo $vote->getData('team_b_score') ?>
					</div>
				</div>
			</li>
		</ul>
		<?php endif ?>
	</div>
	<?php endif ?>
</div>