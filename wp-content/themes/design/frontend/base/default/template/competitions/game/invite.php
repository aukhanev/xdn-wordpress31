<?php /* @var $this Xedin_Competitions_Block_Game_Invite */ ?>
<?php $helper = $this->getUnitsHelper() ?>
<?php $game = $this->getCurrentGame() ?>
<div class="area-dark">
	<div class="container">
		<div class="knobbed">
			<h3>
				Invite all your friends to get ahead in the race
			</h3>
		</div>
			<?php if( $this->getSuccessMessage() ): ?>
			<div class="message success-message">
				<?php echo $this->getSuccessMessage() ?>
			</div>
			<?php endif ?>
			<?php if( $this->getFailureMessage() ): ?>
			<div class="message failure-message">
				<?php echo $this->getFailureMessage() ?>
			</div>
			<?php endif ?>
		<div class="connect-box">
			<div class="gray-box box-left">
				<div class="btn-facebook-connect btn-fb-share" id="btn-fb-invite"></div>
			</div>
			<div class="gray-box box-right">
				<div class="btn btn-available btn-small">
					<?php $pointsPerScore = $game->getPointsPerScore() ?>
					<span class="amount"><?php echo $helper->getExplicitSign($pointsPerScore) ?><?php echo abs($pointsPerScore) ?></span><br />
					<span class="unit"><?php echo $helper->getGrammaticalNumber($pointsPerScore, 'mile', 'miles') ?></span>
				</div>
				<hr />
				<p>
					With every invite you send out
				</p>
			</div>
		</div>
		<script type="text/javascript">
			(function($) {
				$('#btn-fb-invite').bind('click', function() {
					var competition = competition || Xedin_Competition_Abstract.init("#<?php echo $this->getMainSelector() ?>").setBaseUrl("<?php echo $this->getGameActionUrl() ?>").setKey('<?php echo $this->getNonce() ?>');
					competition.takeAuthorisedAction(function() {

						FB.ui({method: 'apprequests',
						message: 'Race against me and your friends to earn miles with the Gauci Miles Competition. The person with the most miles will win 2 tickets to Formula 1 in Monza this September.',
						exclude_ids: [<?php echo $this->getExcludeIdsString() ?>]
						}, function(response) {
							if( !response['request'] || !response['to'] ) {
								return false;
							}

							competition.executeGameAction('<?php echo $this->getGameCode() ?>', {
								'fb_response':	response
							});
						});
					});
				})
			}(jQuery));
		</script>
	</div>
    <div style="padding-top:40px;">[ Gain 2 extra miles each time a friend accepts your invite. ]</div>
</div>