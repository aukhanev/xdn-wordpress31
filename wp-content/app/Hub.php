<?php
/**
 * This hub class is modified for use with WordPress 3.1 
 * * Changed PATH_DESIGN and PATH_SKIN constants
 * 
 * @author Xedin Unknown <xedin.unknown+wpxdn@gmail.com>
 * @version 0.1.1
 * + Added getConfigClassName() and setConfigClassName() functions.
 */
final class Hub {

    protected static $_registry;
    protected static $_app;
    private static $_classNames = array();

    private static $_basePath;
    private static $_fileExtension = 'php';
	
	protected static $_isDebugMode = false;
	protected static $_logFileName;
	
	protected static $_configClassName = 'Xedin_Config';
	
	protected static $_events;	

    const DEFAULT_READ_CONNECTION = 'core_read';
    const DEFAULT_WRITE_CONNECTION = 'core_write';
	
	const XML_PATH_URI = 'global/uri';
	const XML_PATH_URI_BASE_SECURE = 'global/uri/base_secure';
	const XML_PATH_URI_BASE_UNSECURE = 'global/uri/base_unsecure';
	const XML_PATH_URI_SKIN_SECURE = 'global/uri/skin_secure';
	const XML_PATH_URI_SKIN_UNSECURE = 'global/uri/skin_unsecure';
    
    const URI_SEPARATOR = '/';
    const URI_PREFIX_BLOCK = 'block_';
    const REG_PREFIX_SINGLETON = '_singleton';
	const REG_PREFIX_CONSTANT = '_constant';
	const REG_PREFIX_HELPER = '_helper';
	
	const PATH_TYPE_BASE = 'base';
	const PATH_TYPE_DESIGN = 'design';
	const PATH_TYPE_THEME = 'theme';
	const PATH_TYPE_TEMPLATE = 'template';
	
	const URL_TYPE_BASE = 'base';
	const URL_TYPE_SKIN = 'skin';
	
	const PATH_DESIGN = 'wp-content/themes/design/';
	const PATH_SKIN = 'wp-content/themes/skin/';

    public static function register($object, $key) {
		
		if( !is_string($key) ) {
			throw self::exception('Xedin_Core', 'Could not set registry value: key is not a string.');
		}
		
		self::$_registry[$key] = $object;
    }

    public static function registry($key) {
        if (!isset(self::$_registry[$key])) {
            return null;
        }

        return self::$_registry[$key];
    }

    public static function unregister($key) {
        if (isset(self::$_registry[$key])) {
            unset(self::$_registry[$key]);
        }
    }
	
	public static function isRegistered($key) {
		return isset(self::$_registry[$key]);
	}
    
    public static function setFileExtension($extension) {
        self::$_fileExtension = $extension;
    }

    public static function getFileExtension() {
        return self::$_fileExtension;
    }

    public static function getBasePath() {
        return self::$_basePath;
    }

    public static function setBasePath($basePath) {
        self::$_basePath = $basePath;
    }

    public static function getClassName($classIdentifier) {
        if( !isset(self::$_classNames[$classIdentifier]) ) {
            $className = strtolower( str_ireplace( array('_', '/'), ' ', $classIdentifier ) );
            $className = ucwords($className);
            $className = str_ireplace( ' ', '_', $className );
            self::$_classNames[$classIdentifier] = $className;
        }
        return self::$_classNames[$classIdentifier];
    }

    public static function getFilePath($classIdentifier) {
        $className = self::getClassName($classIdentifier);
        
        
        $filePath = str_ireplace( '_', '/', $className ) . '.' . self::$_fileExtension;

        return self::$_basePath . $filePath;
    }

	/**
	 * 
	 * @param type $classPath
	 * @return Xedin_Core_Model_Abstract
	 */
    public static function getSingleton($classPath) {
        $classPath = trim($classPath);
        $regKey = '_singleton/' . $classPath;

        if (is_null(self::registry($regKey))) {
            self::register(self::getModel($classPath), $regKey);
        }

        return self::registry($regKey);
    }
    
    /**
     *
     * @todo Refactor legacy code that contains "model_".
     * @param string $uri Something of format "module/class_name"
     */
    public static function getModelUri($uri) {
        $uri = explode('/', $uri);
        if( strpos($uri[1], 'model_') !== 0 ) {
            $uri[1] = 'model_' . $uri[1];
        }
        $uri = implode('/', $uri);
        return self::getClassName($uri);
    }

    public static function exception($namespace = '', $message = '') {
        $className = 'Exception';
        if( !empty($namespace) ) {
            $className = self::getClassName($namespace) . '_' . $className;
        }

        return new $className($message);
    }

	/**
	 *
	 * @param type $classPath
	 * @return \Xedin_Core_Model_Abstract
	 */
    public static function getModel($classPath) {
        $className = Hub::getConfig()->getModelClassName($classPath);
		return self::getInstance($className);
    }
    
    public static function getInstance($classIdentifier) {
//        self::loadClass($classIdentifier, false);
		$className = $classIdentifier;
		if( strpos($classIdentifier, self::URI_SEPARATOR) !== false ) {
			$className = self::getClassName($classIdentifier);
		}
		
        if( !class_exists($className) ) {
			throw new Exception('Class "' . $className . '" for identifier "' . $classIdentifier . '" does not exist');
            return null;
        }
        
        $args = func_get_args();
        array_shift($args);
        
        $instance = null;
        if( count($args) ) {
            $instance = new $className($args[0]);
        }
        else {
            $instance = new $className();
        }
		
        return $instance;
    }

    public static function loadClass($classIdentifier, $silent = false) {
        $classFilePath = self::getFilePath($classIdentifier);
        try {
			if( $silent ) {
				@include_once( $classFilePath );
			}
			else {
				include_once( $classFilePath );
			}
//			if( !class_exists($classIdentifier, false) ) {
//				throw new Exception('Class ' . $classIdentifier . ' not found in ' . $classFilePath);
//			}
        }
        catch( Exception $e ) {
            if( $silent ) {
                return false;
            }
            throw $e;
        }
    }

    public static function getLibrary($classPath, $params=null) {
        $className = self::getClassName($classPath);

        if (is_null($params)) {
            return new $className($params);
        }

    }

	/**
	 * @param type $classPath
	 * @return \Xedin_Core_Model_Resource_Abstract
	 */
    public static function getResourceModel($classPath) {
		$regKey = '_resource_singleton_' . $classPath;
		if( !self::registry($regKey) ) {

			$modelClassName = Hub::getConfig()->getResourceModelClassName($classPath);
			$modelConfig = Hub::getConfig()->getResourceModelConfig($classPath);

			$readConn = $modelConfig['read_resource'] ? $modelConfig['read_resource'] : self::DEFAULT_READ_CONNECTION;
			$writeConn = $modelConfig['write_resource'] ? $modelConfig['write_resource'] : self::DEFAULT_WRITE_CONNECTION;
			self::register(new $modelClassName($readConn, $writeConn), $regKey);
		}
		
		return self::registry($regKey);
    }

	/**
	 *
	 * @return Xedin_Core_Model_App
	 */
    public static function app() {
        if (empty(self::$_app)) {
            self::$_app = self::getModel('core/app');
        }

        return self::$_app;
    }

    public static function run($options=array()) {
		set_exception_handler(array(__CLASS__, 'handleUnhandledException'));
		set_error_handler(array(__CLASS__, 'handleUnhandledError'));
		
        if (!self::registry('config')) {
            $config = new Xedin_Config;
            $config->init($options);
            self::register($config, 'config');
        }
		

        self::app()->run();
    }
	
	public static function handleUnhandledError($errorNumber, $message) {
		if( !error_reporting() ) {
			return false;
		}
		
		if( $errorNumber == E_NOTICE || $errorNumber == E_USER_NOTICE ) {
			return false;
		}
		
		if( $errorNumber == E_WARNING || $errorNumber == E_USER_WARNING ) {
			return false;
		}
		
		if( $errorNumber == E_STRICT ) {
			return false;
		}
		
		throw new ErrorException($message, $errorNumber);
	}
	
	public static function handleUnhandledException(Exception $e) {
		echo '<h1>An exception of type ' . get_class($e) . ' has occured</h1>';
		echo '<h2>' . $e->getMessage() . '</h2>';
		echo '<pre>' . $e->getTraceAsString() . '</pre>';
	}

	/**
	 * @param string $path
	 * @return Xedin_Config
	 */
    public static function getConfig($path=null) {
        if (is_null($path)) {
            return self::registry('config');
        }

        return self::registry('config')->getValue($path);
    }

    public static function isConfig($path) {
        $value = self::getConfig($path);
        return (!empty($value) && (int) $value);
    }
    
	/**
	 *
	 * Gets a block instance for URI.
	 * Currently, only the 'Xedin' vendor is supported: no rewrites.
	 * @todo Make config-dependant.
	 * 
	 * @param type $blockUri
	 * @param type $attributes
	 * @return Xedin_Core_Block_Abstract
	 */
    public static function getBlock($blockUri, $attributes = array()) {
        if( stripos(self::URI_SEPARATOR, $blockUri) === false ) {
            $uriParts = explode(self::URI_SEPARATOR, $blockUri);
            $uriParts[1] = self::URI_PREFIX_BLOCK . $uriParts[1];
			
			if( stripos($uriParts[0], 'xedin_') !== 0 ) {
				$uriParts[0] = 'xedin_' . $uriParts[0];
			}
			
            $blockUri = implode(self::URI_SEPARATOR, $uriParts);
        }
//        echo 'get block "' . $blockUri . '"' . "\n";
        return self::getInstance($blockUri, $attributes);
    }
    
    public static function getLayout() {
        return self::app()->getLayout();
    }
    
    public static function getControllerPath($uri) {
        if(stripos($uri, '/') ) {
            $uriParts = explode('/', $uri);
            
            $module = $uriParts[0];
            $controller = $uriParts[1];
            
            /**
             * @todo Switch to config-dependent algorythm
             */
            $module = 'xedin_' . $module;
            $module = str_ireplace('_', ' ', $module);
            $module = ucwords($module);
            $module = str_ireplace(' ', '/', $module);
            
            $controller = str_ireplace('_', ' ', $controller);
            $controller = ucwords($controller);
            $controller = 'controllers/' . $controller . 'Controller';
            $controller = str_ireplace(' ', '/', $controller);
            
            $path = $module . '/' . $controller . '.php';
            return self::$_basePath . $path;
        }
    }
    
    public static function getControllerClassName($uri) {
        /**
         * @todo Switch to config-dependent algorythm
         */
        $uri = 'xedin_' . $uri;
        $className = self::getClassName($uri) . 'Controller';
        return $className;
    }
    
    public static function getControllerInstance($uri) {
        $filePath = self::getControllerPath($uri);
        $className = self::getControllerClassName($uri);
        
        if( !file_exists($filePath) ) {
            throw new Exception('No controller fount at ' . $filePath . '.');
        }
        
        require_once($filePath);
        if( !class_exists($className) ) {
            throw new Exception('Class ' . $className . ' not found in ' . $filePath);
        }
        
        return new $className();
    }
    
    public static function getUrl($url = '', $type = self::URL_TYPE_BASE, $absolute = false) {
        switch( $type ) {
            case self::URL_TYPE_SKIN:
				$isSecureUri = self::app()->getFrontController()->getRequest()->isSecure();
				$basePath = '';
				if( $isSecureUri ) {
					$basePath = ( $secureSkinBase = self::getConfig(self::XML_PATH_URI_SKIN_SECURE)) ? $secureSkinBase : self::getConfig(self::XML_PATH_URI_BASE_SECURE);
				}
				else {
					$basePath = ( $unsecureSkinBase = self::getConfig(self::XML_PATH_URI_SKIN_UNSECURE) ) ? $unsecureSkinBase : self::getConfig(self::XML_PATH_URI_BASE_UNSECURE);
				}
				return $basePath . self::PATH_SKIN . str_replace('\\', '/', self::app()->getDesign()->getRelativeTemplatePath($url) );
            default:
            case self::URL_TYPE_BASE:
				$basePath = self::app()->getFrontController()->getRequest()->isSecure() ? self::XML_PATH_URI_BASE_SECURE : self::XML_PATH_URI_BASE_UNSECURE;
                return self::getConfig($basePath) . $url;
                break;
        }
    }
    
    public static function getPath($path='', $type = self::PATH_TYPE_BASE) {
        switch( $type ) {
			case self::PATH_TYPE_DESIGN:
				return PATH_ROOT . DS . self::PATH_DESIGN . $path;
				break;
			
            case self::PATH_TYPE_THEME:
                return self::getPath() . 'wp-content' . get_raw_theme_root(get_template()) . DIRECTORY_SEPARATOR . get_template() . DIRECTORY_SEPARATOR . $path;
                break;
            
            case self::PATH_TYPE_BASE:
            default:
                return realpath('./') . '/' . $path;
                break;
        }
    }
	
	/**
	 * Gets the value of a constant.
	 * If the second parameter is omitted, gets a class constant of Mage_Loader.
	 * If the second parameter is a boolean false, gets a constant from the global namespace.
	 * If the second parameter is a string and is a model URI (contains the model URI separator), retrieves the constant of the class that the URI resolves to.
	 * If the second parameter is an object, and has a method called 'getConstant', retrieves the return value of that method.
	 * If the second parameter is an object, and does not have a method called 'getConstant', retrieves the value of the constant of the class of that object.
	 * 
	 * @see Mage_Loader::URI_SEPARATOR
	 * @param string $constantName Name of the constant to retrieve.
	 * @param string|object $class Either a class instance, or a class name, or a model URI.
	 * @param null|mixed The value to return if the constant is not eventually found.
	 * @return mixed Value of the retrieved constant.
	 */
	public static function getConstant($constantName, $class = null, $default = null) {
		$constantName = self::normalizeConstantName($constantName);
		
		if( $class === false ) {
			if( !defined($constantName) ) {
				return $default;
			}
			
			return constant( $constantName );
		}
		
		if( is_null($class) ) {
			$class = get_called_class();
			
			if( !is_null(self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName)) ) {
				return self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName);
			}
		}
		
		if( is_string($class) && (strpos($class, self::URI_SEPARATOR) !== false) ) {
			$class = self::getClassName(self::getModelUri($class));
		}
		
		if( is_object($class) ) {
			if( method_exists($class, 'getConstant') ) {
				return $class->getConstant($constantName);
			}
			$class = get_class($class);
		}
		
		
		$constantName = $class . '::' . $constantName;
		if( !defined($constantName) ) {
			return $default;
		}
		
		return constant($constantName);
	}
	
	/**
	 * Normalizes the given constant name.
	 * 
	 * @param string $constantName The constant name to normalized.
	 * @return string A valid constant name.
	 */
	public static function normalizeConstantName($constantName) {
		$constantName = str_ireplace('-', '_', $constantName);
		$constantName = strtoupper($constantName);
		return $constantName;
	}
	
	/**
	 * Whether or not a constant with the given name exists.
	 * See getConstant() for the places which will be checked.
	 * 
	 * @param string $constantName Name of the constant to check for.
	 * @param string|object $class See $class parameter of getConstant() for allowed values.
	 * @return boolean True if the constant exists, false otherwise. 
	 */
	public static function hasConstant($constantName, $class = null) {
		return is_null( self::getConstant($constantName, $class, null) );
	}
	
	/**
	 * Whether or not a constant exists internally to this class.
	 * 
	 * @param string $constantName Name of the constant to check for.
	 * @return boolean True if a constant with the given name exists; false otherwise.
	 */
	public static function hasOwnConstant($constantName) {
		$constantName = self::normalizeConstantName($constantName);
		return is_null(self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName));
	}
	
	/**
	 * Sets a value for a constant.
	 * 
	 * @param string $name The name of the constant to set the value for.
	 * @param mixed $value The value to set to the constant.
	 */
	public static function setConstant($name, $value) {
		$name = self::normalizeConstantName($name);
		self::register(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $name, $value);
	}
	
	/**
	 * Sets a value for a constant if it doesn't already exist.
	 * 
	 * @param string $name The name of the constant to set the value for.
	 * @param mixed $value The value to give to the constant.
	 */
	public static function addConstant($name, $value) {
		if( !self::hasOwnConstant($name) ) {
			self::setConstant($name, $value);
		}
	}
	
	public static function unsConstant($name) {
		if( self::hasOwnConstant($name) ) {
			$name = self::normalizeConstantName($name);
			self::unregister(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $name);
		}
	}
	
	public static function isDebugMode($debug = null) {
		if( is_null($debug) ) {
			return self::$_isDebugMode;
		}
		
		self::$_isDebugMode = (bool)$debug;
	}
	
	public static function getLogFileName() {
		if( !self::$_logFileName ) {
			self::$_logFileName = 'log-' . gmdate('Y-m-d');
		}
		
		return self::$_logFileName;
	} 
	
	public static function log($message) {
		if( !self::isDebugMode() ) {
			return false;
		}
		
		$filePath = 'var/log/' . self::getLogFileName();
		if( !($fp = @fopen(self::getPath($filePath), 'a')) ) {
			throw self::exception('Xedin_Core', 'Log failed: could not open file "' . $filePath . '".');
		}
		
		$microtime = microtime(true);
		$microseconds = $microtime - floor($microtime);
		$milliseconds = substr($microseconds, 2, 3);
		
		if( !@fwrite($fp, gmdate('[H:i:s:') . $milliseconds . '] ' . $message . "\n") ) {
			throw self::exception('Xedin_Core', 'Log failed: could not write to file "' . $filePath . '".');
		}
		
		@fclose($fp);
		return true;
	}

	/**
	 * @param string $classPath
	 * @return \Xedin_Core_Helper_Abstract
	 */
    public static function getHelper($classPath) {
		$regKey = self::REG_PREFIX_HELPER . self::URI_SEPARATOR . $classPath;
		if( !self::registry($regKey) ) {
			$className = Hub::getConfig()->getHelperClassName($classPath);
			self::register( self::getInstance($className), $regKey );
		}
		
		return self::registry($regKey);
    }
	
	public static function getConfigClassName() {
		return self::$_configClassName;
	}
	
	public static function setConfigClassName($className) {
		self::$_configClassName = $className;
	}
	
	public static function dispatchEvent($eventCode, $args = array(), $caller = null) {
		self::app()->getEventController()->dispatchEvent($eventCode, $args, $caller);
	}
}