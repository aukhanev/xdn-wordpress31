<?php

/**
 * Description of Abstract
 *
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 * @version 0.1.1
 * + Main block name is now settable through property _mainBlockName.
 */
abstract class Xedin_Competitions_Controller_Abstract extends Xedin_Core_Controller_Abstract {
	
	protected $_facebook;
	protected $_fbInitBlock;
	
	protected $_gameBlocks = array();
	protected $_mainBlockName = 'play.main';
	
	protected $_currentGameBlock;
	protected $_isUsingFacebook = true;
	
	/**
	 * @return Xedin_Facebook_Model_Facebook
	 */
	public function getFacebook() {
		if( !$this->_facebook ) {
			$this->_facebook = $this->getCurrentCompetition()->getFacebook();
		}
		
		return $this->_facebook;
	}
	
	/**
	 * @return Xedin_Facebook_Block_Init
	 */
	public function getFbInitBlock() {
		if( !$this->_fbInitBlock ) {
			$this->getLayout()->getBlock('facebook.init')->setFbOptions('appId', $this->getFacebook()->getAppModel()->getApiKey());
		}
		
		return $this->_fbInitBlock;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function getParticipantModel() {
		return Hub::getSingleton('competitions/participant');
	}
	
	public function getGameBlockType($gameCode, $default = null) {
		if( !isset($this->_gameBlocks[$gameCode]) ) {
			return $default;
		}
		
		return $this->_gameBlocks[$gameCode];
	}
	
	/**
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getCurrentGameModel() {
		return $this->getGameHelper()->getGameModel();
	}
	
	/**
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getGameModel($id = null) {
		$game = Hub::getSingleton('competitions/game');
		if( !is_null($id) ) {
			$game->load($id);
		}
		
		return $game;
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Game
	 */
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
	
	public function getCurrentGame() {
		return $this->getGameHelper()->getCurrentGame();
	}
	
	/**
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCurrentCompetition() {
//		var_dump(Hub::getHelper('competitions/competition')->getCurrentCompetition());
		return Hub::getHelper('competitions/competition')->getCurrentCompetition();
	}
	
	/**
	 * @return Xedin_Facebook_Helper_Url
	 */
	public function getFacebookUrlHelper() {
		return Hub::getHelper('facebook/url');
	}
	
	/**
	 * @return Xedin_Competitions_Block_Game_Abstract
	 * @throws Xedin_Competitions 
	 */
	public function getCurrentGameBlock() {
		if( !$this->_currentGameBlock ) {
			$currentGame = $this->getCurrentGame();
			$blockType = $this->getGameBlockType($currentGame->getCode());
			$mainBlockName = $this->getMainBlockName();
			$mainBlock = $this->getLayout()->getBlock($mainBlockName);
			if( !$mainBlock ) {
				throw Hub::exception('Xedin_Competitions', 'Could not find block with name "' . $mainBlockName . '".');
			}
			$this->_currentGameBlock = $this->getLayout()->createBlock($blockType, $mainBlock->getNameInLayout())
						->setMainSelector($mainBlock->getMainSelector())
						->setActionButtonSelector($mainBlock->getActionButtonSelector())
						->setGameCode($currentGame->getCode());
		}
		
		return $this->_currentGameBlock;
	}
	
	public function getMainBlockName() {
		return $this->_mainBlockName;
	}
	
	public function handleCurrentGame() {
		$game = $this->getCurrentGame();
		if( !$game ) {
			return $this;
		}
		
		$handler = $game->getGameTypeHandler();
		if( !$handler ) {
			return $this;
		}
	
		$handler->handleGame($this->getCurrentParticipant());
		
		return $this;
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Participantd
	 */
	public function getCurrentParticipant() {
		return $this->getParticipantHelper()->getCurrentParticipant();
	}
	
	public function isUsingFacebook() {
		return $this->_isUsingFacebook;
	}
	
	public function getRedirectUrl($url = '', $prepare = true, $params = array()) {
		if( $this->_isUsingFacebook ) {
			$facebook = $this->getFacebook();
			if( !$facebook ) {
				$this->_noFacebook();
			}
			
			$signedRequest = $facebook->getRawSignedRequest();

			if( $signedRequest ) {
				$params['signed_request'] = $signedRequest;
			}
		}
		return parent::getRedirectUrl($url, $prepare, $params);
	}
	
	protected function _noFacebook() {
		throw Hub::exception('Xedin_Competitions', 'No facebook found.');
	}
}