<?php

/**
 * Description of Play
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Controller_Play extends Xedin_Competitions_Controller_Abstract {
	
	protected $_defaultGameRedirectUri = '*/welcome';
	protected $_defaultGameRedirectUriParams = array();
	protected $_newParticipantPostKey = 'participant';
	protected $_isParticipantRequired = true;
	protected $_isCreateParticipantIfNone = true;
	
	protected function _gameAction() {
		$gameCode = $this->getParam('code');
		if( !$gameCode ) {
			$this->_defaultGame();
		}
		
		$blockType = $this->getGameBlockType($gameCode);
		if( !$blockType ) {
			$this->_noBlockType($gameCode);
		}
		
		if(  $this->isParticipantRequired() && !$this->getCurrentParticipant()) {			
			if( $this->isCreateParticipantIfNone() ) {
				$participantDetails = $this->getRequest()->getPost($this->getParticipantPostKey());
				if( !$participantDetails ) {
					$this->_noParticipant();
				}
				$this->createNewParticipant($participantDetails);
			}
			else {
				$this->_noParticipant();
			}
		}
		
		if( $this->_beforeHandleCurrentGame() ) {
			$this->handleCurrentGame();
			$this->_afterHandleCurrentGame();
		}
		
		try {
			// Creates the current game block;
			$this->getCurrentGameBlock();
		}
		catch( Exception $e ) {	
			$this->_couldNotCreateCurrentGameBlock($blockType);
		}
	}
	
	public function getParticipantPostKey() {
		return $this->_newParticipantPostKey;
	}
	
	public function createNewParticipant($participantDetails = null) {
		$this->getParticipantHelper()->createNewParticipant($participantDetails);
	}
	
	protected function _defaultGame() {
		$this->redirect($this->getDefaultGameRedirectUri(), true, $this->getDefaultGameRedirectUriParams());
	}
	
	protected function _noBlockType($gameCode) {
		throw Hub::exception('Xedin_Competitions', 'No block type for code "' . $gameCode . '" could be found.');
	}
	
	protected function _noParticipant() {
		throw Hub::exception('Xedin_Competitions', 'No participant found.');
	}
	
	protected function _couldNotCreateCurrentGameBlock($blockType) {
		throw Hub::exception('Xedin_Competitions', 'No block for type "' . $blockType . '" could be found.');
	}
	
	public function getDefaultGameRedirectUri() {
		return $this->_defaultGameRedirectUri;
	}
	
	public function getDefaultGameRedirectUriParams() {
		return $this->_defaultGameRedirectUriParams;
	}
	
	public function isParticipantRequired() {
		return $this->_isParticipantRequired;
	}
	
	public function isCreateParticipantIfNone() {
		return $this->_isCreateParticipantIfNone;
	}
	
	public function getGameBlockType($gameCode, $default = null) {
		if( !isset($this->_gameBlocks[$gameCode]) ) {
			$game = $this->getGameModel()->getInstance($gameCode)->loadMeta();
			if( !$game->getId() ) {
				return $default;
			}
			
			$this->_gameBlocks[$gameCode] = $game->getGameBlock();
		}
		return $this->_gameBlocks[$gameCode];
	}
	
	protected function _beforeHandleCurrentGame() {
		return $this;
	}
	
	protected function _afterHandleCurrentGame() {
		return $this;
	}
}