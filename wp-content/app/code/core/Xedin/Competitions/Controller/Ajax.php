<?php

/**
 * Description of Ajax
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Controller_Ajax extends Xedin_Competitions_Controller_Abstract {
	
	/** @var Xedin_Object */
	protected $_ajaxResponse;
	
	public function __construct() {
		parent::__construct();
		$this->_ajaxResponse = new Xedin_Object;
	}
	
	protected function _play() {
		try {
			ob_start();
			$this->_processPlayRequest();
			echo $this->getLayout()->getOutput();
			$ob = ob_get_clean();
			if( !empty($ob) ) {
				$this->setResponseHtml($ob);
				$ob = null;
			}
			$this->_outputResponse();
		}
		catch( Zend_Db_Exception $e ) {
			$writeConnection = Hub::getSingleton('core/resource')->getConnection('core_write');
			/* @var $writeConnection Zend_Db_Adapter_Abstract */
			$sql = $writeConnection->getProfiler()->getLastQueryProfile()->getQuery();
			$this->setErrorText($e->getMessage() . "\n" . 'Query: ' . $sql)
					->setErrorCode($e->getCode());
		}
		catch( Exception $e ) {
			$this->setErrorText($e->getMessage())
					->setErrorCode($e->getCode());
		}
		$this->_outputResponse();
	}
	
	protected function _outputResponse() {
		$response = $this->_getAjaxResponse();
		if( !$response->hasData('error') ) {
			$response->setData('error', false);
		}
		
		$response->setData('type', $this->getRequest()->getPost('type'));
		
		echo $response->toJson();
		exit();
	}
	
	protected function _getAjaxResponse() {
		return $this->_ajaxResponse;
	}
	
	protected function setResponseHtml($html) {
		$this->_getAjaxResponse()->setData('html', $html);
		return $this;
	}
	
	public function setError($error = true) {
		$this->_getAjaxResponse()->setData('error', (bool)$error);
		if( is_numeric($error) ) {
			$this->_getAjaxResponse()->setErrorCode($error);
		}
		elseif( is_string($error) ) {
			$this->_getAjaxResponse()->setErrorText($error);
		}
		
		return $this;
	}
	
	public function setErrorCode($code) {
		$code = intval($code);
		$this->setError($code);
		
		return $this;
	}
	
	public function setErrorText($text) {
		$text = (string)$text;
		$this->setError($text);
		
		return $this;
	}
	
	protected function _processPlayRequest() {
		return $this;
	}
}