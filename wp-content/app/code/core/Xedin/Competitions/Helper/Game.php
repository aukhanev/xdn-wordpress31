<?php

/**
 * Description of Game
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Helper_Game extends Xedin_Competitions_Helper_Abstract {
	
	const REG_PATH_CURRENT_GAME = 'competitions/current_game';
	
	/**
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getCurrentGame() {
		if( !Hub::registry(self::REG_PATH_CURRENT_GAME) ) {
			$gameCode = $this->getController()->getParam('code');
			if( !$gameCode ) {
				return null;
			}
			
			$game = $this->getGameModel($gameCode);
			if( $game->hasData() ) {
				Hub::register( $game->loadMeta(), self::REG_PATH_CURRENT_GAME);
			}
		}
		
		return Hub::registry(self::REG_PATH_CURRENT_GAME);
	}
	
	/**
	 * @param string|int $id
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getGameModel($id) {
		$gameModel = Hub::getSingleton('competitions/game');
		
		if( !is_null($id) ) {
			$gameModel->load($id);
		}
		
		return $gameModel;
	}
}