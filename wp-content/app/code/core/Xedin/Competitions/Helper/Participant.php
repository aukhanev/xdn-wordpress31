<?php

/**
 * Description of Participant
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Helper_Participant extends Xedin_Competitions_Helper_Abstract {
	
	const REG_KEY_CURRENT_PARTICIPANT = 'competitions/current_participant';
	const ATTRIBUTE_PARTICIPANT_FACEBOOK_ID = 'facebook-id';
	
	/**
	 * @return Xedin_Competitions_Model_Participant|null|boolean
	 * @throws Xedin_Competitions_Exception
	 */
	public function getCurrentParticipant() {
		if( !Hub::registry(self::REG_KEY_CURRENT_PARTICIPANT) ) {
			$currentCompetition = $this->getCompetitionHelper()->getCurrentCompetition();
			if( !$currentCompetition ) {
				return null;
			}
			$facebook = $currentCompetition->getFacebook();
			$fbUser = $facebook->getUser();
			
			if( $fbUser ) {
				try {
					$participant = $this->getParticipantModel()->getByAttribute(self::ATTRIBUTE_PARTICIPANT_FACEBOOK_ID, $fbUser);
					
					if( !$participant ) {
						return false;
					}
					
					Hub::register($participant->loadMeta(), self::REG_KEY_CURRENT_PARTICIPANT);
				}
				catch( FacebookApiException $e ) {
					throw Hub::exception('Xedin_Competitions', 'Communication with Facebook failed: ' . "\n" . $e->getMessage());
				}
			}
		}
		
		return Hub::registry(self::REG_KEY_CURRENT_PARTICIPANT);
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	public function getParticipantModel($id = null) {
		$participant = Hub::getModel('competitions/participant');
		
		if( !empty($id) ) {
			$participant->load($id);
		}
		
		return $participant;
	}
	
	/**
	 * @param array $participantDetails
	 * @param bool $reload
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function createNewParticipant($participantDetails, $reload = true) {
		if( !($participantDetails instanceof Varien_Object) ) {
			$participantDetails = $this->getParticipantModel()->setData($participantDetails);
		}
		
		
		$fbUser = $this->getCompetitionHelper()->getCurrentCompetition()->getFacebook()->getUser();
		
		$participant = $this->getParticipantModel()
						->setData(array(
							self::ATTRIBUTE_PARTICIPANT_FACEBOOK_ID	=>	$fbUser,
							'attribute_set_id'						=>	'participant',
							'username'								=>	'fb-' . $fbUser,
							'first_name'							=>	$participantDetails->getFirstName(),
							'last_name'								=>	$participantDetails->getLastName(),
							'facebook_id'							=>	$fbUser
						));
		if( $participantDetails->hasEmail() ) {
			$participant->setEmail($participantDetails->getEmail());
		}
//			var_dump($participant);
//			exit('participant details asdasd');
		
		$participant->save();
		
		if( $reload ) {
			$participant->load();
		}
		
		return $participant;
	}
}