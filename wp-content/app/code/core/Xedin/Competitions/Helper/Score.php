<?php

/**
 * Description of Score
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Helper_Score extends Xedin_Competitions_Helper_Abstract {
	
	static protected $_currentTotal;
	
	public function getCurrentTotal() {
		if( !isset(self::$_currentTotal) ) {
			self::$_currentTotal = $this->getLeaderboardParticipantModel()->load($this->getParticipantHelper()->getCurrentParticipant()->getId())->getScore();
		}
		
		return self::$_currentTotal;
	}
	
	/**
	 * @return Xedin_Competitions_Leaderboard_Participant
	 */
	public function getLeaderboardParticipantModel() {
		return Hub::getSingleton('competitions/leaderboard_participant');
	}


	/**
	 * @return Xedin_Competitions_Model_Score
	 */
	public function getScoreModel() {
		return Hub::getSingleton('competitions/score');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
}