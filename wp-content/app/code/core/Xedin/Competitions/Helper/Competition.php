<?php

/**
 * Description of Competition
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Helper_Competition extends Xedin_Competitions_Helper_Abstract {
	
	const REG_KEY_ACTIVE_COMPETITION = 'competitions/active_competition';
	
	/**
	 * @return Xedin_Competitions_Model_Competition_Collection
	 */
	public function getActiveCompetitionsCollection() {
		return $this->getCompetitionModel()->getCollection()->addFieldToFilter('is_active', 1);
	}
	
	/**
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCurrentCompetition() {
		if( !Hub::registry(self::REG_KEY_ACTIVE_COMPETITION) ) {
			$competition = $this->getActiveCompetitionsCollection()->getFirstItem();
			if( $competition ) {
				Hub::register($competition->load(), self::REG_KEY_ACTIVE_COMPETITION);
			}
		}
		return Hub::registry(self::REG_KEY_ACTIVE_COMPETITION);
	}
	
	/**
	 * @param int|string $id
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCompetitionModel($id = null) {
		$competition = Hub::getSingleton('competitions/competition');
		
		if( !empty($id) ) {
			$competition->load($id);
		}
		
		return $competition;
	}
	
	protected function _getGlobalModel($modelUri, $id = null) {
		parent::_getGlobalModel($modelUri, $id);
	}
}