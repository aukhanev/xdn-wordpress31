<?php

/**
 * Description of Game
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Helper_Quiz_Question extends Xedin_Competitions_Helper_Abstract {
	
	const REG_KEY_RELEVANT_QUESTION = 'competitions/relevant_question';
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Question
	 */
	public function getRelevantQuestion() {
		if( !Hub::registry(self::REG_KEY_RELEVANT_QUESTION) ) {
			$start = strtotime('midnight UTC');
			$end = strtotime('midnight +1day -1 second UTC');
			$question = $this->getQuestionModel()->getCurrentlyActiveQuestionsCollection($start, $end)->getFirstItem();
			if( $question && $question->getId() ) {
				Hub::register($question, self::REG_KEY_RELEVANT_QUESTION);
			}
		}
		return Hub::registry(self::REG_KEY_RELEVANT_QUESTION);
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Question
	 */
	public function getQuestionModel() {
		return Hub::getSingleton('competitions/quiz_question');
	}
}