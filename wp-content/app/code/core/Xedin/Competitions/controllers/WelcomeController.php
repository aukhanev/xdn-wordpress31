<?php

/**
 * Description of WelcomeController
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 * + Participant registration moved to separate function.
 */
class Xedin_Competitions_WelcomeController extends Xedin_Competitions_Controller_Abstract {
	
	const ATTRIBUTE_PARTICIPANT_FACEBOOK_ID = 'facebook-id';
	
	/** @var Xedin_Competitions_Model_Game */
	protected $_defaultGame;
	
	public function indexAction() {
		$this->getFbInitBlock();
		$facebook = $this->getFacebook();
		
		if( $facebook->isLiked() ) {
			$this->redirectToDefaultGame();
		}
		$fbUser = $facebook->getUser();
		
		if( $fbUser ) {
			try {
				$participant = $this->getParticipantModel()->getByAttribute(self::ATTRIBUTE_PARTICIPANT_FACEBOOK_ID, $fbUser);
				if( $participant ) {
					
				}
			}
			catch( FacebookApiException $e ) {
			}
		}
		else {
		}
	}
	
	protected function _registerNewParticipant($details = null) {
		if( empty($details) ) {
			$details = $this->getFacebook()->api('/me');
		}
		
		$appData = $facebook->getDecodedAppData();

		$participant = $this->getParticipantModel()
				->setData(array(
					self::ATTRIBUTE_PARTICIPANT_FACEBOOK_ID	=>	$fbUser,
					'attribute_set_id'						=>	'participant',
					'username'								=>	'fb-' . $fbUser,
					'first_name'							=>	$details->getFirstName(),
					'last_name'								=>	$details->getLastName()
				));
		if( $details->hasEmail() ) {
			$participant->setEmail($details->getEmail());
		}

		$invitorId = null;
		if( empty($appData) || !$appData->hasData('ref') ) {
			$invitorId = $facebook->getInviteRequestModel()->loadForApp($fbUser, $facebook->getAppModel())->getHostId();

			if( $invitorId ) {
				$participant->setInvitorId($invitorId);
				$gameHandler = $this->getGameModel('accept-invite')->getGameTypeHandler();
				if( $gameHandler ) {
					$gameHandler->handleGame($invitorId);
				}
			}
		}
		else {
			$invitorId = $this->getParticipantModel()->loadByReferenceKey($appData->getRef())->getId();

			if( $invitorId ) {
				$participant->setInvitorId($invitorId);
				$gameHandler = $this->getGameModel('accept-share')->getGameTypeHandler();
				if( $gameHandler ) {
					$gameHandler->handleGame($invitorId);
				}
			}
		}
		$participant->save();
		return $this;
	}
	
	public function testNonceAction() {
		$user = Hub::getModel('competitions/participant')->load(1);
		$nonce = Hub::getModel('crypt/nonce');
		/* @var $nonce Xedin_Crypt_Model_Nonce */
		$nonceKey = $nonce->getNewNonce($user->getId(), 1);
		var_dump($user);
		var_dump($nonceKey);
		var_dump($nonce->assertEqualNonce($nonceKey, 1, 1));
		exit();
	}
	
	public function redirectToDefaultGame($params = array()) {
		$game = $this->getDefaultGame();
		if( !$game ) {
			throw Hub::exception('Xedin_Competitions', 'No game specified as default.');
		}
		
		$this->redirect('*/play/game', true, array_merge($params, array('code' => $game->getCode())) );
	}
	
	public function redirectAction() {
		$game = $this->getDefaultGame();
		var_dump(Hub::getUrl('asdasdasd'));
//		var_dump($this->getRedirectUrl('*/play/game', true, array('code' => $game->getCode()) ));
	}
	
	public function getDefaultGame($default = null) {
		if( !$this->_defaultGame ) {
			$competition = $this->getCurrentCompetition();
			if( !$competition ) {
				return $default;
			}

			$this->_defaultGame = $competition->getDefaultGame();
		}
		
		return $this->_defaultGame;
	}
	
	public function errorTestAction() {
		$obj = 4;
		$obj->do();
	}
}