<?php

class Xedin_Competitions_IndexController extends Xedin_Competitions_Controller_Abstract {
	
	protected $_isUsingFacebook = false;
	
	public function indexAction() {
		return false;
		for( $i=0; $i<10; $i++ ) {
			$phpFile = Hub::getModel('codegen/php_file');
			/* @var $phpFile Xedin_Codegen_Model_Php_File */
			$phpFile->setFullPath('test/test-' . ($i+1) . '.php');
			$phpFile->save();
		}
	}
	
	public function testAction() {
		$scoreModel = Hub::getSingleton('competitions/score');
		$scoreCollection = $scoreModel->getCollection();
		$profiler = $scoreCollection->getProfiler()->setEnabled(true);
		/* @var $profiler Zend_Db_Profiler */
		$scoreCollection->addFieldToFilter($scoreModel->getFieldNames('game_id'), '4')
			->addFieldToFilter($scoreModel->getFieldNames('participant_id'), 2)
				->addDaysCountToSelect()
				->load();
		var_dump($profiler->getLastQueryProfile());
		
	}
}