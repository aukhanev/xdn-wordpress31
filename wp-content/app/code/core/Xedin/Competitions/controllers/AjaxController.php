<?php

/**
 * Description of AjaxController
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_AjaxController extends Xedin_Competitions_Controller_Ajax {
	
	
//	<block type="competitions/template" name="main.game.area" template="page/main/game.area.php">
//		<block type="competitions/game_list" name="play.header" template="page/main/section.play.header.php"></block>
//		<block type="competitions/game_main" name="play.main" template="competitions/game/main.php"></block>
//	</block>
	
	
	protected $_gameListBlock;
	
	
//	public function playAction() {
////		var_dump( $this->getCurrentGameBlock() );
//		$fb = $this->getFacebook(); // Has to be done, sets reg. entry. Fix later.
//		
//		$this->handleCurrentGame();
//		
//		$game = $this->getCurrentGame();
//		if( $game->getRefreshOnUpdate() ) {
//			echo '<script type="text/javascript">
//			top.location.href = "' . $this->getFacebook()->getAppUrl(array('code' => $game->getCode())) . '";
//</script>';
//			exit();
//		}
//		else {
//		$this->getCurrentGameBlock();
//		}
//		
//	}
	
	protected function _processPlayRequest() {
		parent::_processPlayRequest();
		$this->handleCurrentGame();
		$handler = $this->getCurrentGame()->getGameTypeHandler();
		
		if( $this->getRequest()->getPost('type') == 'spin' ) {
			/* @var $handler Xedin_Competitions_Model_Game_Type_Sector_Random */
			$this->_getAjaxResponse()->setPointsGained($handler->getPointsGained())
					->setSector($handler->getSelectedSector()->getData());
		}
	}
	
	public function playAction() {
		$this->_play();
	}
}