<?php
/**
 * Description of WelcomeController
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 * + Now extends Xedin_Competitions_Controller_Play
 * + Added pre-game action - _beforeHandleCurrentGame
 */
class Xedin_Competitions_PlayController extends Xedin_Competitions_Controller_Play {
	
//	protected $_isUsingFacebook = false;
	protected $_defaultGameRedirectUri = '*/play/game';
	protected $_defaultGameRedirectUriParams = array('code' => 'fortune-wheel');
//	protected $_isParticipantRequired = false;
	
	const ATTRIBUTE_PARTICIPANT_FACEBOOK_ID = 'facebook_id';
	
	public function indexAction() {
		$this->_defaultGame();
	}
	
//	public function ___gameAction() {
//		$gameCode = $this->getParam('code');
//		if( !$gameCode ) {
//			$this->_defaultGame();
//		}
//		
//		$blockType = $this->getGameBlockType($gameCode);
//		
//		if( !$blockType ) {
//			$this->_noBlockType($gameCode);
//		}
//		
//		if( !$this->getCurrentParticipant() ) {
//			$participantDetails = $this->getRequest()->getPost('participant');
//			if( $participantDetails ) {
//				$this->getParticipantHelper()->createNewParticipant($participantDetails);
//			}
//		}
//		
//		$this->getFacebook()->getSignedRequest();
//		$this->addOpenGraphMeta();
//		
//		$this->getFbInitBlock();
//		$this->handleCurrentGame();
//		
//		try {
//			$this->getCurrentGameBlock();
//		}
//		catch( Exception $e ) {	
//			throw Hub::exception('Xedin_Competitions', 'No block for type "' . $blockType . '" could be found.');
//		}
//	}
	
	public function gameAction() {
		$this->getFacebook()->getSignedRequest();
		$this->addOpenGraphMeta();
		$this->getFbInitBlock();
		$this->_gameAction();
	}
	
	protected function _beforeHandleCurrentGame() {
		parent::_beforeHandleCurrentGame();
		if( !$this->getRequest()->getPost('type') ) {
			return false;
		}
		
		return $this;
	}
	
	public function addOpenGraphMeta() {
		$competition = $this->getCurrentCompetition();
		$fb = $this->getFacebook();
		$headBlock = $this->getLayout()->getBlock('head');
		$headBlock->addMeta('og:title', $competition->getLabel(), 'property')
				->addMeta('og:type', 'game', 'property')
				->addMeta('og:url', $this->getFacebookUrlHelper()->getFacebookPageUrl($fb), 'property')
				->addMeta('og:image', $headBlock->getLogoUri(), 'property')
				->addMeta('og:site_name', $competition->getLabel(), 'property')
				->addMeta('fb:app_id', $fb->getAppModel()->getApiKey(), 'property');
		
		return $this;
	}
	
	protected function _noParticipant() {
//		$this->createNewParticipant();
	}
	
	public function createNewParticipant($participantDetails = null) {
		if( is_null($participantDetails) ) {
			$participantDetails = $this->getFacebookUserDetails();
		}
		
		if( $participantDetails ) {
			$this->getParticipantHelper()->createNewParticipant($participantDetails);
		}
		
		return $this;
	}
	
	protected function getFacebookUserDetails() {
		$facebook = $this->getFacebook();
//		$facebook->getSignedRequest();
		$fbUser = $facebook->getUser();
		
		if( $fbUser ) {
			try {
				$details = $facebook->api('/me'); 
				$participant = new Xedin_Object;
				$appData = $facebook->getDecodedAppData();

				$participant->setData(array(
							self::ATTRIBUTE_PARTICIPANT_FACEBOOK_ID	=>	$fbUser,
							'attribute_set_id'						=>	'participant',
							'username'								=>	'fb-' . $fbUser,
							'first_name'							=>	$details->getFirstName(),
							'last_name'								=>	$details->getLastName()
						));
				if( $details->hasEmail() ) {
					$participant->setEmail($details->getEmail());
				}

//					$invitorId = null;
//					if( empty($appData) || !$appData->hasData('ref') ) {
//						$invitorId = $facebook->getInviteRequestModel()->loadForApp($fbUser, $facebook->getAppModel())->getHostId();
//					
//						if( $invitorId ) {
//							$participant->setInvitorId($invitorId);
//						}
//					}
//					else {
//						$invitorId = $this->getParticipantModel()->loadByReferenceKey($appData->getRef())->getId();
//						
//						if( $invitorId ) {
//							$participant->setInvitorId($invitorId);
//						}
//					}
			}
			catch( FacebookApiException $e ) {
				throw Hub::exception('Xedin_Competitions', 'Something went wrong while acquiring participant details.');
			}
			
			return $participant;
		}
		
		return null;
	}
	
//	protected function _defaultGame() {
//		$this->redirect('*/welcome');
//	}
}