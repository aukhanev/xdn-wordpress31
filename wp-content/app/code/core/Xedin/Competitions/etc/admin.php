<?php

return array(
	'system' => array(
		'modules' => array(
			'competitions' => array(
				'name' => 'competitions',
				'label' => 'Competitions',
				'models' => array(
					'competition' => array(
						'path'			=>	'competitions/competition',
						'label'			=>	'Competitions',
						'fields'		=>	array(
							'is_active'		=>	array(
								'label'			=>	'Is Active',
								'field'			=>	'is_active',
								'render_model'	=>	'core/render_select_yesno'
							)
						)
					),
					'game' => array(
						'path'			=>	'competitions/game',
						'label'			=>	'Games',
						'fields'		=>	array(
							'competition_id'	=>	array(
								'label'				=>	'Competition',
								'field'				=>	'competition_id',
								'render_model'		=>	'competitions/render_competitions'
							),
							
							'is_visible_on_frontend'	=>	array(
								'label'				=>	'Show on Frontend',
								'field'				=>	'is_visible_on_frontend',
								'render_model'		=>	'core/render_select_yesno'
							)
						),
						'details_block'	=>	'competitions/admin_grid_details_game_sector'
					),
					'participant'	=> array(
						'path'			=>	'competitions/participant',
						'label'			=>	'Participants'
					),
					'score'			=> array(
						'path'			=>	'competitions/score',
						'label'			=>	'Scores',
						'fields'		=>	array(
							'game_id'		=>	array(
								'label'				=>	'Game',
								'field'				=>	'game_id',
								'render_model'		=>	'competitions/render_game',
								'sort_position'		=>	11
							)
						)
					),
					'game_sector'	=> array(
						'path'			=>	'competitions/game_sector',
						'label'			=>	'Sectors'
					)
//					'poll' => array(
//						'path'			=>	'competitions/poll',
//						'label'			=>	'Polls',
//						'fields'		=>	array(
//							'vote_attribute_set'	=>	array(
//								'label'				=>	'Vote Attribute Set',
//								'field'				=>	'vote_attribute_set',
//								'render_model'		=>	'eav/render_fields_admin_attribute_set'
//							)
//						)
//					),
//					'poll_vote' => array(
//						'path'			=>	'competitions/poll_vote',
//						'label'			=>	'Poll Votes',
//						'fields'		=>	array(
//							'poll_id'			=>	array(
//								'label'				=>	'Poll',
//								'field'				=>	'poll_id',
//								'render_model'		=>	'competitions/render_poll'
//							),
//							'created_at'		=>	array(
//								'label'				=>	'Created At',
//								'field'				=>	'created_at',
//								'render_model'		=>	'core/render_fields_datetime'
//							),
//							'participant_id'		=>	array(
//								'label'				=>	'Participant',
//								'field'				=>	'participant_id',
//								'render_model'		=>	'competitions/render_participant_anchor'
//							)
//						)
//					),
//					'football_team' => array(
//						'path'			=>	'competitions/football_team',
//						'label'			=>	'Football Teams'
//					)
				)
			),
			
			'reports'			=>	array(
				'name'				=>	'reports',
				'label'				=>	'Reports',
				'models'			=>	array(
					'poll_vote'			=>	array(
						'path'				=>	'competitions/participant_poll_vote',
						'label'				=>	'Participant Votes',
						'fields'			=>	array(
							'participant_id'		=>	array(
								'label'				=>	'Participant',
								'field'				=>	'participant_id',
								'render_model'		=>	'competitions/render_participant_anchor'
							),
							'poll_id'		=>	array(
								'label'				=>	'Poll',
								'field'				=>	'poll_id',
								'render_model'		=>	'competitions/render_poll'
							),
							'team_a'			=>	array(
								'label'				=>	'Team A',
								'field'				=>	'team_a',
								'render_model'		=>	'competitions/render_football_team'
							),
							
							'team_a_score'		=>	array(
								'label'				=>	'Team A Score',
								'field'				=>	'team_a_score'
							),
							'team_b'			=>	array(
								'label'				=>	'Team B',
								'field'				=>	'team_b',
								'render_model'		=>	'competitions/render_football_team'
							),
							
							'team_b_score'		=>	array(
								'label'				=>	'Team B Score',
								'field'				=>	'team_b_score'
							)
//							'point_total'		=>	array(
//								'label'				=> 'Total Points',
//								'field'				=>	'point_total'
//							),
//							'rank'		=>	array(
//								'label'				=> 'Rank',
//								'field'				=> 'rank',
//								'sort_position'		=>	11
//							)
						)
					),
					'participantGame'	=>	array(
						'path'				=>	'competitions/leaderboard_participant',
						'label'				=>	'Participant Score',
						'fields'			=>	array(
							'point_total'		=>	array(
								'label'				=> 'Total Points',
								'field'				=>	'point_total'
							),
							'rank'		=>	array(
								'label'				=> 'Rank',
								'field'				=> 'rank',
								'sort_position'		=>	11
							)
						)
					)
				)
			)
		)
	)
)
		
?>