<?php

return array(
	'modules' => array(
		'Xedin_Competitions' => array(
			'render' => array(
				'game_type' => array(
					'like' => array(
						'uri'	=> 'competitions/game_type_like',
						'label' => 'Like'
					),
					'share' => array(
						'uri'	=> 'competitions/game_type_share',
						'label' => 'Share'
					),
					'invite' => array(
						'uri'	=> 'competitions/game_type_invite',
						'label' => 'Invite'
					),
					'quiz_answer' => array(
						'uri'	=> 'competitions/game_type_quiz_answer',
						'label' => 'Quiz Answer'
					),
					'quiz_question' => array(
						'uri'	=> 'competitions/game_type_quiz_question',
						'label' => 'Quiz Question'
					),
					'invite_accept' => array(
						'uri'	=> 'competitions/game_type_invite_accept',
						'label' => 'Accept Invite'
					),
					'correct_bonus_code' => array(
						'uri'	=> 'competitions/game_type_bonus_code',
						'label' => 'Correct Bonus Code'
					),
					'quiz_wrong_answer' => array(
						'uri'	=> 'competitions/game_type_quiz_wrong',
						'label' => 'Wrong Quiz Answer'
					),
					'share_accept'		=> array(
						'uri'	=> 'competitions/game_type_share_accept',
						'label' => 'Accept Share'
					),
					'vote_generic'		=>	array(
						'uri'	=>	'competitions/game_type_vote_generic',
						'label'	=>	'Generic Vote'
					),
					'random_sector'		=>	array(
						'uri'	=>	'competitions/game_type_sector_random',
						'label'	=>	'Random Sector'
					)
				),
				
				'game_state'			=>	array(
					'disabled'				=>	array(
						'code'					=>	'disabled',
						'label'					=>	'Disabled'
					),
					'available'				=>	array(
						'code'					=>	'available',
						'label'					=>	'Available'
					),
					'not-available'			=>	array(
						'code'					=>	'not-available',
						'label'					=>	'Unavailable'
					),
					'active'				=>	array(
						'code'					=>	'active',
						'label'					=>	'Active'
					)
				)
			)
		),
		
		'Xedin_Eav'			=>	array(
			'render'			=>	array(
				'values'			=>	array(
					'3'					=> array(
						'uri'				=>	'competitions/render_game_type',
						'label'				=>	'Game Type'
					),
					'competition_game'	=> array(
						'uri'				=>	'competitions/render_competition_game',
						'label'				=>	'Same Competition Game'
					),
					'competition_game_any'	=> array(
						'uri'				=>	'competitions/render_game',
						'label'				=>	'Any Game'
					),
					'yesno'				=> array(
						'uri'				=>	'core/render_select_yesno',
						'label'				=>	'Yes or No'
					),
					'game_state'		=>	array(
						'uri'				=>	'competitions/render_game_state',
						'label'				=>	'Game State'
					),
					'datetime'			=>	array(
						'uri'				=>	'core/render_fields_datetime',
						'label'				=>	'Date &amp; Time'
					),
					'competitions_football_team'	=>	array(
						'uri'				=>	'competitions/render_football_team',
						'label'				=>	'Football Team'
					),
					'competitions_poll'	=>	array(
						'uri'				=>	'competitions/render_poll',
						'label'				=>	'Poll'
					),
					
					'game_block'		=>	array( // The admin field block URI for the main game block
						'uri'				=>	'competitions/render_game_block',
						'label'				=>	'Game Block'
					)
				)
			)
		)
	)
)
?>