<?php

/**
 * Description of Startracing
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Leaderboard extends Xedin_Competitions_Block_Template {
	
	protected $_currentParticipant;
	protected $_leaderboardCollection;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		$this->setLimit(10);
	}
	
	public function getParticipantGameReportModel() {
		return Hub::getSingleton('competitions/leaderboard_participant');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Leaderboard_Participant_Collection
	 */
	public function getReportCollection() {
		if( !$this->_leaderboardCollection ) {
			return $this->getParticipantGameReportModel()->getCollection()
					->addRankLimit($this->getLimit()+1)
					->addParticipantIdToFilter($this->getCurrentParticipant()->getId())
					->isMetaAutoLoad(true);
		}
		
		return $this->_leaderboardCollection;
	}
	
	public function getCurrentParticipant() {
		if( !$this->_currentParticipant ) {
			$this->_currentParticipant = parent::getCurrentParticipant();
		}
		return $this->_currentParticipant;
	}
	
	public function getCurrentReport($currentParticipant = null) {
		if( is_null($currentParticipant) ) {
			$currentParticipant = $this->getCurrentParticipant();
		}
		
		foreach( $this->getReportCollection()->getItems() as $_id => $_report ) {
			if( $this->isCurrentReport($_report, $currentParticipant) ) {
				return $_report->setRank($_id);
			}
		}
		
		return null;
	}
	
	public function isCurrentReport($report, $currentParticipant = null) {
		if( is_null($currentParticipant) ) {
			$currentParticipant = $this->getCurrentParticipant();
		}
		
		if( $currentParticipant instanceof Xedin_Competitions_Model_Participant ) {
			$currentParticipant = $currentParticipant->getId();
		}
		
		if( $report instanceof Xedin_Core_Model_Abstract ) {
			$report = $report->getId();
		}
		return ( $report == $currentParticipant );
	}
	
	
	/**
	 * @return Xedin_Core_Helper_String_Numbers
	 */
	public function getNumberHelper() {
		return Hub::getHelper('core/string_numbers');
	}
}