<?php

/**
 * Description of Ajax
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Clientside_Ajax_Init extends Xedin_Clientside_Block_Script {
	
	
	public function getOutputModel() {
		if( !$this->_outputModel ) {
			$this->_outputModel = $this->js();
			$currentGame = $this->getCurrentGame();
			if( $currentGame ) {
				$this->_outputModel->concat(

				'(function($) {
					$(document).bind("fb-ready", function() {
						var competition = competition || Xedin_Competition_Abstract.init("#' . $this->getMainSelector() . '").setBaseUrl("' . $this->getBaseUrl() . '");
						FB.Event.subscribe("edge.create", function(response) {
							competition.executeGameAction("' . $currentGame->getCode() . '");
						});
					})
				}(jQuery));
				'
				);
			}
		}
		
		return $this->_outputModel;
	}
	
	public function getBaseUrl() {
		return $this->getRedirectUrl('*/ajax/play');
	}
	
	public function getCurrentGame() {
		return $this->getGameHelper()->getCurrentGame();
	}
	
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
}