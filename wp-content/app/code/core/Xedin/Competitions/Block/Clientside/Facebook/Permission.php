<?php

/**
 * Description of Permission
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Clientside_Facebook_Permission extends Xedin_Clientside_Block_Script {
	
	
	public function getOutputModel() {
		
		$signedRequest = $this->getCompetitionHelper()->getCurrentCompetition()->getFacebook()->getSignedRequest();
		$signedRequest = $signedRequest ? ('?signed_request=' . $signedRequest) : '';
		return $this->js()->concat('
                                    jQuery("#' . $this->getButtonSelector() . '").bind("click", function() {
										var competition = competition || Xedin_Competition_Abstract.init();
										if( !FB ) { return false; } 
										var fbPermissions = "";
										
										competition.takeAuthorisedAction(function() {
											top.location = "' . $this->getRedirectLocation() . $signedRequest . '";
										});
									});
		');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
}