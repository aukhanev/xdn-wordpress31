<?php

/**
 * Description of Startracing
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Startracing extends Xedin_Competitions_Block_Template {
	
	protected $_permissionBlock;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setButtonSelector('btn-start-racing');
	}
	
	public function getPermissionBlock() {
		if( !$this->_permissionBlock ) {
			$facebook = $this->getFacebook();
			if( !$facebook ) {
				throw Hub::exception('Xedin_Competitions', 'Could not retrieve Facebook data from controller.');
			}
			$appId = 
			$this->_permissionBlock = $this->getLayout()->createBlock('competitions/clientside_facebook_permission', $this->getNameInLayout())
				->setButtonSelector($this->getButtonSelector())
//				->setRedirectLocation($this->getController()->getRedirectUrl('*/*'));' . $this->getRedirectLocation() . '
				->setRedirectLocation( $facebook->getAppUrl() );
		}
		return $this->_permissionBlock;
	}
	
	protected function _toHtml() {
		return parent::_toHtml() . $this->getPermissionBlock()->toHtml();
	}
}