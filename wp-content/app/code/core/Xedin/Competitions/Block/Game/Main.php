<?php

/**
 * Description of Header
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Main extends Xedin_Competitions_Block_Template {
	
	protected $_ajaxInitBlock;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setMainSelector('main')
				->setActionButtonSelector('competition-action-button');
	}
	
	protected function _toHtml() {
		return parent::_toHtml();// . $this->getAjaxInitBlock()->toHtml();
	}
	
	/**
	 * @return Xedin_Competitions_Block_Clientside_Ajax_Init
	 */
	public function getAjaxInitBlock() {
		if( !$this->_ajaxInitBlock ) {
			$this->_ajaxInitBlock = $this->getLayout()->createBlock('competitions/clientside_ajax_init')
					->setMainSelector($this->getMainSelector())
					->setActionButtonSelector($this->getActionButtonSelector());
		}
		
		return $this->_ajaxInitBlock;
	}
}