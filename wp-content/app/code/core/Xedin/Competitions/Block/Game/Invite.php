<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Invite extends Xedin_Competitions_Block_Game_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/invite.php');
	}
	
	public function getFacebookPageUri() {
		return $this->getFacebookUrlHelper()->getFacebookPageUrl($this->getFacebook());
	}
	
	/**
	 * @param type Xedin_Facebook_Model_Invite_Request
	 */
	public function getInviteRequestModel($id = null) {
		$request = Hub::getSingleton('facebook/invite_request');
		
		if( !is_null($id) ) {
			$request->load($id);
		}
		
		return $request;
	}
	
	public function getExcludeIds($participant = null) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		$fbApp = $this->getFacebook()->getAppModel();
		return $this->getInviteRequestModel()->getCollection()->addFieldToFilter('app_id', $fbApp->getId())
				->addFieldToFilter('host_id', $participant->getId())
				->load()
				->getColumn('facebook_id');
	}
	
	public function getExcludeIdsString($participant = null) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		return implode(', ', $this->getExcludeIds($participant));
	}
}