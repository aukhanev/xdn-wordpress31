<?php

/**
 * Description of List
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_List extends Xedin_Competitions_Block_Game_Abstract {
	
	const GAME_STATE_DISABLED = 'disabled';
	const GAME_STATE_AVAILABLE = 'available';
	const GAME_STATE_UNAVAILABLE = 'not-available';
	const GAME_STATE_ACTIVE = 'active';
	
	/**
	 * @return Xedin_Competitions_Model_Game_Collection 
	 */
	public function getGamesCollection() {
		$currentCompetition = $this->getCurrentCompetition();
		if( !$currentCompetition ) {
			return null;
		}
		return $this->getGameModel()->getFrontentGamesCollection()->isMetaAutoLoad(true)
				->addSortOrder('sort_order', true)
				->addFieldToFilter('competition_id', $currentCompetition->getId());
	}
	
	public function getGameUrl($game) {
		if( $game instanceof Xedin_Competitions_Model_Game ) {
			$game = $game->getCode();
		}
		
		return $this->getRedirectUrl('*/play/game', true, array('code' => $game));
	}
	
	public function canScore($game, $participant = null) {
		if( !is_object($game) ) {
			$game = $this->getGameModel($game);
		}
		/* @var $game Xedin_Competitions_Model_Game */
		
		if( empty($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		return $game->canScore($participant);
	}
	
	public function isAvailable($game, $participant = null) {
		if( !is_object($game) ) {
			$game = $this->getGameModel($game);
		}
		/* @var $game Xedin_Competitions_Model_Game */
		
		if( empty($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		return $game->getIsActiveIfParticipantCannotScore() || $this->canScore($game, $participant);
	}
	
	public function isDisabled($game, $participant = null) {
		if( !is_object($game) ) {
			$game = $this->getGameModel($game);
		}
		/* @var $game Xedin_Competitions_Model_Game */
		
		if( empty($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		return !$game->isGameDependencySatisfied($participant);
	}
	
	public function isActive($game) {
		return $this->getCurrentGame()->getCode() == $game->getCode();
	}
	
	public function getCurrentParticipant() {
		return $this->getParticipantHelper()->getCurrentParticipant();
	}
	
	public function getCurrentCompetition() {
		return $this->getCompetitionHelper()->getCurrentCompetition();
	}
	
	public function getGameState($game = null) {
		if( is_null($game) ) {
			$game = $this->getCurrentGame();
		}
		
		if( $this->isDisabled($game) ) {
			return self::GAME_STATE_DISABLED;
		}
		
		if( $this->isActive($game) ) {
			if( $game->getAppearAsIfActive() ) {
				return $game->getAppearAsIfActive();
			}
			
			return self::GAME_STATE_ACTIVE;
		}
		
		if( $game->getIsNotAvailableIfGameCannotScore() ) {
			$conditionGame = Hub::getModel('competitions/game')->load($game->getIsNotAvailableIfGameCannotScore());
			/* @var $game Xedin_Competitions_Model_Game */
			if( $conditionGame->getId() && !$conditionGame->canScore($this->getCurrentParticipant()) ) {
				return self::GAME_STATE_UNAVAILABLE;
			}
		}
		
		if( $this->isAvailable($game) ) {
			return self::GAME_STATE_AVAILABLE;
		}
		
		return self::GAME_STATE_UNAVAILABLE;
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
}