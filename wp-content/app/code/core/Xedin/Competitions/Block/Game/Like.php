<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Like extends Xedin_Competitions_Block_Game_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/like.php');
	}
	
	public function getFacebookPageUri() {
		return $this->getFacebookUrlHelper()->getFacebookPageUrl($this->getFacebook());
	}
}