<?php

/**
 * Description of Code
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Bonus_Code extends Xedin_Competitions_Block_Game_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/lost.and.found.php');
	}
	
	public function getFormActionUrl() {
		return $this->getRedirectUrl('competitions/play/game', true, array('code' => 'lost-and-found'));
	}
	
	public function getTotalCodesAmount() {
		return $this->getBonusCodeModel()->getCollection()->load()->count();
	}
	
	public function getUsedCodesAmount($participant = null) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		return $this->getScoreModel()->getExtendedCollection($participant->getId(), $this->getCurrentGame()->getCode())
				->load()
				->count();
	}
	
	/**
	 * @return Xedin_Competitions_Model_Score
	 */
	public function getScoreModel() {
		return Hub::getSingleton('competitions/score');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Bonus_Code
	 */
	public function getBonusCodeModel() {
		return Hub::getSingleton('competitions/bonus_code');
	}
	
	public function isAnsweredCorrect() {
		return $this->getSession()->getFlashVar('bonus_code_result');
	}
}