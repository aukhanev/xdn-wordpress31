<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Quiz_Answer extends Xedin_Competitions_Block_Game_Abstract {
	
	protected $_question;
	protected $_lastAttempt;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/quiz/answer.php');
	}
	
	public function getFacebookPageUri() {
		return $this->getFacebookUrlHelper()->getFacebookPageUrl($this->getFacebook());
	}
	
	public function getAnswersCollection() {
		if( !$this->getQuestion() ) {
			return Hub::getSingleton('competitions/quiz_question')->getAnswersCollection();
		}
		return $this->getQuestion()->getAnswersCollection();
	}
	
	public function getFormActionUrl() {
		return $this->getRedirectUrl('competitions/play/game', true, array('code' => 'quiz-answer'));
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Quiz_Question
	 */
	public function getQuestionHelper() {
		return Hub::getHelper('competitions/quiz_question');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Quiz_Question
	 */
	public function getFormHelper() {
		return Hub::getHelper('html/form_field');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Question
	 */
	public function getQuestion() {
		if( !$this->_question ) {
			$this->_question = $this->getQuestionHelper()->getRelevantQuestion();
		}
		return $this->_question;
	}
	
	public function isAnswered($participant = null)  {
		return (bool)$this->getLastAttempt($participant);
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Attempt
	 */
	public function getLastAttempt($participant = null) {
		if( !$this->_lastAttempt ) {
			if( (!$question = $this->getQuestion()) ) {
				return null;
			}

			if( is_null($participant) ) {
				$participant = $this->getParticipantHelper()->getCurrentParticipant();
			}

			$this->_lastAttempt = $this->getQuestion()->getAttemptCollection($participant)->getFirstItem();
		}
		return $this->_lastAttempt;
	}
}