<?php

/**
 * Description of Fortunewheel
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Fortunewheel extends Xedin_Competitions_Block_Game_Abstract{
	
	protected $_gameCode = 'fortune-wheel';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/fortune.wheel.php');
	}
	
	public function getGameActionUrl() {
		return $this->getAjaxActionUrl('play', array('code' => $this->getGameCode()));
	}
	
	public function getFacebookPageTabUrl() {
		return $this->getFacebook()->getAppModel()->getFbPageTabUrl();
	}
	
	public function getFacebookPageUri() {
		return $this->getFacebook()->getAppModel()->getFbPageUrl();
	}
}