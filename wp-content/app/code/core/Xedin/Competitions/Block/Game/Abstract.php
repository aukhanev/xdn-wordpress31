<?php

/**
 * Description of Abstract
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Competitions_Block_Game_Abstract extends Xedin_Competitions_Block_Template {
		
	/**
	 * @param type $id
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getGameModel($id = null) {
		$game = Hub::getSingleton('competitions/game');
		if( !is_null($id) ) {
			$game->load($id);
		}
		return $game;
	}
	
	public function getAjaxActionUrl($action, $params = array()) {
		return $this->getRedirectUrl('competitions/ajax/' . $action, true, $params);
	}
	
	public function getGameActionUrl() {
		return $this->getAjaxActionUrl('play');
	}
	
	public function getNonce() {
		return $this->getCurrentParticipant()->getNonceKey();
	}
	
	public function getCurrentParticipant() {
		return $this->getParticipantHelper()->getCurrentParticipant();
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
	
	public function getCurrentGame() {
		return $this->getGameHelper()->getCurrentGame();
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Game
	 */
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
	
	public function getGameCode() {
		return $this->getCurrentGame()->getCode();
	}
	
	/**
	 * @return Xedin_Html_Helper_Form_Field
	 */
	public function getFormHelper() {
		return Hub::getHelper('html/form_field');
	}
	
	public function getSuccessMessage() {
		return $this->getCurrentGame()->getSuccessMessage();
	}
	
	public function getFailureMessage() {
		return $this->getCurrentGame()->getFailureMessage();
	}
}