<?php

/**
 * Description of Teamandscode
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Teamandscore extends Xedin_Competitions_Block_Game_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/team.and.score.php');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Football_Team_Collection
	 */
	public function getTeamsCollection() {
		return $this->getTeamModel()->getCollection();
	}
	
	/**
	 * @return Xedin_Competitions_Model_Football_Team
	 */
	public function getTeamModel($id = null) {
		$team = Hub::getSingleton('competitions/football_team');
		if( !is_null($id) ) {
			$team->load($id);
		}
		
		return $team;
	}
	
	public function getFormActionUrl() {
		if( !$this->hasData('form_action_url') ) {
			$this->setData( 'form_action_url', $this->getRedirectUrl('*/*/*', true, array('code' => 'team-and-score')) );
		}
		
		return $this->getData('form_action_url');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Poll_Vote 
	 */
	public function getVote($participant = null) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		if( !$participant ) {
			throw Hub::exception('Xedin_Competitions', 'Could not retrieve current user\'s info.');
		}
		return Hub::getSingleton('competitions/poll_vote')->getCollection()->isMetaAutoload(true)
				->addFieldToFilter('participant_id', $participant->getId())
				->getFirstItem();
	}
	
	public function getFacebookPageUri() {
		$fb = $this->getFacebook();
		return $fb->getUrlHelper()->getFacebookPageUrl($fb);
	}
	
	public function getFacebookPageTabUrl() {
		$fb = $this->getFacebook();
		return $fb->getUrlHelper()->getFacebookPageTabUrl($fb);
	}
	
	public function getShareDialogPictureUrl() {
		return Hub::getUrl('images/share-dialog.png', Hub::URL_TYPE_SKIN);
	}
	
	function getSkinImageUrl($url) {
		return Hub::getUrl($url, Hub::URL_TYPE_SKIN);
	}
}