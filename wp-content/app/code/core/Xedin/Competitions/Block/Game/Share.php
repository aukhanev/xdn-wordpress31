<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Game_Share extends Xedin_Competitions_Block_Game_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('competitions/game/share.php');
	}
	
	public function getFacebookPageUri() {
		return $this->getFacebookUrlHelper()->getFacebookPageTabUrl($this->getFacebook()) . '?app_data=' . $this->getEncodedData();
	}
	
	public function getSkinImageUrl($path = '') {
		return Hub::getUrl('images/' . $path, Hub::URL_TYPE_SKIN);
	}
	
	public function getEncodedData() {
		return $this->getFacebook()->getEncodedAppData(array('ref' => $this->getCurrentParticipant()->getReferenceKey()));
	}
}