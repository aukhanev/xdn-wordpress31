<?php

/**
 * Description of Sectors
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Admin_Grid_Details_Game_Game_Sectors extends Xedin_Adminhtml_Block_Grid_View_Abstract {
	
	protected $_allChildrenCollection;
	protected $_childrenCollection;
	protected $_currentSectors;
	
	protected $_attributeModelUri = 'eav/attribute';
	
	protected $_sectorsSelect;
	
	protected function _construct($layout = null, $sectors = array()) {
		parent::_construct($layout, $sectors);
		
		$this->setTemplate('competitions/grid/details/game.game.sectors.php');
	}
	
	public function getChildrenCollection() {
		if( is_null($this->_allChildrenCollection) ) {
			$this->_allChildrenCollection = $this->getCurrentObject()->getChildren();
//			var_dump($this->_allChildrenCollection);
//			exit();
		}
		
		return $this->_allChildrenCollection;
	}
	
	public function getAllChildrenCollection() {
		if( is_null($this->_childrenCollection) ) {
			$this->_childrenCollection = $this->getCurrentObject()->getRelationModel()->getResourceModel()->getChildModel()->getCollection();
		}
		
		return $this->_childrenCollection;
	}
	
	public function getAttributeModelUri() {
		return $this->_attributeModelUri;
	}
	
	public function getAttributeModel() {
		return Hub::getSingleton( $this->getAttributeModelUri() );
	}
	
	/**
	 * 
	 */
	public function getChildrenSelect() {
		if( !$this->_sectorsSelect ) {
			$currentObject = $this->getCurrentObject();
			$this->_sectorsSelect = $this->getLayout()->createBlock('html/element_input_select')->setMultiple(true);
			
			if( !($currentObject instanceof Xedin_Core_Model_Relation_Party_Abstract) ) {
				return $this->_sectorsSelect;
			}
			
			/* @var $currentObject Xedin_Competitions_Model_Game */
//			$attributeFieldName = $currentObject->getAttributeSetAttributeModel()->getAttributeIdFieldName();
			$selectFieldName = $currentObject->getRelationModel()->getChildIdFieldName();
			$selectName = $this->getFieldName($selectFieldName);
			$selectId = $this->getFieldId($selectFieldName);
			$this->_sectorsSelect->setId($selectId)->setName($selectName);
			$ids = $this->getChildrenCollection()->load()->getIds();
//			var_dump($ids);
			$this->_sectorsSelect->setOptions($this->getAllChildrenCollection()->getItems(), $ids);
		}
		
		return $this->_sectorsSelect;
	}
}