<?php

/**
 * Description of Sector
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Admin_Grid_Details_Game_Sector extends Xedin_Eav_Block_Admin_Grid_Details_Meta {
	
	protected $_sectorsBlockUri = 'competitions/admin_grid_details_game_game_sectors';
	protected $_sectorsBlockName = 'game.game.sectors';
	protected $_sectorsBlock;
	
	protected function _afterCreation() {
		parent::_afterCreation();
		
		$this->getSectorsBlock();
	}
	
	public function setSectorsBlockUri($blockUri) {
		$this->_sectorsBlockUri = $blockUri;
		return $this;
	}
	
	public function getSectorsBlockUri() {
		return $this->_sectorsBlockUri;
	}
	
	public function getSectorsBlock() {
		if( !$this->hasSectorsBlock() ) {
			$params = array(
				'after'			=>	'-',
			);

			if( $this->hasSectorsBlockName() ) {
				$params['name'] = $this->getSectorsBlockName();
			}
			
			$this->_setSectorsBlock( $this->getLayout()->createBlock( $this->getSectorsBlockUri(), $this->getNameInLayout(),  $params) );
			$this->setSectorsBlockName($this->_sectorsBlock->getNameInLayout());
			
		}
		
		return $this->_sectorsBlock;
	}
	
	protected function _setSectorsBlock(Xedin_Core_Block_Template $block) {
		$this->_sectorsBlock = $block;
	}
	
	public function hasSectorsBlock() {
		return ($this->_sectorsBlock instanceof Xedin_Core_Block_Template);
	}
	
	public function getSectorsBlockName() {
		return $this->_sectorsBlockName;
	}
	
	public function setSectorsBlockName($blockName) {
		$this->_sectorsBlockName = $blockName;
		return $this;
	}
	
	public function hasSectorsBlockName() {
		return !empty($this->_sectorsBlockName);
	}
}