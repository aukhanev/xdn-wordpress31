<?php

/**
 * Description of Dashboard
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Dashboard extends Xedin_Competitions_Block_Template {
	
	/**
	 * @return Xedin_Competitions_Helper_Score
	 */
	public function getScoreHelper() {
		return Hub::getHelper('competitions/score');
	}
	
	public function getCurrentTotal() {
		return $this->getScoreHelper()->getCurrentTotal();
	}
}