<?php

/**
 * Description of Like
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Facebook_Like extends Xedin_Facebook_Block_Ui_Like {
	
	const DATA_KEY_FACEBOOK_PAGE_URL = 'facebook_page_url';
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCurrentCompetition() {
		return $this->getCompetitionHelper()->getCurrentCompetition();
	}
	
	public function getFacebook() {
		if( !$this->_facebook ) {
			$this->_facebook = $this->getCurrentCompetition()->getFacebook();
		}
		return $this->_facebook;
	}
}