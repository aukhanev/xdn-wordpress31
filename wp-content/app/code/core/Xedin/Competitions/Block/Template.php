<?php

/**
 * Description of Template
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Block_Template extends Xedin_Core_Block_Template {
	
	/**
	 * @return Xedin_Facebook_Helper_Url
	 */
	public function getFacebookUrlHelper() {
		return Hub::getHelper('facebook/url');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCurrentCompetition() {
		return $this->getCompetitionHelper()->getCurrentCompetition();
	}
	
	public function getFacebook() {
		return $this->getCurrentCompetition()->getFacebook();
	}
	
	public function isLiked() {
		return $this->getFacebook()->isLiked();
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
	
	public function getCurrentParticipant() {
		return $this->getParticipantHelper()->getCurrentParticipant();
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Game
	 */
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
	
	public function getCurrentGame() {
		return $this->getGameHelper()->getCurrentGame();
	}
	
	/**
	 * @return Xedin_Core_Helper_String_Units
	 */
	public function getUnitsHelper() {
		return Hub::getHelper('core/string_units');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Session
	 */
	public function getSession() {
		return Hub::getSingleton('competitions/session');
	}
	
	public function getHomeUrl() {
		return $this->getRedirectUrl('competitions/welcome');
	}
	
	/**
	 * @return Xedin_Html_Helper_Form_Field 
	 */
	public function getFormHelper() {
		return Hub::getHelper('html/form_field');
	}
}