<?php

/**
 * Description of Session
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Session extends Xedin_Core_Model_Session_Abstract {
	
	protected $_namespace = 'competitions';
}