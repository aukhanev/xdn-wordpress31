<?php

/**
 * Description of Anchor
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Render_Participant_Anchor extends Xedin_Core_Model_Render_Entity_Anchor_Abstract {
	
	protected $_hrefMask = 'competitions/participant/view';
	protected $_modelUri = 'competitions/participant';
	protected $_labelFieldName = 'username';
}