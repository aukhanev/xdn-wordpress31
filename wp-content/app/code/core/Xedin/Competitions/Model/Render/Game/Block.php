<?php

/**
 * Description of Participants
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Render_Game_Block extends Xedin_Adminhtml_Model_Render_Details_Text {
	protected $_modelUri = 'competitions/game_block';
	protected $_blockUri = 'html/element_input_text';
}