<?php

/**
 * Description of Participants
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Render_Competition_Game extends Xedin_Adminhtml_Model_Render_Fields_Select_Abstract {
	protected $_modelUri = 'competitions/game';
	protected $_labelFieldName = 'code';
	
	public function getOptionsCollection() {
		return parent::getOptionsCollection()->addFieldToFilter('competition_id', $this->getValue()->getCompetitionId());
	}
}