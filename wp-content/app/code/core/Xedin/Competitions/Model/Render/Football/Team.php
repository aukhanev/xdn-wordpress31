<?php

/**
 * Description of Team
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Render_Football_Team extends Xedin_Adminhtml_Model_Render_Fields_Select_Abstract {
	protected $_modelUri = 'competitions/football_team';
}