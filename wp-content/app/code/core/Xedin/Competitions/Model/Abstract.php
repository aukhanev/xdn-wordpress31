<?php

abstract class Xedin_Competitions_Model_Abstract extends Xedin_Core_Model_Abstract {
	
	/**
	 * @return Xedin_Competitions_Helper_Game
	 */
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Session
	 */
	public function getSession() {
		return Hub::getSingleton('competitions/session');
	}
}