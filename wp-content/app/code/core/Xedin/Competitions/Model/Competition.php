<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Xedin_Competitions_Model_Competition extends Xedin_Competitions_Model_Meta_Abstract {
	
	protected $_facebook;
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/competition')
				->_setMetaModelName('competitions/competition_meta');
	}
	
	public function getGameModel($id = null) {
		$game = Hub::getSingleton('competitions/games');
		if( !is_null($id) ) {
			$game->load($id);
		}
		
		return $game;
	}
	
	/**
	 * @return Xedin_Facebook_Model_Facebook
	 */
	public function getFacebook() {
		
		if( !$this->_facebook ) {
			$this->_facebook = Hub::getModel('facebook/facebook');
			if( $this->getFacebookAppId() ) {
				$this->_facebook->load($this->getFacebookAppId());
			}
			/* @var $facebook Xedin_Facebook_Model_Facebook */
			
			$this->_facebook->getUser();
		}
		
		if( !is_object($this->_facebook->getApiModel()) ) {
//			var_dump($this->_facebook->getApiModel());
//			exit();
			return null;
		}
		return $this->_facebook;
	}
	
	public function getTimeSinceStart() {
		if( !$this->hasStartsAt() ) {
			return null;
		}
		
		$startsAt = strtotime($this->getStartsAt());
		return $this->getResourceModel()->getDatetimeHelper()->getTimeDifference($startsAt, $this->getResourceModel()->getTimeGmt());
	}
	
	public function getDaysSinceStart() {
		$timeDiff = $this->getTimeSinceStart();
		if( is_null($timeDiff) ) {
			return null;
		}
		
		$humanTime = $this->getResourceModel()->getDatetimeHelper()->getHumanTime($timeDiff, true, 'day');
		return $humanTime['day'];
	}
	
	/**
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getDefaultGame() {
		if( !$this->getDefaultGameId() ) {
			return null;
		}
		
		return Hub::getModel('competitions/game')->load($this->getDefaultGameId());
	}
}