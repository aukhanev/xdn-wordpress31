<?php

/**
 * Description of Game
 *
 * @version 0.1.1
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game extends Xedin_Competitions_Model_Meta_Abstract {
	
	
	const RESULT_MESSAGE_TYPE_SUCCESS = 'success';
	const RESULT_MESSAGE_TYPE_FAILURE = 'failure';
	
	/* @var Xedin_Competitions_Model_Game_Type_Abstract */
	protected $_gameHandler;
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/game')
				->_setMetaModelName('competitions/game_meta');
		
		$this->_setRelationModelUri('competitions/game_sector_relation');
	}
	
	/**
	 * @param Xedin_Competitions_Model_Participant|int|string $participantId
	 * @param Xedin_Competitions_Model_Game_Sector|int|string $sector 
	 */
	public function addScore($participantId, $sector = null, $score = null) {
		if( $participantId instanceof Xedin_Competitions_Model_Participant ) {
			$participantId = $participantId->getId();
		}
		
		if( !$this->canScore($participantId) ) {
			throw Hub::exception('Xedin_Competitions', 'Could not add score: participant #' . $participantId . ' cannot score any more points in game #' . $this->getId());
		}
		
		$scoreAmount = $this->getPointsPerScore();
		
		if( is_numeric($sector) ) {
			$sector = $this->getRelationModel()->load($sector);
		}
		
		if( !is_null($score) ) {
			$scoreAmount = $score;
		}
		elseif( $sector instanceof Xedin_Core_Model_Abstract ) {
			$scoreAmount = $sector->getValue();
		}
		
		$score = $this->getScoreModel()->reset()
				->setGameId($this->getId())
				->setParticipantId($participantId)
				->setValue($scoreAmount)
				->setSectorId($sector->getId())
				->save();
	}
	
	public function canScore($participantId) {
		
		if( $participantId instanceof Xedin_Competitions_Model_Participant ) {
			$participantId = $participantId->getId();
		}
		
		if( !$this->hasScorePerUser() ) {
			if( !$this->getId() ) {
				throw Hub::exception('Xedin_Competitions', 'Could not run ' . __METHOD__ . ' on a model with no ID set.');
			}
			$this->load($this->getId());
		}
		
		if( !$this->hasScorePerUser() ) {
			throw Hub::exception('Xedin_Competitions', 'Could not run ' . __METHOD__ . ' on a model with no Score Per User set.');
		}
		
		return !$this->isScoreLimitReached($participantId);
	}
	
	public function isScoreLimitReached($participant) {
		if( $participant instanceof Xedin_Competitions_Model_Participant ) {
			$participant = $participant->getId();
		}
		
		$scoreModel = $this->getScoreModel();
		$scoreCollection = $scoreModel->getCollection()->addFieldToFilter($scoreModel->getFieldNames('game_id'), $this->getId())
			->addFieldToFilter($scoreModel->getFieldNames('participant_id'), $participant)->load();
		$isScoreLimitReached = ( $this->getScorePerUser() && ( ($scoreCollection->count()+1) > $this->getScorePerUser() ));
		
		// Checking today's score
		$isScorePerDayLimitReached = false;
		if( $this->getScorePerDay() ) {
			$scoresToday = $scoreModel->getCollection()->addFieldToFilter('game_id', $this->getId())
					->addFieldToFilter('participant_id', $participant)
					->addDaysCountToSelect()
					->getFirstItem();
			$scoresToday = $scoresToday ? $scoresToday->getCount() : 0;
			$isScorePerDayLimitReached = ( ($scoresToday+1) > $this->getScorePerDay() );
			if( $this->getCode() == 'share' ) {
				$log = array();
				$log[] = __METHOD__ . ' has concluded the following:';
				$log[] = 'Game ID: ' . $this->getId();
				$log[] = 'Participant ID: ' . $participant;
				$log[] = 'Score Today: ' . $scoresToday;
				$log[] = 'Game Score Limit Reached: ' . ($isScoreLimitReached ? 'true' : 'false');
				$log[] = 'Score Per Day Limit Reached: ' . ($isScorePerDayLimitReached ? 'true' : 'false');
				
				Hub::log(implode("\n", $log));
			}
		}
		
		
		return $isScoreLimitReached || $isScorePerDayLimitReached;
	}
	
	public function isGameDependencySatisfied($participant) {
		if( $participant instanceof Xedin_Competitions_Model_Participant ) {
			$participant = $participant->getId();
		}
		
		$isActiveIfGameCannotScore = $this->getIsActiveIfGameCannotScore();
		
		$isActiveIfPageIsLiked = $this->getIsActiveIfPageIsLiked();
		if( !empty($isActiveIfPageIsLiked) ) {
			$fb = $this->getCurrentCompetition()->getFacebook();
			if( !$fb->isLiked() ) {
				return false;
			}
		}
		
		if( $this->getIsActiveIfParticipantCannotScore() ) {
			return true;
		}
		
		if( !empty($isActiveIfGameCannotScore) ) {
			$game = Hub::getModel('competitions/game')->load($isActiveIfGameCannotScore);
			/* @var $game Xedin_Competitions_Model_Game */
			if( $game->getId() && $game->canScore($participant) ) {
				return false;
			}
		}
		
		
		// Eventually...
		return true;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Score
	 */
	public function getScoreModel() {
		$score = Hub::getSingleton('competitions/score');
		/* @var $score Xedin_Competitions_Model_Score */
		if( $this->getId() ) {
			$score->setData($score->getFieldNames('game_id'), $this->getId());
		}
		return $score;
	}
	
	/**
	 * Get handler, if it is set.
	 * 
	 * @param sting|int $id
	 * @param null|mixed $default
	 * @return Xedin_Competitions_Model_Game_Type_Abstract
	 * @throws Xedin_Competitions_Exception
	 */
	public function getGameTypeHandler($id = null, $default = null) {
		if( !$this->_gameHandler ) {
			if( is_null($id) ) {
				$id = $this->getData('game_type');
			}

			if( empty($id) ) {
				return $default;
			}

			$type = $this->getGameTypeModel($id);
			if( !$type ) {
				return $default;
			}


			if( !$type->getUri() ) {
				throw Hub::exception('Xedin_Competitions', 'Game Type "' . $id . '" must have the "uri" field set.');
			}

			$this->_gameHandler = Hub::getModel($type->getUri())->setGame($this);
		}
		return $this->_gameHandler;
	}
	
	/**
	 * @param int|string $id
	 * @return Xedin_Competitions_Model_Game_Type
	 */
	public function getGameTypeModel($id = null) {
		$gameType = Hub::getSingleton('competitions/game_type');
		if( !is_null($id) ) {
			$gameType->load($id);
		}
		return $gameType;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Game_Collection
	 */
	public function getFrontentGamesCollection() {
		return $this->getCollection()->addFieldToFilter('is_visible_on_frontend', 1);
	}
	
	public function setResultMessage($text, $type = self::RESULT_MESSAGE_TYPE_SUCCESS) {
		$this->getSession()->setFlashVar($this->getMessageTypeName($type), $text);
		return $this;
	}
	
	public function getResultMessage($type) {
		return $this->getSession()->getFlashVar($this->getMessageTypeName($type));
	}
	
	public function getMessageTypeName($type) {
		return $this->getCode() . '-' . $type;
	}
	
	public function hasResultMessage($type) {
		return (bool)$this->getResultMessage($type);
	}
	
	public function getSuccessMessage() {
		return $this->getResultMessage(self::RESULT_MESSAGE_TYPE_SUCCESS);
	}
	
	public function getFailureMessage() {
		return $this->getResultMessage(self::RESULT_MESSAGE_TYPE_FAILURE);
	}
	
	public function hasSuccessMessage() {
		return (bool)$this->getSuccessMessage();
	}
	
	public function hasFailureMessage() {
		return (bool)$this->getFailureMessage();
	}
}