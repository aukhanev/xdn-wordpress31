<?php

/**
 * Description of Total
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Score_Total extends Xedin_Competitions_Model_Abstract {
	
	const FIELD_PARTICIPANT_ID = 'participant_id';
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/score_total');
	}
	
	public function loadByParticipant($participant) {
		if( $participant instanceof Xedin_Competitions_Model_Participant ) {
			$participant = $participant->getId();
		}
		
		$this->getResourceModel()->load($this, $participant, self::FIELD_PARTICIPANT_ID);
		return $this;
	}
}