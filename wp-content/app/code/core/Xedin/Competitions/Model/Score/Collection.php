<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Score_Collection extends Xedin_Core_Model_Db_Collection {
	
	protected $_addScoreToSelect;
	protected $_addPointTotalToSelect;
	protected $_addDaysCountToSelect;
	protected $_daysCountFieldName;
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/score');
	}
	
	public function addGameCodeFilter($code) {
		$gameTableName = $this->getGameModel()->getTableName();
		$gameIdFieldName = $this->getGameModel()->getIdFieldName();
		$scoreTableName = $this->getModel()->getTableName();
		$this->getSelect()->joinLeft($gameTableName, $scoreTableName . '.game_id = ' . $gameTableName . '.' . $gameIdFieldName)
			->where($gameTableName . '.code = ?', $code);
	}
	
	public function addScoreToSelect() {
		if( !$this->_addScoreToSelect ) {
			$this->_addScoreToSelect = true;
		}
		
		return $this;
	}
	
	protected function _beforeLoad($select) {
		$select = parent::_beforeLoad($select);
		
		$gameTableName = $this->getGameModel()->getTableName();
		if( $this->_addScoreToSelect ) {
//			$this->getSelect()->from($gameTableName, array('points' => new Zend_Db_Expr('SUM(' . $gameTableName . '.points_per_score)')) );
			$this->getSelect()->joinLeft($gameTableName, $this->getTableName() . '.game_id = ' . $gameTableName . '.' . $this->getGameModel()->getIdFieldName(), array())
					->columns(array($gameTableName . '.points_per_score'), $gameTableName );
		}
		
		if( $this->_addPointTotalToSelect )  {
			$this->getSelect()->columns( array('point_total' => new Zend_Db_Expr('SUM(' . $gameTableName . '.points_per_score)')), $gameTableName);
		}
		
		if( $this->_addDaysCountToSelect ) {
			$this->getSelect()
//					->reset(Zend_Db_Select::COLUMNS)
					->columns(array($this->_daysCountFieldName => new Zend_Db_Expr('COUNT(' . $this->getTableName() . '.participant_id)')), $this->getTableName())
					->group(array($this->getTableName() . '.participant_id', $this->getTableName() . '.game_id'))
					->where(new Zend_Db_Expr('DATE_FORMAT(' . $this->getTableName() . '.scored, \'%Y-%m-%d\') = \'' . $this->_addDaysCountToSelect . '\''));
		}
		
		return $this;
	}
	
	public function addPointTotalToSelect() {
		$this->_addPointTotalToSelect = true;
		return $this;
	}
	
	public function getGameModel() {
		return Hub::getSingleton('competitions/game');
	}
	
	public function getCompetitionModel() {
		return Hub::getSingleton('competitions/competition');
	}
	
	public function addDaysCountToSelect($day = null, $fieldName = 'count') {
		if( is_null($day) ) {
			$dateTime = new DateTime(); 
			$dateTime->setTimeZone(new DateTimeZone('UTC')); 
			$day = $dateTime->format("Y-m-d");
		}
		
		$this->_addDaysCountToSelect = $day;
		$this->_daysCountFieldName = $fieldName;
		return $this;
	}
	
}