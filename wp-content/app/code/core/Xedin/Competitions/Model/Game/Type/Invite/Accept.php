<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Invite_Accept extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	public function handleGame($participant = null) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		if( !$participant ) {
			return $this;
		}
		if( !$this->_canScore($participant) ) {
			return $this;
		}
		
		$this->getGame()->addScore($participant);		
		
		return $this;
	}
	
	protected function _canScore($participant) {
		 
		if( !parent::_canScore($participant) ) {
			return false;
		}
		
		if( !$this->isNonceCorrect($participant) ) {
			throw Hub::exception('Xedin_Competitions', 'Request key wrong, possibly expired. Please share within ' . $type->getPeriod() . ' seconds.');
		}
		
		return parent::_canScore($participant);
	}
	
	public function getRequestKey() {
		return $this->getRequest()->getPost('request_key');
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce
	 */
	public function getNonceModel() {
		return Hub::getSingleton('crypt/nonce');
	}
	
	public function getParticipantModel() {
		return Hub::getSingleton('competitions/participant');
	}
	
	public function getNonceTypeModel() {
		return Hub::getSingleton('crypt/nonce_type');
	}
}