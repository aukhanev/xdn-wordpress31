<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Quiz_Answer extends Xedin_Competitions_Model_Game_Type_Quiz_Abstract {
	
	protected $_wrongAnswerModel;
	
	public function handleGame($participant = null) {
		$answer = $this->getAnswer();
		if( !$answer || !$answer->getId() ) {
			return $this;
		}
		
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		if( !$participant ) {
			return $this;
		}
		
		if( $this->isAnswered() ) {
			return $this;
		}

		if( $answer->getIsCorrect() ) {
			$answeredInRow = (int)$participant->getQuestionsInRow();
			$answeredInRow++;
			$participant->setQuestionsInRow($answeredInRow);
			$questionHandler = $this->getGameModel('quiz-question')->getGameTypeHandler();
			if( $questionHandler ) {
				$questionHandler->handleGame($participant);
			}
		
			if( $this->_canScore($participant) ) {
				$this->getGame()->addScore($participant);
			}
			
			// Setting flashvar
			$this->getSession()->setFlashVar('quiz_answer_correct', $answeredInRow);
			$participant->save();
		}
		else {
			$this->handleWrongAnswer($participant);
		}
		
		
		if( !$answer->getId() ) {
			throw Hub::exception('Xedin_Competitions', 'Invalid answer.');
		}
		
		$this->getQuestion()->attempt($answer->getId(), $participant);
		
		return $this;
	}
	
	protected function _canScore($participant) {
		if( !$this->isNonceCorrect($participant) ) {
			throw Hub::exception('Xedin_Competitions', 'Request key wrong, possibly expired. Please share within ' . $type->getPeriod() . ' seconds.');
		}
		
		return parent::_canScore($participant);
	}
	
	public function handleWrongAnswer($participant) {
		$handler = $this->getWrongAnswerModel()->getGameTypeHandler();
		if( $handler ) {
			$handler->handleGame($participant);
		}
		
		return $this;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getWrongAnswerModel() {
		if( !$this->_wrongAnswerModel ) {
			$this->_wrongAnswerModel = Hub::getModel('competitions/game')->load('quiz-wrong-answer');
		}
		return $this->_wrongAnswerModel;
	}
}