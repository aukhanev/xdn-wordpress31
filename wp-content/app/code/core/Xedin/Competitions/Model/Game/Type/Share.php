<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Share extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	public function handleGame($participantId = null) {
		$fb = $this->getFacebook();
		if( is_null($participantId) ) {
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participantId = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId);
		}
		
		if( !$participantId ) {
			$this->setResultMessage('No participant found. Possibly, session has expired.', 'failure');
			return $this;
		}
		
		if( $this->_canScore($participantId) ) {
			$this->getGame()->addScore($participantId);
			$this->setResultMessage(array('Thank you, you have been awarded %u miles.', $this->getGame()->getPointsPerScore()));
		}
		else {
			if( $this->getFbResponse() ) {
				$this->setResultMessage('No more points can be scored with this game right now.', 'failure');
			}
		}
		
		return $this;
	}
	
	protected function _canScore($participant) {
		 
		$response = $this->getFbResponse();
		if( !$response ) {
			return false;
		}
		if( !isset($response['post_id']) ) {
			return false;
		}		
		
		if( !$this->isNonceCorrect($participant) ) {
			$type = $this->getNonceTypeModel()->load($this->getGame()->getNonceType());
			$this->setResultMessage(array('Request key wrong, possibly expired. Please share within %u seconds.', $type->getPeriod()), 'failure');
		}
		
		return parent::_canScore($participant);
	}
	
	public function getFbResponse() {
		return $this->getRequest()->getPost('fb_response');
	}
	
	public function getRequestKey() {
		return $this->getRequest()->getPost('request_key');
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce
	 */
	public function getNonceModel() {
		return Hub::getSingleton('crypt/nonce');
	}
	
	public function getParticipantModel() {
		return Hub::getSingleton('competitions/participant');
	}
	
	public function getNonceTypeModel() {
		return Hub::getSingleton('crypt/nonce_type');
	}
}