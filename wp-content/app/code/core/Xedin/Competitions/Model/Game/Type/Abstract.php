<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Competitions_Model_Game_Type_Abstract extends Xedin_Competitions_Model_Abstract {
	
	/** @var Xedin_Competitions_Model_Game */
	protected $_game;
	
	protected $_facebook;
	
	public function setGame($game) {
		if( is_object($game) && (is_string($game) || is_numeric($game)) ) {
			$game = $this->getGameModel($game);
		}
		$this->_game = $game;
		return $this;
	}
	
	public function getGame() {
		return $this->_game;
	}
	
	/**
	 * @param type $id
	 * @return Xedin_Competitions_Model_Game
	 */
	public function getGameModel($id = null) {
		$gameModel = Hub::getModel('competitions/game');
		if( !is_null($id) ) {
			$gameModel->load($id);
		}
		
		return $gameModel;
	}
	
	protected function _canScore($participant) {
		if( $participant instanceof Xedin_Competitions_Model_Participant ) {
			$participant = $participant->getId();
		}
		
		$game = $this->getGame();
		return $game && $game->canScore($participant);
	}
	
	
	public function handleGame($participantId = null) {
		$fb = $this->getFacebook();
		
		if( $participantId instanceof Xedin_Core_Model_Abstract ) {
			$participantId = $participantId->getId();
		}
		
		if( !($signedRequest = $fb->getSignedRequest()) ) {
			return $this;
		}
		
		if( !$fb->isLiked() ) {
			return $this;
		}
		
		if( is_null($participantId) ) {
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participantId = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId);
		}
		
		if( !$participantId ) {
			return $this;
		}
		
		if( $this->_canScore($participantId) ) {
			$this->getGame()->addScore($participantId);
		}
		
		return $this;
	}
	
	/**
	 * @param type $id
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function getParticipantModel($id = null) {
		$participant = Hub::getSingleton('competitions/participant');
		if( !is_null($id) ) {
			$participant->load($id);
		}
		
		return $participant;
	}
	
	/**
	 * @return Xedin_Facebook_Model_Facebook
	 */
	public function getFacebook() {
		return $this->getCurrentCompetition()->getFacebook();
	}
	
	public function getController() {
		return Hub::app()->getController();
	}
	
	public function getRequest() {
		return $this->getController()->getRequest();
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Competition
	 */
	public function getCompetitionHelper() {
		return Hub::getHelper('competitions/competition');
	}
	
	public function getCurrentCompetition() {
		return $this->getCompetitionHelper()->getCurrentCompetition();
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce
	 */
	public function getNonceModel() {
		return Hub::getSingleton('crypt/nonce');
	}
	
	public function isNonceCorrect($participant) {
		 		
		if( $this->getGame()->getNonceType() ) {
			if( !$this->getRequestKey() ) {
				return false;
			}
			
			
			if( $participant instanceof Xedin_Competitions_Model_Participant ) {
				$participantId = $participant->getId();
			}
			else {
				$participantId = $participant;
			}
			
			$type = $this->getNonceTypeModel()->load($this->getGame()->getNonceType());
			
			if( !$this->getNonceModel()->assertEqualNonce($this->getRequestKey(), $participantId, $type) ) {
				return false;
			}
		}
		
		return true;
	}
	
	public function getRequestKey() {
		return $this->getRequest()->getPost('request_key');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Participant
	 */
	public function getParticipantHelper() {
		return Hub::getHelper('competitions/participant');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function getCurrentParticipant() {
		return $this->getParticipantHelper()->getCurrentParticipant()->loadMeta();
	}
	
	public function getNonceTypeModel() {
		return Hub::getSingleton('crypt/nonce_type');
	}
	
	public function setResultMessage($text, $type = 'success') {
		$this->getGame()->setResultMessage($text, $type);
		return $this;
	}
	
	/**
	 * @return Xedin_Core_Helper_String_Numbers
	 */
	public function getNumbersHelper() {
		return Hub::getHelper('core/string_numbers');
	}
}