<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Bonus_Code extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	public function handleGame($participantId = null) {
		if( is_null($participantId) ) {
			$participantId = $this->getCurrentParticipant();
		}
		
		if( !$participantId ) {
			return $this;
		}
		
		
		$codeData = $this->getBonusCodeData();
		if( !$codeData || !isset($codeData['code']) ) {
			return false;
		}
		
		$code = $codeData['code'];
		
		if( !$this->_canScore($participantId) ) {
			$this->setResultMessage('Could not score any more points', 'failure');
			return $this;
		}
		
		$result = $this->getBonusCodeModel()->assertCodeCorrect($code);
		$this->getSession()->setFlashVar('bonus_code_result', $result);

		if( $result ) {
			$this->getGame()->addScore($participantId);
			$this->setResultMessage(array('Thank you. %u miles have been awarded.', $this->getGame()->getPointsPerScore()));
		}
		else {
			$this->setResultMessage('Incorrect code. Please try again.', 'failure');
		}
		
		return $this;
	}
	
	protected function _canScore($participant) {
		
		if( !$this->isNonceCorrect($participant) ) {
				$this->setResultMessage(array('Request key wrong, possibly expired. Please take action within %u seconds.', $type->getPeriod()));
		}
		
		return parent::_canScore($participant);
	}
	
	public function getBonusCodeData() {
		return $this->getRequest()->getPost('bonus_code');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Bonus_Code
	 */
	public function getBonusCodeModel() {
		return Hub::getSingleton('competitions/bonus_code');
	}
}