<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Quiz_Question extends Xedin_Competitions_Model_Game_Type_Quiz_Abstract {
	
	public function handleGame($participant = null) {
		$answer = $this->getAnswer();
		if( !$answer ) {
			return $this;
		}
		if( is_null($participant) ) {
			$fb = $this->getFacebook();
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participant = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId)->loadMeta();
		}
		
		if( !$participant ) {
			return $this;
		}

		$question = $this->getQuestion();
//		var_dump($answer->getIsCorrect());
		if( $answer->getIsCorrect() ) {
			$answeredInRow = (int)$participant->getQuestionsInRow();
//			var_dump($answeredInRow);
//			var_dump($question->getBonusSuccessionAmount());
			if( $question->getBonusSuccessionAmount() && $answeredInRow >= $question->getBonusSuccessionAmount() ) {
				if( $this->_canScore($participant) ) {
					$this->getGame()->addScore($participant);
				}
				$participant->setQuestionsInRow(0);
			}
		}
		
		return $this;
	}
	
	protected function _canScore($participant) {
		 		
		if( !$this->isNonceCorrect($participant) ) {
			throw Hub::exception('Xedin_Competitions', 'Request key wrong, possibly expired. Please share within ' . $type->getPeriod() . ' seconds.');
		}
		
		return parent::_canScore($participant);
	}
}