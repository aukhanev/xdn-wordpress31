<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Competitions_Model_Game_Type_Quiz_Abstract extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	/**
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function getParticipantModel() {
		return Hub::getSingleton('competitions/participant');
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce_Type
	 */
	public function getNonceTypeModel() {
		return Hub::getSingleton('crypt/nonce_type');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Question
	 */
	public function getQuestionModel($id = null) {
		$question = Hub::getSingleton('competitions/quiz_question');
		if( !is_null($id) ) {
			$question->load($id);
		}
		return $question;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Answer
	 */
	public function getAnswerModel($id = null) {
		$answer = Hub::getSingleton('competitions/quiz_answer');
		if( !is_null($id) ) {
			$answer->load($id);
		}
		return $answer;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Answer
	 */
	public function getAnswer() {
		$answerId = $this->getAnswerId();
		return $this->getAnswerModel($answerId);
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Quiz_Question
	 */
	public function getQuestionHelper() {
		return Hub::getHelper('competitions/quiz_question');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Quiz_Question
	 */
	public function getQuestion() {
		return $this->getQuestionHelper()->getRelevantQuestion();
	}
	
	public function isAnswered($participant = null)  {
		if( is_null($participant) ) {
			$participant = $this->getParticipantHelper()->getCurrentParticipant();
		}
		
		$attempt = $this->getQuestion()->getAttemptCollection($participant)->getFirstItem();
		return (bool)$attempt;
	}
	
	public function getAnswerId() {
		$answer = $this->getRequest()->getPost('answer');
		return $answer['id'];
	}
}