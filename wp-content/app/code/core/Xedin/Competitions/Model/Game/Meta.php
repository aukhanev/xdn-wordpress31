<?php

/**
 * Description of Meta
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Meta extends Xedin_Competitions_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/game_meta');
	}
}