<?php

/**
 * Description of Random
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Sector_Random extends Xedin_Competitions_Model_Game_Type_Sector_Abstract {
	
	protected $_selectedSector;
	protected $_pointsGained;
	
	public function getSelectedSector() {
		return $this->_selectedSector;
	}
	
	public function getPointsGained() {
		return $this->_pointsGained;
	}
	
	public function handleGame($participantId = null) {
		if( $participantId instanceof Xedin_Core_Model_Abstract ) {
			$participantId = $participantId->getId();
		}
		
		$fb = $this->getFacebook();
		
		if( !($signedRequest = $fb->getSignedRequest()) ) {
			return $this;
		}
		
		if( !$fb->isLiked() ) {
			return $this;
		}
		
		if( is_null($participantId) ) {
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participantId = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId);
		}
		
		if( !$participantId ) {
			return $this;
		}
		
		$childCollection = $this->getGame()->getChildren()->randomizeOrder()->setPerPage(1)->setPage(1);
		/* @var $childCollection Xedin_Core_Model_Db_Collection */
//		$profiler = $childCollection->getProfiler()->setEnabled(true);
//		if( $this->getCurrentParticipant()->getId() == 27 ) {
//			$randomSector = Hub::getSingleton('competitions/game_sector')->load(6)->loadMeta();
//		}
//		else {
			$randomSector = $this->getGame()->getChildren()->randomizeOrder()->setPerPage(1)->setPage(1)->getFirstItem()->loadMeta();
//		}
//		$randomSector = Hub::getModel('competitions/game_sector')->load(6);
		/* @var $randomSector Xedin_Competitions_Model_Game_Sector */
		$this->_selectedSector = $randomSector;
		$sectorCode = $randomSector->getSectorCode();
		
		if( !$this->getGame()->canScore($participantId) ) {
			$this->_pointsGained = 0;
		}
		elseif( empty($sectorCode) || $sectorCode == 'regular' ) {
			$this->_pointsGained = $randomSector->getValue();
		}
		elseif( $sectorCode == 'total-multiplier' ) {
			$scoreTotal = $this->getScoreTotalModel()->loadByParticipant($participantId)->getScore();
			$this->_pointsGained = (int)$scoreTotal;
		}
		
		$fb = $this->getFacebook();
		
		if( $participantId instanceof Xedin_Core_Model_Abstract ) {
			$participantId = $participantId->getId();
		}
		
		if( $this->_canScore($participantId) ) {
			$this->getGame()->addScore($participantId, $randomSector, $this->_pointsGained);
			$fb = $this->getFacebook();
			$accessToken = $fb->getApiModel()->getAccessToken();
			if( $accessToken ) {
				$args = array(
						'access_token'			=>	$accessToken,
						'name'					=>	'The Point - Fortune Wheel',
						'message'				=>	'I have just earned another ' . $this->_pointsGained . ' points!',
						'caption'				=>	'Play Fortune Wheel with The Point, and earn many great prizes!',
	//					'link'					=>	$fb->getAppModel()->getFbPageTabUrl(),
	//					'description'			=>	'Notification from The Point - Fortune Wheel',
						'picture'				=>	Hub::getUrl('images/post-image.png', Hub::URL_TYPE_SKIN)
				);
//				var_dump($accessToken);
//				var_dump($args);
//				var_dump('/' . $fb->getUser() . '/feed/');
				try {
					$return = $fb->api('/' . $fb->getUser() . '/feed/', 'POST', $args);
//					var_dump($return);
				}
				catch( FacebookApiException $e ) {
					throw Hub::exception('Xedin_Competitions', $e->getMessage());
				}
			}
		}
		
		return $this;
	}
}