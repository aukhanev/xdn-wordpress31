<?php

/**
 * Description of Generic
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Vote_Generic extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	
	public function handleGame($participantId = null) {
		
		if( is_null($participantId) ) {
			$participantId = $this->getParticipantHelper()->getCurrentParticipant();
			
			if( !$participantId ) {
				// Return silently
//				$this->setResultMessage('Could not handle request: no participant seems to have been created.', 'failure');
				return $this;
			}
		}
		
		if( !$participantId ) {
			$this->setResultMessage('No participant found. Possibly, session has expired.', 'failure');
			return $this;
		}
		
		$voteDetails = $this->getRequest()->getPost('vote');
		if( !$voteDetails ) {
			// Return silently
//			$this->setResultMessage('Vote details must be supplied to score with this game.', 'failure');
			return $this;
		}
		
		if( $this->_canScore($participantId) ) {
			$this->_addVote($voteDetails, $participantId);
			$this->getGame()->addScore($participantId);
			$this->setResultMessage(array('Thank you, your vote has been accepted.'));
		}
		else {
			if( $this->getFbResponse() ) {
				$this->setResultMessage('No more points can be scored with this game right now.', 'failure');
			}
		}
		
		return $this;
	}
	
	protected function _canScore($participant) {
		
		if( !$this->isNonceCorrect($participant) ) {
			$type = $this->getNonceTypeModel()->load($this->getGame()->getNonceType());
			$this->setResultMessage(array('Request key wrong, possibly expired. Please share within %u seconds.', $type->getPeriod()), 'failure');
		}
		
		return parent::_canScore($participant);
	}
	
	/**
	 * @param Varien_Object|array $voteDetails
	 * @param Xedin_Competitions_Model_Participant|int $participant
	 * @param Xedin_Competitions_Model_Poll|int $poll
	 * @param bool $reload
	 * @return type 
	 */
	protected function _addVote($voteDetails, $participant = null, $poll = null, $reload = false) {
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		if( $participant instanceof Xedin_Competitions_Model_Participant ) {
			$participant = $participant->getId();
		}
		
		if( is_null($poll) ) {
			$poll = $this->getCurrentCompetition()->getDefaultPoll();
		}
		
		if( !($poll instanceof Xedin_Competitions_Model_Poll) ) {
			$poll = $this->getPollModel($poll);
		}
		
		$attributeSetId = null;
		if( $poll instanceof Xedin_Competitions_Model_Poll ) {
			$attributeSetId = $poll->getVoteAttributeSet();
			$poll = $poll->getId();
		}
		
		$voteDetails['team_a_score'] = $this->_cleanNumber($voteDetails['team_a_score']);
		$voteDetails['team_b_score'] = $this->_cleanNumber($voteDetails['team_b_score']);
		
		$vote = $this->getVoteModel()
				->setData($voteDetails)
				->setParticipantId($participant)
				->setPollId($poll);
		
		if( $attributeSetId ) {
			$vote->setAttributeSetId($attributeSetId);
		}
		
		$vote->save();
		
		if( $reload ) {
			$vote->load();
		}
		
		return $vote;
	}
	
	protected function _cleanNumber($number, $int = true) {
		$number = preg_replace('[\d\s]', '', $number);
		
		return ($int ? intval($number) : floatval($number));
	}
	
	/**
	 * @param type $id
	 * @return Xedin_Competitions_Model_Poll_Vote
	 */
	public function getVoteModel($id = null) {
		$vote = Hub::getSingleton('competitions/poll_vote');
		if( !is_null($id) ) {
			$vote->load($id);
		}
		return $vote;
	}
	
	/**
	 * @param type $id
	 * @return Xedin_Competitions_Model_Poll
	 */
	public function getPollModel($id = null) {
		$vote = Hub::getSingleton('competitions/poll');
		if( !is_null($id) ) {
			$vote->load($id);
		}
		return $vote;
	}
}