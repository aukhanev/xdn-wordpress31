<?php

/**
 * Description of Game
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Sector extends Xedin_Competitions_Model_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/game_sector')
				->_setMetaModelName('competitions/game_sector_meta');
	}
}