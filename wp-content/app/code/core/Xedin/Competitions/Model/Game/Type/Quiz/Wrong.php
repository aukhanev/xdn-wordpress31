<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Quiz_Wrong extends Xedin_Competitions_Model_Game_Type_Quiz_Abstract {
	
	public function handleGame($participant = null) {
		$answer = $this->getAnswer();
		if( !$answer || !$answer->getId() ) {
			return $this;
		}
		
		if( is_null($participant) ) {
			$participant = $this->getCurrentParticipant();
		}
		
		if( !$participant ) {
			return $this;
		}
		
		if( $this->isAnswered() ) {
			return $this;
		}
		
		if( !$answer->getId() ) {
			throw Hub::exception('Xedin_Competitions', 'Invalid answer.');
		}

		if( !$answer->getIsCorrect() ) {
			$participant->setQuestionsInRow(0);
			$participant->save();
		}
		
		if( $this->_canScore($participant) ) {
			$this->getGame()->addScore($participant);
		}
		
		return $this;
	}
	
	protected function _canScore($participant) {
		if( !$this->isNonceCorrect($participant) ) {
			throw Hub::exception('Xedin_Competitions', 'Request key wrong, possibly expired. Please share within ' . $type->getPeriod() . ' seconds.');
		}
		
		return parent::_canScore($participant);
	}
}