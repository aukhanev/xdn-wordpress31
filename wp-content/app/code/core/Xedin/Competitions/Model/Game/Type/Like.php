 <?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Like extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	
	public function handleGame($participantId = null) {
		$fb = $this->getFacebook();
//		
		
		
		if( !($signedRequest = $fb->getSignedRequest()) ) {
			return $this;
		}
		
		if( !$fb->isLiked() ) {
			return $this;
		}
		
//		var_dump($signedRequest);
		
		if( is_null($participantId) ) {
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participantId = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId);
		}
		
		if( !$participantId ) {
			return $this;
		}
		
		if( $this->_canScore($participantId) ) {
			$this->getGame()->addScore($participantId);
		}
		
		return $this;
	}
}