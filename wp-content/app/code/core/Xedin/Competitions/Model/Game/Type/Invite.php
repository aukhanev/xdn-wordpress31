<?php

/**
 * Description of Like
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Type_Invite extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	public function handleGame($participant = null) {
		$fb = $this->getFacebook();
		if( is_null($participant) ) {
			if( !($fbUserId = $fb->getUser()) ) {
				return $this;
			}
			$participant = $this->getParticipantModel()->getByAttribute('facebook-id', $fbUserId);
		}
		
		if( !$participant ) {
			return $this;
		}
		
		if( !$this->_canScore($participant) ) {
			return $this;
		}
		
		$response = $this->getFbResponse();
		if( !$response
				||!isset($response['request'])
				|| !isset($response['to'])
				|| empty($response['to'])
				|| !is_array($response['to']) ) {
			return $this;
		}
		
		$fbApp = $fb->getAppModel();
		if( !$fbApp || !$fbApp->getId() ) {
			return $this;
		}
		
		$invitees = $response['to'];
		$existingInvites = $this->getInviteRequestModel()->getCollection()->addFieldToFilter('app_id', $fbApp->getId())
				->addFieldToFilter('facebook_id', $invitees, 'in')
				->addFieldToFilter('host_id', $participant->getId())
				->load()
				->getColumn('facebook_id');
		
		$invitedScore = 0;
		foreach( $invitees as $_idx => $_facebookId ) {
			if( in_array($_facebookId, $existingInvites) ){
				continue;
			}
			
			if( !$this->getGame()->canScore($participant) )  {
				$this->setResultMessage('No more scoring is allowed for this game.', 'failure');
				break;
			}
			
			$this->getInviteRequestModel()->reset()
					->setAppId($fbApp->getId())
					->setFacebookId($_facebookId)
					->setHostId($participant->getId())
					->setObjectId($response['request'])
					->save();
			$this->getGame()->addScore($participant);
			$invitedScore += $this->getGame()->getPointsPerScore();
		}
		
		if( $invitedScore ) {
			$this->setResultMessage(array('Thank you, you have been awarded %u %s.', $invitedScore, $this->getNumbersHelper()->getGrammaticalNumber($invitedScore, 'mile', 'miles')), 'failure');
		}
		
		
		return $this;
	}
	
	protected function _canScore($participant) {
		 
		if( !parent::_canScore($participant) ) {
			return false;
		}
		
		if( $this->getGame()->getNonceType() ) {
			if( !$this->getRequestKey() ) {
				return false;
			}
			
			
			if( $participant instanceof Xedin_Competitions_Model_Participant ) {
				$participantId = $participant->getId();
			}
			else {
				$participantId = $participant;
			}
			
			$type = $this->getNonceTypeModel()->load($this->getGame()->getNonceType());
			
			if( !$this->getNonceModel()->assertEqualNonce($this->getRequestKey(), $participantId, $type) ) {
				throw Hub::exception('Xedin_Competitions', 'Request key wrong, possibly expired. Please share within ' . $type->getPeriod() . ' seconds.');
			}
		}
		
		return parent::_canScore($participant);
	}
	
	public function getFbResponse() {
		return $this->getRequest()->getPost('fb_response');
	}
	
	public function getRequestKey() {
		return $this->getRequest()->getPost('request_key');
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce
	 */
	public function getNonceModel() {
		return Hub::getSingleton('crypt/nonce');
	}
	
	public function getParticipantModel() {
		return Hub::getSingleton('competitions/participant');
	}
	
	public function getNonceTypeModel() {
		return Hub::getSingleton('crypt/nonce_type');
	}
	
	public function getInviteRequestModel() {
		return Hub::getSingleton('facebook/invite_request');
	}
}