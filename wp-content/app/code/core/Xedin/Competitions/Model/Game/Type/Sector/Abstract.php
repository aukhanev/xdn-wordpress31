<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Competitions_Model_Game_Type_Sector_Abstract extends Xedin_Competitions_Model_Game_Type_Abstract {
	
	/**
	 * @param int $id
	 * @return Xedin_Competitions_Model_Score_Total
	 */
	public function getScoreTotalModel($id = null)  {
		$model = Hub::getSingleton('competitions/score_total');
		if( !empty($id) ) {
			$model->load($id);
		}
		
		return $model;
	}
}