<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_State_Collection extends Xedin_Core_Model_Config_Instance_Collection {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/game_state');
	}
}