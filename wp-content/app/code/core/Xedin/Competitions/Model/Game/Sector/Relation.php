<?php

/**
 * Description of Relation
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Game_Sector_Relation extends Xedin_Core_Model_Relation_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/game_sector_relation');
	}
}