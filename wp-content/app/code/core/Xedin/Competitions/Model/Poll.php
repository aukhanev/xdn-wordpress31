<?php

/**
 * Description of Poll
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Poll extends Xedin_Competitions_Model_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/poll')
				->_setMetaModelName('competitions/poll_meta');
	}
}