<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Leaderboard_Participant_Collection extends Xedin_Competitions_Model_Participant_Collection {
	
	protected $_selectHasRank;
	protected $_participantIdsInHaving = array();
	protected $_rankLimit;
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/participant_game_report');
		$this->addRankToSelect();
	}
	
	public function addRankToSelect() {
		if( !$this->_selectHasRank ) {
			$this->_selectHasRank = true;
		}
		
		return $this;
	}
	
	protected function _getLoadSelect() {
		$select = parent::_getLoadSelect();
		$expr = new Zend_Db_Expr(', (SELECT @rn:=0) t2');
		parent::_beforeLoad($select);
		
		$totalScoreModel = Hub::getSingleton('competitions/score_total');
		$totalsTableName = $totalScoreModel->getTableName();
		$nativeTableName = $this->getModel()->getTableName();
		$nativeTablePrimary = $this->getModel()->getIdFieldName();
		$innerViewTableName = 't1';
		$rankFieldName = 'rank';
		$totalScoresParticipantId = 'participant_id';
		$innerView = new Zend_Db_Expr('(SELECT `' . $totalsTableName . '`.*, 
@prev := @curr, 
@realrank := @realrank+1,
@curr := `' . $totalsTableName . '`.score, 
@rank := IF(@prev = @curr, @rank, @realrank) AS ' . $rankFieldName . ' 
FROM `' . $totalsTableName . '`, (SELECT @curr := null, @prev := null, @rank := 0, @realrank := 0) sel1 
ORDER BY `' . $totalsTableName . '`.score DESC)');
		
		if( $this->_selectHasRank ) {
			$this->_select = $this->getConnection()->select()->reset('columns')
					->reset('from')
					->from(array($innerViewTableName => $innerView), array(
						$rankFieldName => $rankFieldName,
						$totalScoresParticipantId => $totalScoresParticipantId,
						'point_total' => 'score'
					))
					->joinInner($nativeTableName, $nativeTableName . '.' . $nativeTablePrimary . ' = ' . $innerViewTableName . '.' . $totalScoresParticipantId)
					->order("convert(" . $innerViewTableName . '.' . $rankFieldName . ',signed) ASC');
			
			if( $this->_rankLimit ) {
					$this->_select->having($rankFieldName . ' <= ?', (int)$this->_rankLimit);
			}
			
			foreach( $this->_participantIdsInHaving as $_idx => $_participantId ) {
				$this->_select->orHaving($totalScoresParticipantId . ' = ?', $_participantId);
			}
			
//			if ( $this->_rankLimit ) {
//				$this->_select->limit($this->_rankLimit+1);
//			}
		}
		return $this->_select;
	}
	
//	protected function _getLoadSelect() {
//		$select = parent::_getLoadSelect();
//		$expr = new Zend_Db_Expr(', (SELECT @rn:=0) t2');
//		parent::_beforeLoad($select);
//		
//		$nativeTableName = $this->getModel()->getTableName();
//		$nativeTablePrimary = $this->getModel()->getIdFieldName();
//		$innerViewTableName = 't1';
//		$rankFieldName = 'rank';
//		$totalScoresParticipantId = 'participant_id';
//		$innerView = new Zend_Db_Expr('(SELECT `competitions_total_scores`.*, 
//@prev := @curr, 
//@realrank := @realrank+1,
//@curr := `competitions_total_scores`.score, 
//@rank := IF(@prev = @curr, @rank, @realrank) AS ' . $rankFieldName . ' 
//FROM `competitions_total_scores`, (SELECT @curr := null, @prev := null, @rank := 0, @realrank := 0) sel1 
//ORDER BY `competitions_total_scores`.score DESC)');
//		
//		if( $this->_selectHasRank ) {
//			$this->_select = $this->getConnection()->select()->reset('columns')
//					->reset('from')
//					->from(array($innerViewTableName => $innerView), array(
//						$rankFieldName => $rankFieldName,
//						$totalScoresParticipantId => $totalScoresParticipantId,
//						'point_total' => 'score'
//					))
//					->joinInner($nativeTableName, $nativeTableName . '.' . $nativeTablePrimary . ' = ' . $innerViewTableName . '.' . $totalScoresParticipantId)
//					->order("convert(" . $innerViewTableName . '.' . $rankFieldName . ',signed) ASC');
//			
//			if( $this->_rankLimit ) {
//					$this->_select->having($rankFieldName . ' <= ?', (int)$this->_rankLimit);
//			}
//			
//			foreach( $this->_participantIdsInHaving as $_idx => $_participantId ) {
//				$this->_select->orHaving($totalScoresParticipantId . ' = ?', $_participantId);
//			}
//			
//			if ( $this->_rankLimit ) {
//				$this->_select->limit($this->_rankLimit+1);
//			}
//		}
//		return $this->_select;
//	}
	
	public function getGameModel() {
		return Hub::getSingleton('competitions/game');
	}
	
	public function getScoreModel() {
		return Hub::getSingleton('competitions/score');
	}
	
	protected function _beforeLoad($select) {
		
		return $this;
	}
	
	public function addParticipantIdToFilter($participantId) {
		$this->_participantIdsInHaving[] = $participantId;
		return $this;
	}
	
	public function addRankLimit($limit) {
		$this->_rankLimit = $limit;
		return $this;
	}
}