<?php

/**
 * Description of Participant
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Leaderboard_Participant extends Xedin_Competitions_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/leaderboard_participant');
	}
}