<?php

/**
 * Description of Code
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Bonus_Code extends Xedin_Competitions_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/bonus_code');
	}
	
	public function assertCodeCorrect($code) {
		foreach( $this->getRelevantCodeCollection()->getItems() as $_idx => $_bonusCode ) {
			/* @var $_bonusCode Xedin_Competitions_Model_Bonus_Code */
			if( strtolower($code) == strtolower($_bonusCode->getCode()) ) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @return Xedin_Competitions_Model_Bonus_Code_Collection
	 */
	public function getRelevantCodeCollection() {
		$now = $this->getDate();
		return $this->getCollection()->addFieldToFilter('starts_at', $now, 'lteq')
			->addFieldToFilter('ends_at', $now, 'gteq');
	}
}