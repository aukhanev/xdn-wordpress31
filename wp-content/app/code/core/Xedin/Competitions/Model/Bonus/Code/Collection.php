<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Bonus_Code_Collection extends Xedin_Core_Model_Db_Collection {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/bonus_code');
	}
}