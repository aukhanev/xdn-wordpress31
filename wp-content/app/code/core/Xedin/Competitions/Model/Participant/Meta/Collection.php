<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Participant_Meta_Collection extends Xedin_Core_Model_Db_Collection {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/participant_meta');
	}
}