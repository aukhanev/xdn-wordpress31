<?php

/**
 * Description of Meta
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Participant_Meta extends Xedin_Competitions_Model_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/participant_meta');
	}
}