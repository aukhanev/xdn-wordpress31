<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Participant_Game_Report_Collection extends Xedin_Competitions_Model_Participant_Collection {
	
	protected $_selectHasRank;
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/participant_game_report');
		$this->addRankToSelect();
	}
	
	/**
	 * @return \Xedin_Competitions_Model_Participant_Game_Report_Collection 
	 */
	public function addPointTotalToSelect() {
		$gameTableName = $this->getGameModel()->getTableName();
		$scoreTableName = $this->getScoreModel()->getTableName();
		$scoreIdFieldName = $this->getScoreModel()->getIdFieldName();
		$participantTableName = $this->getModel()->getTableName();
		$participantIdFieldName = $this->getModel()->getIdFieldName();
//			$this->getSelect()->from($gameTableName, array('points' => new Zend_Db_Expr('SUM(' . $gameTableName . '.points_per_score)')) );
		$this->getSelect()
				->joinLeft($scoreTableName, $scoreTableName . '.participant_id = ' . $participantTableName . '.' . $participantIdFieldName, array())
				->joinLeft($gameTableName, $scoreTableName . '.game_id = ' . $gameTableName . '.' . $this->getGameModel()->getIdFieldName(), array())
				->group($participantTableName . '.' . $participantIdFieldName);
		
		$this->getSelect()->columns( array('point_total' => new Zend_Db_Expr('COALESCE(SUM(' . $gameTableName . '.points_per_score), 0)')), $gameTableName);
		return $this;
	}
	
	public function addRankToSelect() {
		if( !$this->_selectHasRank ) {
			$this->_selectHasRank = true;
		}
		
		return $this;
	}
	
	protected function _getLoadSelect() {
		$select = parent::_getLoadSelect();
		$this->addPointTotalToSelect()
				->addSortOrder('point_total', false);
		$expr = new Zend_Db_Expr(', (SELECT @rn:=0) t2');
		parent::_beforeLoad($select);
		if( $this->_selectHasRank ) {
			$this->_select = $this->getConnection()->select()->from(array('t1' => $select), array('rank' => new Zend_Db_Expr('@rn:=@rn+1')))
					->columns('*', 't1' )
					->assemble()
					. $expr;
		}
		return $this->_select;
	}
	
	public function addGameCodeFilter($code) {
		$gameTableName = $this->getGameModel()->getTableName();
		$gameIdFieldName = $this->getGameModel()->getIdFieldName();
		$scoreTableName = $this->getModel()->getTableName();
		$this->getSelect()->joinLeft($gameTableName, $scoreTableName . '.game_id = ' . $gameTableName . '.' . $gameIdFieldName)
			->where($gameTableName . '.code = ?', $code);
	}
	
	public function getGameModel() {
		return Hub::getSingleton('competitions/game');
	}
	
	public function getScoreModel() {
		return Hub::getSingleton('competitions/score');
	}
	
	protected function _beforeLoad($select) {
		
		return $this;
	}	
}