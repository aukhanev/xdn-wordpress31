<?php

/**
 * Description of Report
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Participant_Game_Report extends Xedin_Competitions_Model_Participant {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/participant_game_report');
	}	
}