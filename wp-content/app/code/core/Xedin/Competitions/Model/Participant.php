<?php

/**
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Participant extends Xedin_Competitions_Model_Meta_Abstract {
	
	static protected $_nonceKey;
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions/participant')
				->_setMetaModelName('competitions/participant_meta');
	}
	
	public function loadByReferenceKey($referenceKey) {
		$this->getResourceModel()->load($this, $referenceKey, 'reference_key');
		return $this;
	}
	
	public function getInviteesCollection() {
		return $this->getCollection()->addFieldToFilter('invitor_id', $this->getId());
	}
	
	public function getNonceKey($default = null) {
		if( !$this->getId() ) {
			return $default;
		}
		
		$game = $this->getGameHelper()->getCurrentGame();
		if( !$game ) {
			return $default;
		}
		
		if( !$game->getNonceType() ) {
			return $default;
		}
		
		$participantId = $this->getId();
		if( !self::$_nonceKey[$participantId] ) {
			self::$_nonceKey[$participantId] = $this->getNonceModel()->getNewNonce($participantId, $game->getNonceType());
		}
		
		return self::$_nonceKey[$participantId];
	}
	
	/**
	 * @return Xedin_Crypt_Model_Nonce
	 */
	public function getNonceModel() {
		return Hub::getSingleton('crypt/nonce');
	}
	
	/**
	 * @return Xedin_Competitions_Helper_Game
	 */
	public function getGameHelper() {
		return Hub::getHelper('competitions/game');
	}
}