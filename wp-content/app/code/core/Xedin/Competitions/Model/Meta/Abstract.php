<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Competitions_Model_Meta_Abstract extends Xedin_Core_Model_Meta_Abstract {
	
	/**
	 * @return Xedin_Competitions_Model_Session
	 */
	public function getSession() {
		return Hub::getSingleton('competitions/session');
	}
	
	/**
	 * @return Xedin_Competitions_Model_Competition
	 */
	public function getCurrentCompetition() {
		return Hub::getHelper('competitions/competition')->getCurrentCompetition();
	}
}