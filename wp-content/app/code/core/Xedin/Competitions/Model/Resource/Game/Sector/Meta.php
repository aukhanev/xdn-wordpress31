<?php

/**
 * Description of Meta
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Game_Sector_Meta extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_game_sector_meta');
	}
}