<?php

/**
 * Description of Competition
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Competition extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions_competitions')
				->_setMetaTableForeignKeyName('competition_id');
	}
}