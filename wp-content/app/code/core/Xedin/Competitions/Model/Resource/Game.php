<?php

/**
 * Description of Game
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Game extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_games')
				->_setMetaTableForeignKeyName('game_id');
	}
}