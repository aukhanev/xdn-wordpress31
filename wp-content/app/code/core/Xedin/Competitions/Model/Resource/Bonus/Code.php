<?php

/**
 * Description of Code
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Bonus_Code extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_bonus_code');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		$object = parent::_beforeSave($object);
		
		$startsAt = $object->getStartsAt();
		if( empty($startsAt) ) {
			$startsAt = time();
		}
		
		if( is_numeric($startsAt) ) {
			$startsAt = $this->getFormattedTimeStamp($startsAt);
			$this->setStartsAt($startsAt);
		}
		
		$endsAt = $this->getEndsAt();
		if( is_numeric($endsAt) ) {
			$endsAt = $this->getFormattedTimeStamp($endsAt);
			$this->setStartsAt($endsAt);
		}
		
		return $object;
	}
}