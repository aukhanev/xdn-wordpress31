<?php

/**
 * Description of Participant
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Participant extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('competitions_participants')
				->_setMetaTableForeignKeyName('participant_id');
	}
	
	protected function _beforeSave(Xedin_Core_Model_Meta_Abstract $model) {
		parent::_beforeSave($model);
		$registered = trim($model->getData('registered'));
		if( empty($registered) ) {
			$model->setData('registered', $this->getFormattedTimeStamp());
		}
		
		$username = trim($model->getData('username'));
		if( empty($username) ) {
			$model->setData('username', uniqid('participant-'));
		}
		
		$referenceKey = $model->getReferenceKey();
		if( empty($referenceKey) ) {
			$referenceKey = $this->generateReferenceKey();
			if( $referenceKey ) {
				$model->setReferenceKey($referenceKey);
			}
		}
		
		return $model;
	}
	
	public function generateReferenceKey() {
		$referenceKey = null;
		$participant = $this->getNewParticipantModel();
		do {
			$referenceKey = uniqid();
			$existingRow = $participant->loadByReferenceKey($referenceKey);
		}
		while( $existingRow->getId() );
		return $referenceKey;
	}
	
	/**
	 *
	 * @param type $id
	 * @return Xedin_Competitions_Model_Participant
	 */
	public function getNewParticipantModel($id = null) {
		$participant = Hub::getModel('competitions/participant');
		if( !is_null($id) ) {
			$participant->load($id);
		}
		return $participant;
	}
}