<?php

/**
 * Description of Participant
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Leaderboard_Participant extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_total_scores', 'participant_id');
	}
}