<?php

/**
 * Description of Relation
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Game_Sector_Relation extends Xedin_Core_Model_Resource_Relation_Abstract {
	
	protected function _construct() {
		$this->setParentIdFieldName('game_id')
				->_setParentUri('competitions/game')
				->setChildIdFieldName('sector_id')
				->_setChildUri('competitions/game_sector')
				->_init('competitions_game_game_sector');
	}
}