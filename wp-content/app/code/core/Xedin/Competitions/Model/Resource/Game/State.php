<?php

/**
 * Description of State
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Game_State extends Xedin_Core_Model_Resource_Config_Instance_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('modules/Xedin_Competitions/render/game_state');
	}
}