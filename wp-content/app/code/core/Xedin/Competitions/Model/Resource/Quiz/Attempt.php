<?php

/**
 * Description of Attempt
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Quiz_Attempt extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_quiz_attempt');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$attemptedAt = $object->getAttemptedAt();
		if( empty($attemptedAt) ) {
			$attemptedAt = $this->getFormattedTimeStamp();
			$object->setAttemptedAt($attemptedAt);
		}
		
		return $object;
	}
}