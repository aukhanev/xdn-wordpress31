<?php

/**
 * Description of Vote
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Poll_Vote extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_poll_vote')
				->_setMetaTableForeignKeyName('vote_id');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$scored = $object->getData('created_at');
		if( empty($scored) ) {
			$object->setData('created_at', $this->getFormattedTimeStamp());
		}
		
		return $object;
	}
}