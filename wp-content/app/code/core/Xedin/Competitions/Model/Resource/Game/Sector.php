<?php

/**
 * Description of Game
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Game_Sector extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_game_sector')
				->_setMetaTableForeignKeyName('sector_id');
	}
	
	protected function _beforeSave(Xedin_Core_Model_Meta_Abstract $model) {
		parent::_beforeSave($model);
		
		$createdAt = $model->getCreatedAt();
		if( empty($createdAt) ) {
			$model->setCreatedAt($this->getFormattedTimeStamp());
		}
		
		return $model;
	}
}