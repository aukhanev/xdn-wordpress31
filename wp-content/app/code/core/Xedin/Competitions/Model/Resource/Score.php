<?php

/**
 * Description of Score
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Resource_Score extends Xedin_Core_Model_Resource_Mysql {
	
	protected $_fieldNames = array(
		'game_id'			=>	'game_id',
		'participant_id'	=>	'participant_id'
	);
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions_scores');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$scored = $object->getData('scored');
		if( empty($scored) ) {
			$object->setData('scored', $this->getFormattedTimeStamp());
		}
		
		return $object;
	}
}