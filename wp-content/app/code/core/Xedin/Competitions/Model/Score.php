<?php

/**
 * Description of Score
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Score extends Xedin_Competitions_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/score');
	}
	
	public function getTotal($participantId = null, $competitionCode = null) {
		return $this->getExtendedCollection($participantId, $competitionCode)->addPointTotalToSelect()
				->getFirstItem()
				->getPointTotal();
	}
	
	public function getExtendedCollection($participantId = null, $gameCode = null) {
		$collection = $this->getCollection();
		
		if( $participantId instanceof Xedin_Core_Model_Abstract ) {
			$participantId = $participantId->getId();
		}
		if( !is_null($participantId) ) {
			$collection->addFieldToFilter('participant_id', $participantId);
		}
		
		if( !is_null($gameCode) ) {
			if( !is_numeric($gameCode) ) {
				$collection->addGameCodeFilter($gameCode);
			}
			else {
				$collection->addFieldToFilter('competition_id', $gameCode);
			}
		}
		
		$collection->addScoreToSelect();
		
		return $collection;
	}
}