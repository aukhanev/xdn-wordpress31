<?php

/**
 * Description of Vote
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Competitions_Model_Poll_Vote extends Xedin_Competitions_Model_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('competitions/poll_vote')
				->_setMetaModelName('competitions/poll_vote_meta');
	}
}