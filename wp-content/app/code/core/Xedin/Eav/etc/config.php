<?php

return array(
	'modules'			=> array(
		'Xedin_Eav'			=>	array(
			'render'			=>	array(
				'values'			=>	array(
					'2'					=> array(
						'uri'				=>	'adminhtml/render_fields_generic',
						'label'				=>	'Text Field'
					),
					'1'					=> array(
						'uri'				=>	'eav/render_fields_admin_field_type',
						'label'				=>	'Admin Field Type'
					)
				)
			)
		)
	)
)
?>
