<?php

return array(
	'system' => array(
		'modules' => array(
			'eav' => array(
				'name' => 'eav',
				'label' => 'EAV',
				'models' => array(
					'attribute' => array(
						'path'			=>	'eav/attribute',
						'label'			=>	'Attributes',
						'list_block'	=>	'eav/admin_grid_list_attribute',
						'details_block'	=>	'eav/admin_grid_details_attribute'
					),
					'attributeSet' => array(
						'path'			=>	'eav/attribute_set',
						'label'			=>	'Attribute Sets',
//						'list_block'	=>	'eav/admin_grid_list_attribute',
						'details_block'	=>	'eav/admin_grid_details_attribute_set'
					)
				)
			)
		)
	)
)
		
?>