<?php
/**
 * Description of Config
 *
 * @author anton
 */
class Xedin_Eav_Model_Config extends Xedin_Core_Model_Config_Abstract {
	
	protected $_renderValues;
	
	const XML_PATH_MODULES_EAV = 'modules/Xedin_Eav';
	
	public function getRenderValuesConfig($index = null, $default = null) {
		if( empty($this->_renderValues) ) {
			$this->_renderValues = $this->getValue( $this->_getNormalizedPath(self::XML_PATH_MODULES_EAV, null, array('render', 'values')) );
		}
		
		if( is_null($index) ) {
			return $this->_renderValues;
		}
		
		if( !$this->_renderValues[$index] ) {
			return $default;
		}
		
		return $this->_renderValues[$index];
	}
}