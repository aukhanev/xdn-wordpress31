<?php
/**
 * Description of Values
 *
 * @author anton
 */
class Xedin_Eav_Model_Resource_Render_Values extends Xedin_Core_Model_Resource_Config_Instance_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('modules/Xedin_Eav/render/values');
	}
}