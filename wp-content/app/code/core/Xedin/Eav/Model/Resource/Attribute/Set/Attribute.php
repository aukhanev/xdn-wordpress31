<?php

/**
 * Description of Attribute
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Resource_Attribute_Set_Attribute extends Xedin_Eav_Model_Resource_Mysql_Abstract {
	
	protected $_attributeIdFieldName = 'attribute_id';
	protected $_attributeSetIdFieldName = 'attribute_set_id';
	
	protected function _construct() {
		parent::_construct();
		$this->_init('eav_attribute_set_attributes');
	}
	
	public function getAttributeIdFieldName() {
		return $this->_attributeIdFieldName;
	}
	
	public function getAttributeSetIdFieldName() {
		return $this->_attributeSetIdFieldName;
	}
}