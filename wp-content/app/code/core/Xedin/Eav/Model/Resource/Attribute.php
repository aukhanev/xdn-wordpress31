<?php

/**
 * Description of Attribute
 *
 * @author Xedin Unknown
 */
class Xedin_Eav_Model_Resource_Attribute extends Xedin_Eav_Model_Resource_Mysql_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav_attributes');
	}
	
	public function loadByCode(Xedin_Core_Model_Abstract $object, $value) {
		$this->load($object, $value, 'code');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$code = trim($object->getData('code'));
		if( empty($code) ) {
			$object->setData('code', $this->getCodeFromLabel($object));
		}
		return $object;
	}
	
	public function getCodeFromLabel($label) {
		if( $label instanceof Varien_Object ) {
			$label = $label->getData('label');
		}
		
		$label = strtolower(preg_replace('!(\s)+!', '-', $label));
		return $label;
	}
}