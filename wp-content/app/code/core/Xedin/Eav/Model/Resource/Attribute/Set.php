<?php

/**
 * Description of Set
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Resource_Attribute_Set extends Xedin_Eav_Model_Resource_Mysql_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('eav_attribute_sets');
	}
	
	protected function _afterSave(Varien_Object $object) {
		if( ($object instanceof Xedin_Eav_Model_Attribute_Set) ) {
			$this->saveAttributeIds($object);
		}
		
		$object = parent::_afterSave($object);
		
		return $object;
	}
	
	public function saveAttributeIds(Xedin_Eav_Model_Attribute_Set $attributeSet) {
		if( !$attributeSet->hasId() ) {
			return $this;
		}
		
		$attributeSetAttribute = $attributeSet->getAttributeSetAttributeModel();
		$attributeIdFieldName = $attributeSetAttribute->getAttributeIdFieldName();
		$attributeSetIdFieldName = $attributeSetAttribute->getAttributeSetIdFieldName();
		
		$attributes = $attributeSet->getData( $attributeIdFieldName );
		$attributeSetAttributeCollection = $attributeSetAttribute->getCollection()
				->reset()
				->addFieldToFilter($attributeSetIdFieldName, $attributeSet->getId())
				->delete();
		if( empty($attributes) ) {
			return $this;
		}
		
		foreach( $attributes as $_idx => $_attributeId ) {
			$attributeSetAttribute->unsetData($attributeSetAttribute->getIdFieldName())
					->setData($attributeIdFieldName, $_attributeId)
					->setData($attributeSetIdFieldName, $attributeSet->getId())
					->save();
		}
		
		return $this;
	}
}