<?php
/**
 * Description of Type
 *
 * @author anton
 */
class Xedin_Eav_Model_Render_Values_Admin_Field_Attribute_Set extends Xedin_Eav_Model_Render_Values_Abstract {
	
	protected $_fieldModelUri = 'eav/attribute_set';
		
	protected function _render($value) {
		$this->setValue($this->getFieldModel($value)->getData($value . '/label'));
		return $this->getValue();
	}
}