<?php
/**
 * Description of Values
 *
 * @author anton
 */
class Xedin_Eav_Model_Render_Values extends Xedin_Eav_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/render_values');
	}
}