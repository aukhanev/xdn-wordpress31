<?php
/**
 * Description of Type
 *
 * @author anton
 */
class Xedin_Eav_Model_Render_Values_Admin_Field_Type extends Xedin_Eav_Model_Render_Values_Abstract {
	
	protected $_fieldModelUri = 'eav/render_values';
		
	protected function _render($value) {
//		var_dump($this->getFieldModel($value));
		$this->setValue($this->getFieldModel($value)->getData($value . '/label'));
		return $this->getValue();
	}
}