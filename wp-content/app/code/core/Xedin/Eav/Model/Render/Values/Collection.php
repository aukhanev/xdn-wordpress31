<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Render_Values_Collection extends Xedin_Core_Model_Config_Instance_Collection {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/render_values');
	}
}