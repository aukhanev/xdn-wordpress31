<?php

/**
 * Description of Type
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Render_Fields_Admin_Field_Type extends Xedin_Eav_Model_Render_Fields_Select_Abstract {
	
	protected $_blockUri = 'html/element_input_select';
	protected $_modelUri = 'eav/render_values';

}