<?php

/**
 * Description of Set
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Attribute_Set extends Xedin_Eav_Model_Abstract {
	
	protected $_attributeSetAttributeModelUri = 'eav/attribute_set_attribute';
	protected $_attributeModelUri = 'eav/attribute';
	
	protected $_attributeSetAttributeCollection;
	protected $_attributeCollection;
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/attribute_set');
	}
	
	/* Attribute Set Attributes ============================================= */
	
	public function getAttributeSetAttributesCollection() {
		if( !$this->_attributeSetAttributeCollection ) {
			$this->_attributeSetAttributeCollection = $this->getAttributeSetAttributeModel()->getCollection();
			$id = (int)$this->getId();
			$this->_attributeSetAttributeCollection->addAttributeSetIdFilter($id);
		}
		return $this->_attributeSetAttributeCollection;
	}
	
	/**
	 * @return Xedin_Eav_Model_Attribute_Set_Attribute
	 */
	public function getAttributeSetAttributeModel() {
		return Hub::getSingleton($this->getAttributeSetAttributeModelUri());
	}
	
	public function getAttributeSetAttributeModelUri() {
		return $this->_attributeSetAttributeModelUri;
	}
	
	/* Attributes =========================================================== */
	
	public function getAttributeModelUri() {
		return $this->_attributeModelUri;
	}
	
	
	/**
	 *
	 * @return Xedin_Eav_Model_Attribute
	 */
	public function getAttributeModel() {
		return Hub::getSingleton($this->getAttributeModelUri());
	}
	
	public function getAttributeCollection() {
		if( !$this->_attributeCollection ) {
			$this->_attributeCollection = $this->getAttributeModel()->getCollection()->resetFieldsToSelect();
			/* @var $attributeSetAttributeCollection Xedin_Eav_Model_Attribute_Set_Attribute_Collection */
			$attributeSetAttributeCollection = $this->getAttributeSetAttributesCollection()->load();
			$attributeIdFieldName = $attributeSetAttributeCollection->getModel()->getAttributeIdFieldName();
			$this->_attributeCollection->addIdToFilter($attributeSetAttributeCollection->getColumn($attributeIdFieldName));
		}
		return $this->_attributeCollection;
	}
}