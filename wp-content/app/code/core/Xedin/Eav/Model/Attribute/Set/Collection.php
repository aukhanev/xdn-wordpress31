<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Attribute_Set_Collection extends Xedin_Eav_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/attribute_set');
	}
}