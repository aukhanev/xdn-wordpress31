<?php

/**
 * Description of Collection
 *
 * @author anton
 */
class Xedin_Eav_Model_Attribute_Collection extends Xedin_Eav_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/attribute');
	}
}
