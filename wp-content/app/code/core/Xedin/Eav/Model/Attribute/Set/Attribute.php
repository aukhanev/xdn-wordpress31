<?php

/**
 * Description of Attribute
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Model_Attribute_Set_Attribute extends Xedin_Eav_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('eav/attribute_set_attribute');
	}
	
	public function getAttributeIdFieldName() {
		return $this->getResourceModel()->getAttributeIdFieldName();
	}
	
	public function getAttributeSetIdFieldName() {
		return $this->getResourceModel()->getAttributeSetIdFieldName();
	}
}