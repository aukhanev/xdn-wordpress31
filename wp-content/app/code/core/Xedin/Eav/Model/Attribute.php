<?php
/**
 * Description of Attribute
 *
 * @author anton
 */
class Xedin_Eav_Model_Attribute extends Xedin_Eav_Model_Abstract {
	
	protected $_adminFieldTypeModelUrl = 'eav/render_values';
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('eav/attribute');
	}
	
	public function load($id = null) {
		if( !is_null($id) && !is_numeric($id) )  {
			$this->loadByCode($id);
			return $this;
		}
		
		parent::load($id);
		return $this;
	}
	
	public function loadByCode($code = null) {
		if( is_null($code) ) {
			$code = $this->getData('code');
		}
		
		$this->getResourceModel()->loadByCode($this, $code);
		return $this;
	}
	
	public function getAdminFieldTypeModel() {
		$model = Hub::getSingleton($this->getAdminFieldTypeModelUri());
		if( $this->hasData('admin_field_type') ) {
			$model->load($this->getData('admin_field_type'));
		}
		return $model;
	}
	
	public function getAdminFieldTypeModelUri() {
		return $this->_adminFieldTypeModelUrl;
	}
}