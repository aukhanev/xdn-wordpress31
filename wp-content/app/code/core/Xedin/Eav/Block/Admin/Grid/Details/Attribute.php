<?php
/**
 * Description of Attribute
 *
 * @author anton
 */
class Xedin_Eav_Block_Admin_Grid_Details_Attribute extends Xedin_Adminhtml_Block_Grid_Details {
	
	protected $_renderValuesModelUri = 'eav/render_values';
	protected $_renderValuesModel;
	
	public function getRenderValuesModelUri() {
		return $this->_renderValuesModelUri;
	}
	
	public function setRenderValuesModelUri($modelUri) {
		$this->_renderValuesModelUri = $modelUri;
		return $this;
	}
	
	/**
	 */
	public function getRenderValuesModel() {
		if( !$this->_renderValuesModel ) {
			$this->_renderValuesModel = Hub::getSingleton($this->getRenderValuesModelUri());
		}
		return $this->_renderValuesModel;
	}
	
	protected function _afterGenerateFields() {
		parent::_beforeToHtml();
		$this->setField(array(
			self::COLUMN_INFO_INDEX_FIELD					=>	'admin_field_type',
			self::COLUMN_INFO_INDEX_LABEL					=>	'Admin Field Type',
			self::COLUMN_INFO_INDEX_RENDER_MODEL			=>	'eav/render_fields_admin_field_type'
		));
	}
}