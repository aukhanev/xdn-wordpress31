<?php

/**
 * Description of Set
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Block_Admin_Grid_Details_Attribute_Set extends Xedin_Adminhtml_Block_Grid_Details {
	
	protected $_attributesBlockUri = 'eav/admin_grid_details_attribute_set_attributes';
	protected $_attributesBlockName = 'attribute.set.attributes';
	protected $_attributesBlock;
	
	protected function _afterCreation() {
		parent::_afterCreation();
		
		$this->getAttributesBlock();
//		$this->getLayout()->getBlock('main')->getSortedChildren();
	}
	
	public function setAttributesBlockUri($blockUri) {
		$this->_attributesBlockUri = $blockUri;
		return $this;
	}
	
	public function getAttributesBlockUri() {
		return $this->_attributesBlockUri;
	}
	
	public function getAttributesBlock() {
		if( !$this->hasAttributesBlock() ) {
			$params = array(
				'after'			=>	'-',
			);
			
//			var_dump($this->getNameInLayout());

			if( $this->hasAttributesBlockName() ) {
				$params['name'] = $this->getAttributesBlockName();
			}
			
			$this->_setAttributesBlock( $this->getLayout()->createBlock( $this->getAttributesBlockUri(), $this->getNameInLayout(),  $params) );
			$this->setAttributesBlockName($this->_attributesBlock->getNameInLayout());
			
		}
		
		return $this->_attributesBlock;
	}
	
	protected function _setAttributesBlock(Xedin_Core_Block_Template $block) {
		$this->_attributesBlock = $block;
	}
	
	public function hasAttributesBlock() {
		return ($this->_attributesBlock instanceof Xedin_Core_Block_Template);
	}
	
	public function getAttributesBlockName() {
		return $this->_attributesBlockName;
	}
	
	public function setAttributesBlockName($blockName) {
		$this->_attributesBlockName = $blockName;
		return $this;
	}
	
	public function hasAttributesBlockName() {
		return !empty($this->_attributesBlockName);
	}
}