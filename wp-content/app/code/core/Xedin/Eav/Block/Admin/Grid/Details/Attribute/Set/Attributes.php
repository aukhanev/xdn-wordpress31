<?php

/**
 * Description of Attributes
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Block_Admin_Grid_Details_Attribute_Set_Attributes extends Xedin_Adminhtml_Block_Grid_View_Abstract {
	
	protected $_allAttributesCollection;
	protected $_currentAttributes;
	
	protected $_attributeModelUri = 'eav/attribute';
	
	protected $_attributesSelect;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('adminhtml/grid/details/attribute.set.attributes.php');
	}
	
	public function getAllAttributesCollection() {
		if( is_null($this->_allAttributesCollection) ) {
			$this->_allAttributesCollection = $this->getAttributeModel()->getCollection();
		}
		
		return $this->_allAttributesCollection;
	}
	
	public function getAttributeModelUri() {
		return $this->_attributeModelUri;
	}
	
	public function getAttributeModel() {
		return Hub::getSingleton( $this->getAttributeModelUri() );
	}
	
	/**
	 * 
	 */
	public function getAttributesSelect() {
		if( !$this->_attributesSelect ) {
			$currentObject = $this->getCurrentObject();
			$this->_attributesSelect = $this->getLayout()->createBlock('html/element_input_select')->setMultiple(true);
			
			if( !($currentObject instanceof Xedin_Eav_Model_Attribute_Set) ) {
				return $this->_attributesSelect;
			}
			
			/* @var $currentObject Xedin_Eav_Model_Attribute_Set */
			$attributeFieldName = $currentObject->getAttributeSetAttributeModel()->getAttributeIdFieldName();
			$selectName = $this->getFieldName($attributeFieldName);
			$selectId = $this->getFieldId($attributeFieldName);
			$this->_attributesSelect->setId($selectId)->setName($selectName);
			
			$this->_attributesSelect->setOptions($this->getAllAttributesCollection()->getItems(), $currentObject->getAttributeCollection()->load()->getIds());
		}
		
		return $this->_attributesSelect;
	}
}