<?php

/**
 * Description of Meta
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Block_Admin_Grid_Details_Meta extends Xedin_Adminhtml_Block_Grid_Details {
	
	protected $_attributeSetModel;
	
	protected function _afterGenerateFields() {
		parent::_afterGenerateFields();
		$model = $this->getCurrentObject();
		if( !($model instanceof Xedin_Core_Model_Meta_Abstract) ) {
			return $this;
		}
		
		if( $model->getId() ) {		
			/* @var $model Xedin_Core_Model_Meta_Abstract */
			$model->loadMeta();

			$setAttributesCollection = $this->getAttributeSetModel()
					->getAttributeCollection()
					->load();
			$setAttributeIds = $setAttributesCollection->getIds();

			foreach( $setAttributesCollection->getItems() as $_id => $_attribute ) {
				/* @var $_attribute Xedin_Eav_Model_Attribute */

				if(in_array($_attribute->getId(), $setAttributeIds) ) {
					$this->addField(array(
						self::COLUMN_INFO_INDEX_NAME			=>	$_attribute->getCode(),
						self::COLUMN_INFO_INDEX_FIELD			=>	$_attribute->getCode(),
						self::COLUMN_INFO_INDEX_LABEL			=>	$_attribute->getLabel(),
						self::COLUMN_INFO_INDEX_RENDER_MODEL	=>	$_attribute->getAdminFieldTypeModel()->getUri()
					));
				}
			}
		}
		
		$this->setField(array(
			self::COLUMN_INFO_INDEX_NAME			=>	$model->getAttributeSetIdFieldName(),
			self::COLUMN_INFO_INDEX_FIELD			=>	$model->getAttributeSetIdFieldName(),
			self::COLUMN_INFO_INDEX_LABEL			=>	'Attribute Set',
			self::COLUMN_INFO_INDEX_RENDER_MODEL	=>	'eav/render_fields_admin_attribute_set',
			self::COLUMN_INFO_INDEX_SORT_POSITION	=>	0
		));
	}
	
	public function getAttributeSetModel() {
		if( !$this->_attributeSetModel ) {
		
			$model = $this->getCurrentObject();

			$attributeSetModelUri = 'eav/attribute_set';
			if( $model instanceof Xedin_Core_Model_Meta_Abstract ) {
				$attributeSetModelUri = $model->getAttributeSetModelUri();
			}

			$this->_attributeSetModel = Hub::getModel($attributeSetModelUri);
			
			if( !$model->getId() ) {
				return $this->_attributeSetModel;
			}
			
			/* @var $model Xedin_Core_Model_Meta_Abstract */
			$this->_attributeSetModel->unsetData()
					->setId($model->getData($model->getAttributeSetIdFieldName()))
					->load();
		}
		
		return $this->_attributeSetModel;
	}
	
	public function getModelName() {
		$modelName = $this->getModelUri();
		$attributeSetModel = $this->getAttributeSetModel();
		
		if( $attributeSetModel->hasData('label') ) {
			$modelName .= ' [' . $attributeSetModel->getLabel() . ']';
		}
		
		return $modelName;
	}
	
	/**
	 *
	 * @return Xedin_Eav_Model_Attribute
	 */
	public function getAttributeModel() {
		return Hub::getSingleton('eav/attribute');
	}
}