<?php

/**
 * Description of Meta
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Eav_Block_Admin_Grid_List_Meta extends Xedin_Adminhtml_Block_Grid_List {
	
	protected function _afterGenerateFields() {
		parent::_afterGenerateFields();
		$model = $this->getCurrentObject();
		if( !($model instanceof Xedin_Core_Model_Meta_Abstract) ) {
			return $this;
		}
		
		$this->setField(array(
			self::COLUMN_INFO_INDEX_NAME			=>	$model->getAttributeSetIdFieldName(),
			self::COLUMN_INFO_INDEX_FIELD			=>	$model->getAttributeSetIdFieldName(),
			self::COLUMN_INFO_INDEX_LABEL			=>	'Attribute Set',
			self::COLUMN_INFO_INDEX_RENDER_MODEL	=>	'eav/render_fields_admin_attribute_set'
		));
	}
}