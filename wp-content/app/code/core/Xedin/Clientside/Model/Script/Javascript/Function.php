<?php

class Xedin_Clientside_Model_Script_Javascript_Function extends Xedin_Clientside_Model_Script_Javascript {
    
    protected $_params;
    
    public function __construct( $inner = null, $params = array()) {
        if( $inner instanceof Xedin_Clientside_Model_Script_Expression ) {
            return parent::__construct($inner . '');
        }
        elseif( is_string($inner) ) {
            return parent::__construct($inner);
        }
    }
    
    public function __toString() {
        return 'function ' . $this->getArgsList($this->getParams()) . ' {' . $this->getLineTerminator() . $this->getInnerExpression() . $this->getLineTerminator() . '}';
    }
    
    public function getParams() {
        return $this->_params;
    }
	
	public function setParams($params = array()) {
		$this->_params = $params;
		return $this;
	}
	
	public function concat($expression) {
		$this->_innerExpression .= $expression;
		return $this;
	}
}
