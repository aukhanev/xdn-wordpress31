<?php

/**
 * This is a wrapper for generating object-oriented JavaScript throught PHP.
 * Naturally, no properties or methods defined in this class or any descendants
 * may be auto-used to generate their client-side analogues; use addCall() instead.
 */
abstract class Xedin_Clientside_Model_Script_Abstract extends Xedin_Clientside_Model_Script_Expression implements ArrayAccess {
    
    protected $_isAutoOutput = false;
    
    protected $_objectName = '';
    protected $_statementTerminator;
    protected $_beforeArgsList;
    protected $_afterArgsList;
    protected $_beforeString;
    protected $_afterString;
    protected $_beforeStringAlternative;
    protected $_afterStringAlternative;
    protected $_propertyAccess;
    protected $_methodAccess;
    protected $_assignmentOperator;
    protected $_varKeyword;
    protected $_newKeyword;
    protected $_lineTerminator = "\n";


    protected $_calls = array();
    protected $_isModified = false;
    protected $_callsCache = array();
    protected $_isUsingCache = true; 
	
	protected $_additionalText = array();
    
    
    public function getObjectName() {
        return $this->_objectName;
    }
    
    public function validateObjectName($objectName) {
        return true;
    }
    
    public function setObjectName($objectName) {
        if( $this->validateObjectName($objectName) ) {
            $this->_objectName = $objectName;
        }
        
        return $this;
    }
    
    public function hasObjectName() {
        return (isset($this->_objectName) && !empty($this->_objectName));
    }
    
    public function isAutoOutput($autoOutput = null) {
        if( is_null($autoOutput) ) {
            return $this->_isAutoOutput;
        }
        
        $this->_isAutoOutput = (bool)$autoOutput;
        return $this;
    }
    
    public function isModified() {
        return $this->_isModified;
    }
    
    public function isUsingCache($isUsingCache = null) {
        if( is_null($isUsingCache) ) {
            return $this->_isUsingCache;
        }
        
        $this->_isUsingCache = (bool)$isUsingCache;
        return $this;
    }
    
    public function getStatementTerminator() {
        return $this->_statementTerminator;
    }
    
    public function getBeforeArgsList() {
        return $this->_beforeArgsList;
    }
    
    public function getAfterArgsList() {
        return $this->_afterArgsList;
    }
    
    public function getBeforeString($alternative = false) {
        if( !$alternative ) {
            return $this->_beforeString;
        }
        
        return $this->_beforeStringAlternative;
    }
    
    public function getAfterString($alternative = false) {
        if( !$alternative ) {
            return $this->_afterString;
        }
        
        return $this->_afterStringAlternative;
    }
    
    public function getPropertyAccess() {
        return $this->_propertyAccess;
    }
    
    public function getMethodAccess() {
        return $this->_methodAccess;
    }
    
    public function getAssignmentOperator() {
        return $this->_assignmentOperator;
    }
    
    public function getVarKeyword() {
        return $this->_varKeyword;
    }
    
    public function getNewKeyword() {
        return $this->_newKeyword;
    }
    
    public function getLineTerminator() {
        return $this->_lineTerminator;
    }
    
    public function __call($methodName, $args = array()) {
        $this->addCall($methodName, $args, 'method');
        return $this;
    }
    
    public function __get($methodName) {
        $this->addCall($methodName, array(), 'get');
		return $this;
    }
    
    public function __set($methodName, $value) {
        $arguments = is_array($value) ? $value : array($value);
        $this->addCall($methodName, $arguments, 'set');
		return $this;
    }
	
	public function offsetExists($offset) {
		return true;
	}
	
	public function offsetUnset($offset) {
		
	}
	
	public function offsetGet($offset) {
		$this->addCall($offset, array(), 'array_get');
		return $this;
	}
	
	public function offsetSet($offset, $value) {
		$this->addCall($offset, array($value), 'array_set');
		return $this;
	}
    
    public function addCall($method, $arguments = array(), $type = 'method') {
		if( !is_array($arguments) ) {
			$arguments = array($arguments);
		}
        
        if( $this->isUsingCache() ) {
            $nextIndex = $this->getTotalCalls();
            $this->_calls[$nextIndex] = array('method' => $method, 'arguments' => $arguments, 'type' => $type, 'position' => $nextIndex);
        }
        $this->_isModified = true;
        return $this;
    }
    
    public function getCalls($index = null) {
        if( is_null($index) ) {
            return $this->_calls;
        }
        
        $index = intval($index);
        
        if( !isset($this->_calls[$index]) ) {
            return null;
        }
        return $this->_calls[$index];
    }
    
    public function getTotalCalls() {
        return count($this->getCalls());
    }
    
    final public function getScript() {
        $this->_beforeGetScript();
        $script = $this->_getScript();
        $this->_afterGetScript();
        return $script;
    }
    
    protected function _getScript() {
        $script = '';
        foreach( $this->getCalls() as $idx => $_call ) {
            if( $this->hasCallCache($idx) ) {
                $script .= $this->getCallCache($idx);
                continue;
            }

            if( $this->isUsingCache() ) {
                $this->_callsCache[$idx] = $this->getCallStatement($_call);
            }
            
            $script .= $this->_callsCache[$idx];
        }
        
        return $script;
    }
    
    protected function getCallCache($idx = null) {
        if( is_null($idx) ) {
            return $this->_callsCache;
        }
        
        if( isset($this->_callsCache[$idx]) ) {
            return $this->_callsCache[$idx];
        }
        
        return null;
    }
    
    public function hasCallCache($idx = null) {
        if( is_null($idx) )  {
            return $this->isUsingCache() && !empty($this->_callsCache);
        }
        
        return (isset($this->_callsCache[$idx]) );
    }
    
    abstract public function getCallStatement($call);
    
    protected function _beforeGetScript() {}
    
    protected function _afterGetScript() {}
    
    abstract public function getArgsList($args);
    
    public function clear() {
        $this->_calls = array();
        $this->_callsCache = array();
    }
    
    abstract protected function _createVariable($varname = null, $initValue = null, $new = false);
    
    abstract public function translateVar($variableValue);
    
    public function __toString() {
        return $this->getScript();
    }
	
	public function concat($expression) {
		$index = count($this->_additionalText);
		$this->_additionalText[$index] = $expression;
		$this->addCall('concat', $index, 'feature');
		return $this;
	}
}
