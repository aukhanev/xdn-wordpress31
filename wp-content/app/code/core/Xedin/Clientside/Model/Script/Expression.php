<?php

class Xedin_Clientside_Model_Script_Expression extends Varien_Object {
    protected $_innerExpression;
    
    public function __construct($expression = null) {
        $this->setInnerExpression($expression);
		$this->_construct();
    }
    
    public function __toString() {
        return $this->toString();
    }
    
    public function toString() {
        return $this->getInnerExpression();
    }
    
    public function getInnerExpression() {
        return $this->_innerExpression;
    }
    
	/**
	 *
	 * @param type $expression
	 * @return \Xedin_Clientside_Model_Script_Expression 
	 */
    public function setInnerExpression($expression) {
        $this->_innerExpression = $expression;
        return $this;
    }
	
	public function concat($expression) {
		$this->_innerExpression .= $expression;
		return $this;
	}
}
