<?php

/**
 * Description of Closure
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Clientside_Model_Script_Javascript_Closure extends Xedin_Clientside_Model_Script_Javascript_Function {
	
	protected $_params;
	
    public function __toString() {
        return '(' . $this->getInnerExpression() . $this->getArgsList($this->getParams()) . ')';
    }
}