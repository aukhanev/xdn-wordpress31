<?php

class Xedin_Clientside_Model_Script_Javascript_Object extends Xedin_Clientside_Model_Script_Expression {
    
	protected $_isPrettyPrint = true;
	protected $_indent = "\t";
    
    public function setInnerExpression($object) {
        if( ($object instanceof Xedin_Clientside_Model_Script_Expression) || is_string($object) ) {
            parent::setInnerExpression($object);
        }
        elseif( is_array($object) ) {
            parent::setInnerExpression( new Varien_Object($object) );
        }
        
        return $this;
    }   
    
    public function getInnerExpression() {
        $innerExpression = parent::getInnerExpression();
        if( $innerExpression instanceof Varien_Object ) {
			$data = $innerExpression->getData();
			if( method_exists('Zend_Json', 'encode') && $this->isPrettyPrint() ) {
				return Zend_Json::prettyPrint( Zend_Json::encode($data), array('indent' => $this->_indent) );
			}
            return $innerExpression->toJson(array(), true);
            
        }
        return $innerExpression . '';
    }
	
	public function isPrettyPrint($prettyPrint = null) {
		if( is_null($prettyPrint) ) {
			return $this->_isPrettyPrint;
		}
		
		$this->_isPrettyPrint = (bool)$prettyPrint;
		
		return $this;
	}
}
