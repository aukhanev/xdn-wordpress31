<?php

class Xedin_Clientside_Model_Script_Javascript_Jquery_Function extends Xedin_Clientside_Model_Script_Javascript_Jquery {
    
    protected $_params;
    
    public function __construct($inner = null, $params = array()) {
        if( $inner instanceof Xedin_Clientside_Model_Script_Expression ) {
            try{
                $inner .= '';
                parent::__construct($inner . '');
            }
            catch(Exception $e){
                debug_print_backtrace();
            }
        }
        elseif( is_string($inner) ) {
            return parent::__construct($inner);
        }
    }
    
    public function __toString() {
        return 'function ' . $this->getArgsList($this->getParams()) . ' {' . $this->getLineTerminator() . $this->getInnerExpression() . $this->getLineTerminator() . '}';
    }
    
    public function getParams() {
        return $this->_params;
    }
}
