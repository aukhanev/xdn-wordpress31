<?php

/**
 * A script abstraction specific to the JavaScript language.
 * 
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
class Xedin_Clientside_Model_Script_Javascript extends Xedin_Clientside_Model_Script_Abstract {
    
    // Common obligatory fetures
    protected $_statementTerminator = ";\n";
    protected $_beforeArgsList = '(';
    protected $_afterArgsList = ')';
    protected $_beforeString = '\'';
    protected $_afterString = '\'';
    protected $_beforeStringAlternative = '"';
    protected $_afterStringAlternative = '"';
    protected $_propertyAccess = '.';
    protected $_methodAccess = '.';
    protected $_varKeyword = 'var';
    protected $_assignmentOperator = '=';
    
    protected $_isChaining = false;
    
    public function getCallStatement($call) {
        $previousCall = $this->getCalls($call['position']-1);
        $nextCall = $this->getCalls($call['position']+1);
        $statement = '';
        switch($call['type']) {
            case 'method':
                $statement .= ( (!$this->isChaining() || is_null($previousCall) || ($nextCall &&
                            $previousCall['type'] == 'feature' &&
                            $previousCall['method'] == 'chain' &&
                            ($prevArgs = $previousCall['arguments']) &&
                            $prevArgs[0] == 'start') ) ? $this->getObjectName() : '') .
                        $this->getMethodAccess() .
                        $call['method'] .
                        $this->getArgsList($call['arguments']) .
                        ( (!$this->isChaining() ||
                            ($nextCall &&
                            $nextCall['type'] == 'feature' &&
                            $nextCall['method'] == 'chain' &&
                            ($nextArgs = $nextCall['arguments']) &&
                            $nextArgs[0] != 'start'))
                        ? $this->getStatementTerminator()
                        : /*$this->getLineTerminator()*/'' );
                break;
            
            case 'get':
                $args = $call['arguments'];
				$suffix = $this->isChaining() ? '' : $this->getStatementTerminator();
                $statement = $this->getObjectName() . $this->getPropertyAccess() . $call['method'] . $suffix;
                break;
            
            case 'set':
                $args = $call['arguments'];
                $statement = $this->getObjectName() . $this->getPropertyAccess() . $call['method'] . ' ' . $this->getAssignmentOperator() . ' ' . $args[0] . $this->getStatementTerminator();
                break;
            
            case 'var':
                $args = $call['arguments'];
//				var_dump($args);
				$args = ( is_array($args) && !is_object($args) ) ? $args[0] : $args;
                $initValue = $args;
                $statement = $this->_createVariable($call['method'], $initValue, false);
                break;
            
            case 'new':
                $args = $call['arguments'];
                $initValue = !empty($args) ? $args[0] : array();
                $statement = $this->_createVariable($call['method'], $initValue, true, array_shift($args));
                break;
            
            case 'feature':
                $args = $call['arguments'];
                switch( $call['method'] ) {
					case 'concat':
						$statement = $this->_additionalText[$args[0]] . $this->getLineTerminator();
						break;
					
                    case 'chain':
                        $this->_isChaining = ( ($args[0] == 'start') ? true : false );
                        break;
                }
				break;
				
			case 'array_get':
				$args = $call['arguments'];
				$prefix = ($this->isChaining() && $previousCall['type'] == 'method') ? '' : $this->getObjectName();
				$statement = $prefix . '[' . $call['method'] . ']' . $this->getStatementTerminator();
				break;
			
			case 'array_set':
				$args = $call['arguments'];
				$statement = $this->getObjectName() . '[' . $call['method'] . '] ' . $this->getAssignmentOperator() . ' ' . $args[0] . $this->getStatementTerminator();
				break;
				
        }
        return $statement;
    }
    
    public function getArgsList($args) {
        $args = (array)$args;
        $argsList = '';
        $argsList .= $this->getBeforeArgsList();
        
        $i = 0;
        foreach( $args as $idx => $_arg ) {
			if( !is_string($_arg) ) {
				$_arg = $this->translateVar($_arg);
			}
            //echo($_arg);
            $argsList .= $_arg . ', ';
            $i++;
        }
        //return $argsList;
        // Removing last comma.
        if( count($args) ) {
            $argsList = substr($argsList, 0, strlen($argsList) - 2);
        }
        $argsList .= $this->getAfterArgsList();
        
        return $argsList;
    }
    
    public function kVar($varname = null, $initValue = null) {
        if( is_null($varname) ) {
            $varname = $this->getObjectName();
        }
        
        if( is_null($initValue) ) {
            
        }
        
        $this->addCall($varname, $initValue, 'var');
        return $this;
    }
    
    public function kNew($varName = null, $initValue = null, $constructorArgs = array()) {
        $this->addCall($varName, $initValue, 'new');
        return $this;
        //return $this->_createVariable($varName, $initValue, true, $constructorArgs);
    }
    
    protected function _createVariable($varname = null, $initValue = null, $new = false, $constructorArgs = array()) {
        $string = '';
        
		if( is_array($varname) ) {
			$varname = implode(', ', $varname);
		}
		
        $string = $this->getVarKeyword() . ' ' . $varname;
        if( is_array($initValue) && empty($initValue) ) {
            return $string . $this->getStatementTerminator();
        }
        
        $string .= ' ' . $this->getAssignmentOperator() . ' ';
        if( $new ) {
            $string .= $this->getNewKeyword() . ' ' . $initValue . $this->getArgsList($constructorArgs);
        }
        else {
            
            $string .= $this->translateVar($initValue);
        }
        
        $string .= $this->getStatementTerminator();
        
        return $string;
    }
    
    public function translateVar($variableValue) {
//        if( is_bool($variableValue) ) {
//            $variableValue = $this->getNewExpression($variableValue);
//        }
            
        if( is_numeric($variableValue)  ) {}
        elseif( $variableValue instanceof Xedin_Clientside_Model_Script_Expression) {
            $variableValue = '' . $variableValue;
        }
        else {
            $variableValue = $this->getBeforeString() . $variableValue . $this->getAfterString();
        }
        
        return $variableValue;
    }
    
    public function fStartChain() {
        $this->addCall('chain', 'start', 'feature');
        return $this;
    }
    
    public function fEndChain() {
        $this->addCall('chain', 'end', 'feature');
        return $this;
    }
    
    public function isChaining() {
        return $this->_isChaining;
    }
    
    public function getNewExpression($expression) {
        return Hub::getModel('clientside/script_expression')
				->setInnerExpression($expression);
    }
    
    public function getNewObject($data = array()) {
        return Hub::getModel('clientside/script_javascript_object')
				->setInnerExpression($data);
    }
    
    public function getNewFunction($expression, $params = array()) {
        return Hub::getModel('clientside/script_javascript_function')
				->setInnerExpression($expression);
    }
}
