<?php

class Xedin_Clientside_Model_Script_Javascript_Jquery extends Xedin_Clientside_Model_Script_Javascript {
    protected $_innerExpression;
    protected $_jQueryVarName = 'jQuery';
    
    public function __construct($expression = null) {
        parent::__construct($expression);
    }
    
    public function setJqueryVarName($varName) {
        $this->_jQueryVarName = $varName;
        return $this;
    }
    
    public function getJqueryVarName() {
        return $this->_jQueryVarName;
    }
    
    public function setInnerSelector($selector) {
		$selector = $this->escapeId(trim($selector));
        $this->setInnerExpression($selector);
        $this->setObjectName($this->getInnerExpression());
        return $this;
    }
    
    public function setInnerObject($object) {
        if( !($object instanceof Xedin_Clientside_Model_Script_Expression) ) {
            $this->setInnerExpression($this->getNewExpression($object));
        }
        
        $this->setObjectName($this->getInnerExpression());
        return $this;
    }
    
    public function getObjectName() {
        $objectName = $this->getJqueryVarName() . $this->getArgsList( array($this->getInnerExpression()) );
        return $objectName;
    }
    
    public function getNewFunction($expression) {
        return Hub::getModel('clientside/script_javascript_jquery_function')
				->setInnerExpression($expression);
    }
	
	public function escapeId($id) {
		return str_ireplace (':', '\\\:', $id);
	}
}
