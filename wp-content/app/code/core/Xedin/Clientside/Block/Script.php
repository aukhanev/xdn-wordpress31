<?php

class Xedin_Clientside_Block_Script extends Xedin_Core_Block_Template {
    
    protected $_outputModel;
    
    protected function _construct($layout = null, $attributes = array()) {
        parent::_construct($layout, $attributes);
        $this->setTemplate('clientside/script.php');
    }
    
    public function setOutputModel($model) {
        $this->_outputModel = $model;
        return $this;
    }
    
    public function getOutputModel() {
        return $this->_outputModel;
    }
    
    public function hasOutputModel() {
        return (isset($this->_outputModel) && !empty($this->_outputModel) );
    }
	
	/**
	 * @return Xedin_Clientside_Model_Script_Javascript
	 */
	public function js($objectName = null) {
		$js = Hub::getModel('clientside/script_javascript');
		if( !is_null($objectName) ) {
			$js->setObjectName($objectName);
		}
		return $js;
	}
	
	/**
	 * @return Xedin_Clientside_Model_Script_Javascript_Jquery
	 */
	public function jQuery($selector = null, $object = false) {
		$jQuery = Hub::getModel('clientside/script_javascript_jquery');
		/* @var $jQuery Xedin_Clientside_Model_Script_Javascript_Jquery */
		
		if( !is_null($selector) ) {
			if( $object ) {
				$jQuery->setInnerObject($selector);
			}
			else {
				$jQuery->setInnerSelector($selector);
			}
		}
		
		return $jQuery;
	}
	
	public function func($expression = null, $args = array()) {
		$func = Hub::getModel('clientside/script_javascript_function');
		/* @var $func Xedin_Clientside_Model_Script_Javascript_Function */
		if( !is_null($expression) ) {
			$func->setInnerExpression($expression);
		}
		
		$func->setParams($args);
		
		return $func;
	}
	
	public function obj($attributes = null) {
		$obj = Hub::getModel('clientside/script_javascript_object');
		/* @var $obj Xedin_Clientside_Model_Script_Javascript_Object */
		
		if( !is_null($attributes) ) {
			$obj->setInnerExpression($attributes);
		}
			
		return $obj;
	}
	
	public function closure(Xedin_Clientside_Model_Script_Expression $function, $functionArgs = array(), $closureArgs = array()) {
		$closure = Hub::getModel('clientside/script_javascript_closure');
		/* @var $closure Xedin_Clientside_Model_Script_Javascript_Closure */
		
		if( !($function instanceof Xedin_Clientside_Model_Script_Javascript_Function) ) {
			$function = $this->func($function);
		} 
		$closure->setInnerExpression($function->setParams($functionArgs))
				->setParams($closureArgs);
		
		return $closure;
	}
}
