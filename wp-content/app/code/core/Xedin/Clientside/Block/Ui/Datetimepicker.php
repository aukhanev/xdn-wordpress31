<?php

/**
 * Description of Datetimepicker
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Clientside_Block_Ui_Datetimepicker extends Xedin_Clientside_Block_Script {
	
	protected $_elementSelector;
	protected $_pickerOptions = array();
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
	}
	
	public function getOutputModel() {
		if( !$this->_outputModel ) {
			/* @var $js Xedin_Clientside_Model_Script_Javascript_Jquery */
			$js = Hub::getModel('clientside/script_javascript_jquery');
			$options = $js->getNewObject($this->getPickerOptions());
			$writer = Hub::getModel('clientside/script_javascript_jquery')->setInnerSelector($this->getElementSelector())->datetimepicker($options);
			$this->_outputModel = $js->setInnerObject('document')->ready($writer->getNewFunction($writer));
		}
		
		return $this->_outputModel;
	}
	
	public function setElementSelector($selector) {
		$this->_elementSelector = $selector;
		return $this;
	}
	
	public function getElementSelector() {
		return $this->_elementSelector;
	}
	
	public function setPickerOptions($name, $value = null) {
		if( is_null($value) && is_array($name) ) {
			$this->_pickerOptions = $name;
			return $this;
		}
		
		$this->_pickerOptions[$name] = $value;
		
		return $this;
	}
	
	public function hasPickerOptions($name = null) {
		if( is_null($name) ) {
			return !empty($this->_pickerOptions);
		}
		
		return isset($this->_pickerOptions[$name]);
	}
	
	public function addPickerOptions($name, $value = null) {
		if( is_null($value) && is_array($name) ) {
			foreach( $name as $_name => $_value ) {
				$this->addPickerOptions($_name, $_value);
			}
			
			return $this;
		}
		
		if( !$this->hasPickerOptions($name) ) {
			$this->setPickerOptions($name, $value);
		}
		
		return $this;
	}
	
	public function getPickerOptions($name = null, $default = null) {
		if( is_null($name) ) {
			return $this->_pickerOptions;
		}
		
		if( !$this->hasPickerOptions($name) ) {
			return $default;
		}
		
		return $this->_pickerOptions[$name];
	}
}