<?php
/**
 * Description of Template
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Users_Block_User_Controls extends Xedin_Users_Block_Template {
	
	public function getUser() {
		return $this->getSession()->getUser();
	}
	
	public function getLoginUrl() {
		return $this->getController()->getAdminUrl( $this->getController()->getLoginPath() );
	}
	
	public function getLogoutUrl() {
		return $this->getController()->getAdminUrl( $this->getController()->getLogoutPath() );
	}
	
	public function isUserLoggedIn() {
		return $this->getController()->isUserLoggedIn();
	}
}