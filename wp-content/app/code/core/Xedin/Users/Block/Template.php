<?php
/**
 * Description of Template
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Users_Block_Template extends Xedin_Core_Block_Template {
	
	/**
	 * @return Xedin_Users_Model_Session
	 */
	public function getSession() {
		return Hub::getSingleton('users/session');
	}
}