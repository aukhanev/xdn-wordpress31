<?php

/**
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Users_Model_Resource_User extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	const FIELD_USERNAME = 'username';
	const FIELD_EMAIL = 'email';
	const FIELD_PASSWORD_HASH = 'password_hash';
	
	protected function _construct() {
		$this->_init('users_users')
				->_setMetaTableForeignKeyName('user_id');
	}
	
	protected function _beforeSave(Xedin_Core_Model_Meta_Abstract $model) {
		
		/* @var Xedin_Users_Model_User */
		$registered = $model->getData('registered');
		if( empty($registered) ) {
			$model->setData('registered', $this->getFormattedTimeStamp());
		}
		
		if( !$model->hasData('password_hash') ) {
			if( $model->hasData('password') ) {
				$passwordHash = $model->getEncryptedPassword($model->getData('password'));
				$model->setData('password_hash', $passwordHash);
				$model->unsetData('password');
			}
		}
		
		$model = parent::_beforeSave($model);
		
		return $model;
	}
	
	public function loadByUid($object, $uid) {

		$this->_beforeLoad($object);
		
        $object->unsetData();
		$results = $this->_getWriteAdapter()->fetchRow(
                                $this->getSelect()
                                ->from($this->getTableName())
                                ->where( self::FIELD_USERNAME . ' LIKE ?', $uid)
								->orWhere( self::FIELD_EMAIL . ' = ?', $uid)
                );
		
		if( !empty($results) ) {
			$object->setData($results);
		}
		
		$this->_afterLoad($object);
		
        return $this;
	}
	
	public function loadByAuthInfo($object, $uid, $password) {
		$this->_beforeLoad($object);
		
        $object->unsetData();
		$whereExpr = new Zend_Db_Expr($this->_getWriteAdapter()->quoteInto(self::FIELD_USERNAME . ' LIKE ?', $uid) . ' OR ' . $this->_getWriteAdapter()->quoteInto(self::FIELD_EMAIL . ' = ?', $uid));
		
		$this->getProfiler()->setEnabled(true);
		$select = $this->getSelect()
				->from($this->getTableName())
				->where( $whereExpr )
				->where( self::FIELD_PASSWORD_HASH . ' = ?', $object->getEncryptedPassword($password) );
		
		$results = $this->_getWriteAdapter()->fetchRow($select);
		
		if( !empty($results) ) {
			$object->setData($results);
		}
		
		$this->_afterLoad($object);
	}
}