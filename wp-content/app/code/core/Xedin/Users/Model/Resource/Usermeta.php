<?php

/**
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Users_Model_Resource_Usermeta extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		$this->_init('users_usermeta');
	}
}