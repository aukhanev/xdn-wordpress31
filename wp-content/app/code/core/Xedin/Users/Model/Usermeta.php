<?php

/**
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Users_Model_Usermeta extends Xedin_Users_Model_Abstract {
	
	protected function _construct() {
		$this->_init('users/usermeta');
	}
}