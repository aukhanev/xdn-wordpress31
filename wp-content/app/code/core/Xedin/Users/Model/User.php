<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
class Xedin_Users_Model_User extends Xedin_Users_Model_Meta_Abstract {

	protected function _construct() {
		$this->_init('users/user')
				->_setMetaModelName('users/usermeta');
	}
	
	public function getRandomPassword() {
		return uniqid();
	}
	
	public function getEncryptedPassword($password) {
		return md5($password);
	}
	
	public function assertPasswordsAreEqual($unencryptedPassword, $encryptedPassword) {
		return $this->getEncryptedPassword($unencryptedPassword) == $encryptedPassword;
	}
	
	public function load($userId = null) {
		if( is_numeric($userId) || is_null($userId) ) {
			return parent::load($userId);
		}
		
		$this->getResourceModel()->loadByUid($this, $userId);
		
		return $this;
	}
	
	public function authenticate($uid, $password) {
		$this->getResourceModel()->loadByAuthInfo($this, $uid, $password);
		return $this->hasData() ? $this : false;
	}
}