<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
class Xedin_Users_Model_Session extends Xedin_Core_Model_Session_Abstract {
	
	protected $_namespace = 'users';
	
	protected $_userModelUri = 'users/user';
	
	protected $_varNameUsername = 'username';
	
	const REG_KEY_CURRENT_USER = '_current_user';
	
	public function getUsernameVarName() {
		return $this->_varNameUsername;
	}
	
	public function isUserLoggedIn() {
		return (bool)$this->getUsername();
	}
	
	/**
	 * @return Xedin_Users_Model_User|null 
	 */
	public function getUser() {
		if( !$this->getUsername() ) {
			return null;
		}
		
		if( !$this->_hasUser() ) {
			$this->_setUser($this->getUserModel()->load($this->getUsername()), self::REG_KEY_CURRENT_USER);
		}
		
		return $this->_getUser();
	}
	
	protected function _getUser() {
		return Hub::registry(self::REG_KEY_CURRENT_USER);
	}
	
	protected function _hasUser() {
		return (bool)$this->_getUser();
	}
	
	protected function _setUser($user) {
		Hub::register($user, self::REG_KEY_CURRENT_USER);
		return $this;
	}
	
	protected function _unsetUser() {
		Hub::unregister(self::REG_KEY_CURRENT_USER);
		return $this;
	}
	
	public function getUsername() {
		return $this->get($this->getUsernameVarName());
	}
	
	protected function _setUsername($username) {
		$this->set($this->getUsernameVarName(), $username);
		return $this;
	}
	
	protected function _unsetUsername() {
		$this->uns($this->getUsernameVarName());
		return $this;
	}
	
	protected function _setVarNameUsername($varName) {
		$this->_varNameUsername = $varName;
		return $this;
	}
	
	public function login($username, $password) {
		$user = $this->getUserModel()->authenticate($username, $password);
		if( !$user ) {
			$this->addErrorMessage('Wrong unsername or password');
		}
		
		$this->_login($user);
		return $this;
	}
	
	public function logout($username) {
		$this->_unsetUsername()
				->_unsetUser();
		return $this;
	}
	
	protected function _login($user) {
		if( !is_object($user) ) {
			$user = $this->getUserModel()->load($user);
		}
		
		
		if( !$user->hasData() ) {
			$this->addErrorMessage('No user was found.');
		}
		
		$this->_setUsername($user->getUsername())
				->_setUser($user);
		
		return $this;
	}
	
	protected function _setUserModelUri($uri) {
		$this->_userModelUri = $uri;
		return $this;
	}
	
	public function getUserModelUri() {
		return $this->_userModelUri;
	}
	
	/**
	 * @return Xedin_Users_Model_User
	 */
	public function getUserModel() {
		return Hub::getModel( $this->getUserModelUri() );
	}
	
	public function addErrorMessage($message) {
		exit($message);
	}
}