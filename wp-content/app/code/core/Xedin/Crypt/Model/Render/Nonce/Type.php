<?php

/**
 * Description of Type
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Render_Nonce_Type extends Xedin_Adminhtml_Model_Render_Fields_Select_Abstract {
	
	protected $_modelUri = 'crypt/nonce_type';
}