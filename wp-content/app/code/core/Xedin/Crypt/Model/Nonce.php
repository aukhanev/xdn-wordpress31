<?php

/**
 * Description of Nonce
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Nonce extends Xedin_Crypt_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt/nonce');
	}
	
	public function getNewNonce($entityId, $type) {
		if( !is_object($type) ) {
			$type = $this->getTypeModel($type);
		}
		
		if( !($type instanceof Xedin_Crypt_Model_Nonce_Type) && !is_null($type) ) {
			$type = is_object($type) ? get_class($type) : gettype($type);
			throw Hub::exception('Xedin_Crypt', 'Invalid nonce type: "' . $type . '".');
		}
		
		$this->loadForEntity($entityId, $type->getId());
		
		$nonce = $this->generateNonce($entityId, $type);
		$gmTime = $this->getTime();
		$this->setKey($nonce->getKey())
				->setTypeId($type->getId())
				->setEntityId($entityId)
				->setExpiresAt($gmTime + (int)$type->getPeriod())
				->setHashAlgorithm($type->getHashAlgorithm())
				->setSalt($nonce->getSalt())
				->unsCreatedAt() // Regenerate 
				->save();
		
		if( !$this->getId() ) {
			return null;
		}
		
		return $this->getKey();
	}
	
	public function loadForEntity($entityId, $type) {
		$this->unsetData();
		
		$nonce = $this->getCollectionForEntity($entityId, $type)->getFirstItem();
		
		if( $nonce ) {
			$this->setData($nonce->getData());
		}
		
		return $this;
	}
	
	public function loadForEntityValid($entity, $type) {
		$this->unsetData();
		$nonce = $this->getCollectionForEntity($entity, $type)
					->addFieldToFilter('expires_at', $this->getDate(), 'gteq')
					->getFirstItem();
		
		if( $nonce ) {
			$this->setData($nonce->getData());
		}
		
		return $this;
	}
	
	public function getCollectionForEntity($entity, $type) {
		if( $type instanceof Xedin_Crypt_Model_Nonce_Type ) {
			$type = $type->getId();
		}
		
		if( $entity instanceof Xedin_Core_Model_Abstract ) {
			$entity = $entity->getId();
		}
		
		return $this->getCollection()->addFieldToFilter('entity_id', $entity)
				->addFieldToFilter('type_id', $type);
	}
	
	public function assertEqualNonce($nonce, $entityId = null, $type = null) {
		if( $type instanceof Xedin_Crypt_Model_Nonce_Type ) {
			$type = $type->getId();
		}
		
		if( $nonce instanceof Varien_Object ) {
			$nonce = $nonce->getKey();
		}
		
		if( !empty($entityId) && !empty($type) ) {
			$this->loadForEntityValid($entityId, $type);
			
			if( !$this->getId() ) {
				return false;
			}
		}	
		
		$nonce = (string)$nonce;
		return $nonce === $this->getKey();
	}
	
	public function generateNonce($entityId, $type = null, $salt = '', $algorithm = null) {
		if( !function_exists('hash') ) {
			throw Hub::exception('Xedin_Crypt', 'Your version of PHP does not support hashing.');
		}
		
		if( !is_null($type) && !is_object($type) ) {
			$type = $this->getTypeModel($type);
		}
		
		if( !($type instanceof Xedin_Crypt_Model_Nonce_Type) && !is_null($type) ) {
			$type = is_object($type) ? get_class($type) : gettype($type);
			throw Hub::exception('Xedin_Crypt', 'Invalid nonce type: "' . $type . '".');
		}
		
		if( empty($type) && empty($salt) ) {
			throw Hub::exception('Xedin_Crypt', 'Either type or salt must be specified.');
		}
		
		if( is_null($algorithm) ) {
			$algorithm = $type->getHashAlgorithm();
		}
		
		if( empty($salt) ) {
			$salt = $type->getSalt();
		}
		$hash = hash($algorithm, $entityId . $salt . time());
		return new Varien_Object(array('key' => $hash, 'salt' => $salt));
	}
	
	public function getTypeModel($id = null) {
		$type = Hub::getSingleton('crypt/nonce_type');
		if( !is_null($id) ) {
			$type->load($id);
		}
		
		return $type;
	}
}