<?php

/**
 * Description of Type
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Resource_Nonce_Type extends Xedin_Crypt_Model_Resource_Mysql_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt_nonce_type');
	}
}