<?php

/**
 * Description of Nonce
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Resource_Nonce extends Xedin_Crypt_Model_Resource_Mysql_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt_nonce');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$expiresAt = $object->getExpiresAt();
		if( is_numeric($expiresAt) ) {
			$expiresAt = $this->getFormattedTimeStamp($expiresAt);
			$object->setExpiresAt($expiresAt);
		}
		
		$createdAt = $object->getCreatedAt();
		if( empty($createdAt) ) {
			$createdAt = $this->getFormattedTimeStamp();
			$object->setCreatedAt($createdAt);
		}
//		var_dump($object);
		return $object;
	}
}