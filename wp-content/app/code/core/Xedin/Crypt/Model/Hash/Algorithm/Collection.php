<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Hash_Algorithm_Collection extends Xedin_Core_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt/hash_algorithm');
	}
	
	protected function _init($modelUri) {
		$this->_modelPath = $modelUri;
		return $this;
	}
	
	public function getResourceModel() {
		return null;
	}
	
	protected function _load() {
		foreach( hash_algos() as $_idx => $_code ) {
			$this->_items[$_code] = Hub::getModel($this->getModelPath())->setData( array('id' => $_code, 'code' => $_code, 'label' => strtoupper($_code)) );
			$this->_ids[] = $_code;
		}
		
		return $this;
	}
}