<?php

/**
 * Description of Type
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Nonce_Type extends Xedin_Crypt_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt/nonce_type');
	}
}