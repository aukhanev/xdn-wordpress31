<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Crypt_Model_Nonce_Collection extends Xedin_Crypt_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('crypt/nonce');
	}
}