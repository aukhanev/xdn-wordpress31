<?php

return array(
	'modules'			=> array(
		'Xedin_Eav'			=>	array(
			'render'			=>	array(
				'values'			=>	array(
					'nonce_type'			=> array(
						'uri'				=>	'crypt/render_nonce_type',
						'label'				=>	'Nonce'
					)
				)
			)
		)
	)
)
?>
