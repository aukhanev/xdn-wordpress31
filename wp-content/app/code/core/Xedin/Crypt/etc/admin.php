<?php

return array(
	'system' => array(
		'modules' => array(
			'crypt' => array(
				'name' => 'crypt',
				'label' => 'Crypt',
				'models' => array(
					'nonce_type' => array(
						'path'			=>	'crypt/nonce_type',
						'label'			=>	'Nonce Types',
						'fields'		=>	array(
							'hash_algorithm'	=>	array(
								'field'				=>	'hash_algorithm',
								'render_model'		=>	'crypt/render_hash_algorithm'
							)
						)
					),
					'nonce' => array(
						'path'			=>	'crypt/nonce',
						'label'			=>	'Nonces',
						'fields'		=>	array(
							'type_id'		=>	array(
								'label'				=>	'Type',
								'field'				=>	'type_id',
								'render_model'		=>	'crypt/render_nonce_type'
							),
							'expires_at'	=>	array(
								'field'				=>	'expires_at',
								'render_model'		=>	'core/render_fields_datetime'
							)
						)
					)
				)
			)
		)
	)
)
		
?>