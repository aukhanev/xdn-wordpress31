<?php

/**
 * Description of Head
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
class Xedin_Page_Block_Head extends Xedin_Page_Block_Abstract {
	
	protected $_tags = array();
	protected $_metaTags = array();
	
	protected $_urls = array(
		'js'			=>	'js/',
		'css'			=>	'css/'
	);
	
	const TAG_TYPE_JS = 'js';
	const TAG_TYPE_CSS = 'css';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
	}
	
	public function addTag($type, $value, $prepareUrl = true, $urlType = Hub::URL_TYPE_BASE) {
		if( !isset($this->_tags[$type]) ) {
			$this->_tags[$type] = array();
		}
		
        if( array_search($value, $this->_tags[$type]) !== false ) {
            return $this;
        }
        
        if( $prepareUrl ) {
            $value = Hub::getUrl($value, $urlType);
        }
		if( $this->getRequest()->isSecure() ) {
			$value = preg_replace('!^https?!', 'https', $value);
		}
		
        $this->_tags[$type][] = $value;
	}
	
	public function getTags($type = null, $default = array()) {
		if( is_null($type) ) {
			return $this->_tags;
		}
		
		if( !$this->hasTags($type) ) {
			return $default;
		}
		
		return $this->_tags[$type];
	}
	
	public function hasTags($type = null) {
		if( is_null($type) ) {
			return !empty($this->_tags);
		}
		
		return isset($this->_tags[$type]) && !empty($this->_tags[$type]); 
	}
	
	public function addJs($url, $prepare = true) {
		if( $prepare ) {
			$url = $this->_urls[self::TAG_TYPE_JS] . $url;
		}
		
		$this->addTag(self::TAG_TYPE_JS, $url, $prepare);
		return $this;
	}
	
	public function addCss($url, $prepare = true) {
		if( $prepare ) {
			$url = $this->_urls[self::TAG_TYPE_CSS] . $url;
		}
		
		$this->addTag(self::TAG_TYPE_CSS, $url, $prepare, Hub::URL_TYPE_SKIN);
		return $this;
	}
	
	public function addMeta($name, $content, $type='name') {
		$this->_metaTags[$type][$name] = $content;
		return $this;
	}
	
	public function getMeta($type = null, $dafault = null) {
		if( is_null($type) ) {
			return $this->_metaTags;
		}
		
		if( isset($this->_metaTags[$type]) ) {
			return $default;
		}
		
		return $this->_metaTags[$type];
	}
	
	public function setFavicon($url, $prepare = true) {
	    $this->setData('favicon', $this->prepareUrl($url, $prepare));
		return $this;
	}
}