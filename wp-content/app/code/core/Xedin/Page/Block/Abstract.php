<?php

/**
 * Description of Abstract
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 * + Added prepareUrl() method.
 */
class Xedin_Page_Block_Abstract extends Xedin_Core_Block_Template {
	
	public function prepareUrl($url, $prepare = true, $urlType = Hub::URL_TYPE_BASE, $convertSchema = true) {
		$uri = Hub::getSingleton('core/uri');
		/* @var $uri Xedin_Core_Model_Uri */
		return $uri->prepareUrl($url, $prepare, $urlType, $convertSchema);
	}
}