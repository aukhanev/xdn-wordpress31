<?php

/**
 * Description of Reader
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Csv_Model_Resource_Reader extends Luke_Csv_Reader {
	
	public function __construct($path = null, Luke_Csv_Dialect $dialect = null) {
		if( !is_null($path) ) {
			parent::__construct($path, $dialect);
		}
	}
	
	public function load($model) {
		if( !$model->getPath() ) {
			throw Hub::exception('Xedin_Csv', 'Could not load CSV: no path specified');
		}
		$this->setPath($model->getPath());
		
		$data = $this->toArray();
		$model->setData($data);
	}
}