<?php

return array(
	'system' => array(
		'modules' => array(
			'people' => array(
				'name' => 'users',
				'label' => 'People',
				'models' => array(
					'user' => array(
						'path' => 'adminhtml/users_user',
						'label' => 'Users'
					)
				)
			)
		)
	)
)
		
?>