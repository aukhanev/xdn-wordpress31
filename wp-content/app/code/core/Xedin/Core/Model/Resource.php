<?php

class Xedin_Core_Model_Resource {

    protected $_connections;

    const XML_PATH_CONNECTIONS = 'global/connections';

    public function getConnection($connectionName) {
        if( isset($this->_connections[$connectionName]) ) {
            return $this->_connections[$connectionName];
        }
        
        $connectionConfig = (array)Hub::getConfig(self::XML_PATH_CONNECTIONS . '/' . $connectionName);
        if( !$connectionConfig ) {
            if( false !== strpos($connectionName, 'read') ) {
                $connectionConfig = (array)Hub::getConfig( self::XML_PATH_CONNECTIONS . '/' . str_replace('read', 'write', $connectionName) );
                if( !$connectionConfig ) {
                    throw new Xedin_Code_Exception('No valid connection found');
                }
            }
        }

        $this->_connections[$connectionName] = new Zend_Db_Adapter_Pdo_Mysql($connectionConfig);
        return $this->_connections[$connectionName];
    }
}

?>