<?php
/**
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 * @version 0.1.0
 */

class Xedin_Core_Model_Http_Request extends Xedin_Object {
    
    const SCHEME_HTTP = 'http';
    const SCHEME_HTTPS = 'https';
    
    protected $_paramSources = array('_GET', '_POST');
    
    /**
     * Get a value from the global $_POST array, or the whole array.
     * 
     * @param null|string $key The key at which to return the value from $_POST;
     * @param null|mixed $default The value to return if $key is not found in $_POST.
     * @return array If the first parameter is specified and exists, contents of the global $_POST variable; otherwise null.
     */
    public function getPost($key = null, $default = null) {
        if( is_null($key) ) {
            return $_POST;
        }
        
        return ( !isset($_POST[$key]) ? $default : $_POST[$key] );
    }
    
    /**
     * Get a value from the global $_GET array, or the whole array.
     * 
     * @param null|string $key The key at which to return the value from $_GET;
     * @param null|mixed $default The value to return if $key is not found in $_GET.
     * @return array If the first parameter is specified and exists, contents of the global $_GET variable; otherwise null.
     */
    public function getQuery($key = null, $default = null) {
        if( is_null($key) ) {
            return $_GET;
        }
        
        return (!isset($_GET[$key]) ? $default : $_GET[$key]);
    }
    
    /**
     * Get a value from the global $_COOKIE array, or the whole array.
     * 
     * @param null|string $key The key at which to return the value from $_COOKIE;
     * @param null|mixed $default The value to return if $key is not found in $_COOKIE.
     * @return array If the first parameter is specified and exists, contents of the global $_COOKIE variable; otherwise null.
     */
    public function getCookie($key = null, $default = null) {
        if( is_null($key) ) {
            return $_COOKIE;
        }
        
        return (!isset($_COOKIE[$key]) ? $default : $_COOKIE[$key]);
    }
    
    /**
     * Get a value from the global $_SERVER array, or the whole array.
     * 
     * @param null|string $key The key at which to return the value from $_SERVER;
     * @param null|mixed $default The value to return if $key is not found in $_SERVER.
     * @return array If the first parameter is specified and exists, contents of the global $_SERVER variable; otherwise null.
     */
    public function getServer($key = null, $default = null) {
        $key = strtoupper(str_replace('-', '_', $key));
        if( is_null($key) ) {
            return $_SERVER;
        }
        
        return (!isset($_SERVER[$key]) ? $default : $_SERVER[$key]);
    }
    
    /**
     * Get a value from the global $_ENV array, or the whole array.
     * 
     * @param null|string $key The key at which to return the value from $_SERVER;
     * @param null|mixed $default The value to return if $key is not found in $_SERVER.
     * @return array If the first parameter is specified and exists, contents of the global $_SERVER variable; otherwise null.
     */
    public function getEnv($key = null, $default = null) {
        if( is_null($key) ) {
            return $_ENV;
        }
        
        return (!isset($_ENV[$key]) ? $default : $_ENV[$key]);
    }
    
    /**
     * Get current protocol scheme (http/https).
     * 
     * @return string 'https' if https is used; otherwise, 'http';
     */
    public function getScheme() {
        $https = $this->getServer('HTTPS');
        return ( (empty($https) || $https == 'off') ? self::SCHEME_HTTP : self::SCHEME_HTTPS );
    }
    
    /**
     * Whether or not the current scheme is HTTPS.
     * 
     * @return type bool True if the current scheme is HTTPS; false otherwise.
     */
    public function isHttps() {
        return ( strtolower($this->getScheme()) == 'https' );
    }
    
    /**
     * Alias of isHttp().
     * 
     * @return bool True if the current scheme is HTTPS; otherwise, false.
     */
    public function isSecure() {
        return $this->isHttps();
    }
    
    /**
     * Get the request method.
     * 
     * @return string The method of this request, e.g. 'GET', 'POST', 'PUT', etc.
     */
    public function getMethod() {
        return $this->getServer('REQUEST_METHOD');
    }
    
    /**
     * Whether or not the request methhod is 'POST'.
     * 
     * @return bool True if the request method is 'POST'; otherwise, false.
     */
    public function isPost() {
        return ( strtolower($this->getMethod() == 'post') );
    }
    
    /**
     * Whether or not the request methhod is 'GET'.
     * 
     * @return bool True if the request method is 'GET'; otherwise, false.
     */
    public function isGet() {
        return ( strtolower($this->getMethod() == 'get') );
    }
    
    /**
     * Whether or not the request methhod is 'PUT'.
     * 
     * @return bool True if the request method is 'PUT'; otherwise, false.
     */
    public function isPut() {
        return ( strtolower($this->getMethod() == 'put') );
    }
    
    /**
     * Whether or not the request methhod is 'HEAD'.
     * 
     * @return bool True if the request method is 'HEAD'; otherwise, false.
     */
    public function isHead() {
        return ( strtolower($this->getMethod() == 'head') );
    }
    
    /**
     * Whether or not the request methhod is 'DELETE'.
     * 
     * @return bool True if the request method is 'DELETE'; otherwise, false.
     */
    public function isDelete() {
        return ( strtolower($this->getMethod() == 'delete') );
    }
    
    /**
     * Whether or not the request methhod is 'OPTIONS'.
     * 
     * @return bool True if the request method is 'OPTIONS'; otherwise, false.
     */
    public function isOptions() {
        return ( strtolower($this->getMethod() == 'options') );
    }
    
    /**
     * Get name and, possibly, the port of the request 'host' header.
     * 
     * @return string If the port is standard for the request scheme, returns <hostname>; otherwise, returns <hostname>:<port>.
     */
    public function getHttpHost() {
        $host = $this->getServer('HTTP_HOST');
        if (!empty($host)) {
            return $host;
        }

        $scheme = $this->getScheme();
        $name   = $this->getServer('SERVER_NAME');
        $port   = $this->getServer('SERVER_PORT');

        if(null === $name) {
            return '';
        }
        elseif (($scheme == self::SCHEME_HTTP && $port == 80) || ($scheme == self::SCHEME_HTTPS && $port == 443)) {
            return $name;
        } else {
            return $name . ':' . $port;
        }
    }
    
    /**
     * Get header value by name.
     * 
     * @param string $header Header name, such as 'Accept-Encoding'.
     * @return type string Header value.
     */
    public function getHeader($header) {
        // Try to get it from the $_SERVER array first
        $temp = 'HTTP_' . strtoupper(str_replace('-', '_', $header));
        if (isset($_SERVER[$temp])) {
            return $_SERVER[$temp];
        }

        // This seems to be the only way to get the Authorization header on Apache
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
            if (isset($headers[$header])) {
                return $headers[$header];
            }
            $header = strtolower($header);
            foreach ($headers as $key => $value) {
                if (strtolower($key) == $header) {
                    return $value;
                }
            }
        }

        return false;
    }

    /**
     * Get the client's IP addres. 
     *
     * @param boolean $checkProxy Whether or not to chech for the 'HTTP_X_FORWARDED_FOR' value in place of 'REMOVE_ADDR'.
     * @return string The client's IP address.
     * @see http://php.net/manual/en/reserved.variables.server.php#93599
     */
    public function getClientIp($checkProxy = true) {
        if ($checkProxy && $this->getServer('HTTP_X_FORWARDED_FOR') != null) {
            return $this->getServer('HTTP_X_FORWARDED_FOR');
        } else {
            return $this->getServer('REMOTE_ADDR');
        }
    }
    
    public function getRequestPathFromUrlPath($urlPath) {
        $urlSegments = preg_split('![_/]!', $urlPath);
        $requestSegments = preg_split('![_/]!', $this->getRequestPath());
        $newRequestSegments = array();
        foreach( $urlSegments as $idx => $_urlSegment ) {
            if( isset($requestSegments[$idx]) && !empty($requestSegments[$idx]) ) {
                $newRequestSegments[$idx] = ($_urlSegment == '*') ? $requestSegments[$idx] : $_urlSegment;
            }
        }
        return implode('_', $newRequestSegments);
     }
    
    public function init() {
        $requestPath = $this->getRequestPath();
        $this->setOriginalRequestPath($requestPath);
        $requestParts = explode('/', $requestPath);
        $moduleName = $requestParts[0];
        $controllerName = (isset($requestParts[1]) && !empty($requestParts[1])) ? $requestParts[1] : 'index';
        $actionName = (isset($requestParts[2]) && !empty($requestParts[2])) ? $requestParts[2] : 'index';
        $this->setModuleName($moduleName);
        $this->setControllerName($controllerName);
        $this->setActionName($actionName);
        $this->setRequestPath(implode('_', array($moduleName, $controllerName, $actionName)));
        return $this;
    }
}
?>
