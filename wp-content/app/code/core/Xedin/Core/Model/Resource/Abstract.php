<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1 
 */
abstract class Xedin_Core_Model_Resource_Abstract extends Varien_Object {

	/** @var Zend_Db_Adapter_Abstract */
    protected $_read;
	
	/** @var Zend_Db_Adapter_Abstract */
    protected $_write;
	
    protected $_tableName;
	
	protected $_tableDescription;
	

    public function __construct($readName='core_read', $writeName='core_write') {
        $this->_read = $this->getResource()->getConnection($readName);
        $this->_write = $this->getResource()->getConnection($writeName);
		$this->_construct();
    }

    public function getResource() {
        return Hub::getSingleton('core/resource');
    }

    public function getSelect() {
        return $this->_read->select();
    }
	
	public function getReadAdapter() {
		return $this->_getReadAdapter();
	}
	
	public function getWriteAdapter() {
		return $this->_getWriteAdapter();
	}

    protected function _getReadAdapter() {
        return $this->_read;
    }

	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
    protected function _getWriteAdapter() {
        return $this->_write;
    }

	/**
	 * @param string $tableName
	 * @param string $idFieldName
	 * @return \Xedin_Core_Model_Resource_Abstract 
	 */
    protected function _init($tableName = null, $idFieldName='id') {
        $this->_tableName = $tableName;
        $this->_idFieldName = $idFieldName;
		return $this;
    }

    public function getTableName() {
        return $this->_tableName;
    }

	public function getTableDescription() {
		if( !$this->_tableDescription ) {
			$this->_tableDescription = $this->getReadAdapter()->describeTable($this->getTableName());
		}

		return $this->_tableDescription;
	}
	
	abstract public function load(Xedin_Core_Model_Abstract $object, $value=null, $field=null);
	
	public function save() {
		return $this;
	}
	
	public function getTimeGmt($date = null) {
		return $this->getDatetimeHelper()->getTimeGmt($date);
	}
	
	public function getFormattedTimeStamp($timestamp = null, $format = null) {
		if( is_null($format) ) {
			$format = $this->getDatetimeHelper()->getMysqlDatetimeFormat();
		}
		return $this->getDatetimeHelper()->getFormattedTimeStamp($timestamp, $format);
	}

	/**
	 * @return Xedin_Core_Helper_Datetime
	 */
	public function getDatetimeHelper() {
		return Hub::getHelper('core/datetime');
	}
}