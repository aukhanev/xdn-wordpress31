<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Resource_Relation_Abstract extends Xedin_Core_Model_Resource_Mysql {
	
	protected $_parentIdFieldName = 'parent_id';
	protected $_childIdFieldName = 'child_id';
	
	protected $_parentUri;
	protected $_childUri;
	
	/**
	 * @param string $fieldName Name of the field containing child entity ID.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	public function setParentIdFieldName($fieldName) {
		$this->_parentIdFieldName = (string)$fieldName;
		return $this;
	}
	
	public function getParentIdFieldName() {
		return $this->_parentIdFieldName;
	}
	
	/**
	 * @param string $fieldName Name of the field containing parent entity ID.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	public function setChildIdFieldName($fieldName) {
		$this->_childIdFieldName = (string)$fieldName;
		return $this;
	}
	
	public function getChildIdFieldName() {
		return $this->_childIdFieldName;
	}
	
	/**
	 * @param string $modelUri URI of the parent entity model.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	protected function _setParentUri($modelUri) {
		$this->_parentUri = $modelUri;
		return $this;
	}
	
	public function getParentUri() {
		return $this->_parentUri;
	}
	
	/**
	 * @param string $modelUri URI of the child entity model.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	protected function _setChildUri($modelUri) {
		$this->_childUri = $modelUri;
		return $this;
	}
	
	public function getChildUri() {
		return $this->_childUri;
	}
	
	/**
	 * @return Xedin_Core_Model_Abstract A singleton of the parent model.
	 */
	public function getParentModel() {
		return Hub::getSingleton($this->getParentUri());
	}
	
	/**
	 * @return Xedin_Core_Model_Abstract A singleton of the child model.
	 */
	public function getChildModel() {
		return Hub::getSingleton($this->getChildUri());
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection A collection of parent entity models.
	 */
	public function getParentCollection() {
		return $this->getParentModel()->getCollection();
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection A collection of child entity models.
	 */
	public function getChildCollection($parentId = null) {
		$collection = $this->getChildModel()->getCollection();
		
		if( empty($parentId) ) {
			return $collection;
		}
		
		$childModelTable = $this->getChildModel()->getTableName();
		$childModelIdField = $this->getChildModel()->getIdFieldName();
		$join = $childModelTable . '.' . $childModelIdField . ' = ' . $this->getTableName() . '.' . $this->getChildIdFieldName();
//		$collection->getSelect()->joinRight($this->getTableName(), $join, array())
//			->where($this->getTableName() . '.' . $this->getParentIdFieldName() . ' = ?', $parentId);
		$collection->joinLeft($this->getTableName(), $join, array());
		$collection->getSelect()
			->where($this->getTableName() . '.' . $this->getParentIdFieldName() . ' = ?', $parentId);
		$profiler = $collection->getProfiler()->setEnabled(true);
//		$collection->load();
//		var_dump($collection->getItems());
//		exit();
//		var_dump($profiler->getQueryProfiles());
//		exit();
		return $collection;
	}
	
	/**
	 * @param null|mixed $default
	 * @return Xedin_Core_Model_Abstract|null
	 */
	public function getParent($default = null) {
		$parentCollection = $this->getParentCollection();
		if( $parentCollection->count() ) { 
			return $parentCollection->getFirstItem();
		}
		
		return $default;
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection.
	 * @see getParentCollection().
	 */
	public function getParents() {
		return $this->getParentCollection();
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection
	 * @see getChildCollection().
	 */
	public function getChildren($parentId = null) {
		return $this->getChildCollection($parentId);
	}
	
	public function createRelations($parent, $children) {
		if( empty($children) ) {
			return $this;
		}
		
		$parent = $this->_normalizeParent($parent);
		$children = $this->_normalizeChildren($children);
		
		$conn = $this->_getWriteAdapter();
		$data = array(
			$this->getParentIdFieldName()			=>	$parent
		);
		
		foreach( $children as $_id => $_child ) {
			$data[$this->getChildIdFieldName()]	= $_child;
			$conn->insert($this->getTableName(), $data);
		}
		return $this;
	}
	
	public function removeRelations($parent, $children = null, $except = false) {
		$parent = $this->_normalizeParent($parent);
		$children = $this->_normalizeChildren($children);
		
		$conn = $this->_getWriteAdapter();
		$conditions = array();
		$conditions[] = $conn->quoteInto($this->getParentIdFieldName() . ' = ?', $parent);
		if( !empty($children) ) {
			$conditions[] = $conn->quoteInto($this->getChildIdFieldName() . ($except ? ' NOT' : '') . ' IN (?)', $children);
		}
		
		$conn->delete($this->getTableName(), $conditions);
		return $this;
	}
	
	public function setRelations($parent, $children) {
		$this->removeRelations($parent);
		$this->createRelations($parent, $children);
		
		return $this;
	}
	
	protected function _normalizeChildren($children) {
		if( $children instanceof Xedin_Core_Model_Collection_Abstract ) {
			$children = $children->getIds();
		}
		
		if( $children instanceof Xedin_Core_Model_Abstract ) {
			$children = $children->getId();
		}
		
		if( !is_array($children) && !empty($children) ) {
			$children = array($children);
		}
		
		return $children;
	}
	
	protected function _normalizeParent($parent) {
		if( $parent instanceof Xedin_Core_Model_Abstract ) {
			$parent = $parent->getId();
		}
		
		return $parent;
	}
}