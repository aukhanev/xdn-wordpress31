<?php
/**
 * This base abstract class allows viewing config entries as objects (models),
 * arrange them in collections and in most cases treat them as any other model:
 * an object with state.
 * 
 * For Config Instance resource models, the object state is usually loadable,
 * but in most cases not savable.
 *
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
abstract class Xedin_Core_Model_Resource_Config_Instance_Abstract extends Xedin_Core_Model_Resource_Abstract {
	
	protected $_basePath;
	protected $_config;
	
	protected function _init($basePath, $config = null) {
		$this->_basePath = $basePath;
		
		if( is_null($config) ) {
			$config = Hub::getConfig();
		}
		
		$this->_config = $config;
	}
	
	/**
	 *
	 * @return Xedin_Config
	 */
	public function getConfig() {
		return $this->_config;
	}
	
	public function getBasePath($path = '') {
		$path = trim($path);
		if( $path === '' ) {
			$path = $this->_basePath;
		}
		else {
			$path = $this->_basePath . '/' . $path;
		}
		return $path;
	}
	
	public function load(Xedin_Core_Model_Abstract $object, $value=null, $field=null) {
		$object = $this->_beforeLoad($object);
		try {
			$value = $this->getConfig()->getValue($this->getBasePath($value));
			if( !$value ) {
				return $object;
			}
			$object->setId($value)
					->addData( $value );
		}
		catch( Exception $e ) {
			throw Hub::exception('Xedin_Core', $e->getMessage());
		}
		
		$object = $this->_afterLoad($object);
		return $this;
	}
	
	protected function _beforeLoad(Xedin_Core_Model_Abstract $object) {
		return $object;
	}
	
	protected function _afterLoad(Xedin_Core_Model_Abstract $object) {
		return $object;
	}
}