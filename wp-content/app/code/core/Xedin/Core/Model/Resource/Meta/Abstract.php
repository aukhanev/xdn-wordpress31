<?php

/**
 * Description of Abstract
 *
 * @version 0.1.1
 * + $_metaTableForeignKeyName now defaults to 'entity_id'.
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Resource_Meta_Abstract extends Xedin_Core_Model_Resource_Mysql {

	protected $_tableDescription;
	protected $_metaTableForeignKeyName = 'entity_id';
	protected $_metaTableKeyFieldName = 'key';
	protected $_metaTableValueFieldName = 'value';
	protected $_metaTableAttributeSetIdFieldName = 'attribute_set_id';
	protected $_attributeSetIdFieldName = 'attribute_set_id';


	protected function _getMetaResourceModel(Xedin_Core_Model_Meta_Abstract $model) {
		return $model->getMetaResourceModel();
	}
	
	protected function _getMetaModel(Xedin_Core_Model_Meta_Abstract $model) {
		return $model->getMetaModel()->unsetData();
	}

	protected function _getMetaCollection(Xedin_Core_Model_Meta_Abstract $model) {
		return $model->getMetaCollection();
	}
	
	protected function _setMetaTableForeignKeyName($metaTableForeignKeyName) {
		$this->_metaTableForeignKeyName = $metaTableForeignKeyName;
		return $this;
	}

	public function getMetaTableForeignKeyName() {
		return $this->_metaTableForeignKeyName;
	}
	
	protected function _setMetaTableKeyFieldName($keyFieldName) {
		$this->_metaTableKeyFieldName = $keyFieldName;
		return $this;
	}
	
	public function getMetaTableKeyFieldName() {
		return $this->_metaTableKeyFieldName;
	}
	
	protected function _setMetaTableValueFieldName($valueFieldName) {
		$this->_metaTableValueFieldName = $valueFieldName;
		return $this;
	}
	
	public function getMetaTableValueFieldName() {
		return $this->_metaTableValueFieldName;
	}

	protected function _afterLoad(Xedin_Core_Model_Meta_Abstract $model) {
		$model = parent::_afterLoad($model);

//		var_dump($model->getData());
		
		$this->loadMeta($model);
		
		return $model;
	}
	
	public function loadMeta(Xedin_Core_Model_Meta_Abstract $model) {
		if( !$model->hasData() ) {
			return $this;
		}
		
		$collection = $this->_getMetaCollection($model);
		if( !($collection instanceof Xedin_Core_Model_Collection_Abstract) ) {
			throw Hub::exception('Xedin_Core', 'Meta collection must be an instance of Xedin_Core_Model_Collection_Abstract. Attempted to retreive in ' . get_class($this));
		}
//		$profiler = $collection->getConnection()->getProfiler()->$profiler->setEnabled(true);
		$collection
				->reset()
				->addFieldToFilter($this->getMetaTableForeignKeyName($model), $model->getId())
				->load();
//		var_dump($profiler->getQueryProfiles());
		foreach( $collection->getItems() as $_id => $_model ) {
//			var_dump($_model->getData());
//			var_dump('===================================================');
			/* @var $_model Xedin_Core_Model_Meta_Abstract */
			if( !$model->hasData($_model->getData($this->getMetaTableKeyFieldName())) ) {
				$model->setData($_model->getData($this->getMetaTableKeyFieldName()), $_model->getData($this->getMetaTableValueFieldName()));
			}
		}
		
		$model->isMetaLoaded(true);
		
		return $model;
	}

	protected function _beforeSave(Xedin_Core_Model_Meta_Abstract $model) {
		
		/** @var Xedin_Core_Model_Meta_Abstract $model */
		$model = parent::_beforeSave($model);

		foreach( $model->getData() as $_key => $_value ) {
			if( !$this->isStaticField($_key) ) {
				$model->setMetadata($_key, $_value)
						->unsetData($_key);
			}
		}
		
		// Setting Attribute Set ID by it's code
		$attributeSetIdFieldName = $this->getAttributeSetIdFieldName();
		$attributeSetId = $model->getData($attributeSetIdFieldName);
		if( !is_numeric($attributeSetId) ) {
			$attributeSetId = $this->getAttributeSetModel($attributeSetId)->getId();
			if( $attributeSetId ) {
				$model->setData($attributeSetIdFieldName, $attributeSetId);
			}
		}

		return $model;
	}

	protected function _afterSave(Xedin_Core_Model_Meta_Abstract $model) {
		$model = parent::_afterSave($model);
		/* @var $model Xedin_Core_Model_Meta_Abstract */
		$foreignKeyName = $this->getMetaTableForeignKeyName($model);
		
		if( $model->isDeleted() ) {
			$model->getMetaModel()->getCollection()->addFieldToFilter($foreignKeyName, $model->getId())
					->delete();
			return $this;
		}
		
		if( !$model->hasMetadata() ) {
			return $model;
		}
		

		// Saving metadata
		foreach( $model->getMetadata() as $_key => $_value ) {
			if( $model->hasMetaToRemove($_key) ) {
				continue;
			}
			
			// Try to get the existing metadata...
			$metaModel = $this->_getMetaCollection($model)
					->reset()
					->addFieldToFilter($this->getMetaTableKeyFieldName(), $_key)
					->addFieldToFilter($foreignKeyName, $model->getId())
					->getFirstItem();
			
			// If none exists, create new one
			if( !$metaModel ) {
				$metaModel = $this->_getMetaModel($model);
			}
			$metaModel->addData(array(
						$foreignKeyName			=>	$model->getId(),
						$this->getMetaTableKeyFieldName()	=>	$_key,
						$this->getMetaTableValueFieldName()	=>	$_value
					))
					->save();
		}
		
		if( $model->hasMetaToRemove() ) {
			// Removing meta...
			$this->_getMetaCollection($model)
					->reset()
					->addFieldToFilter($foreignKeyName, $model->getId())
					->addFieldToFilter($this->getMetaTableKeyFieldName(), $model->getMetaToRemove())
					->delete();
		}
		
		return $model;
	}
	
	public function getMetaTableAttributeSetIdFieldName() {
		return $this->_metaTableAttributeSetIdFieldName;
	}
	
	protected function _setMetaTableAttributeSetIdFieldName($fieldName) {
		$this->_metaTableAttributeSetIdFieldName = $fieldName;
		return $this;
	}
	
	public function getAttributeSetIdFieldName() {
		return $this->_attributeSetIdFieldName;
	}

	protected function _setAttributeSetIdFieldName($fieldName) {
		$this->_attributeSetIdFieldName = $fieldName;
		return $this;
	}
	
	protected function getAttributeSetModel($attributeSetId = null) {
		$attributeSet = Hub::getSingleton('eav/attribute_set');
		if( !is_null($attributeSetId) ) {
			$attributeSet->load($attributeSetId);
		}
		
		return $attributeSet;
	}
}