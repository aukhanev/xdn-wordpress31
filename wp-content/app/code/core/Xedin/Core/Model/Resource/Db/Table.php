<?php

/**
 * Description of Table
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Resource_Db_Table extends Xedin_Core_Model_Resource_Db_Abstract {
	
	public function setTableName($tableName) {
		$this->_tableName = $tableName;
		return $this;
	}
	
	public function setId($id) {
		$this->setTableName();
		return $this;
	}
	
	public function getId() {
		return $this->getTableName();
	}
	
	public function load($id = null) {
		if( is_null($id) ) {
			$id = $this->getId();
		}
	}
}