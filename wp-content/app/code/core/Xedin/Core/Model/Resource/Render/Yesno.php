<?php

/**
 * Description of Yesno
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Resource_Render_Yesno extends Xedin_Core_Model_Resource_Config_Instance_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('modules/Xedin_Core/render/yesno');
	}
}