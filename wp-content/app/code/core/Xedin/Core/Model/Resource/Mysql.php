<?php

class Xedin_Core_Model_Resource_Mysql extends Xedin_Core_Model_Resource_Abstract {

	protected $_fieldNames = array();
	
	
    public function __construct($readName='core_read', $writeName='core_write') {
        parent::__construct($readName, $writeName);
    }

    public function load(Xedin_Core_Model_Abstract $object, $value=null, $field=null) {
		try {

			if (is_null($field)) {
				$field = $this->getIdFieldName();
			}
			
			$this->_beforeLoad($object);

			$object->unsetData();
			$connection = $this->_getWriteAdapter();
			$profiler = $connection->getProfiler()->setEnabled(true);
			
			$results = $connection->fetchRow(
									$this->getSelect()
									->from($this->getTableName())
									->where($field . ' = ?', $value)
					);

			if( !empty($results) ) {
				$object->setData($results);
			}
		}
		catch( Zend_Db_Adapter_Exception $e ) {
			$lastQueryProfile = $profiler->getLastQueryProfile();
			$errorText = $e->getMessage() . "\n" . ($lastQueryProfile ? 'Query: ' . $lastQueryProfile->getQuery() : '');
			throw Hub::exception('Xedin_Core', $errorText);
		}
			
//		$profiler->setEnabled(false);
		$this->_afterLoad($object);
		
        return $this;
    }

    public function save(Xedin_Core_Model_Abstract $object) {
		
		$object = $this->_beforeSave($object);
		$data = $this->_prepareSaveData($object->getData());
		$connection = $this->_getWriteAdapter();
//		$profiler = $connection->getProfiler();
//		$profiler->setEnabled(true);
		
		try {
			if ($object->getId()) {
				if( $object->isDeleted() ) {
					$this->_afterSave($object);
					$this->_delete($object);
					return $this;
				}

				$this->_getWriteAdapter()
	//					->exec('SET foreign_key_checks = 0')
						->update($this->getTableName(),
								$data,
								$this->_getWriteAdapter()->quoteInto($this->getIdFieldName() . ' = ?', $object->getId()));
//				var_dump($profiler->getLastQueryProfile());
			} else {
				$this->_getWriteAdapter()
						->insert($this->getTableName(), $data);
				$object->setId($this->_getWriteAdapter()->lastInsertId());
			}
		}
		catch( Zend_Db_Adapter_Exception $e ) {
			throw Hub::exception('Xedin_Core', $e->getMessage() . "\n" . 'Query: ' . $profiler->getLastQueryProfile()->getQuery());
		}
 
		$this->_afterSave($object);
		
        return $this;
    }

    protected function _delete(Xedin_Core_Model_Abstract $object) {
        $this->_getWriteAdapter()->delete($this->getTableName(),
                $this->_getWriteAdapter()->quoteInto('`' . $this->getIdFieldName() . '` = ?', $object->getId()));
        $object->unsData();

        return $this;
    }
	
	protected function _beforeLoad(Varien_Object $object) {
		return $object;
	}
	
	protected function _afterLoad(Varien_Object $object) {
		return $object;
	}
	
	protected function _beforeSave(Varien_Object $object) {
		return $object;
	}
	
	protected function _afterSave(Varien_Object $object) {
		return $object;
	}
	
	public function getStaticFieldNames() {
		return array_keys($this->getTableDescription());
	}

	public function isStaticField($fieldName) {
		return in_array($fieldName, $this->getStaticFieldNames());
	}
	
	protected function _prepareSaveData($data) {
		$preparedData = array();
		foreach( $data as $_key => $_value ) {
			if( $this->isStaticField($_key) ) {
				$preparedData[$_key] = $_value;
			}
		}
		
		return $preparedData;
	}
	
	public function getFieldNames($name = null, $default = null) {
		if( is_null($name) ) {
			return $this->_fieldNames;
		}
		
		if( !isset($this->_fieldNames[$name]) ) {
			return $default;
		}
		
		return $this->_fieldNames[$name];
	}
	
	public function getProfiler() {
		$connection = ($connection = $this->_getReadAdapter()) ? $connection : $this->_getWriteAdapter();
		if( !$connection ) {
			return null;
		}
		
		return $connection->getProfiler();
	}
}