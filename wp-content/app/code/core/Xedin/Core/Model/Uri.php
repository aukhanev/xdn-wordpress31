<?php

/**
 * Description of Uri
 *
 * @version 0.1.1
 * + Added prepareUrl() method.
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Uri extends Zend_Uri_Http {
	
	public function __construct($scheme = 'http', $schemeSpecific = '') {
		parent::__construct($scheme, $schemeSpecific);
		$this->_construct();
	}
	
	protected function _construct() {
		
	}
	
	/**
	 * @param string $scheme Either "http" or "https".
	 * @throws Xedin_Core_Exception
	 */
	public function setScheme($scheme) {
		$scheme = trim($scheme, '/:');
		
		if( !in_array($scheme, array('http', 'https')) ) {
			throw Hub::exception('Xedin_Core', 'The scheme must be either "http" or "https.');
		}
		
		$this->_scheme = $scheme;
		return $this;
	}
	
	public function parseUri($uri) {
		$uri = explode(':', $uri, 2);
		if( isset($uri[1]) ) {
			$this->setScheme($uri[0]);
		}
		
		$uri = isset($uri[1]) ? $uri[1] : $uri[0];
		
		parent::_parseUri($uri);
		return $this;
	}
	
	/**
	 * @param string $url
	 * @param bool $prepare
	 * @param string $urlType
	 * @param bool $convertSchema
	 * @return string
	 */
	public function prepareUrl($url, $prepare = true, $urlType = Hub::URL_TYPE_BASE, $convertSchema = true) {
		$url = trim($url);
		
	    if( $prepare ) {
            $value = Hub::getUrl($url, $urlType);
        }
		
		if( !$convertSchema ) {
			return $value;
		}
		
		$value = $this->getRequest()->isSecure() ?
			preg_replace('!^https?!', 'https', $url) :
			preg_replace('!^https?!', 'http', $url);
		
		return $value;
	}
	
    public function getRequest() {
        return Hub::app()->getController()->getRequest();
    }
}