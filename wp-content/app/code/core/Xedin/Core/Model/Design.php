<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
class Xedin_Core_Model_Design extends Xedin_Core_Model_Abstract {
	
	const XML_PATH_DESIGN_THEME = 'global/design/theme';
	const XML_PATH_DESIGN_TEMPLATE = 'global/design/template';
	
	const PATH_TEMPLATE = 'template/';
	const PATH_LAYOUT = 'layout/';
	
	const THEME_ROOT_FRONTEND = 'frontend';
	const THEME_ROOT_ADMIN = 'admin';
	
	
	public function getDefaultThemeName() {
		return Hub::getConfig(self::XML_PATH_DESIGN_THEME . '/default');
	}
	
	public function getDefaultTemplateName() {
		return Hub::getConfig(self::XML_PATH_DESIGN_TEMPLATE . '/default');
	}
	
	public function getTemplateName() {
		return $this->getDefaultTemplateName();
	}
	
	public function getThemeName() {
		return $this->getDefaultThemeName();
	}
	
	public function getTemplateRootPath($path = '') {
		return $this->getThemePath( $this->getTemplateName() . DS . $path );
	}
	
	public function getThemePath($path = '') {
		return Hub::getPath($this->getRelativeThemePath($path), Hub::PATH_TYPE_DESIGN);
	}
	
	public function getRelativeThemePath($path = '') {
		$themeRoot = $this->isAdmin() ? self::THEME_ROOT_ADMIN : self::THEME_ROOT_FRONTEND;
		return $themeRoot . DS . $this->getThemeName() . DS .  $path;
	}
	
	public function getRelativeTemplatePath($path) {
		return $this->getRelativeThemePath($this->getTemplateName() . DS . $path);
	}
	
	public function getTemplatePath($path = '') {
		return $this->getTemplateRootPath(self::PATH_TEMPLATE . $path);
	}
	
	public function getLayoutPath($path = '') {
		return $this->getTemplateRootPath(self::PATH_LAYOUT . $path);
	}
	
	public function getFrontController() {
		return Hub::app()->getFrontController();
	}
	
	/**
	 * @return Xedin_Core_Model_Layout
	 */
	public function getLayout() {
		$layoutKey = '_layout';
		
		if( !Hub::isRegistered($layoutKey) ) {
			Hub::register(Hub::getModel('core/layout'), $layoutKey);
			Hub::registry($layoutKey)->init(array(
				'request_path' => $this->getFrontController()->getNormalizedRequestPath()
			));
		}
		
		return Hub::registry($layoutKey);
	}
	
	public function isAdmin() {
		return Hub::app()->getController()->isAdmin();
	}
}