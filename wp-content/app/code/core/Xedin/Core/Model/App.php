<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 * + Added getEventController()
 */
class Xedin_Core_Model_App {

    protected $_frontController;
	protected $_design;
	
	/** @var Xedin_Core_Model_Event_Controller */
	protected $_eventController;
	protected $_eventControllerUri = 'core/event_controller';
	
	protected $_runCode;
	
	protected $_profiler;

    public function run() {
		
		
        $this->getFrontController()->dispatch();
		
		echo $this->getLayout()->getOutput();
    }

	/**
	 *
	 * @return Xedin_Controller_Front
	 */
    public function getFrontController() {
		if( !$this->_frontController ) {
			$this->_frontController = Hub::getLibrary('xedin_controller_front');
		}
		
        return $this->_frontController;
    }
	
	/**
	 * @return Xedin_Core_Controller_Abstract
	 */
	public function getController() {
		return $this->getFrontController()->getCurrentController();
	}
	
	/**
	 *
	 * @return Xedin_Core_Model_Design
	 */
	public function getDesign() {
		if( !$this->_design ) {
			$this->_design = Hub::getModel('core/design');
		}
		return $this->_design;
	}
	
	public function getLayout() {
		return $this->getDesign()->getLayout();
	}
	
	/**
	 * @return Xedin_Core_Model_Event_Controller
	 */
	public function getEventController() {
		if( empty($this->_eventController) ) {
			$this->_eventController = Hub::getSingleton( $this->_eventControllerUri );
		}
		
		return $this->_eventController;
	}
	
	/**
	 * @return Xedin_Profiler
	 */
	public function getProfiler() {
		if( !$this->_profiler ) {
			$this->_profiler = new Xedin_Profiler($this->getRunCode());
		}
		
		return $this->_profiler;
	}
	
	public function getRunCode() {
		if( !$this->_runCode ) {
			$this->_runCode = uniqid();
		}
		
		return $this->_runCode;
	}
}