<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Observer_Abstract extends Xedin_Core_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->setDefaultHandlerName('defaultHandler');
	}
	
	/**
	 * Handles an event.
	 * @param string $eventName Name of the event being handled.
	 * @param Object $caller Object that called the event.
	 * @param string $handlerName Name of handler function.
	 * @param array $args Array of arguments to the handler function.
	 */
	public function handleEvent($eventName, Object $caller, $handlerName, $args = array()) {
		if( !method_exists($this, $handlerName) ) {
			$this->_executeHandler($eventName, $caller, $this->getDefaultHandlerName(), $args);
			return null;
		}
		
		return $this->_executeHandler($eventName, $caller, $handlerName, $args);
	}
	
	public function defaultHandler($eventName, Object $caller, $args = array()) {
		return null;
	}
	
	protected function _executeHandler($eventName, Object $caller, $handlerName, $args = array()) {
		if( !method_exists($this, $handlerName) ) {
			throw Hub::exception('Xedin_Core', 'Observer "' . get_class($this) . '" does not have a handle called "' . $handlerName . '"');
		}
		
		array_unshift($args, $caller, $eventName);
		return call_user_func_array(array($this, $handlerName), $args);
	}
}