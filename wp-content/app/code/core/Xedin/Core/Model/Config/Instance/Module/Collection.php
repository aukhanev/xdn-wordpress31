<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Config_Instance_Module_Collection extends Xedin_Core_Model_Config_Instance_Collection {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('core/config_instance_module');
	}
}