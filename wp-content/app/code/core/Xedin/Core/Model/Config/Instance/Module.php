<?php

/**
 * Description of Module
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Config_Instance_Module extends Xedin_Core_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('core/config_instance_module');
	}
}