<?php
/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Config_Instance_Collection extends Xedin_Core_Model_Collection_Abstract {
	
	protected function _init($modelUri) {
		$this->_modelPath = $modelUri;
		return $this;
	}
	
	protected function _load() {
		$resourceModel = $this->getModel()->getResourceModel();
		if( !($resourceModel instanceof Xedin_Core_Model_Resource_Config_Instance_Abstract) ) {
			throw Hub::exception('Xedin_Core', 'A config instance collection\'s resource model must be an instance of Xedin_Core_Model_Resource_Config_Instance_Abstract');
		}
		
		/* @var $resourceModel Xedin_Core_Model_Resource_Config_Instance_Abstract */
//		var_dump(Hub::getConfig()->getValue());
		$configValue = (array)$resourceModel->getConfig()->getValue($resourceModel->getBasePath());
		if( is_null($configValue) ) {
			$this->_isLoaded = true;
			return $this;
		}
		
//		var_dump($configValue);
		foreach($configValue as $_id => $_config) {
//		var_dump($_config);
			$model = Hub::getModel($this->getModelPath());
			$this->_items[$_id] = $model->setData((array)$_config);
//		var_dump(0-
			$this->_ids[] = $_id;
		}

		$this->_isLoaded = true;
		return $this;
	}
}