<?php

/**
 * Description of Event
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Event extends Xedin_Core_Model_Abstract {
	
	protected $_idFieldName = 'code';
	protected $_defaultObserverUri = 'core/event_observer';
	
	/** @var Xedin_Core_Model_Event_Observer_Collection */
	protected $_observerCollection;
	
	protected $_handlers = array();
	protected $_caller;
	
	/**
	 * @return Xedin_Core_Model_Event_Collection 
	 */
	public function getCollection() {
		return Hub::getModel('core/event_collection');
	}
	
	/**
	 * @return Xedin_Core_Model_Event_Observer_Collection
	 */
	public function getObserverCollection() {
		if( !$this->_observerCollection ) {
			$this->_observerCollection = $this->getObserverModel()->getCollection();
		}
		
		return $this->_observerCollection;
	}
	
	/**
	 * @return Xedin_Core_Model_Event_Observer_Abstract
	 */
	public function getObserverModel() {
		return Hub::getSingleton( $this->getObserverUri() );
	}

	/**
	 * Retrieves observer_uri data value, if it is set.
	 * Otherwise, the default Observer URI for this model.
	 * 
	 * @return string The URI of an Observer model.
	 */
	public function getObserverUri() {
		if( $observerUri = $this->getData('observer_uri') ) {
			return $observerUri;
		}
		
		return $this->_defaultObserverUri;
	}
	
	/**
	 * Adds an observer to the event.
	 * 
	 * @param Xedin_Core_Model_Event_Observer_Abstract|null $observer The Observer to add, or null to create new Observer. Default: null.
	 * @param array|null $data The data to populate the new observer with. Default: null.
	 * @return \Xedin_Core_Model_Event This instance.
	 */
	public function addObserver($observer = null, $data = null) {
		$this->getObserverCollection()->addObserver($observer, $data);
		return $this;
	}
	
	public function dispatch($args = null) {
		foreach( $this->getObserverCollection()->getItems() as $_observerCode => $_observer ) {
			/* @var $_observer Xedin_Core_Model_Event_Observer_Abstract */
			if( !($handler = $this->getHandler($_observerCode)) ) {
				throw Hub::exception('Xedin_Core', 'No handler found for observer "' . $_observerCode . '" in event "' . $this->getId() . '".');
			}
			$_observer->handleEvent($this, $handler, $args);
		}
		
		return $this;
	}
	
	public function addHandler($observerCode, $methodName) {
		if( !$this->hasHandler($observerCode) ) {
			$this->setHandler($observerCode, $methodName);
		}
		
		return $this;
	}
	
	public function hasHandler($observerCode) {
		return isset( $this->_handlers[$observerCode] );
	}
	
	public function setHandler($observerCode, $methodName) {
		$this->_handlers[$observerCode] = $methodName;
		return $this;
	}
	
	public function getHandler($observerCode = null, $default = null) {
		if( is_null($observerCode) ) {
			return $this->_handlers;
		}
		
		if( !isset($this->_handlers[$observerCode]) ) {
			return $default;
		}
		
		return $this->_handlers[$observerCode];
	}
	
	public function setCaller(Xedin_Object $caller) {
		$this->_caller = $caller;
		return $this;
	}
	
	public function getCaller() {
		return $this->_caller;
	}
}