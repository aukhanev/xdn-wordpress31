<?php

/**
 * Description of Text
 *
 * @author anton
 */
class Xedin_Core_Model_Render_Fields_Text extends Xedin_Core_Model_Render_Fields_Abstract {
	
	protected $_blockUri = 'html/element_input_text';
	protected $_modelUri;
	
	protected function _render($model, $name, $id) {
		if( $this->isRendered() ) {
			return $this;
		}
		
		$value = $model;
		if( $value instanceof Varien_Object ) {
			$value = $value->getData($this->getValueFieldName());
		}
		
		$this->setLabel($value);
		$block = $this->getBlock();
		$block->setName($name)
				->setValue($value)
				->setId($id);
		$this->setField($block->toHtml());
		$this->_isRendered;
		
		return $this;
	}
}