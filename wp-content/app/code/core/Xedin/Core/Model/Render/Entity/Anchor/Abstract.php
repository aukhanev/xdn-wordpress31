<?php

/**
 * Description of Anchor
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Render_Entity_Anchor_Abstract extends Xedin_Core_Model_Render_Fields_Abstract {
	
	protected $_blockUri = 'html/element_anchor';
	protected $_hrefMask;
	
	protected function _render($model, $name, $id) {
		if( $this->isRendered() ) {
			return $this;
		}
		
		if( !$this->hasBlockUri() ) {
			return '';
		}
		
		$value = $model;
		if( $value instanceof Varien_Object ) {
			$value = $value->getData($this->getValueFieldName());
		}
		
		$value = (int)$value;
		
		$foreignModel = $this->getModel()->load($value);
		$label = $foreignModel->getData($this->getLabelFieldName());
		if( is_null($label) ) {
			throw Hub::exception('Xedin_Html', 'The field specified as label field name (' . $this->getLabelFieldName() . ') does not exist in ' . get_class($foreignModel) . '.');
		}
		
		$block = $this->getBlock();
		$block->setName($name)
				->setContent($label)
				->setId($id);
		$model = $this->getModel()->load($value);
		$block->setHref($this->getEntityUrl($value));
		$this->setLabel($block->toHtml());
		$this->setField( $block->toHtml() );
		$this->_isRendered;
		
		return $this;
	}
	
	public function getHrefMask() {
		return $this->_hrefMask;
	}
	
	public function getEntityUrl($id) {
		$url = null;
		$controller = $this->getController();
		$params = array('id' => $id);
		if( $controller instanceof Xedin_Adminhtml_Controller_Abstract ) {
			$url = $controller->getAdminRedirectUrl($this->getHrefMask(), true, $params);
		}
		else {
			$url = $controller->getRedirectUrl($this->getHrefMask(), true, $params);
		}
		
		return $url;
	}
}