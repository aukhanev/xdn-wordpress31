<?php

/**
 * Description of Yesno
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Render_Select_Yesno extends Xedin_Adminhtml_Model_Render_Fields_Select_Abstract {
	protected $_modelUri = 'core/render_yesno';
}