<?php

/**
 * Description of Abstract
 *
 * @author anton
 */
abstract class Xedin_Core_Model_Render_Values_Abstract extends Xedin_Core_Model_Abstract {
	
	protected $_fieldModelUri;
	
	const DATA_FIELD_VALUE = 'value';
	const DATA_FIELD_ID = 'id';
	
	public function setId($id) {
		$this->setData(self::DATA_FIELD_ID);
	}
	
	public function getId() {
		return $this->getData(self::DATA_FIELD_ID);
	}
	
	public function hasId() {
		return $this->hasData(self::DATA_FIELD_ID);
	}
	
	public function setValue($value) {
		$this->setData(self::DATA_FIELD_VALUE, $value);
		return $this;
	}
	
	public function getValue() {
		return $this->getData(self::DATA_FIELD_VALUE);
	}
	
	public function hasValue() {
		return $this->hasData(self::DATA_FIELD_VALUE);
	}
	
	public function getRenderedValue() {
		return $this->_render( $this->getValue() );
	}

	protected function _render($value) {
		return $value;
	}
	
	protected function _setFieldModelUri($fielModelUri) {
		$this->_fieldModelUri = $fielModelUri;
		return $this;
	}
	
	public function getFieldModelUri() {
		return $this->_fieldModelUri;
	}
	
	public function getFieldModel($id = null) {
		if( !$this->hasValue() ) {
			return null;
		}
		
		$model = Hub::getSingleton( $this->getFieldModelUri() );
		
		if( is_null($id) ) {
			return $model;
		}
		
		return $model->load( $this->getValue() );
	}
}