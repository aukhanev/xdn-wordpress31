<?php

/**
 * Description of Yesno
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Render_Yesno extends Xedin_Core_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('core/render_yesno');
	}
}