<?php

/**
 * Description of Abstract
 *
 * @author anton
 */
class Xedin_Core_Model_Render_Fields_Abstract extends Xedin_Core_Model_Abstract {
	const DATA_FIELD_VALUE = 'value';
	const DATA_FIELD_FIELD = 'field';
	const DATA_FIELD_NAME = 'name';
	const DATA_FIELD_ID = 'id';
	const DATA_FIELD_LABEL = 'label';
	
	protected $_blockUri;
	protected $_modelUri;
	
	protected $_isRendered = false;
	
	protected $_labelFieldName = 'label';
	protected $_valueFieldName = 'id';
	
	protected $_value;
	
	public function getLabelFieldName() {
		return $this->_labelFieldName;
	}
	
	protected function _setLabelFieldName($fieldName) {
		$this->_labelFieldName = $fieldName;
		return $this;
	}
	
	public function setValue($value) {
		$this->_value = $value;
		return $this;
	}
	
	public function getValue() {
		return $this->_value;
	}
	
	public function hasValue() {
		return (bool)$this->_value;
	}
	
	public function setName($name) {
		$this->setData(self::DATA_FIELD_NAME, $name);
		return $this;
	}
	
	public function getName() {
		return $this->getData(self::DATA_FIELD_NAME);
	}
	
	public function hasName() {
		return $this->hasData(self::DATA_FIELD_NAME);
	}
	
	public function getId() {
		return $this->getData(self::DATA_FIELD_ID);
	}
	
	public function setId($id) {
		$this->setData(self::DATA_FIELD_ID, $id);
		return $this;
	}
	
	public function hasId() {
		return $this->hasData(self::DATA_FIELD_ID);
	}
	
	public function getField() {
		return $this->getData(self::DATA_FIELD_FIELD);
	}
	
	public function setLabel($label) {
		$this->setData(self::DATA_FIELD_LABEL, $label);
		return $this;
	}
	
	public function getLabel() {
		return $this->getData(self::DATA_FIELD_LABEL);
	}
	
	public function hasLabel() {
		return $this->hasData(self::DATA_FIELD_LABEL);
	}
	
	public function getRenderedField() {
		return $this->_render( $this->getValue(), $this->getName(), $this->getId() )->getField();
	}
	
	public function getRenderedLabel() {
		return $this->_render( $this->getValue(), $this->getName(), $this->getId() )->getLabel();
	}
	
	public function setBlockUri($blockUri) {
		$this->_blockUri = $blockUri;
		return $this;
	}
	
	public function getBlockUri() {
		return $this->_blockUri;
	}
	
	public function hasBlockUri() {
		return !empty($this->_blockUri);
	}
	
	/**
	 *
	 * @return Xedin_Core_Block_Template
	 */
	public function getBlock() {
		if( !$this->hasBlockUri() ) {
			return null;
		}
		
		return Hub::app()->getLayout()->createBlock($this->getBlockUri());
	}
	
	public function getModelUri() {
		return $this->_modelUri;
	}
	
	public function setModelUri($modelUri) {
		$this->_modelUri = $modelUri;
		return $this;
	}
	
	/**
	 * @return Xedin_Core_Model_Abstract
	 */
	public function getModel() {
		return Hub::getSingleton( $this->getModelUri() )->unsetData();
	}
	
	public function isRendered() {
		return $this->_isRendered;
	}

	protected function _render($model, $name, $id) {
		if( $this->isRendered() ) {
			return $this;
		}
		
		if( !$this->hasBlockUri() ) {
			return '';
		}
		
		$value = $model;
		if( $value instanceof Varien_Object ) {
			$value = $value->getData($this->getValueFieldName());
		}
		
		$value = (int)$value;
		
		$block = $this->getBlock();
		$block->setName($name)
				->setValue($value)
				->setId($id);
		
		$this->setLabel($this->getModel()->load($value)->getData($this->getLabelFieldName()));
		$this->setField( $block->toHtml() );
		$this->_isRendered;
		
		return $this;
	}
	
	public function getValueFieldName() {
		return $this->_valueFieldName;
	}
	
	public function setValueFieldName($fieldName) {
		$this->_valueFieldName = $fieldName;
		return $this;
	}
	
	public function getController() {
		return Hub::app()->getController();
	}
	
	public function getRequest() {
		return $this->getController()->getRequest();
	}
}