<?php

/**
 * Description of Datetime
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Render_Fields_Datetime extends Xedin_Core_Model_Render_Fields_Text {
	
	protected $_blockUri = 'html/element_input_datetime';
	
	protected function _render($value, $name, $id) {
		if( $this->isRendered() ) {
			return $this;
		}
		
		if( is_object($value) ) {
			$value = $value->getData($this->getValueFieldName());
		}
		
		$this->setLabel($value);
		$block = $this->getBlock();
		$block->setName($name)
				->setValue($value)
				->setId($id)
				->setPickerOptions('showSecond', true);
		$this->setField($block->toHtml());
		$this->_isRendered;
		
		return $this;
	}
	
}