<?php

/**
 * Description of Text
 *
 * @author anton
 */
class Xedin_Core_Model_Render_Fields_Textarea extends Xedin_Core_Model_Render_Fields_Abstract {
	
	protected $_blockUri = 'html/element_input_textarea';
	protected $_modelUri;
	protected $_valueFieldName = 'text';
	
	protected function _render($value, $name, $id) {
		
		$block = $this->getBlock();
		if( $value instanceof Varien_Object ) {
			$this->setLabel($value->getData($this->getLabelFieldName()));
			$value = (string)$value->getData($this->getValueFieldName());
		}
		else {
			$this->setLabel($value);
		}
		$block->setName($name)
				->setContent($value)
				->setId($id);
		$this->setField($block->toHtml());
		$this->_isRendered = true;
		
		return $this;
	}
}