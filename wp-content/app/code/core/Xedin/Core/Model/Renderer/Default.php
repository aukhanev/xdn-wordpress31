<?php

/**
 * Default renderer for all models.
 * Will just output model data;
 */
class Xedin_Core_Model_Renderer_Default extends Xedin_Core_Model_Renderer_Abstract {
    
    public function getHtml() {
        $html = '';
        foreach( $this->getData() as $key => $value ) {
            $html .= $key . ': ' . $value . "\n";
        }
        
        return $html;
    }
}
?>
