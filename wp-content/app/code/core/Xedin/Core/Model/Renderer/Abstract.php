<?php

/**
 * This base class is intended to abstract model representation per type.
 * For example, you could output post content, or put it in a block. However,
 * there are times where blocks are not a solution, because they exist in a layout.
 * Rendeders are like blocks, only they exist as singletons only, and to not 
 * belong to any layout.
 * 
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
class Xedin_Core_Model_Renderer_Abstract extends Xedin_Object {
    
    protected $_model;
    
    protected $_template;
    
    protected $_isAutoLoadingModel = true;
    
    public function __construct($model = null) {
        if( !is_null($model) ) {
            $this->_model = $model;
        }
    }
    
    public function isAutoLoadingModel($isAutoLoadingModel = null) {
        if( is_null($isAutoLoadingModel) ) {
            return $this->_isAutoLoadingModel;
        }
        
        $this->_isAutoLoadingModel = (bool)$isAutoLoadingModel;
        return $this;
    }
    
    public function getTemplate() {
        return $this->_template;
    }
    
    /**
     *
     * @return Xedin_Core_Model_Abstract
     */
    public function getModel() {
        return $this->_model;
    }
    
    public function getBaseTemplateDirectory() {
        return Mage_Loader::getUrl('', Mage_Loader::URL_TYPE_THEME) . 'template' . '' . DIRECTORY_SEPARATOR;
    }
    
    public function getTemplatePath($path = '') {
        return $this->getBaseTemplateDirectory() . $path;
    }
    
    public function getData($key = '', $index = null) {
        if( empty($key) ) {
            return $this->getModel()->getData();
        }
        
        return $this->getModel()->getDataUsingMethod($key, $index);
    }
    
    public function render() {
        if( $this->isAutoLoadingModel() ) {
            $this->getModel()->load();
        }
        $this->setData($this->getModel()->getData());
        return $this->getHtml();
    }
    
    public function getHtml() {
        return $this->fetchView($this->getTemplate());
    }
    
    public function fetchView($templatePath = '') {
        if( empty($templatePath) ) {
            $templatePath = $this->getTemplate();
            if( is_null($templatePath) ) {
                return '';
            }
        }

        ob_start();

        try {
            include $this->getTemplatePath($templatePath);
        }
        catch( Exception $e ) {
            ob_get_clean();
            throw $e;
        }

        $html = ob_get_clean();
        return $html;
    }
}
?>
