<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.2
 * + $_metaModelName now defaults to resource model name with '_meta' at end.
 * 
 * 0.1.1
 * * Now extends Xedin_Core_Model_Relation_Abstract to facilitate future relations for any Meta model.
 */
abstract class Xedin_Core_Model_Meta_Abstract extends Xedin_Core_Model_Relation_Party_Abstract {
	
	protected $_metaModelName;
	protected $_isMetaLoaded;
	
	/** @var Xedin_Core_Model_Db_Collection */
	protected $_metaCollection;
	
	/** @var array */
	protected $_metaData = array();
	
	protected $_metaToRemove;
	
	
	protected $_attributeSetModelUri = 'eav/attribute_set';
	
	/**
	 * @param string $key
	 * @param mixed $value
	 * @return \Xedin_Core_Model_Meta_Abstract 
	 */
	public function setMetadata($key, $value = null) {
		if( is_null($value) ) {
			$this->_metaData = $key;
			return $this;
		}
		
		$this->_metaData[$key] = $value;
		return $this;
	}
	
	/**
	 * @param string $key
	 * @param null|mixed $default
	 * @return mixed
	 */
	public function getMetadata($key = null, $default = null) {
		if( is_null($key) ) {
			return $this->_metaData;
		}
		
		if( !isset($this->_metaData[$key]) ) {
			return $default;
		}
		
		return $this->_metaData[$key];
	}
	
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function hasMetadata($key = null) {
		if( is_null($key) ) {
			return !empty($this->_metaData);
		}
		
		return isset($this->_metaData[$key]);
	}
	
	/**
	 * @param string $key
	 * @return \Xedin_Core_Model_Meta_Abstract 
	 */
	public function unsMetadata($key = null) {
		if( is_null($key) ) {
			$this->_metaData = array();
			return $this;
		}
		
		if( isset($this->_metaData[$key]) ) {
			unset($this->_metaData[$key]);
		}
		
		return $this;
	}
	
	public function setMetaToRemove($key) {
		$this->_metaToRemove[$key] = true;
		return $this;
	}
	
	public function getMetaToRemove() {
		return array_keys($this->_metaToRemove);
	}
	
	public function hasMetaToRemove($metaKey = null) {
		if( is_null($metaKey) ) {
			return !empty($this->_metaToRemove);
		}
		
		return isset($this->_metaToRemove[$metaKey]);
	}
	
	/**
	 * @param string $fieldName
	 * @return \Xedin_Core_Model_Meta_Abstract 
	 */
//	public function setMetaTableForeignKeyName($fieldName) {
//		$this->getRe = $fieldName;
//		return $this;
//	}
	
	public function getMetaTableForeignKeyName() {
		return $this->getResourceModel()->getMetaTableForeignKeyName();
	}
	
	public function getMetaTableKeyFieldName() {
		return $this->getResourceModel()->getMetaTableKeyFieldName();
	}
	
	public function getMetaTableValueFieldName() {
		return $this->getResourceModel()->getMetaTableValueFieldName();
	}
	
	/**
	 * @param string $metaModelName
	 * @return \Xedin_Core_Model_Meta_Abstract 
	 */
	protected function _setMetaModelName($metaModelName) {
		$this->_metaModelName = $metaModelName;
		return $this;
	}
	
	public function getMetaModelName() {
		if( empty($this->_metaModelName) ) {
			$this->_metaModelName = $this->getResourceModelName() . '_meta';
		}
		return $this->_metaModelName;
	}
	
	/**
	 */
	public function getMetaModel() {
		return Hub::getSingleton($this->getMetaModelName());
	}
	
	public function getMetaResourceModel() {
		return $this->getMetaModel()->getResourceModel();
	}
	
	public function getMetaCollection() {
//		if( !$this->_metaCollection ) {
//			$this->_metaCollection = $this->getMetaModel()->getCollection();
//		}
//		
//		return $this->_metaCollection;
		return $this->getMetaModel()->getCollection();
	}
	
	public function loadMeta() {
		$this->getResourceModel()->loadMeta($this);
//		var_dump($this->getData());
		return $this;
	}
	
	public function getStaticFields() {
		return $this->getResourceModel()->getStaticFieldNames();
	}
	
	public function hasStaticField($fieldName) {
		return $this->getResourceModel()->isStaticField($fieldName);
	}
	
	public function getAttributeSetIdFieldName() {
		return $this->getResourceModel()->getAttributeSetIdFieldName();
	}
	
	/**
	 *
	 * @return Xedin_Eav_Model_Attribute_Set
	 */
	public function getAttributeSetModel() {
		return Hub::getSingleton($this->_attributeSetModelUri);
	}
	
	public function getAttributeSetModelUri() {
		return $this->_attributeSetModelUri;
	}
	
	public function getByAttribute($attribute, $value) {
		$collection = $this->getCollection()->addAttributeToFilter($attribute, $value)->load();
		if( $collection->count() ) {
			return $collection->getFirstItem();
		}
		return null;
	}
	
	public function isMetaLoaded($isMetaLoaded = null) {
		if( is_null($isMetaLoaded) ) {
			return (bool)$this->_isMetaLoaded;
		}
		
		$this->_isMetaLoaded = (bool)$isMetaLoaded;
		return $this;
	}
}