<?php

/**
 * The collection to be used for loading meta-models.
 * 
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Meta_Collection extends Xedin_Core_Model_Db_Collection {
	
//	protected $_attributesToSelect = array();
	protected $_attributesToFilter = array();
	protected $_isMetaAutoLoad = false;
	
//	public function addAttributeToSelect($attribute) {
//		if( is_array($attribute) ) {
//			foreach( $attribute as $_idx => $_attribute ) {
//				$this->addAttributeToSelect($_attribute);
//			}
//			return $this;
//		}
//		 
//		if( !$this->hasAttributesToSelect($attribute) ) {
//			$this->_attributesToSelect[$attribute] = true;
//		}
//		
//		return $this;
//	}
//	
//	public function removeAttributeFromSelect($attribute) {
//		if( is_array($attribute) ) {
//			foreach( $attribute as $_idx => $_attribute ) {
//				$this->removeAttributeFromSelect($_attribute);
//			}
//			return $this;
//		}
//		 
//		if( $this->hasAttributesToSelect($attribute) ) {
//			unset($this->_attributesToSelect[$attribute]);
//		}
//		
//		return $this;
//	}
//	
//	public function getAttributesToSelect() {
//		return array_keys($this->_attributesToSelect);
//	}
//	
//	public function hasAttributesToSelect($attribute = null) {
//		if( is_null($attribute) ) {
//			return (bool)count($this->_attributesToSelect);
//		}
//			
//		return array_key_exists($attribute, $this->_attributesToSelect);
//	}
	
	public function addAttributeToFilter($attribute, $value, $conditionType = 'eq', $or = false, $valueType = null) {
		if( is_array($attribute) ) {
			foreach( $attribute as $_idx => $_attribute ) {
				$this->addAttributeToFilter($_attribute);
			}
			return $this;
		}
		
		if( !array_key_exists($attribute, $this->_attributesToFilter) ) {
			$this->_attributesToFilter[$attribute] = array();
		}
		
		$this->_attributesToFilter[$attribute][] = array(
			'value'			=>	$value,
			'condition'		=>	$conditionType,
			'type'			=>	$valueType,
			'logical'		=>	$or
		);
		return $this;
	}
	
	public function removeAttributeFromFilter($attribute) {
		if( is_array($attribute) ) {
			foreach( $attribute as $_idx => $_attribute ) {
				$this->removeAttributeFromSelect($_attribute);
			}
			return $this;
		}
		 
		if( $this->hasAttributesToFilter($attribute) ) {
			unset($this->_attributesToFilter[$attribute]);
		}
		
		return $this;
	}
	
	public function getAttributesToFilter() {
		return $this->_attributesToFilter;
	}
	
	public function hasAttributesToFilter($attribute = null) {
		if( is_null($attribute) ) {
			return (bool)count($this->_attributesToFilter);
		}
			
		return array_key_exists($attribute, $this->_attributesToFilter);
	}
	
	protected function _getLoadSelect() {
        $select = parent::_getLoadSelect();
		$model = $this->getModel();
		
		if( !($model instanceof Xedin_Core_Model_Meta_Abstract) ) {
			return $select;
		}
		
		/* @var $model Xedin_Core_Model_Meta_Abstract */
		$nativeTableName = $model->getTableName();
		$metaTableName = $model->getMetaModel()->getTableName();
		$metaTableForeignKeyName = $model->getMetaTableForeignKeyName();
		$metaTableKeyFieldName = $model->getMetaTableKeyFieldName();
		$metaTableValueFieldName = $model->getMetaTableValueFieldName();
		
//		if( $this->hasAttributesToSelect() ) {
//			$select->from($metaTableName, $this->getAttributesToSelect())
//					->where($metaTableName . '.' . $metaTableforeignKeyName . ' = ' . $model->getTableName() . '.' . $model->getIdFieldName());
//		}
		
		if( $this->hasAttributesToFilter() ){
			$connection = $this->getConnection();
			$attributeFilterString = '';
			$i = 0;
			foreach( $this->getAttributesToFilter() as $_name => $_attributeConditions ) {
				foreach( $_attributeConditions as $_idx => $_conditionInfo ) {
//					var_dump($_conditionInfo);
					$valueConditionType = $this->getConditionType($_conditionInfo['condition']);
					$aliasSuffix = '';
					$logical = '';
					if( $i ) {
						$logical = ' ' . ($_conditionInfo['logical'] ? 'OR' : 'AND') . ' ';
						$aliasSuffix = '_' . ($i+1);
					}
					
					$keyField = $connection->quoteIdentifier($metaTableName . $aliasSuffix . '.' . $metaTableKeyFieldName);
					$valueField = $connection->quoteIdentifier($metaTableName . $aliasSuffix . '.' . $metaTableValueFieldName);
					$value = is_array($_conditionInfo['value']) ? '(?)' : '?';


					$attributeFilterString .= $logical . '(' . $connection->quoteInto($keyField . ' = ?', $_name) . ' AND ' . $connection->quoteInto($valueField . ' ' . $valueConditionType . ' ' . $value, $_conditionInfo['value']) . ')' . "\n";
					
					$select->joinLeft($metaTableName, $metaTableName . '.' . $metaTableForeignKeyName . ' = ' . $nativeTableName . '.' . $this->getModel()->getIdFieldName(), array()/*, array($metaTableKeyFieldName, $metaTableValueFieldName)*/);
					
					$i++;
				}
			}
//		var_dump($attributeFilterString);
//		var_dump($this->getAttributesToFilter());
			$select->where($attributeFilterString)
					->group($nativeTableName . '.' . $this->getModel()->getIdFieldName());
		}
		return $select;
	}
	
	public function isMetaAutoLoad($autoload = null) {
		if( is_null($autoload) ) {
			return $this->_isMetaAutoLoad;
		}
		
		$this->_isMetaAutoLoad = (bool)$autoload;
		return $this;
	}
	
	/**
	 * @param Xedin_Core_Model_Meta_Abstract $model 
	 */
	protected function _beforeEachSet(Xedin_Core_Model_Meta_Abstract $model) {
		parent::_beforeEachSet($model);
		if( $this->isMetaAutoLoad() ) {
			$model->loadMeta();
		}
		
		return $this;
	}
}