<?php

/**
 * Description of Table
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Db_Table extends Xedin_Core_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('core/db_table');
	}
}