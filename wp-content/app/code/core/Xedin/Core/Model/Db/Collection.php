<?php

/**
 * @version 0.1.5a
 * + Added _beforeLoad() and _afterLoad() functions.
 * + Added function count().
 * + Added setModelPath().
 * * Fixed a bug that caused the collection to reload when calling count().
 * + Added pagination functionality. See setPage(), setPerPage().
 * + added isLoaded() functionality.
 * 
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
abstract class Xedin_Core_Model_Db_Collection extends Xedin_Core_Model_Collection_Abstract {

	
	protected $_resourceModelPath;
    
    protected $_perPage;
    protected $_page;
	protected $_totalNumRows;
	
	protected $_totalNumRowsInSelect;
	
	/** @var Zend_Db_Adapter_Abstract */
	protected $_connection;
	protected $_select;
	protected $_joins = array();
	
	protected $_conditionTypes = array(
		self::CONDITION_TYPE_EQUALS						=>	'=',
		self::CONDITION_TYPE_NOT_EQUALS					=>	'!=',
		self::CONDITION_TYPE_LESS_THAN					=>	'<',
		self::CONDITION_TYPE_GREATER_THAN				=>	'>',
		self::CONDITION_TYPE_LESS_THAN_OR_EQUALS		=>	'<=',
		self::CONDITION_TYPE_GREATER_THAN_OR_EQUALS		=>	'>=',
		self::CONDITION_TYPE_IN							=>	'IN',
		self::CONDITION_TYPE_NOT_IN						=>	'NOT IN',
		self::CONDITION_TYPE_LIKE						=>	'LIKE',
		self::CONDITION_TYPE_NOT_LIKE					=>	'NOT LIKE'
	);
	
	const CONDITION_TYPE_EQUALS = 'eq';
	const CONDITION_TYPE_NOT_EQUALS = 'neq';
	const CONDITION_TYPE_LESS_THAN = 'lt';
	const CONDITION_TYPE_GREATER_THAN = 'gt';
	const CONDITION_TYPE_LESS_THAN_OR_EQUALS = 'lteq';
	const CONDITION_TYPE_GREATER_THAN_OR_EQUALS = 'gteq';
	const CONDITION_TYPE_IN = 'in';
	const CONDITION_TYPE_NOT_IN = 'nin';
	const CONDITION_TYPE_LIKE = 'like';
	const CONDITION_TYPE_NOT_LIKE = 'nlike';

    protected function _init($modelPath) {
        $this->_modelPath = $modelPath;
		$this->setConnection(Hub::getSingleton($modelPath)->getResourceModel()->getReadAdapter());
    }

    protected function _construct() {

    }
	
	public function getConditionType($abbr = null, $default = null) {
		if( is_null($abbr) ) {
			return $this->_conditionTypes;
		}
		
		if( !isset($this->_conditionTypes[$abbr]) ) {
			return $default;
		}
		
		return $this->_conditionTypes[$abbr];
	}

    /**
     * @return Zend_Db_Adapter_Abstract
     */
    public function getConnection() {
		return $this->_connection;
	}

    /**
     * @return Zend_Db_Select
     */
    public function getSelect() {
		return $this->_select;
	}
	
	public function setConnection($connection) {
		if( !($connection instanceof Zend_Db_Adapter_Abstract) ) {
			exit('The connection is not an instance of Zend_Db_Adapter_Abstract');
		}
		
		$this->_connection = $connection;
		$this->_select = $this->_connection->select();
		return $this;
	}
    
    public function setPerPage($perPage) {
        $this->_perPage = $perPage;
        return $this;
    }
    
    public function getPerPage() {
        return $this->_perPage;
    }
    
    public function setPage($page) {
        $this->_page = $page;
        return $this;
    }
    
    public function getPage() {
        return $this->_page;
    }
    
    public function hasPage() {
        return !is_null($this->_page);
    }
    
    public function hasPerPage() {
        return !is_null($this->_perPage);
    }

    /**
     * @param string $fields
     * @return Xedin_Core_Db_Collection
     */
    public function addFieldToSelect($fields, $tableName = null) {
        $this->getSelect()
                ->select($fields, $tableName);

        return $this;
    }

    /**
     * @param array $fieldName
     * @param bool $value
     * @return Xedin_Core_Db_Collection
     */
    public function addFieldToFilter($fieldName, $value = null, $type = 'eq') {
		$values = ($type == 'in' || $type == 'nin') ? '(?)' : '?';
		if( !$value ) {
			$value = (int)$value;
		}
//		var_dump($this->getTableName() . '.' . $fieldName . ' ' . $this->getConditionType($type) . ' ' . $values, $value);
        $this->getSelect()
                ->where($this->getTableName() . '.' . $fieldName . ' ' . $this->getConditionType($type) . ' ' . $values, $value);
        return $this;
    }
	
	protected function _load() {
		if( $this->isLoaded() ) {
			return $this;
		}
		
		$connection = $this->getConnection();
		$profiler = $connection->getProfiler();
		$profiler->setEnabled(true);
		
		try {
			$select = $this->_getLoadSelect();
			$this->_beforeLoad($select);
		
			$result = $this->getConnection()->query($select)->fetchAll();
			$this->_afterLoad();
		}
		catch( Zend_Db_Statement_Exception $e ) {
			throw Hub::exception('Xedin_Core', $e->getMessage() . "\n" . 'Query: ' . $profiler->getLastQueryProfile()->getQuery());
		}
		
        foreach ($result as $idx => $_row) {
            $model = Hub::getModel($this->getModelPath());
			$model->setData($_row);
			$this->_beforeEachSet($model);
            $this->_items[$model->getId()] = $model;
            $this->_ids[] = $model->getId();
        }
        
        $this->_isLoaded = true;
		return $this;
	}
	
	protected function _afterLoad() {
		if( $this->_totalNumRowsInSelect ) {
			$connection = $this->getConnection();
			$this->_totalNumRows = $connection->fetchOne($connection->select()
				->from(null, new Zend_Db_Expr('FOUND_ROWS() as ' . $this->_totalNumRowsInSelect)));
		}
		return $this;
	}
	
	protected function _beforeEachSet($model) {
		return $this;
	}

    public function getTableName() {
        return Hub::getSingleton( $this->getModelPath() )->getTableName();
    }
    
    public function addSortOrder($field, $ascending = false) {
        if( $field instanceof Zend_Db_Expr ) {
            $this->getSelect()->order($field);
            return $this;
        }
        
		$order = $ascending ? 'ASC' : 'DESC';
		
        $this->getSelect()->order($field . ' ' . $order);
        return $this;
    }
	
	public function reset() {
		parent::reset();
//		$this->_select = $this->getConnection()->select();
		
		return $this;
	}
	
	/**
	 * @return string The whatever condition was specified by Zend_Db_Select through any of the exposed methods.
	 */
	protected function _getConditionString() {
		$conditions = $this->getSelect()->getPart(Zend_Db_Select::WHERE);
		return implode(' ', $conditions);
	}
	
	/**
	 * Deletes rows from the main table.
	 * The condition is anything specified using the Zend_Db_Select::where()
	 * through any of the methods exposed by this class.
	 * 
	 * @return integer The number of deleted rows.
	 */
	public function delete() {
//		$this->getConnection()->getProfiler()->setEnabled(true);
		$result = $this->getConnection()->delete($this->getTableName(), $this->_getConditionString());
//		var_dump($this->getConnection()->getProfiler()->getLastQueryProfile());
		return $this;
	}
	
	/**
	 * Updates rows in the main table
	 * The condition is anything specified using the Zend_Db_Select::where()
	 * through any of the methods exposed by this class.
	 * 
	 * @param type $data
	 * @return type 
	 */
	public function update($data) {
		$result = $this->getConnection()->update($this->getTableName(), $data, $this->_getConditionString());
		return $result;
	}
	
	protected function _getLoadSelect() {
        $select = $this->getSelect();
		
		$this->resetFieldsToSelect();
		$select = $this->getSelect()->reset(Zend_Db_Select::FROM);
		
		foreach( $this->_joins as $_type => $_join ) {
			$funcName = 'join' . (ucfirst($_type));
			foreach( $_join as $_idx => $_args ) {
				call_user_func_array(array($select, $funcName), $_args);
			}
		}
		
		$select->from($this->getModel()->getTableName(), $this->_getSelectColumns());
		
        if( $this->hasPage() && $this->hasPerPage() ) {
            $select->limit($this->getPerPage(), $this->getPage()*$this->getPerPage());
        }
		
		return $select;
	}
	
	protected function _getSelectColumns() {
		$columns = array();
		$select = $this->getSelect();
		
		if( $this->_totalNumRowsInSelect ) {
			$columns[] = new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *');
		}
		
        if( count($select->getPart(Zend_Db_Select::COLUMNS)) ) {
            // Always select the ID
			$columns[] = $this->getModel()->getIdFieldName();
        }
		
		if( empty($columns) ) {
			$columns = '*';
		}
		
		return $columns;
	}
	
	public function addIdToFilter($id) {
		if( !is_array($id) ) {
			$id = array($id);
		}
//		var_dump($this->getModel()->getIdFieldName());
//		var_dump($id);
		$this->addFieldToFilter($this->getModel()->getIdFieldName(), $id, self::CONDITION_TYPE_IN);
		return $this;
	}
	
	public function getColumn($fieldName) {		
		$resultSet = array();
		
		foreach( $this->getItems() as $_id => $_item ) {
			/* @var $_item Xedin_Core_Model_Abstract */
			if( $_item->hasData($fieldName) ) {
				$resultSet[$_id] = $_item->getData($fieldName);
			}
		}
		
		return $resultSet;
		
	}
	
	public function resetFieldsToSelect() {
		$this->getSelect()->reset(zend_db_select::COLUMNS);
		return $this;
	}
	
	public function getProfiler() {
		return $this->getConnection()->getProfiler();
	}
	
	/**
	 * @param type $totalNumRows
	 * @return \Xedin_Core_Model_Db_Collection 
	 */
	public function addTotalNumRowsToSelect($totalNumRows = 'total_num_rows') {
		$this->_totalNumRowsInSelect = $totalNumRows;
		return $this;
	}
	
	public function getTotalNumRows() {
		$this->load();
		return $this->_totalNumRows;
	}
	
	public function joinLeft($table, $condition, $fields = '*') {
		$this->_join($table, $condition, $fields, 'left');
		return $this;
	}
	
	public function joinRight($table, $condition, $fields = '*') {
		$this->_join($table, $condition, $fields, 'right');
	}
	
	protected function _join($table, $condition, $fields = '*', $type = 'left') {
		$this->_joins[$type][] = array(
			$table,
			$condition,
			$fields
		);
		
		return $this;
	}
	
	public function randomizeOrder() {
		$this->addSortOrder(new Zend_Db_Expr('RAND()'));
		return $this;
	}
}