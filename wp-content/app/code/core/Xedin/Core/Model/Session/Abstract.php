<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 */
abstract class Xedin_Core_Model_Session_Abstract extends Zend_Session_Namespace {
	
	protected $_namespace = null;
	protected $_cookieConfig;
	protected $_messagesKey = '__messages';
	protected $_globalSessionUri = 'core/session_global';
	/** @var Xedin_Core_Model_Session_Global */
	protected $_globalSession;
	protected $_messageExpirationHops = 0;
	
	protected $_messageTypes = array(
		'info'				=>	self::MESSAGE_TYPE_INFO,
		'notice'			=>	self::MESSAGE_TYPE_NOTICE,
		'warning'			=>	self::MESSAGE_TYPE_WARNING,
		'error'				=>	self::MESSAGE_TYPE_ERROR,
		'misc'				=>	self::MESSAGE_TYPE_MISC
	);
	
	static protected $_temporaryMessages = array();
	
	const XML_PATH_GLOBAL_SESSION_COOKIE = 'global/session/cookie';
	
	const MESSAGE_TYPE_INFO = 'info';
	const MESSAGE_TYPE_NOTICE = 'notice';
	const MESSAGE_TYPE_WARNING = 'warning';
	const MESSAGE_TYPE_ERROR = 'error';
	const MESSAGE_TYPE_MISC = 'miscellaneous';
	
	public function __construct() {
		if( is_null($this->_namespace) ) {
			throw new Xedin_Core_Exception('Could not initialize a session without namespace.');
		}
		
		parent::__construct($this->_namespace, true);
		
		$this->_construct();
	}
	
	protected function _construct() {
		
	}
	
	public function getNamespace() {
		return $this->_namespace;
	}
	
	public function get($name) {
		return $this->__get($name);
	}
	
	public function set($name, $value = null) {
		if( is_array($name) ) {
			foreach( $name as $_name => $_value ) {
				$this->set($_name, $value);
			}
			return $this;
		}
		
		$this->__set($name, $value);
	}
	
	public function uns($name = null) {
		if( is_null($name) ) {
			$this->unsetAll();
			return $this;
		}
		
		$this->__unset($name);
		return $this;
	}
	
	public function has($name = null) {
		return $this->_namespaceIsset($this->_namespace, $name);
	}
	
	public function getCookieConfig($path = null) {
		if( !$this->_cookieConfig ) {
			$this->_cookieConfig = new Varien_Object( Hub::getConfig(self::XML_PATH_GLOBAL_SESSION_COOKIE) );
		}
		$path = (string)$path;
		return $this->_cookieConfig->getData($path);
	}
	
	public function setCookie($name, $value) {
		$config = $this->getCookieConfig();
		$expire = (isset($config['expire']) && $config['expire']) ? time() + (int)$config['expire'] : 0;
		setcookie($name, $value, $expire, $config['path'], $config['domain']);
		return $this;
	}
	
	public function getCookie($key, $default = null) {
		return $this->getRequest()->getCookie($key, $default);
	}
	
	public function getRequest() {
		return Hub::app()->getController()->getRequest();
	}
	
	public function addMessage($text, $type = self::MESSAGE_TYPE_INFO, $name = null) {
		if( !in_array($type, $this->_messageTypes) ) {
			throw Hub::exception('Xedin_Core', 'Invalid message type "' . $type . '".');
		}
//		var_dump($this->_messageExpirationHops);
		if( !(int)$this->_messageExpirationHops ) {
			
			if( !isset(self::$_temporaryMessages[$this->getMessageKey()]) ) {
				self::$_temporaryMessages[$this->getMessageKey()] = array();
			}
			
			if( !isset(self::$_temporaryMessages[$this->getMessageKey()][$type]) ) {
				self::$_temporaryMessages[$this->getMessageKey()][$type] = array();
			}
			
			if( is_null($name) ) {
				self::$_temporaryMessages[$this->getMessageKey()][$type][] = $text;
			}
			else {
				self::$_temporaryMessages[$this->getMessageKey()][$type][$name] = $text;
			}
//			$args = func_get_args();
//			var_dump(self::$_temporaryMessages);
			return $this;
		}
		
		$name = is_null($name) ? count($this->getGlobalSession()->{$this->getMessageKey()}[$type]) : $name;
		$this->getGlobalSession()->{$this->getMessageKey()}[$type][$name] = $text;
		return $this;
	}
	
	public function getMessages($type = null, $name = null) {
		if( $this->_messageExpirationHops ) {
			$messages = $this->getGlobal($this->getMessageKey());
		}
		else {
			if( !isset(self::$_temporaryMessages[$this->getMessageKey()]) ) {
				self::$_temporaryMessages[$this->getMessageKey()] = array();
			}
			$messages = self::$_temporaryMessages[$this->getMessageKey()];
		}
		
//		$args = func_get_args();
//		var_dump($args);
//		var_dump(self::$_temporaryMessages[$this->getMessageKey()]);
		
		if( is_null($type) ) {
			return $messages;
		}
		
		if( !isset($messages[$type]) ) {
			return null;
		}
		
		if( is_null($name) ) {
			return $messages[$type];
		}
		
		if( !isset($messages[$type][$name]) ) {
			return null;
		}
		
		return $messages[$type][$name];
	}
	
	public function getMessageKey() {
		return $this->_messagesKey;
	}
	
	protected function _setMessageKey($key) {
		$this->_messagesKey = $key;
		return $this;
	}
	
	public function getMessageTypes($type = null, $default = null) {
		if( is_null($type) ) {
			return $this->_messageTypes;
		}
		
		if( !isset($this->_messageTypes[$type]) ) {
			return $default;
		}
		
		return $this->_messageTypes[$type];
	}
	
	public function getMessageExpirationHops() {
		return $this->_messageExpirationHops;
	}
	
	public function getGlobal($key) {
		return @$this->getGlobalSession()->get($key);
	}
	
	public function setGlobal($key, $value = null) {
		$this->getGlobalSession()->set($key, $value);
		return $this;
	}
	
	public function getGlobalSessionUri() {
		return $this->_globalSessionUri;
	}
	
	/**
	 * @return Xedin_Core_Model_Session_Abstract
	 */
	public function getGlobalSession() {
		if( !$this->_globalSession ) {
			$this->_globalSession = Hub::getSingleton($this->getGlobalSessionUri());
			$this->_globalSession->setExpirationHops($this->_messageExpirationHops, $this->getMessageKey());
		}
		return $this->_globalSession;
	}
	
	public function addInfo($text) {
		$this->addMessage($text, self::MESSAGE_TYPE_INFO);
		return $this;
	}
	
	public function addNotice($text) {
		$text = func_get_args();
		$this->addMessage($this->_transposeMessage($text), self::MESSAGE_TYPE_NOTICE);
		return $this;
	}
	
	public function addWarning($text) {
		$text = func_get_args();
		$this->addMessage($this->_transposeMessage($text), self::MESSAGE_TYPE_WARNING);
		return $this;
	}
	
	public function addError($text) {
		$text = func_get_args();
		$this->addMessage($this->_transposeMessage($text), self::MESSAGE_TYPE_ERROR);
		return $this;
	}
	
	public function setMisc($name, $value) {
		$this->addMessage($value, self::MESSAGE_TYPE_MISC, $name);
		return $this;
	}
	
	public function setFlashVar($name, $value) {
		$value = (is_array($value) ? $this->_transposeMessage($value) : $value);
		$this->addMessage($value, self::MESSAGE_TYPE_MISC, $name);
		return $this;
	}
	
	public function getFlashVar($name) {
		return $this->getMessages(self::MESSAGE_TYPE_MISC, $name);
	}
	
	public function hasFlashVar($name) {
		return ($this->getMessages(self::MESSAGE_TYPE_MISC, $name) === null);
	}
	
	protected function _transposeMessage($args) {
		return call_user_func_array('sprintf', $args);
	}
	
	public function setTemporary($name, $value) {
		if( !isset(self::$_temporaryMessages[$this->getNamespace()]) ) {
			self::$_temporaryMessages[$this->getNamespace()] = array();
		}
		
		self::$_temporaryMessages[$this->getNamespace()][$name] = $value;
		return $this; 
	}
	
	public function getTemporary($name = null, $default = null) {
		if( !$this->hasTemporary($name) ) {
			return $default;
		}
		
		if( is_null($name) ) {
			return self::$_temporaryMessages[$this->getNamespace()];
		}
		
		return self::$_temporaryMessages[$this->getNamespace()][$name];
	}
	
	public function hasTemporary($name = null) {
		if( !isset(self::$_temporaryMessages[$this->getNamespace()]) ) {
			return false;
		}
		
		if( is_null($name) && empty(self::$_temporaryMessages[$this->getNamespace()]) ) {
			return false;
		}
		
		if( !isset(self::$_temporaryMessages[$this->getNamespace()][$name]) ) {
			return false;
		}
		
		return true;
	}
	
	public function unsTemporary($name = null) {
		if( is_null($name) ) {
			self::$_temporaryMessages[$this->getNamespace()] = array();
			return $this;
		}
		
		if( $this->hasTemporary($name) ) {
			unset(self::$_temporaryMessages[$this->getNamespace()][$name]);
		}
		
		return $this;
	}
}