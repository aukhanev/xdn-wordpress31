<?php

/**
 * Description of Global
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Session_Global extends Xedin_Core_Model_Session_Abstract {
	
	protected $_namespace = 'global';
}