<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Event_Observer_Collection extends Xedin_Core_Model_Collection_Abstract {
	
	protected $_modelPath = 'core/event_observer';
	
	public function addObserver($observer = null, $data = null) {
		$this->_add($observer, $data);
		return $this;
	}
	
	public function removeObserver($observerCode) {
		$this->_remove($observerCode, true);
		return $this;
	}
}