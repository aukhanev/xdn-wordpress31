<?php

/**
 * Responsible for loading observers and dispatching events.
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Event_Controller extends Xedin_Core_Model_Abstract {
		
	const XML_PATH_EVENTS = 'global/events';
	
	const REG_KEY_PREFIX_OBSERVER = '_observer/';
	
	/** @var Xedin_Core_Model_Event_Collection */
	protected $_eventsCollection;
	protected $_eventUri = 'core/event';
	
	protected $_events = array();
	
	public function addEvents($eventConfig = null) {
		if( is_null($eventConfig) && !$this->hasEvents() ) {
			$this->_addEvents();
		}
		
		if( !is_null($eventConfig) ) {
			$this->_addEvents($eventConfig);
		}
		
		return $this;
	}
	
	protected function _addEvents($eventsConfig = null) {
		
		if( is_null($eventsConfig) ) {
			$eventsConfig = $this->getEventsConfig();
		}
		
		if( !is_array($eventsConfig) ) {
			$eventsConfig = $this->getEventsConfig($eventsConfig);
			if( !$eventsConfig ) {
				return $this;
			}
			$eventsConfig = $eventsConfig->children();
		}
		
		foreach( $eventsConfig as $_eventCode => $_eventConfig ) {
			
			// If current event does not exist, allocate space
			if( !isset($this->_events[$_eventCode]) ) {
				$this->_events[$_eventCode] = array();
			}
			
			/* @var $_eventConfig Varien_Simplexml_Element */
			$_eventObservers = (array)$_eventConfig;
			
			foreach( $_eventObservers as $_observerCode => $_observerData ) {
				$_observerData = (array)$_observerData;
				
				if( !isset($_observerData['class']) ) {
					throw Hub::exception('Xedin_Core', 'No class specified for observer "' . $_observerCode . '" in event "' . $_eventCode . '".');
				}
				
				if( !isset($_observerData['method']) ) {
					throw Hub::exception('Xedin_Core', 'No method specified for observer "' . $_observerCode . '" in event "' . $_eventCode . '".');
				}
				
				// This is for future compatibility with observers loaded from database
				if( !isset($_observerData['code']) ) {
					$_observerData['code'] = $_observerCode;
				}
				
				$this->_events[$_eventCode][$_observerCode] = $_observerData;
			}
		}
		
		return $this;
	}
	
	/**
	 * If called without parameters, determines whether the controller has events.
	 * If $eventName is specified, determines whether the controller has a specific event.
	 * 
	 * @param string|null $eventName Name of the event to check for. Default: null.
	 * @return bool Whether or not the controller has any or a particular event.
	 */
	public function hasEvents($eventName = null) {
		if( is_null($eventName) ) {
			return !empty($this->_events);
		}
		
		return isset($this->_events[$eventName]);
	}
	
	/**
	 * If called with no parameters, get the array of added events.
	 * If $eventName is specified, get event with that name.
	 * 
	 * @access protected
	 * @param string|null $eventName Name of the event to get. Default: null.
	 * @param mixed|null $default What to return if event not found.
	 * @return array Either the all events, or the event with the specified name.
	 */
	protected function _getEvents($eventName = null, $default = null) {
		if( !$this->hasEvents() ) {
			$this->addEvents();
		}
		
		if( is_null($eventName) ) {
			return $this->_events;
		}
		
		if( !isset($this->_events[$eventName]) ) {
			return $default;
		}
		
		return $this->_events[$eventName];
	}
	
	/**
	 * Loads events from a config.
	 * If no config specified, will attempt to load from existent global config.
	 * 
	 * @param array|Varien_Simplexml_Config $eventsConfig
	 * @return \Xedin_Core_Model_Event_Controller This instance.
	 */
	public function loadEvents() {
			
		if( $this->isEventCollectionLoaded() ) {
			return $this;
		}
		
		foreach( $this->_getEvents() as $_eventCode => $_eventObservers ) {
			/* @var $_eventObservers Varien_Simplexml_Element */
			$_eventObservers = (array)$_eventObservers;
				
			foreach( $_eventObservers as $_observerCode => $_observerData ) {
				$_observerData = (array)$_observerData;
								
				$observer = null;
				
				if( !is_null($_observerData['class']) ) {
					if( strpos($_observerData['class'], Hub::URI_SEPARATOR) ) {
						$observer = Hub::getSingleton($_observerData['class']);
						$this->_registerObserver($observer, $_observerCode);
					}
					else {
						if( class_exists($_observerData['class']) ) {
							if( !($observer = $this->getRegisteredObserver($_observerCode)) ) {
								$observer = new $_observerData['class'];
							}
							$this->_registerObserver($observer, $_observerCode);
						}
					}
				}
//				var_dump($observer);
				if( !$observer ) {
					throw Hub::exception('Xedin_Core', 'No observer found with class or URI "' . $_observerData['class'] . '" for observer "' . $_observerCode . '" in event "' . $_eventCode . '".');
				}
				
//				var_dump($_observerData);
				$this->getEventsCollection()->getEvent($_eventCode)->addObserver($observer, $_observerData)
					->addHandler($_observerData['code'], $_observerData['method']);
//				var_dump($this->getEventsCollection()->getEvent($_eventCode));
			}
		}
		
		return $this;
	}
	
	protected function _registerObserver($observer, $observerCode = null) {
		if( empty($observerCode) ) {
			$observerCode = $observer->getCode();
		}
		$regKey = $this->getObserverRegistryKeyPrefix($observerCode);
		
		if( !Hub::registry($regKey) ) {
			Hub::register($observer, $regKey);
		}
		
		return $this;
	}
	
	public function getRegisteredObserver($observerCode) {
		return Hub::registry( $this->getObserverRegistryKeyPrefix($observerCode) );
	}
	
	public function getObserverRegistryKeyPrefix($observerCode = '') {
		return self::REG_KEY_PREFIX_OBSERVER . (string)$observerCode;
	}
	
	public function hasRegisteredObserver($observerCode) {
		return (bool)$this->getRegisteredObserver($observerCode);
	}
	
	/**
	 * Get a config section representing the events.
	 * 
	 * @return array|Varien_Simplexml_Element 
	 */
	public function getEventsConfig($config = null) {
		if( is_null($config) ) {
			$config = Hub::getConfig();
		}
		
		if( !is_object($config) ) {
			return null;
		}
			
		if( $config instanceof Xedin_Config ) {
			/* @var $config Xedin_Config */
			return $config->getValue(self::XML_PATH_EVENTS);	
		}
	
		
		/* @var $config Varien_Simplexml_Config */
		return $config->getNode(self::XML_PATH_EVENTS);
	}
	
	/**
	 * @return Xedin_Core_Model_Event_Collection A new collection instance
	 */
	public function getEventsCollection() {
		if( !$this->_eventsCollection ) {
			$this->_eventsCollection = Hub::getSingleton( $this->getEventUri() )->getCollection();
		}
		
		return $this->_eventsCollection;
	}
	
	/**
	 * @return string URI of an event model.
	 */
	public function getEventUri() {
		return $this->_eventUri;
	}
	
	/**
	 * @return bool Whether or not the event collection has been loaded.
	 */
	public function isEventCollectionLoaded() {
		return !empty($this->_eventsCollection);
	}
	
	public function dispatchEvent($eventCode, $args = array(), $caller = null) {
		$collection = $this->loadEvents()
				->getEventsCollection()
						->getEvent($eventCode)
								->setCaller($caller)
								->dispatch($args);
		
		return $this;
	}
}