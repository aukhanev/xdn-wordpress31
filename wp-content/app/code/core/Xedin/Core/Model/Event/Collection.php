<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Event_Collection extends Xedin_Core_Model_Collection_Abstract {
	
	protected $_modelPath = 'core/event';
	
	public function addEvent(Xedin_Core_Model_Event $event, $data = null) {
		$this->_add($event, $data);
		return $this;
	}
	
	/**
	 * Remove an event by ID.
	 * 
	 * @param string|int $eventCode Code/ID of the event to remove.
	 * @return \Xedin_Core_Model_Event_Collection 
	 */
	public function removeEvent($eventCode) {
		$this->_remove($eventCode, true);
		return $this;
	}
	
	/**
	 * Retrieve the event by code.
	 * If the event does not exist, it will be created.
	 * 
	 * @param string $eventCode Code of the event to retrieve.
	 * @return Xedin_Core_Model_Event|null The event, if found or created; null if the event could not be created. 
	 */
	public function getEvent($eventCode) {
		return $this->_getCreateItem($eventCode);
	}
}