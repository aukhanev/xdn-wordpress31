<?php

/**
 * Description of Observer
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Event_Observer_Abstract extends Xedin_Core_Model_Abstract {
	
	protected $_idFieldName = 'code';
	
	protected function _construct() {
		parent::_construct();
		
		$this->setDefaultHandlerName('unhandledEvent');
	}
	
	public function getCollection() {
		return Hub::getModel('core/event_observer_collection');
	}
	
	/**
	 * Handles an event.
	 * @param string $event Name of the event being handled.
	 * @param Object $caller Object that called the event.
	 * @param string $handlerName Name of handler function.
	 * @param array $args Array of arguments to the handler function.
	 */
	public function handleEvent($event, $handlerName, $args = array()) {
		if( !method_exists($this, $handlerName) ) {
			$this->_executeHandler($event, $this->getDefaultHandlerName(), $args);
			return null;
		}
		
		return $this->_executeHandler($event, $handlerName, $args);
	}
	
	public function unhandledEvent($eventName, Object $caller, $args = array()) {
		return null;
	}
	
	protected function _executeHandler($event, $handlerName, $args = array()) {
		if( !method_exists($this, $handlerName) ) {
			throw Hub::exception('Xedin_Core', 'Observer "' . get_class($this) . '" does not have a handle called "' . $handlerName . '"');
		}
		
		array_unshift($args, $event);
		
		$benchmarkCode = 'OBSERVER::' . $this->getId() . '::' . $handlerName;
		Hub::app()->getProfiler()->start($benchmarkCode);
		$result = call_user_func_array(array($this, $handlerName), $args);
		Hub::app()->getProfiler()->stop($benchmarkCode);
		return $result;
	}
}