<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Xedin_Core_Model_Layout_Update extends Varien_Object {
    
    protected $_xml;
    
    /**
     * @return Varien_Simplexml_Element
     */
    public function getXml() {
        if( !$this->_xml ) {
            $this->_xml = $this->_getUpdates();
        }
        return $this->_xml;
    }
    
    protected function _getUpdates() {
        $xmlUpdate = '<?xml version="1.0" encoding="UTF-8"?><layout>';
        $xmlUpdate .= $this->_getCmsUpdate();
        $xmlUpdate .= '</layout>';
        return new Varien_Simplexml_Element($xmlUpdate);
    }
    
    protected function _getCmsUpdate() {
        return new Varien_Simplexml_Element('<layout/>');
    }
}
?>
