<?php

/**
 * Description of Abstract
 *
 * @version 0.1.2
 * + The ID field name now taken from resource model only if this instance's ID field name is not set. This way, the ID field name can change per model instance and defaults to the Resource Model's ID field name.
 * 
 * version 0.1.1
 * + Now extends Xedin_Object
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Abstract extends Xedin_Object {

	/** @var Xedin_Core_Model_Resource_Abstract */
    protected $_resourceModel;
    protected $_resourceModelName;

    public function __construct() {
        $this->_construct();
    }

    protected function _construct() {
        
    }
	
	public function getIdFieldName() {
		if( !$this->_idFieldName ) {
			$this->_idFieldName = $this->getResourceModel()->getIdFieldName();
		}
		
		return $this->_idFieldName;
	}

	/**
	 * @param string $resourceModel
	 * @param string $idFieldName 
	 * @return Xedin_Core_Model_Abstract
	 */
    protected function _init($resourceModel) {
        $this->_resourceModelName = $resourceModel;
//        $this->setIdFieldName($idFieldName);
		return $this;
    }
	
	public function getTableName() {
		return $this->getResourceModel()->getTableName();
	}

	/**
	 * @return Xedin_Core_Model_Resource_Mysql
	 */
    public function getResourceModel() {
		if( !$this->_resourceModel ) {
			$this->_resourceModel = Hub::getResourceModel($this->_resourceModelName);
		}
        return $this->_resourceModel;
    }
	
	public function getResourceModelName() {
		return $this->_resourceModelName;
	}

    public function save() {
        $this->getResourceModel()->save($this);

        return $this;
    }

    public function load($id=null) {
        if( is_null($id) ) {
            $id = $this->getId();
        }
		
		if( !is_numeric($id) ) {
			$this->getResourceModel()->load($this, $id, 'code');
		}
		else {
			$this->getResourceModel()->load($this, $id);
		}
		
		return $this;
    }
	
	/**
	 *
	 * @return Xedin_Core_Model_Db_Collection
	 */
	public function getCollection() {
		return Hub::getModel( $this->getResourceModelName() . '_collection' );
	}
	
	public function getSession() {
		return Hub::getSingleton('core/session');
	}
	
	public function getFieldNames($name = null, $default = null) {
		$resourceModel = $this->getResourceModel();
		if( !method_exists($resourceModel, 'getFieldNames') ) {
			return $default;
		}
		
		return $resourceModel->getFieldNames($name, $default);
	}
	
	public function getDate($timestamp = null, $format = null) {
		if( is_null($format) ) {
			$format = Hub::getHelper('core/datetime')->getMysqlDatetimeFormat();
		}
		return $this->getResourceModel()->getFormattedTimeStamp($timestamp, $format);
	}
	
	public function getTime($date = null) {
		return $this->getResourceModel()->getTimeGmt($date);
	}
	
	public function delete() {
		$this->isDeleted(true);
		return $this;
	}
	
	public function reset() {
		$this->unsetData()
				->isDeleted(false);
		
		return $this;
	}
	
	/**
	 * Very useful if creating an instance of a model if necessary, but the
	 * URI of the model is unknown. Especially handy when called on singletons.
	 * 
	 * If the first parameter is an instance of Varien_Object or an array,
	 * populates the instance's data with the data in the object or array.
	 * 
	 * If the first parameter is a string or a number, attempts to load the
	 * new model instance using it as the unique identifier.
	 * 
	 * @param Varien_Object|array|int|string $data The data for the new instance, or the ID.
	 * @return Xedin_Core_Model_Abstract The new instance
	 */
	public function getInstance($data = null) {
		$className = get_class($this);
		$newInstance = new $className;
		
		if( is_array($data) || $data instanceof Varien_Object ) {
			$newInstance->setData($data);
		}
		
		if( is_numeric($data) || is_string($data) ) {
			$newInstance->load($data);
		}
		
		return $newInstance;
	}
	
	public function getUniformConfigPath($path1, $pathN) {
		$path = array();
		$args = func_get_args();
		foreach( $args as $_idx => $_arg ) {
			if( !is_array($_arg) ) {
				$_arg = explode(Xedin_Config::NODE_DELIMITER, $_arg);
			}
			
		}
	}
}