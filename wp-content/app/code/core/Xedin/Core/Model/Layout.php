<?php

/**
 * Handles the instantiation and assembly of blocks.
 * Because blocks are first completely instantiated, it is possible to manipulate
 * one block from another.
 * A typical example would be adding CSS or JavaScript to the header from
 * somewhere after, e.g.
 * 
 * class Vendor_Module_Block_Myblock extends Xedin_Core_Block_Template {
 *     
 *     protected function _construct() {
 *         $this->getLayout()
 *             ->getBlock('head')
 *             ->addJs('myscript.js')
 *             ->addCss('additional-styles.css');
 *     }
 * }
 * 
 * @version 0.1.1
 * * Now, instead of extending, nodes are appended.
 * 
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
class Xedin_Core_Model_Layout extends Varien_Simplexml_Config {

    protected $_blocks = array();
    protected $_outputBlocks;
    protected $_options;
    protected $_defaultOptions = array(
    );

    const XML_PATH_ROOT = 'layout';

    /**
     * These nodes shall be applied to the initial XML, in this order
     */
    protected $_defaultRequestPaths = array(
        'default',
        '*_default',
        '*_*_default',
        '*_*_*'
    );

	const REQUEST_SEGMENT_DEFAULT = 'default';
	const REQUEST_SEGMENT_WILDCARD = '*';
	
	const PATH_SEGMENT_SEPARATOR = '_';

    public function __construct($source = null) {
        parent::__construct($source);
    }

    public function getDefaultRequestPath($index = null, $default = null) {
        if (is_null($index)) {
            return $this->_defaultRequestPaths;
        }

        if (!isset($this->_defaultRequestPaths[$index])) {
            return $default;
        }

        return $this->_defaultRequestPaths[$index];
    }
	
	/**
	 *
	 * @param Varien_Simplexml_Element $xml
	 * @param type $requestPath
	 * @return \Varien_Simplexml_Element 
	 */
	protected function _getRelevantXml($xml, $requestPath) {
		$relevantXml = '<layout>';
		
		$requestSegments = explode(self::PATH_SEGMENT_SEPARATOR, $requestPath);
		foreach( $this->getDefaultRequestPath() as $_idx => $_defaultPath ) {
			$defaultPathSegments = explode(self::PATH_SEGMENT_SEPARATOR, $_defaultPath);
			foreach( $requestSegments as $_idx2 => $_requestSegment ) {
				if( isset( $defaultPathSegments[$_idx2] ) ) {
					$defaultPathSegments[$_idx2] = str_ireplace(self::REQUEST_SEGMENT_WILDCARD, $_requestSegment, $defaultPathSegments[$_idx2]);
				}
			}
				
			$nodeName = implode(self::PATH_SEGMENT_SEPARATOR, $defaultPathSegments);
			if( $node = $xml->xpath($nodeName) ) {
				foreach( $node as $_elementIndex => $_element ) {
//					var_dump($nodeName);
					$relevantXml .= $_element->innerXml();
				}
				$node = null;
			}
		}
		
		$relevantXml .= '</layout>'; 
		$relevantXml = new Varien_Simplexml_Element($relevantXml);
		
		return $relevantXml;
	}
	
	public function getDesign() {
		return Hub::app()->getDesign(); 
	}
	
	protected function _getLayoutXml($requestPath = null) {
		$globExpression = $this->getDesign()->getLayoutPath() . '*.xml';
		$files = glob($globExpression);
		
		$xml = '<layout>';
		$newXml = new Varien_Simplexml_Config();
		foreach( $files as $_idx => $filePath ) {
			$newXml->loadFile($filePath);
			$xml .= $newXml->getNode()->innerXml();
		}
		$newXml = null;
		$xml .= '</layout>';
		$xml = new Varien_Simplexml_Element($xml);
		
		if( is_null($requestPath) ) {
			return $xml;
		}
		
		$xml = $this->_getRelevantXml($xml, $requestPath);
		
		return $xml;
	}

    /**
     * @todo Implement //remove functionality
     * @param type $options 
     */
    public function init($options = array()) {
        $this->_blocks = array(); 
        $this->_outputBlocks = array();
        $this->_options = array();

        $this->_options = array_merge($this->_defaultOptions, $options);
        $this->_beforeInit($options);
		$this->setXml($this->_getLayoutXml($options['request_path']));
		

        $this->_afterInit();

        $this->_beforeGenerateBlocks();

        $this->generateBlocks();

        $this->_afterGenerateBlocks();
    }

    /**
     * After the layout is read, but before the layout handle is selected.
     * 
     * @param array $options The options that were provided to the layout.
     */
    protected function _beforeInit($options) {
        
    }

    /**
     * After the layout handle is selected, but before the blocks are initialized.
     */
    protected function _afterInit() {
        
    }

    /**
     * After the layout handle is selected, but before the blocks are initialized.
     */
    protected function _beforeGenerateBlocks() {
        
    }

    /**
     * After the layout hande is selected, and all the blocks are initialized.
     */
    protected function _afterGenerateBlocks() {
        
    }

    /**
     * Somehow, this method returns highly-normalized XML.
     * In the resulting XML, all siblings with the same name are collapsed, and
     * their children are grouped. This prevents multiple same-level blocks,
     * actions, references etc.
     * 
     * @param type $options 
     */
    public function ___init($options = array()) {
        $this->_blocks = array();
        $this->_outputBlocks = array();
        $this->_options = array();

        $this->_options = array_merge($this->_defaultOptions, $options);

        $finalXml = new Varien_Simplexml_Element('<?xml version="1.0" encoding="UTF-8"?><' . self::XML_PATH_ROOT . '/>');
        foreach ($this->getDefaultRequestPath() as $idx => $_urlPath) {
            $_requestPath = Mage_Loader::getFrontController()->getRequest()->getRequestPathFromUrlPath($_urlPath);
            if (($node = $this->getNode($_requestPath))) {
                $finalXml->extend($node);
            }
            $finalXml->asNiceXml();
        }

        $this->setXml($finalXml->descend(self::XML_PATH_ROOT . '/' . $this->getDefaultRequestPath(0)));

        $finalXml = null;
        $this->generateBlocks();
//        exit($this->getNode()->asNiceXml());
    }

    public function generateBlocks($parent = null) {
        if (is_null($parent)) {
            $parent = $this->getNode();
        }
        foreach ($parent->xpath('block') as $_block) {
            if ((bool) $_block->getAttribute('ignore')) {
                return true;
            }
            $this->createBlock($_block, $parent);
            $this->generateBlocks($_block);
        }
        foreach ($parent->xpath('reference') as $_block) {
            if ((bool) $_block->getAttribute('ignore')) {
                return true;
            }
            $this->generateBlocks($_block);
        }
        foreach ($parent->xpath('action') as $_block) {
            $this->generateAction($_block, $parent);
        }
        foreach ($parent->xpath('remove') as $_block) {
            $this->removeBlock($_block->getAttribute('name'));
        }

        if ($parent->getName() == 'block' && ($parent = $this->getBlock($parent->getAttribute('name')))) {
            $parent->doneInitializing();
        }
    }

    public function generateAction($node, $parent) {
        if (!empty($parent) && ($parent->getName() == 'block' || $parent->getName() == 'reference' )) {

            $parent = $this->getBlock($parent->getAttribute('name'));
            if ($parent) {
                $args = (array)$node->children();
				foreach($args as $_idx => $_arg) {
					$args[$_idx] = $this->resolveNodeValues($_arg);
				}
                unset($args['@attributes']);
                $method = (string) $node['method'];
                call_user_func_array(array($parent, $method), $args);
            }
        }
    }
	
	public function resolveNodeValues($node) {
		if( $node instanceof Varien_Simplexml_Element ) {
			$children = $node->children();
			if( isset($children[0]) ) {
				$node = $children[0];
			}
			
			if( $node instanceof Varien_Simplexml_Element ) {
				$actor = null;
				switch( $node->getName() ) {					
					case 'helper':
					default:
						try {
							$actor = Hub::getHelper($node->getAttribute('type'));
						}
						catch( Exception $e ) {
							throw Hub::exception('xedin/core', 'No ' . $node->getName() . ' found for URI "' . $node->getAttribute('type') . '"');
						}
						break;
				}
				
				$args = (array)$node;
				unset($args['@attributes']);
				$method = $node->getAttribute('method');
				if( !method_exists($actor, $method) ) {
					throw Hub::exception('xedin/core', ucfirst($node->getName()) . ' with URI "' . $node->getAttribute('type') . '" has no method "' . $method . '"');
				}
				
				$node = call_user_func_array(array($actor, $method), $args);
			}
		}
		
		return $node;
	}

    /**
     *
     * @param type $node
     * @param type $parent
     * @return Xedin_Core_Block_Abstract
     */
    public function createBlock($node, $parent = null, $attributes = array()) {
        try {
            if (is_string($node)) {
                $name = $parent;
				$uniqid = uniqid('block-');
				$name = isset($attributes['name']) ? $attributes['name'] : $uniqid;
                $type = $node;
            } else {
                $type = $node->getAttribute('type');
                $name = $node->getAttribute('name');
                $attributes = (array) $node->attributes();
                $attributes = $attributes['@attributes'];
            }
            $block = $this->_getBlockInstance($type)->addData($attributes);
            $attributes = null;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $block->setType($type);
        $block->setNameInLayout($name);
        $block->setLayout($this);

		$blocksCount = count($this->_blocks);
		if( $blocksCount && !$block->hasBefore() && !$block->hasAfter() ) {
			$blockNames = array_keys($this->_blocks);
			$block->setAfter($blockNames[$blocksCount-1]);
		}
        $this->_blocks[$name] = $block;
		
		
        if ($block->hasOutput()) {
            $this->addOutputBlock($block->getNameInLayout(), $block->getOutput());
        }

//        if (is_string($node)) {
//            return $this->_blocks[$name];
//        }

        if (!empty($parent)) {
            if (($parent instanceof Varien_Simplexml_Element) && ($parent->getName() == 'block' || $parent->getName() == 'reference')) {
                $parent = $this->getBlock($parent->getAttribute('name'));
            }
			elseif( is_string($parent) ) {
				$parent = $this->getBlock($parent);
			}
			elseif (!($parent instanceof Xedin_Core_Block_Abstract)) {
                $parent = null;
            }
        }


        if ($parent) {
            $parent->insert($block);
        }

		$block->doneCreating();
		
//		if(strpos($type, 'menu_models') ) {
//			var_dump($this->_blocks[$name]);
//			exit();
//		}
        return $this->_blocks[$name];
    }

    public function removeBlock($name) {
		foreach( $this->_blocks[$name]->getChildBlocks() as $_name => $_block ) {
			$this->removeBlock($_name);
		}
        unset($this->_blocks[$name]);
        unset($this->_outputBlocks[$name]);
        return $this;
    }

    public function addOutputBlock($name, $method = 'toHtml') {
        $this->_outputBlocks[$name] = $method;
        return $this;
    }

    public function getOutputBlocks() { 
        return $this->_outputBlocks;
    }

    /**
     *
     * @param type $name
     * @return Xedin_Core_Block_Abstract
     */
    public function getBlock($name = null) {
        if (is_null($name)) {
            return $this->getAllBlocks();
        }

        if (!isset($this->_blocks[$name])) {
            return null;
        }

        return $this->_blocks[$name];
    }

    public function getAllBlocks() {
        return $this->_blocks;
    }

    protected function _getBlockInstance($type, $attributes = array()) {
        return Hub::getBlock($type, $attributes);
    }

    public function getOutput() {
        $out = '';
        foreach ($this->getOutputBlocks() as $_name => $_method) {
            $out .= $this->getBlock($_name)->{$_method}();
        }

        return $out;
    }

    public function update($xml) {
        if (!($xml instanceof Varien_Simplexml_Element)) {
            $xml = new Varien_Simplexml_Element($xml);
        }


        foreach ($xml->children() as $idx => $_child) {
            $this->setXml($this->getNode()->appendChild($_child));
        }
        return $this;
    }

}