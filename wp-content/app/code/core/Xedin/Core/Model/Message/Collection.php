<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Model_Message_Collection extends Xedin_Core_Model_Collection_Abstract {
	
	protected $_messageTypeUris = array();
	protected $_defaultMessageType;
	
	protected function __construct() {
		parent::__construct();
	}
	
	public function addMessage($type = null, $text = null) {
		$type = !is_null($type) ? $type : $this->getDefaultMessageType();
		
		if( !is_object($type) ) {
			$messageUri = $this->getMessageTypeUri($type);
			if( !$messageUri ) {
				throw Hub::exception('Xedin_Core', 'No model URI set for message type "' . $type . '".');
			}
			$model = Hub::getModel($messageUri);
			$model->setType($type);
			$type = $model;
		}
		
		if( !is_null($text) ) {
			$type->setText($text);
		}
		
		$this->_add($type);
		return $this;
	}
	
	protected function _setMessageTypeUri($type, $uri) {
		$this->_messageTypeUris[$type] = $uri;
		return $this;
	}
	
	public function getMessageTypeUri($type) {
		return isset($this->_messageTypeUris[$type]) ? $this->_messageTypeUris[$type] : null;
	}
	
	public function getDefaultMessageType() {
		return $this->_defaultMessageType;
	}
}