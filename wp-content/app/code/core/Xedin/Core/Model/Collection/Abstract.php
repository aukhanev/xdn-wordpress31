<?php

/**
 * @version 0.1.2
 * * + _add() now adds item ID to IDs list
 * 
 * version 0.1.1
 * + Added _add() and _remove().
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Collection_Abstract implements ArrayAccess, Iterator {

    /**
     * @var array
     */
    protected $_ids;

    /**
     * @var array
     */
    protected $_items;
    protected $_isLoaded = false;
	protected $_currentItemIndex = 0;
    /**
     * @var string
     */
    protected $_modelPath;

    public function __construct() {
		$this->reset();
        $this->_construct();
    }
	
	protected function _construct() {
		return $this;
	}
    
    protected function _beforeLoad() {}
    
    protected function _afterLoad() {}
	
	/**
	 * @return Xedin_Core_Model_Collection_Abstract 
	 */
    public function load() {
		$this->reset();
		$this->_load();
		return $this;
	}
	
	protected function _load() {
		$this->_beforeLoad();
		$this->_afterLoad();
		return $this;
	}
    
    public function isLoaded() {
        return $this->_isLoaded;
    }
	
	public function reset() {
		$this->_items = array();
		$this->_ids = array();
		$this->_isLoaded = false;
		$this->_currentItemIndex = 0;
		
		return $this;
	}
	
	/**
	 * Retreives an item by it's numeric index.
	 * The ID field name does not matter.
	 * 
	 * @param integer $index Index of the item to retreive.
	 * @return Xedin_Core_Model_Abstract
	 */
	public function getItem($index = null, $default = null) {
		if( is_null($index) ) {
			$index = $this->getCurrentItemIndex();
		}
		
		$itemKeys = array_keys($this->getItems());
//		$this->_currentItemIndex = ( isset($itemKeys[$index+1]) ) ? $index+1 : false;
		
		if( !isset($itemKeys[$index]) ) {
			return $default;
		}
		
		$itemIndex = $itemKeys[$index];
		return $this->_items[$itemIndex];
	}
	
	/**
	 * Retrieve an item by it's field value. Get's the first matched item.
	 * Use getItems() to retreive faster, if the ID is known.
	 * 
	 * @param int|string $value Value of the field of the item to retrieve.
	 * @param string $field Name of the field to retrieve by.
	 * @param mixed $default The value to return if no matching item is found.
	 * @return Xedin_Core_Model_Abstract|mixed Either the found item, or the value of the $default argument.
	 */
	public function getItemByField($value, $field = null, $default = null) {
		if( is_null($field) ) {
			$field = $this->getModel()->getIdFieldName();
		}
		
		foreach( $this->getItems() as $_id => $_item ) {
			/* @var $_item Varien_Object */
			if( $_item->getData($field) == $value ) {
				return $_item;
			}
		}
		
		return $default;
	}

    /**
     * @return array
     */
    public function getItems($id = null) {
        if( empty($this->_items) ) {
            $this->load();
        }
        
        if( is_null($id) ) {
            return $this->_items;
        }
        if( !isset($this->_items[$id]) ) {
            return null;
        }
        
        return $this->_items[$id];
    }

    /**
     * @return array
     */
    public function getIds() {
        return $this->_ids;
    }
    
    public function count() {
        return count($this->getItems());
    }
	
	public function getCurrentItemIndex() {
		return $this->_currentItemIndex;
	}
	
	public function getFirstItem() {
		return $this->getItem(0);
	}
	
	public function getLastItem() {
		$lastIndex = $this->count() - 1;
		return $this->getItem($lastIndex);
	}

    public function getModelPath() {
        return $this->_modelPath;
    }

    public function getModel() {
        return Hub::getSingleton( $this->getModelPath() );
    }
    
    public function setModelPath($uri) {
        $this->_modelPath = $uri;
        return $this;
    }
	
	public function getResourceModel() {
		return $this->getModel()->getResourceModel();
	}
	
	/**
	 * Runs before the item to be added is populated with data.
	 * Return boolean false to cancel.
	 * 
	 * @param Xedin_Core_Model_Abstract $item The item to be added.
	 * @param array|null $data The data to populate the item to be added.
	 * @return array|bool The new data for the item to be added, or false if cancelled.
	 */
	protected function _beforeAdd($item, $data) {
		return $data;
	}
	
	protected function _afterAdd($item) {
		return $this;
	}
	
	/**
	 * Adds an item.
	 * 
	 * @see Xedin_Core_Model_Collection_Abstract::getModelPath().
	 * @param Xedin_Core_Model_Abstract|null $model The model to be added. If none specified, a new instance will be created. Default: null.
	 * @param array|null $data The array to populate the model with, if specified. Default: null.
	 * @return \Xedin_Core_Model_Collection_Abstract This instance.
	 * @throws Xedin_Core_Exception 
	 */
	protected function _add($model = null, $data = null) {
		if( is_null($model) ) {
			$model = $this->getModelPath();
		}
		
		if( is_string($model) ) {
			$model = Hub::getModel($model);
		}
		
		if( !is_object($model) ) {
			throw Hub::exception('Xedin_Core', 'No model to add.');
		}
		
		$data = $this->_beforeAdd($model, $data);
		
		// If add is cancelled
		if( $data === false ) {
			return $this;
		}
		
		if( !is_null($data) ) {
			$model->setData($data);
		}
		
		$index = $model->hasId() ? $model->getId() : count($this->_items);
		$this->_items[$index] = $model;
		$this->_ids[] = $index;
		
		
		
		return $this;
	}
	
	/**
	 * This is run right before removing an item.
	 * Return boolean false to cancel removal, or a new index.
	 * 
	 * @param int|string $index The index (sequence) of the item to remove.
	 * @param Xedin_Core_Model_Abstract $item The item that is to be removed.
	 * @return int|bool int|bool The integer of the item to remove, or false if the removal is cancelled.
	 */
	protected function _beforeRemove($index, $item) {
		return $index;
	}
	
	/**
	 * Runs immediately after removing an item, and only if an item was removed.
	 * 
	 * @param int|string $index The index at which an item was removed.
	 * @return \Xedin_Core_Model_Collection_Abstract 
	 */
	protected function _afterRemove($index) {
		return $this;
	}
	
	/**
	 * Removes an item at a specified index.
	 * 
	 * @param string|int $index The index at which an item should be removed.
	 * @param bool $isId Whether or not the index is an ID, or a sequence number.
	 * @return \Xedin_Core_Model_Collection_Abstract 
	 */
	protected function _remove($index = null, $isId = false) {
		if( is_null($index) ) {
			$index = count($this->_items) - 1;
		}
		
		if( $isId ) {
			$ids = array_keys($this->_items);
			$index = isset($ids[$index]) ? $ids[$index] : null;
		}
		
		if( !is_null($index) && isset($this->_items[$index]) ) {
			$index = $this->_beforeRemove($index, $this->_items[$index]);
			
			// If removal is cancelled
			if( $index !== false ) {
				$item = $this->_items[$index];
				/* @var $item Xedin_Core_Model_Abstract */
				$id = $item->getId();
				unset($this->_items[$index]);
				
				if( $idIndex = array_search($id, $this->_ids) ) {
					unset($this->_ids[$idIndex]);
				}
				
				$this->_afterRemove($index);
			}
		}
		
		return $this;
	}
	
	/**
	 * Get an item by it's ID.
	 * Create empty item (with only the ID data) if it does not exist.
	 * 
	 * @param string|int $id The ID of the item to get or create
	 * @return Xedin_Core_Model_Abstract 
	 */
	protected function _getCreateItem($id) {
		$item = $this->getItems($id);
		
		if( !$item ) {
			$this->addEvent( $this->getModel()->getInstance()->setId($id) );
		}
		
		if( !$item ) {
			$item = $this->getItems($id);
		}
		
		return $item;
	}
	
	/* ArrayAccess ========================================================== */
	
	public function offsetExists($offset) {
		return isset($this->_items[$offset]);
	}
	
	public function offsetGet($offset) {
		return isset($this->_items[$offset]) ? $this->_items[$offset] : null;
	}
	
	public function offsetUnset($offset) {
		if( isset($this->_items[$offset]) ) {
			unset($this->_items[$offset]);
		}
	}
	
	public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_items[] = $value;
        } else {
            $this->_items[$offset] = $value;
        }
	}
	
	/* Iterator ========================================================== */

    public function rewind() {
        $this->_currentItemIndex = 0;
    }

    public function current() {
        return $this->getItem($this->getCurrentItemIndex());
    }

    public function key() {
        return $this->getCurrentItemIndex();
    }

    public function next() {
        ++$this->_currentItemIndex;
    }

    public function valid() {
        return (bool)$this->getItem($this->getCurrentItemIndex());
    }
}