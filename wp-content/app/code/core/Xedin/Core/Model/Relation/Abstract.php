<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Relation_Abstract extends Xedin_Core_Model_Abstract {
	
	/**
	 * @param string $fieldName Name of the field containing child entity ID.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	public function setParentIdFieldName($fieldName) {
		$this->getResourceModel()->setParentIdFieldName($fieldName);
		return $this;
	}
	
	public function getParentIdFieldName() {
		return $this->getResourceModel()->getParentIdFieldName();
	}
	
	/**
	 * @param string $fieldName Name of the field containing parent entity ID.
	 * @return \Xedin_Core_Model_Resource_Relation_Abstract This instance.
	 */
	public function setChildIdFieldName($fieldName) {
		$this->getResourceModel()->setChildIdFieldName($fieldName);
		return $this;
	}
	
	public function getChildIdFieldName() {
		return $this->getResourceModel()->getChildIdFieldName();
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection
	 */
	public function getChildren($children = array()) {
		return $this->getResourceModel()->getChildren($children);
	}
	
	public function getParent() {
		return $this->getResourceModel()->getParent();
	}
	
	public function saveRelations($parent, $children) {
		$this->getResourceModel()->setRelations($parent, $children);
		return $this;
	}
}