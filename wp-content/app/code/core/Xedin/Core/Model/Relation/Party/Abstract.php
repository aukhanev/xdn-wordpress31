<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Model_Relation_Party_Abstract extends Xedin_Core_Model_Abstract {
	
	protected $_relationModelUri;
	protected $_relationModel;
	
	const RELATION_DEFAULT = '_default';
	
	protected function _setRelationModelUri($modelUri, $relation = self::RELATION_DEFAULT) {
		$this->_relationModelUri[$relation] = $modelUri;
	}
	
	public function getRelationModelUri($relation = self::RELATION_DEFAULT) {
		return $this->_relationModelUri[$relation];
	}
	
	/**
	 * @return Xedin_Core_Model_Relation_Abstract
	 */
	public function getRelationModel($relation = self::RELATION_DEFAULT) {
		$relationModelUri = $this->getRelationModelUri($relation);
		if( empty($relationModelUri) ) {
			return null;
		}
		
		if( !$this->_relationModel ) {
			$this->_relationModel = Hub::getSingleton($this->getRelationModelUri($relation));
		}
		
		return $this->_relationModel;
	}
	
	public function getChildren($relation = self::RELATION_DEFAULT) {
		return $this->getRelationModel($relation)->getChildren($this->getId());
	}
	
	public function getParent($relation = self::RELATION_DEFAULT) {
		return $this->getRelationModel($relation)->getParent();
	}
	
	public function save() {
		if( !$this->getRelationModel() ) {
			parent::save();
			return $this;
		}
		
		$childIds = $this->getChildIds();
		$this->_clearChildIds();
		parent::save();
		$this->getRelationModel()->saveRelations($this->getId(), $childIds);
		return $this;
	}
	
	public function getChildIdFieldName($relation = self::RELATION_DEFAULT) {
		return $this->getRelationModel($relation)->getChildIdFieldName();
	}
	
	public function getChildIds($relation = self::RELATION_DEFAULT) {
		$key = $this->getChildIdFieldName();
		return $this->getData($key);
	}
	
	protected function _clearChildIds($relation = self::RELATION_DEFAULT) {
		$key = $this->getChildIdFieldName();
		$this->unsetData($key);
		return $this;
	}
}