<?php
/**
 * @version 0.3
 * + Changed getTemplatePath() to use the 'base_template_directory' data member, which should be set before.
 * 
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */
class Xedin_Core_Block_Template extends Xedin_Core_Block_Abstract {
      
    public function getBaseTemplateDirectory() {
		return $this->getDesign()->getTemplatePath();
    }
	
	public function getDesign() {
		return $this->getLayout()->getDesign();
	}
    
    public function getTemplatePath($path = '') {
		return $this->getDesign()->getTemplatePath($path);
    }
    
    public function fetchView($templatePath = '') {
        if( empty($templatePath) ) {
            $templatePath = $this->getTemplate();
            if( is_null($templatePath) ) {
                return '';
            }
        }

        ob_start();

        try {
            include $this->getTemplatePath($templatePath);
        }
        catch( Exception $e ) {
            ob_get_clean();
            throw $e;
        }

        $html = ob_get_clean();
        return $html;
    }
    
    protected function _toHtml() {
        if( ($html = $this->fetchView()) !== '' ) {
            return $html;
        }
        else {
            return $this->getChildHtml();
        }
    }
    
    public function getUrl($url = '') {
        return $this->getController()->getUrl($url);
    }
	
	public function getRedirectUrl($url, $prepare = true, $params = array()) {
		return $this->getController()->getRedirectUrl($url, $prepare, $params);
	}
	
	public function getSkinUrl($url) {
		return Hub::getUrl($url, Hub::URL_TYPE_SKIN, false);
	}
}