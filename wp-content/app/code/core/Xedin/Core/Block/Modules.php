<?php

/**
 * Description of Modules
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Block_Modules extends Xedin_Core_Block_Template {
	
	protected $_modules;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setModuleModelUri('core/config_instance_module')
				->setTemplate('wordpress/core/modules.php');
	}
	
	public function getModulesCollection() {
		if( !$this->_modules ) {
			$this->_modules = Hub::getSingleton($this->getModuleModelUri())->getCollection();
		}
		
		return $this->_modules;
	}
}