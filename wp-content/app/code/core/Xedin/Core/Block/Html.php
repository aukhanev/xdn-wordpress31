<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Xedin_Core_Block_Html extends Xedin_Core_Block_Template {
	
	protected $_doctypes = array(
		'html-4.01-strict'				=>	'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
		'html-4.01-transitional'		=>	'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
		'html-4.01-frameset'			=>	'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">',
		'xhtml-1.0-strict'				=>	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
		'xhtml-1.0-transitional'		=>	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
		'xhtml-1.1'						=>	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
		'html-5'						=>	'<!DOCTYPE html>'
	);
	
	protected $_attributes = array();
	
	const DIR_LEFT_TO_RIGHT = 'ltr';
	const DIR_RIGHT_TO_LEFT = 'rtl';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setDoctype('xhtml-1.0-strict')
				->setTemplate('html.php')
				->setAttributes('dir', self::DIR_LEFT_TO_RIGHT)
				->setAttributes('lang', 'en');
	}
	
	public function getDoctypeDeclaration($doctype = null, $default = null) {
		if( is_null($doctype) ) {
			$doctype = $this->getDoctype();
		}
		
		if( !isset($this->_doctypes[$doctype]) ) {
			return $default;
		}
		
		return $this->_doctypes[$doctype];
	}
	
	public function getAttributes($key = null, $default = null) {
		if( is_null($key) ) {
			return $this->_attributes;
		}
		
		if( !$this->hasAttributes($key) ) {
			return $default;
		}
		
		return $this->_attributes[$key];
	}
	
	public function hasAttributes($key = null) {
		if( is_null($key) ) {
			return !empty($this->_attributes);
		}
		
		return isset($this->_attributes[$key]);
	}
	
	public function setAttributes($key = null, $value = null) {
		if( is_null($key) ) {
			$this->_attributes = $key;
			return $this;
		}
		
		$this->_attributes[$key] = $value;
		return $this;
	}
	
	public function getAttributeString($name = null) {
		if( is_null($name) ) {
			$attributes = array();
			foreach( $this->getAttributes() as $_name => $_value ) {
				$attributes[] = $this->getAttributeString($_name);
			}
			
			return implode(' ', $attributes);
		}
		
		if( !$this->hasAttributes($name) ) {
			return '';
		}
		
		return $name . '="' . $this->getAttributes($name) . '"';
	}
	
	public function getData($key = null, $index = null) {
		if( !is_null($key) && !$this->hasData($key) ) {
			return $this->getAttributes($key);
		}
		
		return parent::getData($key);
	}
}