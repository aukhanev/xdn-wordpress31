<?php
/**
 * @version 0.1.4
 * + added _resolveConditions() with ifmethod support
 * 
 * 0.1.3
 * + Fixed bug in insert().
 * 
 * 0.1.2
 * + Fixed bug in _afterToHtml().
 * + Added support for layout;
 * + Changed function _constructs()'s access to protected;
 * + Added shortcut functions for URL retreival.
 * + Fixed a bug, which caused output from getChildHtml() not to be returned when called without parameters.
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Core_Block_Abstract extends Varien_Object {
    
    protected $_layout;
    protected $_children = array();
    protected $_sortedChildrenNames = array();
	protected $_html;
	
	protected $_tagStartsWith = '{{';
	protected $_tagEndsWith = '}}';
    
    /**
     * Don't forget what you thought today: investigate whether you should
     * overload __construct instead!
     * 
     * @param type $layout
     * @param type $attributes 
     */
    protected function _construct($layout = null, $attributes = array()) {
        parent::_construct();
        
        if( is_null($layout) ) {
            $layout = Hub::getLayout();
        }
        
        $this->_layout = $layout;
        $this->addData($attributes);
        $this->_children = array();
    }
    
    final public function toHtml() {
		if( empty($this->_html) ) {
			if( !$this->_resolveConditions() ) {
				return '';
			}
			$this->_beforeToHtml();
			$this->_html = $this->_toHtml();
			$this->_html = $this->_afterToHtml($this->_html);
		}
        return $this->_html;
    }

    protected function _toHtml() {
        
    }
	
	protected function _resolveConditions() {
		if( $this->hasIfmethod() ) {
			if( method_exists($this, $this->getIfmethod()) ) {
				if( !call_user_func(array($this, $this->getIfmethod())) ) {
					return false;
				}
			}
		}
		
		return true;
	}

    protected function _beforeToHtml() {}

    protected function _afterToHtml($html) {
        return $html;
    }
    
    public function getRequest() {
        return Hub::app()->getController()->getRequest();
    }
	
	public function getController() {
		return Hub::app()->getController();
	}
    
    public function getBaseUrl($url = '') {
        return Hub::getUrl($url, Hub::URL_TYPE_BASE);
    }
    
    public function getTemplateUrl($url = '') {
        return Mage_Loader::getUrl($url, Mage_Loader::URL_TYPE_TEMPLATE);
    }
    
    public function getIncludesUrl($url = '') {
        return Mage_Loader::getUrl($url, Mage_Loader::URL_TYPE_INCLUDES);
    }
    
    /**
     * Called by the Layout singleton after all actions on the block have been
     * executed. At this point it is safe to read the block's internal data;
     */
    protected function _afterInitialize() {}
    
	/**
	 *
	 * @return Xedin_Core_Model_Layout
	 */
    public function getLayout() {
        return $this->_layout;
    }
    
    public function setLayout(Xedin_Core_Model_Layout $layout) {
        $this->_layout = $layout;
        return $this;
    }
    
    /**
     * Adds a child block.
     * 
     * @param string|Xedin_Core_Block_Abstract $block Either a name or an instance of the block to add.
     * @return Xedin_Core_Block_Abstract 
     */
    public function insert($block) {
		try {
			if(is_string($block) ) {
				$blockName = $block;
				$block = $this->getLayout()->getBlock($block);
				if( !$block ) {
					throw new Exception('Block with name "' . $blockName . '" not found in layout.');
				}
			}
			
			if( !$block ) {
				ob_start();
				var_dump($block);
				$blockDump = ob_get_clean();
				throw new Exception('Object is not a block:' . "\n" . $blockDump );
			}
			
			$this->_children[$block->getNameInLayout()] = $block;
			$this->_sortedChildrenNames[] = $block->getNameInLayout();
		}
		catch(Exception $e) {
			throw Hub::exception('Xedin_Core', $e->getMessage());
		}
        return $this;
    }
    
    public function getChildBlocks() {
        return $this->_children;
    }
    
    public function getSortedChildren() {
		uasort($this->_sortedChildrenNames, array($this, '_orderCompare'));
//		var_dump($this->_sortedChildrenNames);
        return $this->_sortedChildrenNames;
    }
	
	protected function _orderCompare($blockNameA, $blockNameB) {
		$blockA = $this->getChild($blockNameA);
		$blockB = $this->getChild($blockNameB);
		
		if( !$blockA || !$blockB ) {
			return 0;
		}
		
		$before = explode(',', $blockA->getBefore());
		if( $blockA->hasBefore() && ( in_array($blockNameB, $before) || $blockA->getBefore() == '-') ) {
			return -1;
		}
		
		$after = explode(',', $blockA->getAfter());
		if( $blockA->hasAfter() && ( in_array($blockNameB, $after) || $blockA->getAfter() == '-') ) {
			return 1;
		}
		
		return 0;
	}
    
    public function getChild($name) {
        if( !isset($this->_children[$name]) ) {
            return null;
        }
        
        return $this->_children[$name];
    }
    
    public function getChildHtml($name='') {
        if( !empty($name) ) {
            $child = $this->getChild($name);
            if( !$child ) {
                return '';
            }
            
            return $child->toHtml();
        }
        
        $html = '';
        foreach( $this->getSortedChildren() as $idx => $_childName ) {
            $html .= $this->getChildHtml($_childName);
        } 
        return $html;
    }
    
    public function doneInitializing() {
        $this->_afterInitialize();
    }
	
	public function doneCreating() {
		$this->_afterCreation();
		return $this;
	}
	
	protected function _afterCreation() {
		return $this;
	}
	
	public function clear() {
		$this->unsTemplate();
		
		$layout = $this->getLayout();
		foreach( $this->_children as $_name => $_block ) {
			/* @var $block Xedin_Core_Block_Abstract */
			$layout->removeBlock($_name);
		}
		
		$this->_children = array();
		$this->_sortedChildrenNames = array();
		return $this;
	}
	
	public function parseTags($input, $data = null) {
		if( is_null($data) ) {
			$data = $this->getData();
		}
		
		foreach( $data as $_tag => $_value )  {
			$_tagText = $this->_tagStartsWith . $_tag . $this->_tagEndsWith;
			$input = preg_replace('!' . preg_quote($_tagText, '!') . '!', $_value, $input);
		}
		
		return $input;
	}
}