<?php

/**
 * Description of Datetime
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Helper_Datetime extends Xedin_Core_Helper_Abstract {
	
	const FORMAT_MYSQL_DATETIME = 'Y-m-d H:i:s'; // YYYY-MM-DD HH:MM:SS
	
	protected $_timeUnits = array(
		'year'				=>	29030400,
		'month'				=>	2419200,
		'week'				=>	604800,
		'day'				=>	86400,
		'hour'				=>	3600,
		'minute'			=>	60,
		'second'			=>	1
	);
	
	public function getTimeDifference($start, $end = null) {
		if( is_null($end) ) {
			$end = time();
		}
		
		return $start - $end;
	}
	
	protected function _getTimeInUnit($unit, $time = null) {
		if( is_null($time) ) {
			$time = time();
		}
		
		$unit = $this->getTimeUnits($unit);
		return $time/$unit;
	}
	
	public function getHumanTime($timestamp, $recursive = true, $timeUnits = null, $float = false) {
		$result = array();
		
		$timeUnits = $this->getTimeUnits($timeUnits);
		
		foreach( $timeUnits as $type => $seconds ) {
			$amount = $timestamp / $seconds;
			$result[$type] = $float ? $amount : floor($amount);
			if( $recursive ) {
				$timestamp -= floor($amount);
			}
		}
		
		return $result;
	}
	
	public function getHumanTimeDifference($startsAt, $endsAt = null, $timeUnits = null) {
		$time = $this->getTimeDifference($startsAt, $endsAt);
		return $this->getHumanTime($time, true, $timeUnits);
	}
	
	public function getTimeGmt($date = null) {
		if( is_null($date) ) {
			$date = $this->getFormattedTimeStamp();
		}
		
		return strtotime($date . ' UTC');
	}
	
	public function getFormattedTimeStamp($timestamp = null, $format = self::FORMAT_MYSQL_DATETIME) {
		if( is_null($timestamp) ) {
			$timestamp = time();
		}
		
		return gmdate($format, $timestamp);
	}
	
	public function getMysqlDatetimeFormat() {
		return self::FORMAT_MYSQL_DATETIME;
	}
	
	/**
	 * If parameter is omitted, an aray of all time units will be returned, each key in format "type => seconds".
	 * If parameter is a string, the number of seconds for that unit will be returned if it exists, or null if unit is not found.
	 * If parameter is an number, an array will be returned similar to the one that is returned if no parameter is passed, but will only contain the specified number of units ordered by smallest first, but in reverse order.
	 * If parameter is an array, an array similar to the one in the prevous case is returned, but only containing the specified units, ordered by largest first.
	 * 
	 * Example:
	 * var_dump($this->getTimeUnits('hour'));
	 * // array( [hour]	=>	3600 )
	 * var_dump($this->getTimeUnits(3));
	 * // array( [second]	=>	1, [minute]	=>	60, [hour]	=>	3600 )
	 * var_dump($this->getTimeUnits(array('year', 'minute', 'day'));
	 * // array( [year]	=>	29030400, [day]	=>	86400, [minute] => 60 )
	 * 
	 * @param null|string|int|array $type
	 * @return int|array|null
	 */
	public function getTimeUnits($type = null) {
		if( is_null($type) ) {
			return $this->_timeUnits;
		}
	
		if( !is_numeric($type) && !is_array($type) ) {
		
			if( !isset($this->_timeUnits[$type]) ) {
				return null;
			}

			return array($type => $this->_timeUnits[$type]);
		}
		
		$toReturn = array();
		
		if( is_numeric($type) ) {
			$units = array_reverse($this->_timeUnits, true);
			$unitKeys = array_keys($units);
			for( $i=0; $i<(int)$type; $i++ ) {
				if( !isset($unitKeys[$i]) ) {
					break;
				}
				$toReturn[$unitKeys[$i]] = $units[$unitKeys[$i]];
			}
			return arsort($toReturn, SORT_NUMERIC);
		}
		
		if( is_array($type) ) {
			foreach( $this->_timeUnits as $_type => $_seconds ) {
				if( !in_array($_type, $type) ) {
					continue;
				}
				
				$toReturn[$_type] = $this->_timeUnits[$_type];
			}
			return arsort($toReturn, SORT_NUMERIC);
		}
		
		return $toReturn;
	}
}