<?php

/**
 * Description of Uri
 *
 * @version 0.1.1
 * + Added getSkinUrl() method.
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Helper_Uri extends Xedin_Core_Helper_Abstract {
	
	/**
	 * @return Xedin_Core_Model_Uri
	 */
	public function getUriModel() {
		return Hub::getSingleton('core/uri');
	}
	
	public function getSkinUrl($url) {
		return Hub::getUrl($url, Hub::URL_TYPE_SKIN);
	}
}