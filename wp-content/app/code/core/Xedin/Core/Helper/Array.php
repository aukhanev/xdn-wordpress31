<?php

/**
 * A helper for array functionality.
 * Includes all PHP array functions. To utilize, call a camel-cased version
 * of the PHP function name, without the 'array_' prefix.<br />
 * Example:
 * $helper = Hub::getHelper('core/array');
 * $index = $helper->search($needle, $haystack);
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Helper_Array extends Xedin_Core_Helper_Abstract {
	
	public function __call($name, $arguments) {
		$stringHelper = Hub::getHelper('core/string');
		/* @var $stringHelper Xedin_Core_Helper_String */
		
		$phpName = 'array_' . $stringHelper->underscore($name);
		if( function_exists($phpName) ) {
			return call_user_func_array($phpName, $arguments);
		}
		
		throw Hub::exception('Xedin_Core', 'No method "' . $name . '" exists in class "' . get_class($this) . '", nor does a function called "' . $phpName . '" exist in PHP ' . phpversion() . '.');
	}
	
	protected function _concatenate($keys = false) {
		$array = array();
		$args = func_get_args();
		$keys = array_shift($args);
		foreach( $args as $_idx => $_arg ) {
			$_arg = (array)$_arg;
			$values = $keys ? array_keys($_arg) : array_values($_arg);
			foreach( $values as $_valIdx => $_value ) {
				$array[] = $_value;
			}
		}
		
		return $array;
	}
	
	/**
	 * Alias of concatenateValues().
	 * @see concatenateValues().
	 * @return type 
	 */
	public function concatenate($array1, $arrayN) {
		$args = func_get_args();
		return call_user_func_array(array($this, 'concatenateValues'), $args);
	}
	
	/**
	 * Combines arrays in a way where the values of the next array will be appended
	 * to the previous, in the order they appear in the array.
	 * Keys are ignored, duplicates are added regardless.
	 * Example:
	 * $array1 = array('pear', 'banana', 'apple');
	 * $array2 = array('fruit' => 'kiwi', 'banana', 'orange');
	 * $array3 = $this->concatenate($array1, $array2);
	 * 
	 * Here, the array 3 would be as so:
	 * array('pear', 'banana', 'apple', 'kiwi', 'banana', 'orange');
	 * 
	 * @param array $array1
	 * @param array $arrayN
	 * @return array The product of concatenation.
	 */
	public function concatenateValues($array1, $arrayN) {
		$args = func_get_args();
		array_unshift(false, $args);
		return call_user_func_array(array($this, '_concatenate'), $args);
	}
	
	/**
	 * Same as concatenateValues(), but the resulting array contains keys.
	 * 
	 * @param array $array1
	 * @param array $arrayN
	 * @return array The product of concatenation. 
	 */
	public function concatenateKeys($array1, $arrayN) {
		$args = func_get_args();
		array_unshift(true, $args);
		return call_user_func_array(array($this, '_concatenate'), $args);
	}
}