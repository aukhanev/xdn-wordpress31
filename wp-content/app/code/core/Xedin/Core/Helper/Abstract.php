<?php

/**
 * Description of Abstract
 *
 * @author anton
 */
abstract class Xedin_Core_Helper_Abstract {
	
	public function getController() {
		return Hub::app()->getFrontController()->getCurrentController();
	}
	
	public function getGlobalModel($regKey, $modelUri) {
		if( !Hub::registry($regKey) ) {
			$model = $this->_getGlobalModel($modelUri);
			if( $model ) {
				Hub::register($model, $regKey);
			}
		}
		
		return Hub::resistry($regKey);
	}
	
	protected function _getGlobalModel($modelUri, $id = null) {
		return $this->getLoadedModel($modelUri, $id);
	}
	
	public function getLoadedModel($modelUri, $id = null) {
		$model = Hub::getModel($modelUri);
		if( !empty($id) ) {
			$model->load($id);
		}
		return $model;
	}
}