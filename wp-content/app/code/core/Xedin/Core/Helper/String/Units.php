<?php

/**
 * Description of Units
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Helper_String_Units extends Xedin_Core_Helper_Abstract {
	
	public function getGrammaticalNumber($amount, $singular, $plural) {
		return $this->getNumbersHelper()->getGrammaticalNumber($amount, $singular, $plural);
	}
	
	public function getExplicitSign($number) {
		if( $number > 0 ) {
			return '+';
		}
		
		if( $number < 0 ) {
			return '-';
		}
		
		return '';
	}
	
	/**
	 * @return Xedin_Core_Helper_String
	 */
	public function getStringHelper() {
		return Hub::getHelper('core/string');
	}
	
	/**
	 * @return Xedin_Core_Helper_String_Numbers
	 */
	public function getNumbersHelper() {
		return Hub::getHelper('core/string_numbers');
	}
}