<?php

class Xedin_Core_Helper_String_Numbers extends Xedin_Core_Helper_String {

	protected $_ones = array(
		'',
		' one',
		' two',
		' three',
		' four',
		' five',
		' six',
		' seven',
		' eight',
		' nine',
		' ten',
		' eleven',
		' twelve',
		' thirteen',
		' fourteen',
		' fifteen',
		' sixteen',
		' seventeen',
		' eighteen',
		' nineteen'
	);
	protected $_tens = array(
		'',
		'',
		' twenty',
		' thirty',
		' forty',
		' fifty',
		' sixty',
		' seventy',
		' eighty',
		' ninety'
	);
	protected $_triplets = array(
		'',
		' thousand',
		' million',
		' billion',
		' trillion',
		' quadrillion',
		' quintillion',
		' sextillion',
		' septillion',
		' octillion',
		' nonillion'
	);

	/**
	 * @see http://stackoverflow.com/questions/3109978/php-display-number-with-ordinal-suffix#3110033
	 * @param int|float $number Any number
	 * @return string The number's ordinal suffix
	 */
	public function getOrdinalSuffix($number) {
		$ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');

		$number = abs(floor($number));
		if (($number % 100) >= 11 && ($number % 100) <= 13)
			$abbreviation = 'th';
		else
			$abbreviation = $ends[$number % 10];

		return $abbreviation;
	}

	public function addOrdinalSuffix($number) {
		return $number . $this->getOrdinalSuffix($number);
	}

	
	protected function _getSpelledOut($num, $tri) {

//		global $this->_ones, $tens, $triplets;

		// chunk the number, ...rxyy
		$r = (int) ($num / 1000);
		$x = ($num / 100) % 10;
		$y = $num % 100;

		// init the output string
		$str = "";

		// do hundreds
		if ($x > 0)
			$str = $this->_ones[$x] . ' hundred';

		// do ones and tens
		if ($y < 20)
			$str .= $this->_ones[$y];
		else
			$str .= $this->_tens[(int) ($y / 10)] . $this->_ones[$y % 10];

		// add triplet modifier only if there
		// is some output to be modified...
		if ($str != "")
			$str .= $this->_triplets[$tri];

		// continue recursing?
		if ($r > 0)
			return $this->_getSpelledOut($r, $tri + 1) . $str;
		else
			return $str;
	}

	public function getSpelledOut($num) {
		$num = (int) $num;	// make sure it's an integer

		if ($num < 0)
			return 'negative ' . trim($this->_getSpelledOut(-$num, 0));

		if ($num == 0)
			return 'zero';

		return trim($this->_getSpelledOut($num, 0));
	}
	
	public function getGrammaticalNumber($amount, $singular, $plural) {
		$amount = floor(abs(trim($amount)));
		if( $this->getStringHelper()->endsWith($amount, 1) ) {
			return $singular;
		}
		
		return $plural;
	}
	
	/**
	 * @return Xedin_Core_Helper_String
	 */
	public function getStringHelper() {
		return Hub::getHelper('core/string');
	}

}