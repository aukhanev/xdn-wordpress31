<?php

/**
 * @version 0.1.1
 * + Added underscore().
 * * endsWith() includes now a third parameter to control case-sensitivity.
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Core_Helper_String extends Xedin_Core_Helper_Abstract {
	
	/**
	 * Determine whether or not the first string ends with the second string.
	 *
	 * @param string $string The string to check in.
	 * @param string $against The string to search for.
	 * $param bool $caseSensitive Whether or not the comparison will be done in a case-sensitive manner. Default: false;
	 * @return bool True if $string ends with $against; otherwise, false;
	 */
	public function endsWith($string, $against, $caseSensitive = false) {
		$endsWith = substr($string, strlen($against)*-1);
		if( $caseSensitive ) {
			return (strcmp($string, $against) === 0);
		}
		
		return strcasecmp($string, $against);
	}
	
	/**
	 * Returns an "underscored" version of a camel-cased string. <br />
	 * Example: underscore('MyName') == 'my_name'
	 * 
	 * @param string $string The string to underscore.
	 * @return string An "underscored" version of $string.
	 */
	public function underscore($string) {
        return strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $string));
	}
}