<?php

class Xedin_Core_Controller_Abstract extends Xedin_Controller_Abstract {

	protected $_adminUrlBase = 'admin';
	protected $_loginControllerName = 'login';
	protected $_logoutActionName = 'logout';
	protected $_userNamespace = 'user';
    
    public function getRequest() {
        return Hub::app()->getFrontController()->getRequest();
    }
	
	public function getUserNamespace() {
		return $this->_userNamespace;
	}
	
	protected function _setUserNamespace($userNamespace) {
		$this->_userNamespace = $userNamespace;
	}
	
	public function isLoginController() {
		return $this->getControllerName() == $this->getLoginControllerName();
	}
	
	public function isLoginPage() {
		return $this->isLoginController() && $this->getActionName() == 'index';
	}
	
	public function isLogoutPage() {
		return $this->isLoginController() && $this->getActionName() == $this->getLogoutActionName();
	}
	
	public function getLoginControllerName() {
		return $this->_loginControllerName;
	}
	
	public function getLogoutActionName() {
		return $this->_logoutActionName;
	}
	
	public function getLoginPath() {
		return $this->getLoginControllerName();
	}
	
	public function getLogoutPath() {
		return $this->getLoginControllerName() . '/' . $this->getLogoutActionName();
	}
	
	/**
	 * @return Xedin_Users_Model_Session
	 */
	public function getUserSession() {
		return Hub::getSingleton('users/session');
	}
	
	public function isUserLoggedIn() {
		return $this->getUserSession()->isUserLoggedIn();
	}

	public function getAdminUrl($url = '') {
		if( !empty($url) ) {
			$url = $this->_adminUrlBase . '/' . $url;
		}
		return $this->getRedirectUrl($url);
	}
	
	public function getLayout() {
		return Hub::app()->getLayout();
	}
	
	protected function _404() {
		$this->getFrontController()->notFound();
	}
	
	public function getFrontController() {
		return Hub::app()->getFrontController();
	}
}