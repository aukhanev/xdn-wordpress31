<?php

return array(
	'system' => array(
		'modules' => array(
			'b2b' => array(
				'name' => 'b2b',
				'label' => 'B2B',
				'models' => array(
					'business' => array(
						'path'			=>	'b2b/business',
						'label'			=>	'Businesses'
					),
					'representative' => array(
						'path'			=>	'b2b/representative',
						'label'			=>	'Representative',
						'fields'		=>	array(
							'business_id'	=>	array(
								'label'			=>	'Business',
								'field'			=>	'business_id',
								'render_model'	=>	'b2b/render_business'
							)
						)
					)
				)
			)
		)
	)
)
		
?>