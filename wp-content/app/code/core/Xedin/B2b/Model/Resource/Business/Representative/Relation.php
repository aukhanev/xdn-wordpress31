<?php

/**
 * Description of Relation
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_B2b_Model_Resource_Business_Representative_Relation extends Xedin_Core_Model_Resource_Relation_Abstract {
	
	protected function _construct() {
		$this->setParentIdFieldName('business_id')
				->_setParentUri('b2b/business')
				->setChildIdFieldName('representative_id')
				->_setChildUri('b2b/representative')
				->_init('b2b_business_business_representative');
	}
}