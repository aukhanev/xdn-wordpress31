<?php

/**
 * Description of Business
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_B2b_Model_Resource_Business extends Xedin_B2b_Model_Resource_Meta_Abstract {

	protected function _construct() {
		parent::_construct();
		$this->_init('b2b_business');
	}

}