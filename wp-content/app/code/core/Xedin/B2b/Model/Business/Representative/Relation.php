<?php

/**
 * Description of Relation
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_B2b_Model_Business_Representative_Relation extends Xedin_Core_Model_Relation_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('b2b/business_representative_relation');
	}
}