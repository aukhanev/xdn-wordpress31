<?php

/**
 * Description of Meta
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_B2b_Model_Business_Meta extends Xedin_B2b_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('b2b/business_meta');
	}
}