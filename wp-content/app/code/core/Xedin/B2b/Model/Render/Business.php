<?php

/**
 * Description of Business
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_B2b_Model_Render_Business extends Xedin_Adminhtml_Model_Render_Fields_Select_Abstract {
	protected $_modelUri = 'b2b/business';
	protected $_labelFieldName = 'code';
}