<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
class Xedin_Adminhtml_Model_Meta_Abstract extends Xedin_Core_Model_Meta_Abstract {
	
	public function getSession() {
		return Hub::getSingleton('adminhtml/session');
	}
	
	public function getUserSession() {
		return Hub::getSingleton('users/session');
	}
}
