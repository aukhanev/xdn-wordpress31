<?php

/**
 * Description of Config
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Adminhtml_Model_Config extends Xedin_Core_Model_Config_Abstract {
	
	const XML_PATH_SYSTEM_CONFIG = 'system';
	const XML_NODE_MODULES = 'modules';
	const XML_NODE_MODELS = 'models';
	
	public function getSystemConfig($path = '') {
		$path = $this->_getNormalizedPath($path, self::XML_PATH_SYSTEM_CONFIG);
		return $this->getValue($path);
	}
	
	public function getModules() {
		return $this->getSystemConfig(self::XML_NODE_MODULES);
	}
	
	public function getModuleConfig($moduleName) {
		return $this->getSystemConfig( array(self::XML_NODE_MODULES, $moduleName) );
	}
	
	public function getModelConfig($modelHandle, $moduleName) {
		return $this->getSystemConfig( array(self::XML_NODE_MODULES, $moduleName, self::XML_NODE_MODELS, $modelHandle) );
	}
}