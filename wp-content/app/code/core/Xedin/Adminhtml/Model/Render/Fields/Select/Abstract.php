<?php

/**
 * Description of Type
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Adminhtml_Model_Render_Fields_Select_Abstract extends Xedin_Adminhtml_Model_Render_Fields_Abstract {
	
	protected $_blockUri = 'html/element_input_select';
	protected $_sortOrders = array();
	static protected $_optionsCollection = array();
	
	public function addSortOrder($fieldName = null, $ascending = false) {
		if( is_null($fieldName) ) {
			$fieldName = $this->getLabelFieldName();
		}
		
		$ascending = (bool)$ascending;
		
		$this->_sortOrders[$fieldName] = array('field_name' => $fieldName, 'ascending' => $ascending);
		return $this;
	}
	
	public function getSortOrder($fieldName = null, $default = null) {
		if( is_null($fieldName) ) {
			return $this->_sortOrders;
		}
		
		if( !isset($this->_sortOrders[$fieldName]) ) {
			return $default;
		}
		
		return $this->_sortOrders[$fieldName];
	}
	
	protected function _render($model, $name, $id) {
		if( $this->isRendered() ) {
			return $this;
		}
		
		if( !$this->hasBlockUri() ) {
			return '';
		}
		
		$value = $model;
		if( $value instanceof Varien_Object ) {
			$value = $value->getData($this->getValueFieldName());
		}
		
		if( !$value ) {
			$value = (int)$value;
		}
		
		$block = $this->getBlock()->setLabelFieldName($this->getLabelFieldName());
		$block->setName($name)
				->setValue($value)
				->setId($id);
		
//		$profiler = $this->getOptionsCollection()->getProfiler()->setEnabled(true);
		$items = $this->getOptionsCollection()->getItems();
//		var_dump($profiler->getLastQueryProfile());
		if( !isset($items[0]) ) {
			$items[0] = array($this->getLabelFieldName() => '[Please Select]');
		}
		
		if( !empty($items) ) {
			$block->setOptions($items);
		}
		
		$this->setLabel($this->getModel()->load($value)->getData($this->getLabelFieldName()));
		$this->setField( $block->toHtml() );
		$this->_isRendered;
		
		return $this;
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection
	 */
	public function getOptionsCollection() {
		if( !isset(self::$_optionsCollection[$this->getModelUri()]) ) {
			self::$_optionsCollection[$this->getModelUri()] = $this->getModel()->getCollection();
			foreach( $this->getSortOrder() as $_fieldName => $_sortInfo ) {
//				var_dump($_fieldName);
				self::$_optionsCollection[$this->getModelUri()]->addSortOrder($_fieldName, $_sortInfo['ascending']);
			}
		}
		return self::$_optionsCollection[$this->getModelUri()];
	}
}