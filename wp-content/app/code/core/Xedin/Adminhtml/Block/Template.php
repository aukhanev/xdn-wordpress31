<?php

/**
 * 
 */
class Xedin_Adminhtml_Block_Template extends Xedin_Core_Block_Template {
	
	protected $_modelUri;
	protected $_modelFields;
	
	public function setModelUri($modelUri) {
		$this->_modelUri = $modelUri;
		return $this;
	}
	
	public function getModelUri() {
		if( !$this->_modelUri ) {
			$modelConfig = $this->getModelConfig();
			$this->_modelUri = $modelConfig['path'];
		}
		
		return $this->_modelUri;
	}
	
	public function getModelFields() {
		if( !$this->_modelFields ) {
			$modelConfig = $this->getModelConfig();
			$this->_modelFields = isset($modelConfig['fields']) ? $modelConfig['fields'] : array();
		}
		
		return $this->_modelFields;
	}
	
	public function getModel() {
		return Hub::getSingleton( $this->getModelUri() );
	}
	
	/**
	 * @param string|array $path
	 * @return Xedin_Adminhtml_Model_Config|mixed
	 */
	public function getConfig($path = null) {
		if( is_null($path) ) {
			return Hub::getSingleton('adminhtml/config');
		}
		
		return Hub::getSingleton('adminhtml/config')->getValue($path);
	}
	
	public function getSystemConfig($path = '') {
		return $this->getConfig()->getSystemConfig($path);
	}

	/**
	 * @return \Xedin_Adminhtml_Controller_Abstract
	 * @throws Xedin_Adminhtml_Exception
	 */
	public function getController() {
		$controller = parent::getController();
		if( !($controller instanceof Xedin_Adminhtml_Controller_Abstract) ) {
			throw Hub::exception('Xedin_Adminhtml', __CLASS__ . ' can only be used while in admin.');
		}
		
		return $controller;
	}
	
	public function getCurrentModule() {
		return $this->getController()->getAdminModule();
	}
	
	public function getCurrentModel() {
		return $this->getController()->getCurrentModel();
	}
	
	public function getCurrentModelAction() {
		return $this->getController()->getCurrentModelAction();
	}
	
	public function getAdminUrlHelper() {
		return Hub::getHelper('adminhtml/url');
	}
	
	public function getModuleConfig($moduleName = null) {
		if( is_null($moduleName) ) {
			$moduleName = $this->getCurrentModule();
		}
		return $this->getConfig()->getModuleConfig($moduleName);
	}
	
	public function getModelConfig($modelHandle = null, $moduleName = null) {
		if( is_null($modelHandle) ) {
			$modelHandle = $this->getCurrentModel();
		}
		
		if( is_null($moduleName) ) {
			$moduleName = $this->getCurrentModule();
		}
		return $this->getConfig()->getModelConfig($modelHandle, $moduleName);
	}
	
	public function getAdminLinkUrl($moduleName = null, $modelHandle = null, $actionHandle = null, $params = array()) {		
		if( trim($actionHandle) == '*' ) {
			$actionHandle = $this->getCurrentModelAction();
		}
		
		if( trim($modelHandle) == '*' ) {
			$modelHandle = $this->getCurrentModel();
		}
		
		if( trim($moduleName) == '*' ) {
			$moduleName = $this->getCurrentModule();
		}
		$uri = array();
		foreach( array($moduleName, $modelHandle, $actionHandle) as $_idx => $_part ) {
			if( !is_null($_part) ) {
				$uri[] =  $_part;
			}
		}
		
		$url = $this->getController()->getAdminUrl( implode('/', $uri) );
		
		if( !empty($params) ) {
			foreach( $params as $_key => $_value ) {
				$url .= '/' . $_key . '/' . $_value;
			}
		}
		
		return $url;
	}
}