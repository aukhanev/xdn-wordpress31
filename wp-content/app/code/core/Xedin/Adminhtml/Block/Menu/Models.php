<?php

/** 
 *
 * @author anton
 */
class Xedin_Adminhtml_Block_Menu_Models extends Xedin_Adminhtml_Block_Menu_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('page/header/menu/models.php');
	}
	
	public function getModels() {
		$moduleConfig = $this->getModuleConfig();
		return $moduleConfig['models'];
	}
}