<?php

/**
 * 
 */
class Xedin_Adminhtml_Block_Login extends Xedin_Adminhtml_Block_Template {
	
	
	public function getFormActionUrl() {
		return Hub::app()->getController()->getRedirectUrl('*');
	}
	
	public function getUserNamespace() {
		return $this->getController()->getUserNamespace();
	}
	
	public function getUserFieldName($name) {
//		return 'user[' . $name . ']';
		return Hub::getHelper('html/form_field')->getNameFromPath(array('user', $name));
	}
	
	public function getUserFieldId($id) {
//		return 'user:' . $id;
		return Hub::getHelper('html/form_field')->getIdFromPath(array('user', $id));
	}
	
	public function getUserFieldLabel($name) {
		$name = str_replace(array('/_'), ' ', $name);
		return ucwords($name);
	}
	
	public function getFields() {
		return array(
			'username'			=>	array(),
			'password'			=>	array(
				'type'				=>	'password'
			)
		);
	}
}