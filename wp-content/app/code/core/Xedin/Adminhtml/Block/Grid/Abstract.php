<?php
/**
 * Description of List
 *
 * @author anton
 */
abstract class Xedin_Adminhtml_Block_Grid_Abstract extends Xedin_Adminhtml_Block_Grid_View_Abstract {
	
	protected $_fields = array();
	protected $_currentFieldIndex = 1;
	
	const COLUMN_INFO_INDEX_NAME = 'name';
	const COLUMN_INFO_INDEX_FIELD = 'field';
	const COLUMN_INFO_INDEX_LABEL = 'label';
	const COLUMN_INFO_INDEX_RENDER_MODEL = 'render_model';
	const COLUMN_INFO_INDEX_SORT_POSITION = 'sort_position';
	
	protected $_columnFields = array(
		'name'			=>	self::COLUMN_INFO_INDEX_NAME,
		'field'			=>	self::COLUMN_INFO_INDEX_FIELD
	);
	
	protected function _toHtml() {
		if( !$this->getModelUri() ) {
			return '';
		}
		
		return parent::_toHtml();
	}
	
	public function getModelClassName() {
		return str_ireplace('/', '-', $this->getModelUri());
	}	
	
	public function setField(array $columnInfo) {
		if( !isset($columnInfo[self::COLUMN_INFO_INDEX_NAME]) && isset($columnInfo[self::COLUMN_INFO_INDEX_FIELD]) ) {
			$columnInfo[self::COLUMN_INFO_INDEX_NAME] = $columnInfo[self::COLUMN_INFO_INDEX_FIELD]; 
		}
		
		if( !isset($columnInfo[self::COLUMN_INFO_INDEX_FIELD]) && isset($columnInfo[self::COLUMN_INFO_INDEX_NAME]) ) {
			$columnInfo[self::COLUMN_INFO_INDEX_FIELD] = $columnInfo[self::COLUMN_INFO_INDEX_NAME]; 
		}
		
		$columnInfo = array_merge($this->getFieldDefaultInfo($columnInfo), $columnInfo);
		
		$this->_fields[$columnInfo[self::COLUMN_INFO_INDEX_NAME]] = new Varien_Object($columnInfo);
		$this->_currentFieldIndex++;
		return $this;
	}
	
	public function getFields($name = null, $default = null) {
		if( empty($this->_fields) ) {
			$this->_generateFields();
			$this->_sortFields();
		}
		
		if( is_null($name) ) {
			return $this->_fields;
		}
		
		if( !isset($this->_fields[$name]) ) {
			return $default;
		}
		
		return $this->_fields[$name];
	}
	
	protected function _sortFields() {
		uasort($this->_fields, array($this, '_compareFields'));
		return $this;
	}
	
	protected function _compareFields($fieldA, $fieldB) {
		$fieldASortOrder = $fieldA[self::COLUMN_INFO_INDEX_SORT_POSITION];
		$fieldBSortOrder = $fieldB[self::COLUMN_INFO_INDEX_SORT_POSITION];
		
		if( $fieldASortOrder > $fieldBSortOrder ) {
			return 1;
		}
		
		if( $fieldBSortOrder > $fieldASortOrder ) {
			return -1;
		}
		
		return 0;
	}
	
	public function hasFields($name = null) {
		if( is_null($name) ) {
			return !empty($this->_fields);
		}
		
		return isset($this->_fields[$name]);
	}
	
	public function addField($columnInfo) {
		if( !$this->hasFields($columnInfo[self::COLUMN_INFO_INDEX_NAME]) ) {
			$this->setField($columnInfo);
		}
		
		return $this;
	}
	
	protected function _generateFields() {
		$this->_beforeGenerateFields();
		$this->_generateAdditionalFields();
		$this->_afterGenerateFields();
		return $this;
	}
	
	protected function _generateAdditionalFields() {
		$this->_beforeGenerateAdditionalFields();
		
		$modelFields = $this->getModelFields();
		if( !empty($modelFields) ) {
			foreach( $modelFields as  $_fieldName => $_fieldInfo ) {
				$_newFieldInfo = array(
					self::COLUMN_INFO_INDEX_NAME			=>	$_fieldName,
					self::COLUMN_INFO_INDEX_FIELD			=>	$_fieldInfo[self::COLUMN_INFO_INDEX_FIELD]
				);
				
				$optionalFields = array(
					self::COLUMN_INFO_INDEX_LABEL,
					self::COLUMN_INFO_INDEX_RENDER_MODEL,
					self::COLUMN_INFO_INDEX_SORT_POSITION
				);

				foreach( $optionalFields as $_idx => $_fieldName ) {
					if( isset($_fieldInfo[$_fieldName]) ) {
						$_newFieldInfo[$_fieldName] = $_fieldInfo[$_fieldName];
					}
				}
				
				$this->setField($_newFieldInfo);
			}
		}
		
		$this->_afterGenerateAdditionallFields();
		return $this;
	}
	
	protected function _beforeGenerateAdditionalFields() {
		return $this;
	}
	
	protected function _afterGenerateAdditionallFields() {
		return $this;
	}
	
	public function getFieldDefaultInfo($field = null) {
		$defaultInfo = array();
		return $defaultInfo;
	}
	
	public function getFieldValue($model, $columnName) {
		$dataFieldName = $this->getFields($columnName)->getData(self::COLUMN_INFO_INDEX_FIELD);
		return $model->getData($dataFieldName);
	}
	
	public function getColumnInfo($col, $infoField, $default = null) {
		if( !isset($this->_columnFields[$infoField]) ) {
			return $default;
		}
		
		return $col->getData($this->_columnFields[$infoField]);
	}
	
	public function getColumnFields($name = null, $default = null) {
		if( is_null($name) ) {
			return $this->_columnFields;
		}
		
		if( !isset($this->_columnFields[$name]) ) {
			return $default;
		}
		
		return $this->_columnFields[$name];
	}
	
	public function hasColumnField($name = null) {
		if( is_null($name) ) {
			return !empty($this->_columnFields);
		}
		
		return isset($this->_columnFields[$name]);
	}
	
	public function setColumnFields($name, $value = null) {
		if( is_null($value) && is_array($name) ) {
			$this->_columnFields = $name;
		}
		
		$this->_columnFields[$name] = $value;
		return $this;
	}
	
	protected function _beforeGenerateFields() {
		
	}
	
	protected function _afterGenerateFields() {
		
	}
}