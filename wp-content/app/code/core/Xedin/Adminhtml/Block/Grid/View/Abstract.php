<?php

/**
 * Description of Abstract
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Adminhtml_Block_Grid_View_Abstract extends Xedin_Adminhtml_Block_Template {
	
	protected $_modelId;
	protected $_modelIdParamKey = 'id';
	protected $_currentModel;
	
	public function getModelIdParamKey() {
		return $this->_modelIdParamKey;				
	}
	
	public function setModelIdParamKey($paramKey) {
		$this->_modelIdParamKey = $paramKey;
		return $this;
	}
	
	public function getModelId() {
		if( !$this->_modelId ) {
			$this->_modelId = $this->getController()->getParam( $this->getModelIdParamKey() );
		}
		return $this->_modelId;
	}
	
	public function setModelId($modelId) {
		$this->_modelId = $modelId;
		return $this;
	}
	
	/**
	 * The model, whose details are to be displayed.
	 * @return Xedin_Core_Model_Abstract
	 */
	public function getCurrentObject() {
		if( !$this->_currentModel ) {
			$model = Hub::getModel($this->getModelUri());
			if( $this->getModelId()) {
				$model->load($this->getModelId());
			}
//			$this->_currentModel = $model->hasData() ? $model : null;
			$this->_currentModel = $model;
		}
		
		return $this->_currentModel;
	}
	
	public function getModelFieldNamespace($modelUri = null) {
		if( is_null($modelUri) ) {
			$modelUri = $this->getModelUri();
		}
		
		return $this->getFormFieldHelper()->getModelNamespace($modelUri);
	}
	
	public function getFormFieldHelper() {
		return Hub::getHelper('html/form_field');
	}
	
	public function getFieldName($field, $model = null) {
		$model = $this->getModelFieldNamespace($model);
		return $this->getFormFieldHelper()->getNameFromPath(array($model, $field));
	}
	
	public function getFieldId($field, $model = null) {
		$model = $this->getModelFieldNamespace($model);
//		var_dump(array($model, $field));
		return $this->getFormFieldHelper()->getIdFromPath(array($model, $field));
	}
}