<?php
/**
 * Description of List
 *
 * @author anton
 */
class Xedin_Adminhtml_Block_Grid_List extends Xedin_Adminhtml_Block_Grid_Abstract {
	
	protected $_collection;
	
	protected $_idColWidth = 5;
	protected $_actionColWidth = 10;
	
	protected $_controlsBlockUri = 'adminhtml/grid_list_controls';
	protected $_controlsBlock;
	
	const COLUMN_INFO_INDEX_WIDTH = 'width';
	const COLUMN_INFO_INDEX_LABEL = 'label';
	const COLUMN_INFO_INDEX_COL_TYPE = 'col_type';
	const COLUMN_INFO_INDEX_RENDER_MODEL = 'render_model';
	
	const COL_TYPE_DATA = 'data';
	const COL_TYPE_ACTIONS = 'actions';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setColumnFields('width', self::COLUMN_INFO_INDEX_WIDTH);
		$this->setColumnFields('label', self::COLUMN_INFO_INDEX_LABEL);
		$this->setColumnFields('col_type', self::COLUMN_INFO_INDEX_COL_TYPE);
		
		$this->setTemplate('adminhtml/grid/list.php');
		$this->getLayout()->getBlock('main')->getControlsBlock()->addItem($this->getModelViewUrl(), 'Add New', null, false);
	}
	
	public function getIdColWidthPercentage() {
		return $this->_idColWidth;
	}
	
	public function setIdColWidthPercentage($width) {
		$this->_idColWidth = $width;
		return $this;
	}
	
	public function getActionColWidth() {
		return $this->_actionColWidth;
	}
	
	public function setActionColWidth($width) {
		$this->_actionColWidth = $width;
		return $this;
	}
	
	/**
	 * @return Xedin_Core_Model_Db_Collection
	 */
	public function getCollection() {
		if( !$this->_collection ) {
			$this->_collection = $this->getModel()->getCollection()->setPerPage($this->getPerPage())
					->setPage($this->getCurrentPage())
					->addTotalNumRowsToSelect();
		}
		return $this->_collection;
	}
	
	public function getLoadedCollection() {
		return $this->getCollection()->load();
	}
	
	protected function _generateFields() {
		$this->_beforeGenerateFields();
		$fields = array_keys( $this->getModel()->getResourceModel()->getTableDescription() );
		$fieldCount = count($fields);
		$percentPerCol = ( 100 - ($this->getIdColWidthPercentage() + $this->getActionColWidth()) ) / $fieldCount;
		$this->_currentFieldIndex = 1;
		
		foreach( $fields as $_idx => $_fieldName ) {
			$width = $percentPerCol;
			if( $_fieldName == $this->getModel()->getIdFieldName() ) {
				$width = $this->getIdColWidthPercentage();
			}
			
			$this->addField(array(
				self::COLUMN_INFO_INDEX_COL_TYPE		=>	self::COL_TYPE_DATA,
				self::COLUMN_INFO_INDEX_NAME			=>	$_fieldName,
				self::COLUMN_INFO_INDEX_FIELD			=>	$_fieldName,
				self::COLUMN_INFO_INDEX_WIDTH			=>	$width . '%'
			));
			$this->_currentFieldIndex++;
		}
		$this->_generateAdditionalFields();
		
		$this->addField(array(
			self::COLUMN_INFO_INDEX_COL_TYPE		=>	self::COL_TYPE_ACTIONS,
			self::COLUMN_INFO_INDEX_NAME			=>	'sys-actions',
			self::COLUMN_INFO_INDEX_WIDTH			=>	$this->getActionColWidth() . '%',
			self::COLUMN_INFO_INDEX_LABEL			=>	'Actions',
			self::COLUMN_INFO_INDEX_SORT_POSITION	=>	99999
		));
		
		$this->_afterGenerateFields();
		
		return $this;
	}
	
	public function getFieldDefaultInfo($field = null) {
		$defaultInfo = array();
		$defaultInfo[self::COLUMN_INFO_INDEX_NAME] = uniqid('field-');
		$defaultInfo[self::COLUMN_INFO_INDEX_WIDTH]	= '50px';
		$defaultInfo[self::COLUMN_INFO_INDEX_LABEL] = '';
		$defaultInfo[self::COLUMN_INFO_INDEX_FIELD] = '';
		$defaultInfo[self::COLUMN_INFO_INDEX_COL_TYPE] = self::COL_TYPE_DATA;
		$defaultInfo[self::COLUMN_INFO_INDEX_RENDER_MODEL] = 'adminhtml/render_fields_generic';
		$defaultInfo[self::COLUMN_INFO_INDEX_SORT_POSITION] = $this->_currentFieldIndex*10;
		
		if( $field ) {
			if( (!isset($defaultInfo[self::COLUMN_INFO_INDEX_LABEL]) || empty($defaultInfo[self::COLUMN_INFO_INDEX_LABEL])) && isset($field[self::COLUMN_INFO_INDEX_NAME]) ) {
				$defaultInfo[self::COLUMN_INFO_INDEX_LABEL] = ucwords( str_replace(array('_', '/'), ' ', $field[self::COLUMN_INFO_INDEX_NAME]) );
			}
		}
		return $defaultInfo;
	}
	
	public function getModelViewUrl($id = null) {
		if( $id instanceof Xedin_Core_Model_Abstract ) {
			$id = $id->getId();
		}
		
		$controller = $this->getController();
		$extraParams = $id ? array('id' => $id) : array();
		return $this->getAdminLinkUrl('*', '*', $controller->getModelActionSegmentValue('view'), $extraParams);
	}
	
	public function getModelDeleteUrl($id) {
		if( $id instanceof Xedin_Core_Model_Abstract ) {
			$id = $id->getId();
		}
		
		$controller = $this->getController();
		return $this->getAdminLinkUrl('*', '*', $controller->getModelActionSegmentValue('delete'), array('id' => $id));
	}
	
	public function getFieldValue($model, $columnName, $default = 'N/A') {
		$columnInfo = $this->getFields($columnName);
		if( !$columnInfo ) {
			return $default;
		}

//		var_dump($model->getData());
		$value = $model->getData($this->getColumnInfo($columnInfo, 'field'));
		$renderModel = Hub::getSingleton($columnInfo->getRenderModel());
//		var_dump($renderModel);
		/* @var $renderModel Xedin_Adminhtml_Model_Render_Values_Abstract */
		
		return $renderModel->setValue($value)->getRenderedLabel();
	}
	
	/**
	 * @return Xedin_Adminhtml_Block_Grid_List_Controls
	 */
	public function getControlsBlock() {
		if( !$this->_controlsBlock ) {
			$this->_controlsBlock = $this->getLayout()->createBlock($this->getControlsBlockUri(), $this->getNameInLayout());
			$this->_controlsBlock->setListBlock($this);
		}
		
		return $this->_controlsBlock;
	}
	
	public function getControlsBlockUri() {
		return $this->_controlsBlockUri;
	}
	
	public function setControlsBlockUri($blockUri) {
		$this->_controlsBlockUri = $blockUri;
		return $this;
	}
	
	public function getPerPage() {
		return $this->getControlsBlock()->getPerPage();
	}
	
	public function getCurrentPage() {
		return $this->getControlsBlock()->getCurrentPage();
	}
}