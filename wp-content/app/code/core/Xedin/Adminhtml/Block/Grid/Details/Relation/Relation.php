<?php

/**
 * Block responsible for displaying related entities.
 * This block is only responsible for one entity type.
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Adminhtml_Block_Grid_Details_Relation_Abstract extends Xedin_Adminhtml_Block_Grid_View_Abstract {
	
	/**
	 * @var Xedin_Core_Model_Collection_Abstract
	 */
	protected $_allChildrenCollection;
	/**
	 * @var Xedin_Core_Model_Collection_Abstract
	 */
	protected $_childrenCollection;
	protected $_currentSelection;
	
	protected $_attributeModelUri = 'eav/attribute';
	
	/**
	 * @var Xedin_Html_Block_Abstract The control block used to represent the child list visually.
	 */
	protected $_controlBlock;
	
	protected function _construct($layout = null, $sectors = array()) {
		parent::_construct($layout, $sectors);
		
		$this->setControlBlockUri( 'html/element_input_select' );
		
		/**
		 * @todo Do this in concrete descendants. 
		 */
//		$this->setTemplate('competitions/grid/details/game.game.sectors.php');
	}
	
	/**
	 * @return Xedin_Core_Model_Collection_Abstract
	 */
	public function getChildrenCollection() {
		if( is_null($this->_allChildrenCollection) ) {
			$this->_allChildrenCollection = $this->getCurrentObject()->getChildren();
		}
		
		return $this->_allChildrenCollection;
	}
	
	/**
	 * @return Xedin_Core_Model_Collection_Abstract
	 */
	public function getAllChildrenCollection() {
		if( is_null($this->_childrenCollection) ) {
			$this->_childrenCollection = $this->getCurrentObject()->getRelationModel()->getResourceModel()->getChildModel()->getCollection();
		}
		
		return $this->_childrenCollection;
	}
	
	public function getAttributeModelUri() {
		return $this->_attributeModelUri;
	}
	
	/**
	 * @return Xedin_Eav_Model_Attribute
	 */
	public function getAttributeModel() {
		return Hub::getSingleton( $this->getAttributeModelUri() );
	}
	
	/**
	 * @return Xedin_Html_Block_Abstract
	 */
	public function getControlBlock() {
		if( !$this->_controlBlock ) {
			$currentObject = $this->getCurrentObject();
			$this->_controlBlock = $this->getLayout()->createBlock('html/element_input_select')->setMultiple(true);
			
			if( !($currentObject instanceof Xedin_Core_Model_Relation_Party_Abstract) ) {
				return $this->_controlBlock;
			}
			
			/* @var $currentObject Xedin_Competitions_Model_Game */
			$selectFieldName = $currentObject->getRelationModel()->getChildIdFieldName();
			$selectName = $this->getFieldName($selectFieldName);
			$selectId = $this->getFieldId($selectFieldName);
			$this->_controlBlock->setId($selectId)->setName($selectName);
			$ids = $this->getChildrenCollection()->load()->getIds();
			$this->_controlBlock->setOptions($this->getAllChildrenCollection()->getItems(), $ids);
		}
		
		return $this->_controlBlock;
	}
}