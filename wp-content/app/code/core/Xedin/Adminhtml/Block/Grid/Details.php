<?php
/**
 * Description of Details
 *
 * @author anton
 */
class Xedin_Adminhtml_Block_Grid_Details extends Xedin_Adminhtml_Block_Grid_Abstract {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('adminhtml/grid/details.php');
		$this->getLayout()->getBlock('main')->getControlsBlock()->addItem('javascript: document.getElementById(\'' . $this->getFormName() . '\').submit()', 'Save', null, false);
	}
	
	protected function _generateFields() {
		$this->_beforeGenerateFields();
		$fields = array_keys( $this->getModel()->getResourceModel()->getTableDescription() );
		
		foreach( $fields as $_idx => $_fieldName ) {
			$this->addField(array(
				self::COLUMN_INFO_INDEX_NAME			=>	$_fieldName,
				self::COLUMN_INFO_INDEX_FIELD			=>	$_fieldName,
			));
		}
		
		$this->_generateAdditionalFields();
		
		$this->_afterGenerateFields();
		return $this;
	}
	
	public function getFieldDefaultInfo($field = null) {
		$defaultInfo = array();
		$defaultInfo[self::COLUMN_INFO_INDEX_NAME] = uniqid('field-');
		$defaultInfo[self::COLUMN_INFO_INDEX_LABEL] = '';
		$defaultInfo[self::COLUMN_INFO_INDEX_FIELD] = '';
		$defaultInfo[self::COLUMN_INFO_INDEX_RENDER_MODEL] = 'adminhtml/render_fields_generic';
		$defaultInfo[self::COLUMN_INFO_INDEX_SORT_POSITION] = $this->_currentFieldIndex*10;
		
		if( $field ) {
			if( (!isset($defaultInfo[self::COLUMN_INFO_INDEX_LABEL]) || empty($defaultInfo[self::COLUMN_INFO_INDEX_LABEL])) && isset($field[self::COLUMN_INFO_INDEX_NAME]) ) {
				$defaultInfo[self::COLUMN_INFO_INDEX_LABEL] = ucwords( str_replace(array('_', '/'), ' ', $field[self::COLUMN_INFO_INDEX_NAME]) );
			}
		}
		return $defaultInfo;
	}
	
	public function getFormActionUrl() {
		$actionSegment = null;
		if( $this->getCurrentObject() ) {
			$actionSegment = 'save';
		}
		else {
			$actionSegment = 'create';
		}
		return $this->getAdminLinkUrl('*', '*', $this->getController()->getModelActionSegmentValue($actionSegment));
	}
	
	public function getFormName() {
		return 'entity_form_' . $this->getModelFieldNamespace();
	}
	
	public function getModelName() {
		return $this->getModelUri();
	}
	
	public function getFieldValue($model, $columnName, $default = 'N/A') {
		$columnInfo = $this->getFields($columnName);
		if( !$columnInfo ) {
			return $default;
		}

		$field = $this->getColumnInfo($columnInfo, 'field');
		$name = $this->getFieldName($field);
		$value = $model->getData($field);
		$id = $this->getFieldId($field);
		$renderModel = Hub::getSingleton($columnInfo->getRenderModel());
		/* @var $renderModel Xedin_Core_Model_Render_Fields_Abstract */
		
		$renderModel->setValue($model)->setName($name)->setId($id)->setValueFieldName($field);
		
		return $renderModel->getRenderedField();
	}
}