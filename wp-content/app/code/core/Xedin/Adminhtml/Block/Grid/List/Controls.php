<?php

/**
 * Description of Controls
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Adminhtml_Block_Grid_List_Controls extends Xedin_Adminhtml_Block_Template {
	
	protected $_listBlock;
	
	protected $_currentPageVarName = 'current_page';
	protected $_perPageVarName = 'per_page';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$perPage = $this->getRequest()->getParam($this->getPerPageVarName());
		$perPage = !is_null($perPage) ? (int)$perPage : 20;
		$this->setPerPage($perPage);
		
		$currentPage = $this->getRequest()->getParam($this->getCurrentPageVarName());
		$currentPage = !is_null($currentPage) ? (int)$currentPage : 0;
		$this->setCurrentPage($currentPage)
				->setTemplate('adminhtml/grid/list/controls.php')
				->setVisiblePagesAmount(20);
	}
	
	public function getCurrentPageVarName() {
		return $this->_currentPageVarName;
	}
	
	public function getPerPageVarName() {
		return $this->_perPageVarName;
	}
	
	/**
	 * @param Xedin_Core_Block_Template $block
	 * @return \Xedin_Adminhtml_Block_Grid_List_Controls
	 * @throws Xedin_Adminhtml_Exception
	 */
	public function setListBlock($block) {
		if( !($block instanceof Xedin_Core_Block_Template) ) {
			throw Hub::exception('Xedin_Adminhtml', 'List block must be an instance of Xedin_Core_Block_Template.');
		}
		
		$this->_listBlock = $block;
		return $this;
	}
	
	/**
	 * @return Xedin_Adminhtml_Block_Grid_List
	 */
	public function getListBlock() {
		return $this->_listBlock;
	}
	
	public function getTotalRecordsAmount() {
		if( !$this->hasData('total_records_amount') ) {
			$totalRecordsAmount = $this->getListBlock()->getCollection()->load()->getTotalNumRows();
			$this->setData('total_records_amount', $totalRecordsAmount);
		}
		
		return $this->getData('total_records_amount');
	}
	
	public function getVisibleRecordsAmount() {
		return $this->getListBlock()->getCollection()->count();
	}
	
	public function getPagesAmount() {
		if( !$this->hasData('pages_amount') ) {
			$pages = ceil($this->getTotalRecordsAmount() / $this->getPerPage());
			$this->setData('pages_amount', $pages);
		}
		
		return $this->getData('pages_amount');
	}
	
	public function getPageUrl($pageNumber, $perPage = null) {
		if( is_null($perPage) ) {
			$perPage = $this->getPerPage();
		}
		
		$pageNumber = (int)$pageNumber;
		
		return $this->getController()->getRedirectUrl('*/*/*/*', true, array(
			$this->getPerPageVarName()			=>	$perPage,
			$this->getCurrentPageVarName()		=>	$pageNumber
		));
	}
	
	/**
	 *
	 * @todo Regardless of whether current page was explicitly passed, the function will always return the result of the 1st calculation. Fix this.
	 * @param int|null $currentPage
	 * @param mixed|null $default
	 * @return int 
	 */
	public function getPreviousPageNumber($currentPage = null, $default = null) {
		if( !$this->hasData('previous_page_number') ) {
			if( is_null($currentPage) ) {
				$currentPage = $this->getCurrentPage();
			}
			
			$currentPage = ($currentPage != 0) ? $currentPage -1 : $default;
			$this->setData('previous_page_number', $currentPage);
		}
		
		return $this->getData('previous_page_number');
	}
	
	
	/**
	 *
	 * @todo Regardless of whether current page was explicitly passed, the function will always return the result of the 1st calculation. Fix this.
	 * @param int|null $currentPage
	 * @param mixed|null $default
	 * @return int 
	 */
	public function getNextPageNumber($currentPage = null, $default = null) {
		if( !$this->hasData('next_page_number') ) {
			if( is_null($currentPage) ) {
				$currentPage = $this->getCurrentPage();
			}
			
			$currentPage = ($currentPage != $this->getPagesAmount() - 1) ? $currentPage + 1 : $default;
			$this->setData('next_page_number', $currentPage);

		}
		
		return $this->getData('next_page_number');
	}
	
	
	/**
	 *
	 * @todo Regardless of whether current page was explicitly passed, the function will always return the result of the 1st calculation. Fix this.
	 * @param int|null $currentPage
	 * @return int 
	 */
	public function getStartingRecordNumber($currentPage = null) {
		if( !$this->hasData('starting_record_number') ) {
			if( is_null($currentPage) ) {
				$currentPage = $this->getCurrentPage();
			}
			
			$startingRecordNumber = ($this->getPerPage() * $currentPage) + 1;
			$this->setData('starting_record_number', $startingRecordNumber);

		}
		
		return $this->getData('starting_record_number');
	}
	
	public function getEndingRecordNumber($currentPage = null) {
		if( !$this->hasData('ending_record_number') ) {
			if( is_null($currentPage) ) {
				$currentPage = $this->getCurrentPage();
			}
			
			$endingRecordNumber = $this->getPerPage() * ($currentPage + 1);
			if( $endingRecordNumber > $this->getTotalRecordsAmount() ) {
				$endingRecordNumber = $this->getTotalRecordsAmount();
			}
			
			$this->setData('ending_record_number', $endingRecordNumber);

		}
		
		return $this->getData('ending_record_number');
	}
}