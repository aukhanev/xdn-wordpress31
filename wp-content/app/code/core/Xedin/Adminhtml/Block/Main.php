<?php

/**
 * Description of Main
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Adminhtml_Block_Main extends Xedin_Adminhtml_Block_Template {
	
	protected $_controlsBlockName = 'main.controls';
	protected $_controlsBlockUri = 'adminhtml/main_controls';
	
	public function getControlsBlockName() {
		return $this->_controlsBlockName;
	}
	
	public function getControlsBlockUri() {
		return $this->_controlsBlockUri;
	}
	
	public function getControlsBlock() {
		if( !$this->getChild($this->getControlsBlockName()) ) {
			$block = $this->getLayout()->createBlock($this->getControlsBlockUri(), $this->getNameInLayout(), array('name' => $this->getControlsBlockName()))
					->setBefore('-')
					->setTemplate('adminhtml/main/controls.php');
		}
		return $this->getChild($this->getControlsBlockName());
	}
}