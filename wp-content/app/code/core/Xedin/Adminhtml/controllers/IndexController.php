<?php

class Xedin_Adminhtml_IndexController extends Xedin_Adminhtml_Controller_Abstract {
	
	protected $_modelConfig;
	
	/**
	 * Dont forget: the model class will be matched IN THIS ORDER.
	 * This means that the more generic model class should be at the end.
	 * @var array
	 */
	protected $_baseModelListBlocks = array(
		'Xedin_Core_Model_Meta_Abstract'				=>	'eav/admin_grid_list_meta',
		'Xedin_Core_Model_Abstract'						=>	'adminhtml/grid_list'
	);
	protected $_gridListBlockUri;
	protected $_gridListBlock;
	
	/**
	 * @see $_baseModelListBlocks
	 * @var array
	 */
	protected $_baseModelDetailsBlocks = array(
		'Xedin_Core_Model_Meta_Abstract'				=>	'eav/admin_grid_details_meta',
		'Xedin_Core_Model_Abstract'						=>	'adminhtml/grid_details'
	);
	protected $_gridDetailsBlockUri;
	protected $_gridDetailsBlock;
	
	public function indexAction() {
		
		if( $this->getAdminModule() ) {
			$this->getLayout()->createBlock('adminhtml/menu_models', 'main');
		}
		if( $this->getCurrentModel() && (/* !$this->getCurrentModelAction() || */ $this->getCurrentModelAction() == 'list') ) {
			$this->getGridListBlock();
		}
		
		if( $this->getCurrentModelAction() == 'view' /* && $this->getParam('id') */ ) {
			$this->getGridDetailsBlock();
		}
		
		if( $this->getCurrentModelAction() == $this->getModelActionSegmentValue('save') ) {
			Hub::getModel($this->getModelUri())
					->setData($this->getIncomingModelData())
					->save();
			$this->adminRedirect('*/*');
		}
		
		if( $this->getCurrentModelAction() == $this->getModelActionSegmentValue('delete') ) {
			$model = Hub::getModel($this->getModelUri());
			$incomingData = $this->getIncomingModelData();
			$model->setId($this->getParam('id'))
					->isDeleted(true);
			$model->save();
			$this->adminRedirect('*/*');
		}
	}
	
	public function getIncomingModelData($modelUri = null) {
		if( !$modelUri ) {
			$modelUri = $this->getModelUri();
		}
		$postVarName =  Hub::getHelper('html/form_field')->getModelNamespace($modelUri);
		return $this->getRequest()->getPost($postVarName);
	}
	
	public function getModelUri() {
		return $this->getModelConfig()->getData('path');
	}
	
	/* Grid List Model to Block ============================================= */
	
	public function getModelListBlock() {
		return $this->getModelConfig()->getData('list_block');
	}
	
	public function getGridListBlock() {
		if( !$this->_gridListBlock ) {
			if( ($listBlockUri = $this->getModelListBlock()) ) {
//				var_dump('Setting Grid List Block URI to ' . $listBlockUri);
				$this->_setGridListBlockUri($listBlockUri);
			}
			$this->_gridListBlock = $this->getLayout()->createBlock($this->getGridListBlockUri(), 'main');
		}
		
		return $this->_gridListBlock;
	}
	
	public function getGridListBlockUri() {
		if( empty($this->_gridListBlockUri) ) {
			foreach( $this->_baseModelListBlocks as $_modelBaseClass => $_blockUri ) {
//				var_dump('Is ' . $this->getModelUri() . ' a ' . $_modelBaseClass . '?');
				if( is_a(Hub::getSingleton($this->getModelUri()), $_modelBaseClass) ) {
//					var_dump('Yes! Use ' . $_blockUri);
					return $_blockUri;
				}
			}
		}
		
		return $this->_gridListBlockUri;
	}
	
	protected function _setGridListBlockUri($blockUri) {
		$this->_gridListBlockUri = $blockUri;
		return $this;
	}
	
	public function setGridListModelToBlock($modelClass, $blockUri) {
		$this->_gridListBlockUri[$modelClass] = $blockUri;
		return $this;
	}
	
	public function addGridListModelToBlock($modelClass, $blockUri) {
		if( !$this->hasGridListModelToBlock($modelClass) ) {
			$this->setGridListModelToBlock($modelClass, $blockUri);
		}
		
		return $this;
	}
	
	public function removeGridListModelToBlock($modelClass) {
		if( $this->hasGridListModelToBlock($modelClass) ) {
			unset($this->_gridListBlockUri[$modelClass]);
		}
		
		return $this;
	}
	
	public function hasGridListModelToBlock($modelClass) {
		return isset($this->_gridListBlockUri[$modelClass]);
	}
	
	/* Grid Details Model to Block ========================================== */
	
	public function getModelDetailsBlock() {
		return $this->getModelConfig()->getData('details_block');
	}
	
	public function getGridDetailsBlock() {
		if( !$this->_gridDetailsBlock ) {
			if( ($detailsBlockUri = $this->getModelDetailsBlock()) ) {
				$this->_setGridDetailsBlockUri($detailsBlockUri);
			}
			$this->_gridDetailsBlock = $this->getLayout()->createBlock($this->getGridDetailsBlockUri(), 'main');
		}
		
		return $this->_gridDetailsBlock;
	}
	
	public function getGridDetailsBlockUri() {
		if( empty($this->_gridDetailsBlockUri) ) {
			foreach( $this->_baseModelDetailsBlocks as $_modelBaseClass => $_blockUri ) {
//				var_dump('Is ' . $this->getModelUri() . ' a ' . $_modelBaseClass . '?');
				if( is_a(Hub::getSingleton($this->getModelUri()), $_modelBaseClass) ) {
//					var_dump('Yes! Use ' . $_blockUri);
					return $_blockUri;
				}
			}
		}
		
		return $this->_gridDetailsBlockUri;
	}
	
	protected function _setGridDetailsBlockUri($blockUri) {
		$this->_gridDetailsBlockUri = $blockUri;
		return $this;
	}
	
	public function setGridDetailsModelToBlock($modelClass, $blockUri) {
		$this->_gridDetailsBlockUri[$modelClass] = $blockUri;
		return $this;
	}
	
	public function addGridDetailsModelToBlock($modelClass, $blockUri) {
		if( !$this->hasGridDetailsModelToBlock($modelClass) ) {
			$this->setGridDetailsModelToBlock($modelClass, $blockUri);
		}
		
		return $this;
	}
	
	public function removeGridDetailsModelToBlock($modelClass) {
		if( $this->hasGridDetailsModelToBlock($modelClass) ) {
			unset($this->_gridDetailsBlockUri[$modelClass]);
		}
		
		return $this;
	}
	
	public function hasGridDetailsModelToBlock($modelClass) {
		return isset($this->_gridDetailsBlockUri[$modelClass]);
	}
}