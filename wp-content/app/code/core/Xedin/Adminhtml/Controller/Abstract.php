<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.2 
 */
class Xedin_Adminhtml_Controller_Abstract extends Xedin_Core_Controller_Abstract {
	
	protected $_adminUrlBase = 'admin';
	protected $_loginControllerName = 'login';
	protected $_logoutActionName = 'logout';
	
	protected $_adminModuleKey = 'admin_module';
	protected $_modelKey = 'model';
	protected $_modelActionKey = 'rest_action';
	protected $_adminModule;
	protected $_currentModel;
	protected $_currentModelAction;
	protected $_defaultModelAction = 'list';
	
	protected $_modelActions = array(
		'view'			=>	'view',
		'create'		=>	'create',
		'save'			=>	'save',
		'delete'		=>	'delete'
	);
	
	protected $_modelConfig;
	
	public function __construct() {
		parent::__construct();
		
//		var_dump($this->isUserLoggedIn());
//		var_dump($this->isLogoutPage());
		
		/*
		 * If user logged in and wants to logout, log him out and redirect
		 * to login page. 
		 */
		if( $this->isUserLoggedIn() && $this->isLogoutPage() ) {
			$this->getUserSession()->logout($this->getUserSession()->getUsername());
			$this->adminRedirect($this->getLogoutPath());
		}
		
		/*
		 * If user logged in and trying to access login page, redirect to admin
		 * default page. 
		 */
		if( $this->isUserLoggedIn() && $this->isLoginPage()) {
			$this->adminRedirect();
		}
		
		$user = $this->getRequest()->getPost($this->getUserNamespace());
		$username = $user['username'];
		$password = $user['password'];

		/**
		 * If not user logged in and this is the login page and has specified
		 * all login credentials, redirect to login page.
		 */
		if( (!$this->isUserLoggedIn() && !($this->isLoginPage() || ($username && $password))) ) {
			$this->adminRedirect($this->getLoginPath());
		}
		
		/**
		 * If everythign is ok, attempt to log user in. 
		 */
		if( $username && $password ) {
			$this->getUserSession()->login($username, $password);
		}
	}
	
	public function getConfig($path = null) {
		if( is_null($path) ) {
			return Hub::getSingleton('adminhtml/config');
		}
		
		return Hub::getSingleton('adminhtml/config')->getValue($path);
	}
	
	public function getUrl($path = '') {
		return Hub::getUrl($path);
	}
	
	public function setAdminModuleKey($moduleKey) {
		$this->_adminModuleKey = $moduleKey;
		return $this;
	}
	
	public function getAdminModuleKey() {
		return $this->_adminModuleKey;
	}
	
	public function setModelKey($modelKey) {
		$this->_modelKey = $modelKey;
		return $this;
	}
	
	public function getModelKey() {
		return $this->_modelKey;
	}
	
	public function setModelActionKey($modelActionKey) {
		$this->_modelActionKey = $modelActionKey;
		return $this;
	}
	
	public function getModelActionKey() {
		return $this->_modelActionKey;
	}
	
	public function getAdminModule() {
		if( !$this->_adminModule ) {
			$this->_adminModule = $this->getParam($this->getAdminModuleKey());
		}
		return $this->_adminModule;
	}
	
	public function getCurrentModel() {
		if( !$this->_currentModel ) {
			
			$this->_currentModel = $this->getParam($this->getModelKey());
		}
		return $this->_currentModel;
	}
	
	public function getDefaultModelAction() {
		return $this->_defaultModelAction;
	}
	
	public function getCurrentModelAction() {
		if( !$this->_currentModelAction ) {
			$modelActionKey = $this->getModelActionKey();
			$this->_currentModelAction = $this->getParam($modelActionKey) ? $this->getParam($modelActionKey) : $this->getDefaultModelAction();
			$this->setParam($modelActionKey, $this->_currentModelAction);
		}
		
		return $this->_currentModelAction;
	}
	
	public function getModelActionSegmentValue($type, $default = null) {
		if( !isset($this->_modelActions[$type]) ) {
			return $default;
		}
		
		return $this->_modelActions[$type];
	}
	
	public function getAdminUrlHelper() {
		return Hub::getHelper('adminhtml/url');
	}
	
	
	public function getModuleConfig($moduleName = null) {
		if( is_null($moduleName) ) {
			$moduleName = $this->getCurrentModule();
		}
		
		return $this->getConfig()->getModuleConfig($moduleName);
	}
	
	/**
	 *
	 * @param type $modelHandle
	 * @param type $moduleName
	 * @return Varien_Object
	 */
	public function getModelConfig($modelHandle = null, $moduleName = null) {
		if( !$this->_modelConfig ) {
			if( is_null($modelHandle) ) {
				$modelHandle = $this->getCurrentModel();
			}

			if( is_null($moduleName) ) {
				$moduleName = $this->getAdminModule();
			}
			$this->_modelConfig = new Varien_Object( $this->getConfig()->getModelConfig($modelHandle, $moduleName) );
		}
		
		return $this->_modelConfig;
	}
	
	public function adminRedirect($url = '', $prepare = true, $params = array()) {
		header('Location: ' . $this->getAdminRedirectUrl($url, $prepare, $params));
		exit();
	}
	
	public function getAdminRedirectUrl($url = '', $prepare = true, $params = array()) {
		if( !is_array($url) ) {
			$url = explode('/', $url);
		}
		
		array_unshift($url, $this->_adminUrlBase);
		return $this->getRedirectUrl($url, $prepare, $params);
	}
}