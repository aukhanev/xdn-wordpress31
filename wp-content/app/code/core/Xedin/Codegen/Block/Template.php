<?php

/**
 * Description of Template
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Block_Template extends Xedin_Core_Block_Template {
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		$this->setModuleRoot('codegen' . DS);
	}
	
	public function getTemplatePath($path = '') {
		$path = $this->getModuleRoot() . $path;
		return parent::getTemplatePath($path);
	}
}