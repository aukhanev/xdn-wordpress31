<?php

/**
 * Description of Function
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_Function extends Xedin_Codegen_Model_Php_Abstract {

	protected function _toString() {
		
		if( !$this->hasName() ) {
			$this->addMessage('A function must have a name.', 'error');
		}
		
		$string = parent::_toString();
	}
}