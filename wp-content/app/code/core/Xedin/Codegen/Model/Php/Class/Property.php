<?php

/**
 * Description of Property
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_Class_Property extends Xedin_Codegen_Model_Php_Expression {
	
	protected $_isAutoVisibility;
	protected $_allowedVisibility = array(
		self::VISIBILITY_PUBLIC,
		self::VISIBILITY_PRIVATE,
		self::VISIBILITY_PROTECTED
	);
	
	const VISIBILITY_PUBLIC = 'public';
	const VISIBILITY_PROTECTED = 'protected';
	const VISIBILITY_PRIVATE = 'private';
	const KEYWORD_STATIC = 'static';
	
	protected function _construct() {
		parent::_construct();
		
		$this->isAutoVisibility(true);
//		$this->en
	}
	
	public function isAutoVisibility($value = null) {
		if( is_null($value) ) {
			return $this->_isAutoVisibility;
		}
		
		$this->_isAutoVisibility = (bool)$value;
		return $this;
	}
	
	protected function _toString() {
		
		if( !$this->hasName() ) {
			$this->addMessage('Class property must have a name.', 'error');
		}
		
		$keywords = array();
		
		switch( true ) {
			case $this->getIsPrivate():
				$keywords[] = self::VISIBILITY_PRIVATE;
				break;
			
			case $this->getIsProtected():
				$keywords[] = self::VISIBILITY_PROTECTED;
				break;
			
			default:
			case $this->getIsPublic():
				if( $this->isAutoVisibility() ) {
					$keywords[] = self::VISIBILITY_PUBLIC;
				}
				break;
		}
		
		if( $this->getIsStatic() ) {
			$keywords[] = self::KEYWORD_STATIC;
		}
		
		$s = self::S;
		$vs = self::VARIABLE_START;
		$keywords = implode($s, $keywords);
		
		return $keywords . $s . $vs . $this->getName() . ($this->hasContent() ? parent::_toString() : '');
	}
	
	public function setVisibility($visibility) {
		if( in_array($visibility, $this->_allowedVisibility) ) {
			$this->setData('is_' . $visibility);
		}
		
		return $this;
	}
}