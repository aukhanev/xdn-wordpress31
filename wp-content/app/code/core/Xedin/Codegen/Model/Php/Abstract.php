<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_Abstract extends Xedin_Codegen_Model_Abstract {
	
	protected $_dialect;
	
	const S = ' ';
	const N = PHP_EOL;
	const T = "\t";
	const BLOCK_START = '{';
	const BLOCK_END = '}';
	const EXPRESSION_END = ';';
	const OPERATOR_SCOPE_RESOLUTION = '::';
	const OPERATOR_ACCESS_OBJECT = '->';
	const VARIABLE_START = '$';
}