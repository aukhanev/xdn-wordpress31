<?php

/**
 * Description of Class
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_Class extends Xedin_Codegen_Model_Php_Abstract {

	protected $_properties;
	
	const PHP_CLASS_KEYWORD_FINAL = 'final';
	
	protected function _enclose($content) {
		if( !$this->hasClassName() ) {
			$this->addMessage('No class name was defined.');
		}
		
		$keywords = array('class');
		if( $this->getIsFinal() ) {
			$keywords[] = 'final';
		}
		
		$lf = $this->getLineBreak();
		$s = self::S;
		
		$keywords = implode($s, $keywords);
		
		return $keywords . $s . $this->getClassName() . $s . self::BLOCK_START . $lf . $content . $lf . self::BLOCK_END;
	}
	
	/**
	 *
	 * @param Varien_Object, Xedin_Codegen_Model_Php_Class_Property_Collection, array, string $property
	 * @param mixed $value
	 * @param string $visibility
	 * @param bool $isStatic
	 * @return \Xedin_Codegen_Model_Php_Class 
	 */
	public function addProperties($property, $value = null, $visibility = null, $isStatic = null) {
		if( is_array($property) || $property instanceof Xedin_Codegen_Model_Php_Class_Property_Collection ) {
			foreach( $property as $_idx => $_property ) {
				$this->addProperties($_property);
			}
			
			return $this;
		}
		
		if( !$property instanceof Xedin_Codegen_Model_Php_Class_Property ) {
			$propertyName = $property;
			$property = Hub::getModel('codegen/php_class_property')->setName($propertyName);
		}
		
		if( !is_null($value) ) {
			$property->setContent($value);
		}
		
		if( !is_null($visibility) ) {
			$property->setVisibility($visibility);
		}
		
		if( !is_null($isStatic) ) {
			$property->setIsStatic((bool)$isStatic);
		}
		
		$this->_properties[] = $property;
		
		return $this;
	}
}