<?php

/**
 * Description of Expression
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_Expression extends Xedin_Codegen_Model_Php_Abstract {
	
	public function __construct($string = null) {
		parent::__construct();
		$this->setContent($string);
	}
	
	public function getContent() {
		$content = $this->getData('content');
		$ee = self::EXPRESSION_END;
		$content = preg_replace('!' . preg_quote($ee) . '\s*$!', $ee, $content);
		return $content;
	}
}