<?php

/**
 * Description of File
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Php_File extends Xedin_Codegen_Model_Php_Abstract {
	
	protected $_isUsingIndividualStream = false;
	protected static $_sharedStream;
	protected static $_lastPath;
	protected $_individualStream;
	
	const FILE_OPEN_MODE = 'a+';
	
	const PHP_START = '<?php';
	const PHP_END = '?>';

	protected function _construct() {
		parent::_construct();
		$this->setAutoClosePhp(true)
				->setAutoStartPhp(true);
		$this->setLineSeparator(self::N);
	}

	public function setFullPath($fullPath, $directorySeparator = null) {
		if( is_null($directorySeparator) ) {
			$directorySeparator = DIRECTORY_SEPARATOR;
		}
		
		$fullPath = explode($directorySeparator, $fullPath);
		$fileName = array_pop($fullPath);
		$this->setFileName($fileName);
		$this->setFilePath( implode(DIRECTORY_SEPARATOR, $fullPath) );
		return $this;
	}
	
	public function getFullPath() {
		$path = $this->getFilePath();
		if( strlen($path) ) {
			$path = $path . DIRECTORY_SEPARATOR;
		}
		return $path . $this->getFileName();
	}
	
	public function isUsingIndividualStream($isUsingIndividualStream = null) {
		if( is_null($isUsingIndividualStream) ) {
			return $this->_isUsingIndividualStream;
		}
		
		$this->_isUsingIndividualStream = (bool)$isUsingIndividualStream;
		return $this;
	}
	
	public function getOpenStream() {
		$stream = $this->isUsingIndividualStream() ? $this->_individualStream : self::$_sharedStream;
		if( $stream ) {
			if( $this->getFullPath() !== $this->getLastFilePath() ) {
				fclose($stream);
				$stream = $this->_getOpenStream();
				$this->_setStream($stream);
			}
		}
		
		if( !$stream ) {
			$stream = $this->_getOpenStream();
			$this->_setStream($stream);
		}
		
		return $this->isUsingIndividualStream() ? $this->_individualStream : self::$_sharedStream;
	}
	
	public function getLastFilePath() {
		return self::$_lastPath;
	}
	
	protected function _getOpenStream() {
		return fopen($this->getFullPath(), self::FILE_OPEN_MODE);
	}
	
	protected function _setStream($stream) {
		if( $this->isUsingIndividualStream() ) {
			$this->_individualStream = $stream;
		}
		else {
			self::$_sharedStream = $stream;
		}
		
		return $this;
	}
	
	public function save() {
		$this->_save();
		return $this;
	}
	
	protected function _save() {
		$realUsage = false;
		$mem = memory_get_usage($realUsage);
		$stream = $this->getOpenStream();
		$mem = memory_get_usage($realUsage) - $mem;
		echo $mem . "\n";
		fwrite($stream, $this->toString());
		fclose($stream);
		$this->_setStream(null);
		return $this;
	}
	
	protected function _afterToString($string) {
		$string = parent::_afterToString($string);
		$lf = $this->getLineSeparator();
		
		if( $this->getAutoStartPhp() && !preg_match('!^\s*' . preg_quote(self::PHP_START) . '!', $string)  ) {
			$string = self::PHP_START . $lf . $string;
		}
		
		if( $this->getAutoClosePhp() && !preg_match('!' . preg_quote(self::PHP_END) . '\s*$!', $string)  ) {
			$string .= $lf . self::PHP_END;
		}
		
		return $string;
	}
}