<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Codegen_Model_Abstract extends Xedin_Core_Model_Abstract {
	
	const N = "\n";
	
	/** @var Xedin_Codegen_Model_Message_Collection */
	protected static $_messageCollection;
	
	protected  function _construct() {
		parent::_construct();
		$this->setIsSilent(!Hub::isDebugMode());
		$this->setLineBreak(self::N);
	}

	public function __toString() {
		return $this->toString();
	}
	
	final public function toString() {
		$this->_beforeToString();
		$string = $this->_toString();
		$string = $this->_afterToString($string);
		return $string;
	}
	
	protected function _beforeToString() {}
	
	protected function _afterToString($string) {
		return $string;
	}
	
	protected function _toString() {
		return (string)$this->getContent();
	}
	
	/**
	 * @return Xedin_Codegen_Model_Message_Collection
	 */
	public function getMessageCollection() {
		if( !self::$_messageCollection ) {
			self::$_messageCollection = Hub::getModel('codegen/message_collection');
		}
		
		return self::$_messageCollection;
	}
	
	public function addMessage($text, $type = null) {
		$this->getMessageCollection()->addMessage($type, $text);
		return $this; 
	}
}