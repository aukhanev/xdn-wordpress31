<?php

/**
 * Description of Url
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Helper_Url extends Xedin_Core_Helper_Uri {
	
	protected $_facebookBase = 'http://www.facebook.com/';
	
	const DATA_KEY_FB_BASE_USERNAME = 'fb_base_username';
	
	/**
	 * @param Xedin_Facebook_Model_Facebook $facebook
	 * @param type $appId
	 * @param type $params
	 * @return type 
	 */
	public function getFacebookPageTabUrl($facebook, $appId = null, $params = array()) {
		if( is_null($appId) ) {
			$appId = $facebook->getAppModel()->getApiKey();
		}
				
		$url = $this->getUriModel()->parseUri( (string)$this->getFacebookPageUrl($facebook) );
		$appString = 'app_' . $appId;
		$additionalParams = array('sk' => $appString);
		
		if( !empty($params) ) {
			$params = $this->getFacebookModel()->getEncodedAppData($params);
			$additionalParams['app_data'] = $params;
		}
		
		$url->setScheme($this->getController()->getRequest()->getScheme());
		$url->addReplaceQueryParameters($additionalParams);
		return  $url;
	}
	
	public function getFacebookPageUrl($facebook = null) {
		if( $facebook instanceof Xedin_Facebook_Model_Facebook ) {
			$facebook = $facebook->getAppModel();
		}
		
		if( !($facebook instanceof Xedin_Facebook_Model_App) ) {
			throw Hub::exception('Xedin_Facebook', 'The first argument must be an instance of Xedin_Facebook_Model_Facebook or Xedin_Facebook_Model_App.');
		}
		
		return $this->getUriModel()->parseUri( $facebook->getFbPageUrl() )->setScheme($this->getController()->getRequest()->getScheme());
	}
	
	public function getFacebookBaseUrl($url = '') {
		return $this->_facebookBase . $url;
	}
	
	/**
	 * @return Xedin_Facebook_Model_Facebook
	 */
	public function getFacebookModel() {
		return Hub::getSingleton('facebook/facebook');
	}
}