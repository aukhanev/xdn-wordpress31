<?php

/**
 * Description of Init
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Block_Init extends Xedin_Clientside_Block_Script {

	protected $_fbOptions = array(
		'status' => true,
		'cookie' => true,
		'xfbml' => true,
		'oauth' => true
	);
	
	public function getOutputModel() {
		if (!$this->_outputModel) {
			$this->_outputModel = $this->js('window');
			$this->_outputModel->fbAsyncInit = $this->func(
					$this->js('FB')->init( $this->obj($this->getFbOptions()) )
					->fStartChain()
					->Canvas->setAutoGrow()
					->fEndChain()
			)->concat(
					$this->jQuery('document', true)->trigger('"fb-ready"')
			);
			
			$el = $this->js('d')->fStartChain()->getElementsByTagName('s');
			$el = $el[0];
			
			$closure = $this->closure(
					$this->js()->kVar(array('js', 'fjs'), $el)
					->concat('if(d.getElementById(id)) {return;}
						js = d.createElement(s)
            js.src = "//connect.facebook.net/en_GB/all.js"; //#xfbml=1&appId=' . $this->getFbOptions('appId') . '";
            fjs.parentNode.insertBefore(js, fjs);')
					, array('d', 's', 'id'), array('document', '"script"', '"facebook-jssdk"') );
			
			$this->_outputModel->concat($closure);

		}

		return $this->_outputModel;
	}

	public function setFbOptions($name, $value = null) {
		if (is_null($value) && is_array($name)) {
			$this->_fbOptions = $name;
			return $this;
		}

		$this->_fbOptions[$name] = $value;

		return $this;
	}

	public function hasFbOptions($name = null) {
		if (is_null($name)) {
			return !empty($this->_fbOptions);
		}
		return isset($this->_fbOptions[$name]);
	}

	public function addFbOptions($name, $value = null) {
		if (is_null($value) && is_array($name)) {
			foreach ($name as $_name => $_value) {
				$this->addFbOptions($_name, $_value);
			}

			return $this;
		}

		if (!$this->hasFbOptions($name)) {
			$this->setFbOptions($name, $value);
		}

		return $this;
	}

	public function getFbOptions($name = null, $default = null) {
		if (is_null($name)) {
			return $this->_fbOptions;
		}

		if (!$this->hasFbOptions($name)) {
			return $default;
		}

		return $this->_fbOptions[$name];
	}

}