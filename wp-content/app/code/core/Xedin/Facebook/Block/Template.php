<?php

/**
 * Description of Template
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Block_Template extends Xedin_Core_Block_Template {
	
	const DATA_KEY_FACEBOOK_PAGE_URL = 'facebook_page_url';
	const DATA_KEY_FACEBOOK_APP_URL = 'facebook_app_url';
	
	/** @var Xedin_Facebook_Model_Facebook */
	protected $_facebook;
	
	public function getFacebook() {
		if( !$this->_facebook ) {
			$this->_facebook = $this->setFacebook($this->getFacebookAppId());
		}
		
		return $this->_facebook;
	}
	
	public function setFacebook($facebook = null) {
		if( $facebook instanceof Xedin_Facebook_Model_Facebook ) {
			$this->_facebook = $facebook;
			return $this;
		}
		
		$facebook = $this->getNewFacebookModel();
		
		if( $facebook instanceof Xedin_Facebook_Model_App ) {
			$facebook = $facebook->getCode();
		}
		
		if( is_string($facebook) ) {
			$facebook->load($facebook);
		}
		
		$this->_facebook = $facebook;
		
		return $this;
	}
	
	/**
	 * @return Xedin_Facebook_Model_Facebook
	 */
	public function getNewFacebookModel() {
		return Hub::getModel('facebook/facebook');
	}
	
	/**
	 * @return Xedin_Facebook_Helper_Url
	 */
	public function getUrlHelper() {
		return Hub::getHelper('facebook/url');
	}
	
	public function setFacebookPageUrl($url = null) {
		if( is_null($url) ) {
			$url = $this->getUrlHelper()->getFacebookPageUrl($this->getFacebook());
		}
		
		$this->setData(self::DATA_KEY_FACEBOOK_PAGE_URL, $url);
		return $this;
	}
	
	public function getFacebookPageUrl() {
		if( !$this->hasData(self::DATA_KEY_FACEBOOK_PAGE_URL) ) {
			$this->setFacebookPageUrl();
		}
		
		return $this->getData(self::DATA_KEY_FACEBOOK_PAGE_URL);
	}
	
	public function setFacebookAppUrl($url = null) {
		if( is_null($url) ) {
			$url = $this->getUrlHelper()->getFacebookPageTabUrl($this->getFacebook());
		}
		$this->setData(self::DATA_KEY_FACEBOOK_APP_URL, $url);
		return $this;
	}
	
	public function getFacebookPageTabUrl() {
		if( !$this->hasData(self::DATA_KEY_FACEBOOK_APP_URL) ) {
			$this->setFacebookAppUrl();
		}
		
		return $this->getData(self::DATA_KEY_FACEBOOK_APP_URL);
	}
}