<?php

/**
 * Description of Like
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Block_Ui_Like extends Xedin_Facebook_Block_Ui_Abstract {
	
	const LAYOUT_BUTTON_COUNT = 'button_count';
	
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		$this->setTemplate('facebook/ui/like.php');
		$this->setFont('lucida grande')
				->setData('data-sent', 'false')
				->setData('layout', self::LAYOUT_BUTTON_COUNT)
				->setShowFaces('false')
				->setClass('fb-button-like');
	}
}