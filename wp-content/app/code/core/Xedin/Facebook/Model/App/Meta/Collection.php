<?php

/**
 * Description of Collection
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_App_Meta_Collection extends Xedin_Facebook_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('facebook/app_meta');
	}
}