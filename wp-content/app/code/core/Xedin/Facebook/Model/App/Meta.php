<?php

/**
 * Description of Meta
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_App_Meta extends Xedin_Facebook_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook/app_meta');
	}
}