<?php

/**
 * Description of App
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_App extends Xedin_Core_Model_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('facebook/app')
				->_setMetaModelName('facebook/app_meta');
	}
}