<?php

/**
 * Description of Meta
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Resource_App_Meta extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook_app_meta');
	}
}