<?php

/**
 * Description of Request
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Resource_Invite_Request extends Xedin_Core_Model_Resource_Mysql {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook_invite_request');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		parent::_beforeSave($object);
		
		$invitedAt = $object->getInvitedAt();
		if( empty($invitedAt) ) {
			$invitedAt = $this->getFormattedTimeStamp();
			$object->setInvitedAt($invitedAt);
		}
		
		return $object;
	}
}