<?php

/**
 * Description of Facebook
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Resource_Facebook extends Facebook_Facebook {
	
	protected $_signedRequest;
	protected $_user;
	
	public function __construct($config) {
		Zend_Session::start(true);
		parent::__construct($config);
	}
	
	public function load(Xedin_Core_Model_Abstract $object, $value=null, $field=null) {
		
		if( !($object instanceof Xedin_Facebook_Model_Facebook) ) {
			return $this;
		}
		
		/* @var $object Xedin_Facebook_Model_Facebook */
		
		if( !$object->hasApiModel() ) {
			$object->setApiModel($this);
		}
		
		if( !is_null($value) ) {
			$appModel = $object->getAppModel($value);
			if( $appModel->hasId() ) {
				$object->getApiModel()->setAppId($appModel->getApiKey())
						->setApiSecret($appModel->getAppSecret());
//				var_dump($object->getApiModel());
			}
		}
		
		return $this;
	}
	
	public function setIdFieldName($fieldName) {
		return $this;
	}
	
	public function getIdFieldName() {
		return 'id';
	}
	
	public function getSignedRequest() {
		if( !$this->_signedRequest ) {
			$varName = 'xdn-fbsr-' . $this->getAppId();
			$signedRequest = null;
			$signedRequestRaw = $this->getRawSignedRequest();
			if( $signedRequestRaw ) { 
				$signedRequest = $this->parseSignedRequest($signedRequestRaw);
			}
		    
//			$signedCookie /* ;) */ = Hub::app()->getFrontController()->getCurrentController()->getRequest()->getCookie($this->getSignedRequestCookieName());
//			$signedCookie = isset($_COOKIE[$cookieName]) ? $_COOKIE[$cookieName] : null;
//			if( $signedCookie ) {
//				$signedCookie = $this->parseSignedRequest($signedCookie);
//				var_dump($signedCookie);
//				var_dump(isset($signedRequest['page']));
//				var_dump(($signedCookie && isset($signedCookie['page']) && isset($signedCookie['page']['liked'])));
//			}
			
//			if( (!$signedRequest || is_null($signedRequest) || !(isset($signedRequest['page']) && isset($signedRequest['page']['liked'])))
//					&& $signedCookie && isset($signedCookie['page']) && isset($signedCookie['page']['liked'])) {
//					var_dump((string)$_REQUEST['signed_request']);
//					echo 'that is... ';
//					var_dump($this->parseSignedRequest($_REQUEST['signed_request']));
//					var_dump($signedCookie);
//					exit();
//					unset($_REQUEST['signed_request']);
//			}
			
			$signedRequestSession = $this->getSession()->get($varName);
			$signedRequestRaw = $signedRequestRaw ? $signedRequestRaw :
				($signedRequestSession ? $signedRequestSession: null);
			
//			if( !$signedRequestRaw ) {
//				$this->_signedRequest = null;
//			}
			if( $signedRequestRaw ) {
////			var_dump($_COOKIE);	
				$this->getSession()->set($varName, $signedRequestRaw);
//				$this->getSession()->setCookie('xdn-fbsr-' . $this->getAppId(), $signedRequestRaw);
//				var_dump($signedRequestRaw);
//				echo 'that is... ';
//				var_dump($this->parseSignedRequest($signedRequestRaw));
//				var_dump($_REQUEST['signedRequestCookieName() . ' = ' . $this->signedRequest['page']['liked']);
				$this->_signedRequest = $this->parseSignedRequest($signedRequestRaw);
			}
			
		}
		return $this->_signedRequest;
	}
	
	public function getRawSignedRequest() {
		return $this->getRequest()->getParam('signed_request');
	}
	
	public function getRequest() {
		return Hub::app()->getFrontController()->getRequest();
	}
	
//	public function getUser() {
//		if( !$this->_user ) {
//			$this->_user = $this->getUserFromAccessToken();
//			var_dump($this->getAccessToken());
//			var_dump($this->_user);
//		}
//		
//		return $this->_user;
//	}
	
	public function getMainPageUrl() {
		return $this->getUrlHelper()->getFacebookPageUrl($this);
	}

	/**
	 * @return Xedin_Facebook_Helper_Url
	 */
	public function getUrlHelper() {
		return Hub::getHelper('facebook/url');
	}
	
	/**
	 * @return Xedin_Facebook_Model_Session
	 */
	public function getSession() {
		return Hub::getSingleton('facebook/session');
	}
}