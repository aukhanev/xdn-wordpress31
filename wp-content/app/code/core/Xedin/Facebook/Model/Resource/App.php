<?php

/**
 * Description of App
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Resource_App extends Xedin_Core_Model_Resource_Meta_Abstract {
	
	protected function _construct() {
		parent::_construct();
		
		$this->_init('facebook_apps')
				->_setMetaTableForeignKeyName('app_id');
	}
	
	protected function _beforeSave(Varien_Object $object) {
		$object = parent::_beforeSave($object);
		
		$code = $object->getData('code');
		if( empty($code) ) {
			$code = $object->getData('app_namespace');
			$object->setData('code', $code);
		}
		
		return $object;
	}
}