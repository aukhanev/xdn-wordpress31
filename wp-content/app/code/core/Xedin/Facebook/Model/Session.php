<?php

/**
 * Description of Session
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Session extends Xedin_Core_Model_Session_Abstract {
	
	protected $_namespace = 'facebook';
	
	protected function _construct() {
		parent::_construct();
//		$this->setExpirationSeconds(300);
	}
}