<?php

/**
 * Description of Facebook
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Facebook extends Xedin_Facebook_Model_Abstract {
	
	/** @var Xedin_Facebook_Model_Resource_Facebook */
	protected $_apiModel;
	
	protected $_appModel;
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook/facebook');
	}
	
	public function setApiModel($apiModel) {
		$this->_apiModel = $apiModel;
		return $this;
	}
	
	public function getApiModel() {
		return $this->_apiModel;
	}
	
	public function hasApiModel() {
		return !empty($this->_apiModel);
	}
	
	public function api($path) {
		$result = $this->getApiModel()->api($path);
		if( is_array($path) ) {
			return $result;
		}
		return new Varien_Object($result);
	}
	
	public function __call($method, $args) {
		
		$redirectFunctions = array('set', 'get', 'has', 'uns');
		$apiModel = $this->getApiModel();
//		var_dump($apiModel);
		
		if( in_array(substr($method, 0, 3), $redirectFunctions) ) {
			
			if( !$apiModel || !method_exists($apiModel, $method) ) {
//				var_dump('calling Varien_Object');
				return parent::__call($method, $args);
			}
			
//				var_dump('calling API Model 1');
			return call_user_func_array(array($apiModel, $method), $args);
		}
		
		if( !method_exists($apiModel, $method) ) {
			throw Hub::exception('Xedin_Facebook', 'Method "' . $method . '" does not exist.');
		}
		
//				var_dump('calling API Model 2');
		return call_user_func_array(array($apiModel, $method), $args);
	}
	
	public function getAppModel($id = null) {
		if( !$this->_appModel ) {
			$this->_appModel = Hub::getModel('facebook/app');
			
			if( !is_null($id) ) {
				$this->_appModel->load($id);
			}
		}
		return $this->_appModel;
	}
	
	public function isLiked() {
//		$pageId = $this->getApiModel()->getAppId();
//		$userId = $this->getApiModel()->getUser();
//		var_dump($this->fql('SELECT user_id FROM like WHERE object_id="' . $pageId . '"'));
//		exit();
		if( !$this->getApiModel() ) {
			throw Hub::exception('Xedin_Facebook', 'No API model loaded.');
		}
		$signedRequest = $this->getApiModel()->getSignedRequest();
		return ($signedRequest && isset($signedRequest['page']) && isset($signedRequest['page']['liked']) && $signedRequest['page']['liked']);
	}
	
	/**
	 * @param Varien_Object|array $data
	 * @return string
	 */
	public function getEncodedAppData($data) {
		if( !is_array($data) ) {
			$data = array($data);
		}
		
		if( !($data instanceof Varien_Object) ) {
			$data = new Varien_Object($data);
		}
		
		return base64_encode($data->toJson());
	}
	
	/**
	 * @param Varien_Object|array $data
	 * @return string|null
	 */
	public function getDecodedAppData($data = null) {
		if( is_null($data) ) {
			$signedRequest = $this->getApiModel()->getSignedRequest();
			if( !isset($signedRequest['app_data']) ) {
				return null;
			}
			
			$data = $signedRequest['app_data'];
		}
		
		$data = base64_decode($data);
		$data = Zend_Json::decode($data);
		return new Varien_Object($data);                                                                                                                                         
	}
	
	/**
	 * @return Xedin_Facebook_Helper_Url
	 */
	public function getUrlHelper() {
		return Hub::getHelper('facebook/url');
	}
	
	public function getAppUrl($params = array(), $baseUsername = null, $appId = null) {
		if( is_null($baseUsername) ) {
			$baseUsername = $this->getAppModel()->getData('fb-base-username');
		}
		
		if( is_null($appId) ) {
			$appId = $this->getAppModel()->getApiKey();
		}
		
		return $this->getUrlHelper()->getFacebookPageTabUrl($baseUsername, $appId, $params);
	}
	
	/**
	 * @param type $id
	 * @return Xedin_Facebook_Model_Invite_Request
	 */
	public function getInviteRequestModel($id = null) {
		$inviteRequest = Hub::getSingleton('facebook/invite_request');
		if( !is_null($id) ) {
			$inviteRequest->load($id);
		}
		return $inviteRequest;
	}
	
	public function getInviteRequestCollection($host = null) {
		$appModel = $this->getAppModel();
		if( !$appModel->getId() ) {
			return null;
		}
		
		return $this->getInviteRequestModel()->getExtendedCollection($appModel, $host);
	}
	
	public function fql($query) {
		return $this->api(array(
			'method'	=>	'fql.query',
			'query'		=>	$query
		));
	}
}