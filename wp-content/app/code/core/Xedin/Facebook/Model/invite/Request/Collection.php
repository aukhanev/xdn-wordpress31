<?php

/**
 * Description of Collection
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Invite_Request_Collection extends Xedin_Facebook_Model_Collection_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook/invite_request');
	}
}