<?php

/**
 * Description of Request
 *
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Facebook_Model_Invite_Request extends Xedin_Facebook_Model_Abstract {
	
	protected function _construct() {
		parent::_construct();
		$this->_init('facebook/invite_request');
	}
	
	/**
	 * @param type $facebookId
	 * @param type $app
	 * @return \Xedin_Facebook_Model_Invite_Request 
	 */
	public function loadForApp($facebookId, $app) {
		$this->reset();
		$request = $this->getExtendedCollection($app, null, $facebookId)->addSortOrder('invited_at') ->getFirstItem();
		if( $request ) {
			$this->setData($request->getData());
		}
		
		return $this;
	}
	
	/**
	 * @param type $app
	 * @param type $host
	 * @param type $facebookId
	 * @return Xedin_Facebook_Model_Invite_Request_Collection
	 */
	public function getExtendedCollection($app = null, $host = null, $facebookId = null) {
		
		if( $app instanceof Xedin_Facebook_Model_App ) {
			$app = $app->getId();
		}
		
		$collection = $this->getCollection();
		
		if( !is_null($app) ) {
			$collection->addFieldToFilter('app_id', $app);
		}
		
		if( $host instanceof Xedin_Core_Model_Abstract ) {
			$host = $host->getId();
		}
		
		if( !is_null($host) ) {
			$collection->addFieldToFilter('host_id', $host);
		}
		
		if( !is_null($facebookId) ) {
			$collection->addFieldToFilter('facebook_id', $facebookId);
		}
		
		return $collection;
	}
}