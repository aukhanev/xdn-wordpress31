<?php

return array(
	'modules' => array(		
		'Xedin_Eav'			=>	array(
			'render'			=>	array(
				'values'			=>	array(
					'facebook_app'	=>	array(
						'uri'				=>	'facebook/render_app',
						'label'				=>	'Facebook App'
					)
				)
			)
		)
	)
)
?>