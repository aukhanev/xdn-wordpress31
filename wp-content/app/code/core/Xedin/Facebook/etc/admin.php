<?php

return array(
	'system' => array(
		'modules' => array(
			'facebook' => array(
				'name' => 'facebook',
				'label' => 'Facebook',
				'models' => array(
					'app' => array(
						'path' => 'facebook/app',
						'label' => 'Apps'
					)
				)
			)
		)
	)
)
		
?>