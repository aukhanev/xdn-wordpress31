<?php

class Xedin_Cms_Model_Resource_Post extends Xedin_Core_Model_Resource_Mysql {

    public function __construct() {
        parent::__construct();

        $this->_init('wp_posts');
    }
}