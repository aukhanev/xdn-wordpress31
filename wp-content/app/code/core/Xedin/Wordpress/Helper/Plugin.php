<?php

/**
 * Description of Plugin
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Wordpress_Helper_Plugin extends Xedin_Core_Helper_Abstract {
	
	public function getPluginBasename($filePath) {
		return plugin_basename($filePath);
	}
}