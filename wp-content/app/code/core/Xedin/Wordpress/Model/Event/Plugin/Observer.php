<?php

/**
 * Description of Observer
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Wordpress_Model_Event_Plugin_Observer extends Xedin_Wordpress_Model_Event_Observer_Abstract {
	
	/** @var Xedin_Wordpress_Model_Plugin_Abstract */
	protected $_plugin;
	
	public function getPlugin($pluginCode = null) {
		if( !is_null($pluginCode) ) {
			return $this->getWordpress()->getPlugin($pluginCode);
		}
		
		if( !$this->_plugin ) {
			$this->setPlugin();
		}
		
		return $this->_plugin;
	}
	
	public function setPlugin($plugin = null) {
		if( is_null($plugin) ) {
			if( !$this->hasPluginCode() ) {
				throw Hub::exception('Xedin_Wordpress', 'Observer "' . $this->getCode() . '" must have a plugin code specified.');
			}
			
			$plugin = $this->getPluginCode();
		}
		
		if( !is_object($plugin) ) {
			$plugin = $this->getWordpress()->getPlugin($plugin);
		}
		
		$this->_plugin = $plugin;
		return $this;
	}
	
	public function afterPluginRow($event, $pluginBasename, $pluginMeta) {
		$plugin = $this->getPlugin();
		$args = func_get_args();
		array_shift($args);
		
		if( method_exists($plugin, 'afterPluginRow') ) {
			$this->callPluginMethod('afterPluginRow', $args);
		}
		
		if( $pluginBasename === $plugin->getPluginBasename() ) {
			$this->callPluginMethod('afterThisPluginRow', $plugin, $args);
		}
		
		return $this;
	}
	
	public function callPluginMethod($methodName, $plugin = null, $args = array(), $silent = true) {
		if( is_null($plugin) ) {
			$plugin = $this->getPluginCode();
		}
		
		if( !is_object($plugin) ) {
			$plugin = $this->getPlugin($plugin);
		}
		
		if( !method_exists($plugin, $methodName) ) {
			if( !$silent ) {
				throw Hub::exception('Xedin_Wordpress', 'Could not call method "' . $methodName . '" of plugin "' . $plugin->getCode() . '": method does not exist.');
			}
			
			return null;
		}
		
		return call_user_func_array(array($plugin, $methodName), $args);
	}
}