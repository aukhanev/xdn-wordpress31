<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Wordpress_Model_Event_Observer_Abstract extends Xedin_Core_Model_Event_Observer_Abstract {
	
	/**
	 * @return Xedin_Wordpress_Model_Plugin_Abstract
	 */
	public function getWordpress() {
		return Hub::getSingleton('wordpress/wordpress');
	}
}