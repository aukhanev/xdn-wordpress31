<?php

/**
 * Description of Abstract
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Wordpress_Model_Abstract extends Xedin_Core_Model_Abstract {
	
	public function getConfig($path = array()) {
		return Hub::getConfig()->getArrayValue($path, $array);
	}
}