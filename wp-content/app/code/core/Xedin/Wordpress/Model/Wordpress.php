<?php

/**
 * Description of Wordpress
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Wordpress_Model_Wordpress extends Xedin_Wordpress_Model_Abstract {
	
	const XML_PATH_WORDPRESS = 'wordpress';
	const XML_PATH_PLUGINS = 'plugins';
	
	const REG_PREFIX_WORDPRESS_PLUGIN = '_wordpress_plugin/';
	
	protected $_configType = Xedin_Config::CONFIG_TYPE_XML;
	protected $_isConfigLoaded = false;
	protected $_pluginConfigDirectories = array();
	protected $_pluginConfigDirectory;
	protected $_defaultPluginConfigDirectory = 'etc/';
	
	protected function _construct() {
		parent::_construct();
		$this->setPluginConfigDirectory();
	}
	
	public function addAction($actionName, $callable, $priority = 10, $acceptedArgsAmt = 1) {
		add_action($actionName, $callable, $priority, $acceptedArgsAmt);
		return $this;
	}
	
	/**
		 * @return Xedin_Wordpress_Helper_Plugin
	 */
	public function getPluginHelper() {
		return Hub::getHelper('wordpress/plugin');
	}
	
	public function getPlugin($pluginCode) {
		$config = (array)$this->getPluginConfig($pluginCode);
		
		if( empty($config) ) {
			throw Hub::exception('Xedin_Wordpress', 'Plugin config not found for code "' . $pluginCode . '".');
		}
		
		if( !isset($config['class']) ) {
			throw Hub::exception('Xedin_Wordpress', 'Class name must be set for plugin "' . $pluginCode . '".');
		}
		
		if( !class_exists($config['class']) ) {
			throw Hub::exception('Xedin_Wordpress', 'Class "' . $config['class'] . '" for plugin "' . $pluginCode . '" does not exist.');
		}
		
		$this->registerPlugin(new $config['class'], $pluginCode);
		return $this->getRegisteredPlugin($pluginCode)->setCode($pluginCode);
	}
	
	public function registerPlugin($pluginInstance, $pluginCode) {
		
		$registryKey = $this->getPluginReristryKeyPrefix($pluginCode);
		if( !(Hub::registry($registryKey)) ) {
			Hub::register($pluginInstance, $registryKey);
		}
		
		return $this;
	}
	
	public function getRegisteredPlugin($pluginCode) {
		return Hub::registry($this->getPluginReristryKeyPrefix($pluginCode));
	}
	
	public function getPluginReristryKeyPrefix($pluginCode = '') {
		return self::REG_PREFIX_WORDPRESS_PLUGIN . (string)$pluginCode;
	}
	
	public function getConfig($path = array()) {
		$this->_loadConfigs();
		array_unshift($path, self::XML_PATH_WORDPRESS);
		return Hub::getConfig($path);
	}
	
	public function getPluginConfig($pluginCode = null) {
		
		$configPath = array(self::XML_PATH_PLUGINS);
		if( !empty($pluginCode) ) {
			$configPath[] = $pluginCode;
		}
		return $this->getConfig($configPath);
	}
	
	protected function _loadConfigs() {
		if( !$this->_isConfigLoaded() ) {
			foreach( $this->getPluginConfigDirectories() as $_idx => $_configDirectory ) {
				$config = Hub::getConfig()->getConfigs($_configDirectory, $this->getConfigType());
//				var_dump($config);
//				exit();
				Hub::app()->getEventController()->addEvents($config);
			}
			$this->_isConfigLoaded(true);
		}
		return $this;
	}
	
	public function isConfigLoaded() {
		return $this->_isConfigLoaded();
	}
	
	protected function _isConfigLoaded($isLoaded = null) {
		if( is_null($isLoaded) ) {
			return $this->_isConfigLoaded;
		}
		
		$this->_isConfigLoaded = (bool)$isLoaded;
		return $this;
	}
	
	/**
	 *
	 * @param string $type One of the Xedin_Config::CONFIG_TYPE_* constants.
	 * @return \Xedin_Wordpress_Model_Plugin_Abstract  This instance
	 */
	public function setConfigType($type) {
		$this->_configType = $type;
		return $this;
	}
	
	public function getConfigType() {
		return $this->_configType;
	}
	
	public function getPluginDirectory($pluginPath = '') {
		return WP_PLUGIN_DIR . DS . (empty($pluginPath) ? '' : $pluginPath . DS);
	}
	
	public function getPluginConfigDirectories() {
		if( empty($this->_pluginConfigDirectories) ) {
			$basePluginDirectory = $this->getPluginDirectory();
			$globExpression = $basePluginDirectory . '*' . DS . $this->getPluginConfigDirectory();
			$this->_pluginConfigDirectories = ($directories = glob($globExpression)) ? $directories : array();
		}
		
		return $this->_pluginConfigDirectories;
	}
	
	public function getPluginConfigDirectory() {
		return $this->_pluginConfigDirectory;
	}
	
	public function setPluginConfigDirectory($path = null) {
		if( is_null($path) ) {
			$path = $this->_defaultPluginConfigDirectory;
		}
		
		$this->_pluginConfigDirectory = $path;
		return $this;
	}
	
	public function getActivePlugins() {
		return $this->getNativeOption('active_plugins');
	}
	
	public function getNativeOption($optionCode, $default = null) {
		return get_option($optionCode, $default);
	}
}