<?php

/**
 * All plugins will be treated as singletons.
 * Retrieve the plugin singleton like this:
 * $wp = Hub::getSingleton('wordpress/wordpress');
 * $plugin = $wp->getPlugin('plugin_code');
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
abstract class Xedin_Wordpress_Model_Plugin_Abstract extends Xedin_Wordpress_Model_Abstract {
	
	protected $_baseDirectory;
	protected $_configXmlPathStart = 'wordpress/plugin';
	protected $_configXmlPath;
	protected $_isRunning = false;
	protected $_isInitialized = false;
	
	protected $_fileName;
	protected $_basePath;
	protected $_pluginBasename;
	protected $_uniformName;
	protected $_pluginCode;
	
	protected $_options = array();
	protected $_defaultOptions = array();
	
	protected $_dependencies;
	
	protected $_includeExtension = 'php';
	
	public function _construct() {
		parent::_construct();
		$this->_init('wordpress/plugin');
	}
	
	public function setBaseDirectory($path = null) {
		if( is_null($path) ) {
			$path = dirname($this->getBasePath());
		}
		$path = trim($path, ' ' . DS);
		$this->_baseDirectory = $path . '/';
		return $this;
	}
	
	/**
	 * Retrieve the base directory path.
	 * If no additional path specified, includes trailing directory separator.
	 * 
	 * @param string $path Path to concatenate after the base directory
	 * @return string
	 */
	public function getBaseDirectory($path = '') {
		return $this->_baseDirectory . $path;
	}
	
	public function setFileName($fileName = null) {
		if( is_null($fileName) ) {
			$fileName = basename($this->getBasePath(), ".php"); 
		}
		
		$this->_fileName = $fileName;
		return $this;
	}
	
	public function getFileName() {
		return $this->_fileName;
	}	
	
	/**
	 * Sets the base path of the plugin, which is, in most cases, the absolute
	 * path to the main file of the plugin, e.g. the entry point.
	 * If unsure, use the __FILE__ constant.
	 * 
	 * @param string $path Absolute path to the main plugin file.
	 * @return \Xedin_Wordpress_Model_Plugin_Abstract 
	 */
	public function setBasePath($path) {
		$this->_basePath = $path;
		return $this;
	}
	
	/**
	 * @return string The absolute path of the main plugin file.
	 */
	public function getBasePath() {
		return $this->_basePath;
	}
	
	public function getPluginBasename() {
		return $this->_pluginBasename;
	}
	
	public function setPluginBasename($basename = null) {
		if( is_null($basename) ) {
			$basename = $this->getPluginHelper()->getPluginBasename($this->getBasePath());
		}
		$this->_pluginBasename = $basename;
		return $this;
	}
	
	public function setConfigXmlPath($path = null) {
		if( is_null($path) ) {
			$path = $this->getUniformName();
			$nodeDelimiter = Xedin_Config::NODE_DELIMITER;
			$this->_configXmlPath = trim($this->_configXmlPathStart, ' ' . $nodeDelimiter) . $nodeDelimiter . $path;
			return;
		}
		
		$this->_configXmlPath = $path;
	}
	
	public function getConfigXmlPath() {
		return $this->_configXmlPath;
	}
	
	public function setUniformName($name) {
		$this->_uniformName = $name;
		return $this;
	}
	
	public function getUniformName() {
		if( !$this->_uniformName ) {
			$this->_uniformName = preg_replace( '!(-|_)+!', '_', strtolower(basename($this->getFileName())) );
		}
		
		return $this->_uniformName;
	}
	
	public function setPluginCode($code = null) {
		if( is_null($code) ) {
			$code = $this->getFileName();
		}
		
		$this->_pluginCode = $code;
		return $this;
	}
	
	public function getPluginCode() {
		return $this->_pluginCode;
	}
	
	final public function run($options = array()) {
		if( !$this->_isRunning() ) {
			$this->_beforeRun();
			
			// If options are a filename
			if( !is_array($options) ) {
				$options = array('base_path' => $options);
			}
			
			$this->setOptions($options, $this->_defaultOptions);
			
			if( $basePath = $this->getOptions('base_path') ) {
				$this->setBasePath($basePath);
			}
			
			$this->_run();
			$this->_afterRun();
			$this->_isRunning(true);
		}
		return $this;
	}
	
	protected function _run() {
		$this->init();
		
		return $this;
	}
	
	public function init($basePath = null) {
		if( !$this->_isInitialized() ) {
			if( !is_null($basePath) ) {
				$this->setBasePath($basePath);
			}
			
			if( !$this->getBasePath() ) {
				throw Hub::exception('Xedin_Wordpress', 'Could not initialize plugin class "' . get_class($this) . '": base path must be set.');
			}

			if( !$this->getBaseDirectory() ) {
				$this->setBaseDirectory();
			}

			if( !$this->getFileName() ) {
				$this->setFileName();
			}
			
			if( !$this->getPluginBasename() ) {
				$this->setPluginBasename();
			}

			if( !$this->getConfigDirectory() ) {
				$this->setConfigDirectory();
			}

			if( !$this->getConfigXmlPath() ) {
				$this->setConfigXmlPath();
			}

			if( !$this->getPluginCode() ) {
				$this->setPluginCode();
			}
			
			$this->_isInitialized(true);
		}
		
		return $this;
	}
	
	protected function _beforeRun() {
		return $this;
	}
	
	protected function _afterRun() {
		return $this;
	}
	
	protected function _isRunning($isRunning = null) {
		if( is_null($isRunning) ) {
			return $this->_isRunning;
		}
		
		$this->_isRunning = (bool)$isRunning;
		return $this;
	}
	
	public function isRunning() {
		return $this->_isRunning();
	}
	
	protected function _isInitialized($isInitialized = null) {
		if( is_null($isInitialized) ) {
			return $this->_isInitialized;
		}
		
		$this->_isInitialized = (bool)$isInitialized;
		return $this;
	}
	
	public function isInitialized() {
		return $this->_isInitialized();
	}
	
	public function setOptions($options, $defaultOptions = null) {
		if( is_array($options) ) {
			if( !is_null($defaultOptions) ) {
				$this->_mergeOptions($options, $defaultOptions);
				return $this;
			}
			
			$this->_options = $options;
		}
		
		$this->_options[$options] = $defaultOptions;
		
		return $this;
	}
	
	protected function _mergeOptions($options, $defaultOptions = null) {
		if( is_null($defaultOptions) ) {
			$defaultOptions = $this->_defaultOptions;
		}
		
		$this->_options = array_merge_recursive($defaultOptions, $options);
		return $this;
	}
	
	public function getOptions($optionName = null, $default = null) {
		if( is_null($optionName) ) {
			return $this->_options;
		}
		
		if( !isset($this->_options[$optionName]) ) {
			return $default;
		}
		
		return $this->_options[$optionName];
	}
	
	protected function _loadConfigs() {
		if( !$this->_isConfigLoaded() ) {
			$config = Hub::getConfig()->getConfigs($this->getConfigDirectory(), $this->getConfigType());
			Hub::app()->getEventController()->loadEvents($config);
			$this->_isConfigLoaded(true);
		}
		return $this;
	}
	
	public function getConfig($path = null) {
		$config = Hub::getConfig($this->getConfigXmlPath());
		
		if( is_null($path) ) {
			return $config;
		}
		
		return Hub::getConfig()->getArrayValue($path, $config);
	}
	
	public function getDependencies() {
		if( empty($this->_dependencies) ) {
			$this->_dependencies = array_keys((array)$this->getConfig('dependencies'));
		}
		
		return $this->_dependencies;
	}
	
	/**
	 * @return Xedin_Wordpress_Model_Wordpress
	 */
	public static function getWordpress() {
		return Hub::getSingleton('wordpress/wordpress');
	}
	
	public function includeScript($path) {
		$path = $this->getBaseDirectory($path);
		$pathInfo = pathinfo($path);
		if( !isset($pathInfo['extension']) ) {
			$path = $this->getIncludeExtension($path);
		}
		
		if( !is_readable($path) ) {
			Hub::exception('Xedin_Wordpress', 'File "' . $path . '" does not exist or is not readable.');
		}
		
		require_once($path);
		return $this;
	}
	
	public function includeDirectory($path) {
		$path = $this->getBaseDirectory( trim($path, '\\/') ) . DS;
		$globExpression = $path . $this->getIncludeExtension('*');
		foreach( glob($globExpression) as $_idx => $_filePath ) {
			require_once($_filePath);
		}
		
		return $this;
	}
	
	public function getIncludeExtension($filename = '') {
		if( empty($filename) ) {
			return $this->_includeExtension;
		}
		
		return $filename . '.' . $this->_includeExtension;
	}
	
	public function getPluginHelper() {
		return $this->getWordpress()->getPluginHelper();
	}
}