<?php

/**
 * Description of Plugin
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Wordpress_Model_Resource_Plugin extends Xedin_Wordpress_Model_Resource_Mysql_Abstract {
	
	protected $_defaultHeaders = array(
		'Name' => 'Plugin Name',
		'PluginURI' => 'Plugin URI',
		'Version' => 'Version',
		'Description' => 'Description',
		'Author' => 'Author',
		'AuthorURI' => 'Author URI',
		'TextDomain' => 'Text Domain',
		'DomainPath' => 'Domain Path',
		'Network' => 'Network',
		// Site Wide Only is deprecated in favor of Network.
		'_sitewide' => 'Site Wide Only',
	);
	
	public function load(Xedin_Core_Model_Abstract $object, $value=null, $field=null) {
		if( $this->getTableName() ) {
			parent::load($object, $value, $field);
		}
		
		/* @var $object Xedin_Wordpress_Model_Plugin_Abstract */
		
		// ensure path to this file is via main wp plugin path
		$thisFilePath = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", $object->getBasePath());
		$thisPlugin = plugin_basename(trim($thisFilePath));
		$activePlugins = get_option('active_plugins');
		$thisPluginKey = array_search($thisPlugin, $activePlugins);
		$object->setData('order', $thisPluginKey);
		
		// Now load plugin info
		$rawData = $this->getFileData($object->getBasePath());
		$newData = array();
		$newData = array_change_key_case($rawData, CASE_LOWER);
		$object->addData($newData);
		
		
		return $this;
	}
	
	public function save(Xedin_Core_Model_Abstract $object) {
		if( $this->getTableName() ) {
			parent::save($object);
		}
		/* @var $object Xedin_Wordpress_Model_Plugin_Abstract */
		
		// ensure path to this file is via main wp plugin path
		$thisFilePath = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", $object->getBasePath());
		$thisPlugin = plugin_basename(trim($thisFilePath));
		$activePlugins = get_option('active_plugins');
		$thisPluginKey = array_search($thisPlugin, $activePlugins);
		array_splice($activePlugins, $thisPluginKey);
		array_splice($activePlugins, $object->getOrder());
		update_option('active_plugins', $activePlugins);
		
		return $this;
	}
	
	public function getFileData($filePath, $defaultHeaders = null) {
		
		if( is_null($defaultHeaders) ) {
			$defaultHeaders = $this->_defaultHeaders;
		}
		
		if( !function_exists('get_file_data') ) {
			return array();
		}
		
		return get_file_data($filePath, $defaultHeaders);
	}
}