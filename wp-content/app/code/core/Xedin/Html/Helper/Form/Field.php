<?php
/**
 * Description of Field
 *
 * @author anton
 */
class Xedin_Html_Helper_Form_Field extends Xedin_Core_Helper_Abstract {
	
	const PATH_DELIMITER = Hub::URI_SEPARATOR;
	const ATTR_ID_SEPARATOR = ':';
	
	public function getNameFromPath($path, $default = '') {
		$path = $this->getPathArray($path);
		
		$pathCount = count($path);
		if( !$pathCount ) {
			return $default;
		}
		$name = $path[0];
		for( $i=1; $i<$pathCount; $i++ ) {
			$name .= '[' . $path[$i] . ']';
		}
		
		return $name;
	}
	
	public function getIdFromPath($path, $default = '') {
		$path = $this->getPathArray($path);
		
		$pathCount = count($path);
		if( !$pathCount ) {
			return $default;
		}
		
		$id = implode(self::ATTR_ID_SEPARATOR, $path);
		return $id;
	}
	
	public function getPathArray($path) {
		if( !is_array($path) ) {
			$path = explode(self::PATH_DELIMITER, $path);
		}
		
		return $path;
	}
	
	public function getModelNamespace($uri) {
		return str_ireplace(array('/'), '-', $uri);
	}
}