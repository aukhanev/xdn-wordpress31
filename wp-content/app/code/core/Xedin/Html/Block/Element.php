<?php
/**
 * Description of Element
 *
 * @author Xedin Unknown
 */
class Xedin_Html_Block_Element extends Xedin_Html_Block_Abstract {
	
//	protected $_attributes = array();
	protected $_tagName = 'div';
	protected $_isSelfClosing = false;
	
	protected $_attributesToIgnore = array(
		'name_in_layout'			=>	true,
		'template'					=>	true,
		'after'						=>	true
	);
	
	const ATTRIBUTE_VALUE_DISABLED = 'disabled';
	const ATTRIBUTE_VALUE_READONLY = 'readonly';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setTemplate('html/element.php');
	}
	
//	public function setAttribute($name, $value = '') {
//		$this->_attributes[$name] = $value;
//		return $this;
//	}
//	
//	public function setAttributes($attributes) {
//		if( $attributes instanceof Varien_Object ) {
//			$attributes = $attributes->getData();
//		}
//		
//		if( !is_array($attributes) ) {
//			return $this;
//		}
//		
//		foreach( $attributes as $_key => $_value ) {
//			$this->setAttribute($_key, $_value);
//		}
//		
//		return $this;
//	}
//	
//	public function getAttributes($name = null, $default = null) {
//		if( is_null($name) ) {
//			return $this->_attributes;
//		}
//		
//		if( !isset($this->_attributes[$name]) ) {
//			return $default;
//		}
//		
//		return $this->_attribtues[$name];
//	}
//	
//	public function hasAttributes($name = null) {
//		if( is_null($name) ) {
//			return !empty($this->_attributes);
//		}
//		
//		return isset($this->_attributes[$name]);
//	}
//	
//	public function addAttributes($name, $value = null, $overwrite = true) {
//		if( is_null($value) && (is_array($name) || $name instanceof Varien_Object) ) {
//			foreach( $name as $_key => $_value ) {
//				$this->addAttributes($_key, $_value);
//			}
//			return $this;
//		}
//		
//		if( $overwrite || !$this->hasAttributes($name) ) {
//			$this->setAttribute($name, $value);
//		}
//		
//		return $this;
//	}
	
	/**
	 *
	 * @param string $tagName
	 * @return \Xedin_Html_Block_Element 
	 */
	public function setTagName($tagName) {
		$this->_tagName = $tagName;
		return $this;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getTagName() {
		return $this->_tagName;
	}
	
	public function getClass() {
		
	}
	
	protected function getClassArray() {
		return preg_split('!\s*!', $this->getData('class'));
	}
	
	protected function setClassArray($classes) {
		$this->setData('classes', implode(' ', $classes));
	}
	
	public function addClass($className) {
		$classes = $this->getClassArray();
		if( !in_array($className, $classes) ) {
			return $this;
		}
		
		$classes[] = $className;
		
		$this->setClassArray($classes);
		return $this;
	}
	
	public function removeClass($className) {
		$classes = $this->getClassArray();
		if( $classIndex = array_search($className, $classes) ) {
			unset($classes[$classIndex]);
		}
		
		if( !count($classes) ) {
			$this->unsetData('class');
		}
		
		return $this;
	}
	
	public function hasClass($className = null) {
		if( is_null($className) && !$this->hasData('class') ) {
			return $this->hasData('class');
		}
		
		$classes = $this->getClassArray();
		return in_array($className, $classes);
	}
	
	public function isDisabled($disabled = null) {
		if( is_null($disabled) ) {
			return $this->hasData('disabled');
		}
		
		$disabled = (bool)$disabled;
		if( $disabled ) {
			$this->setData('disabled', self::ATTRIBUTE_VALUE_DISABLED);
			return $this;
		}
		
		$this->unsetData('disabled');
		return $this;
	}
	
	public function isReadonly($readonly = null) {
		if( is_null($readonly) ) {
			return $this->hasData('readonly');
		}
		
		$readonly = (bool)$readonly;
		if( $readonly ) {
			$this->setData('readonly', self::ATTRIBUTE_VALUE_READONLY);
			return $this;
		}
		
		$this->unsetData('readonly');
		return $this;
	}
	
	public function isSelfClosing($selfClosing = null) {
		if( is_null($selfClosing) ) {
			return $this->_isSelfClosing;
		}
		
		$this->_isSelfClosing = (bool)$selfClosing;
		return $this;
	}
	
	public function getAttributeString() {
		$attributes = array();
		foreach( $this->getData() as $_name => $_value ) {
			if( $this->hasAttributesToIgnore($_name) ) {
				continue;
			}
			$attributes[] = $_name . '="' . $_value . '"';
		}
		return implode(' ', $attributes);
	}
	
	public function getContent() {
		return $this->getChildHtml();
	}
	
	public function setContent($content) {
		$this->addContent($content);
		return $this;
	}
	
	public function addContent($content) {
		if( is_string($content) ) {
			$content = $this->getLayout()->createBlock('html/element_text')->setContent($content);
		}
		$this->insert($content);
		return $this;
	}
	
	public function toString() {
		return $this->toHtml();
	}
	
	public function __toString() {
		return $this->toString();
	}
	
	public function hasAttributesToIgnore($name = null) {
		if( is_null($name) ) {
			return !empty($this->_attributesToIgnore);
		}
		
		return ( isset($this->_attributesToIgnore[$name]) && $this->_attributesToIgnore[$name] );
	}
	
	public function addAttributesToIgnore($name) {
		$this->_attributesToIgnore[$name] = true;
		return $this;
	}
	
	public function removeAttributesToIgnore($name) {
		if( isset($this->_attributesToIgnore[$name]) ) {
			unset($this->_attributesToIgnore);
		}
	} 
}