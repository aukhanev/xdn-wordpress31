<?php

/**
 * Description of Option
 *
 * @author anton
 */
class Xedin_Html_Block_Element_Input_Select_Option extends Xedin_Html_Block_Element {
	
	protected $_tagName = 'option';
	protected $_isSelfClosing = false;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->addAttributesToIgnore('type');
	}
}