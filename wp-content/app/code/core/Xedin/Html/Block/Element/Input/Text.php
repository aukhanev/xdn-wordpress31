<?php

/**
 * Description of Text
 *
 * @author anton
 */
class Xedin_Html_Block_Element_Input_Text extends Xedin_Html_Block_Element_Input_Abstract {
	
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->setType('text')
				->isSelfClosing(true);
	}
}