<?php

/**
 * Description of Select
 *
 * @author anton
 */
class Xedin_Html_Block_Element_Input_Select extends Xedin_Html_Block_Element_Input_Abstract {
	
	protected $_tagName = 'select';
	protected $_optionBlockUri = 'html/element_input_select_option';
	protected $_labelFieldName = 'label';
	
	const ATTRIBUTE_VALUE_MULTIPLE = 'multiple';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		
		$this->addAttributesToIgnore('type')
				->addAttributesToIgnore('value');
	}
	
	public function getLabelFieldName() {
		return $this->_labelFieldName;
	}
	
	public function setLabelFieldName($fieldName) {
		$this->_labelFieldName = (string)$fieldName;
		return $this;
	}
	
	public function addOption($value = '', $label = '', $params = array()) {
		$this->addContent( $this->getLayout()->createBlock($this->getOptionBlockUri())->setValue($value)->setContent($label)->addData($params) );
		return $this;
	}
	
	public function setOptions($options, $selectedOption = null) {
		if( is_null($selectedOption) && $this->hasValue() ) {
			$selectedOption = $this->getValue();
		}
		if( empty($selectedOption) ) {
			$selectedOption = (int)$selectedOption;
		}
		
		foreach( $options as $_id => $_label ) {
			$params = array();
			
			if( !is_array($selectedOption) ) {
				$selectedOption = array($selectedOption);
			}
			
			foreach( $selectedOption as $_idx => $_value ) {
				$selectedOption[$_idx] = (string)$_value;
			}
			$_id = (string)$_id;
			
			if( in_array($_id, $selectedOption, true) ) {
				$params['selected'] = 'selected';
			}
			
			if( $_label instanceof Varien_Object ) {
				if( is_string($_label->getData()) ) {
					$_label = $_label->getData();
				}
				else {
					$_label = $_label->getData($this->getLabelFieldName());
				}
			}
			elseif( is_array($_label) ) {
				$_label = $_label[$this->getLabelFieldName()];
			}
			
			if( is_null($_label) ) {
				throw Hub::exception('Xedin_Html', 'The field specified as label field name (' . $this->getLabelFieldName() . ') does not exist in ' . get_class($options[$_id]) . '.');
			}
			$this->addOption($_id, $_label, $params);
		}
		return $this;
	}
	
	public function getOptionBlockUri() {
		return $this->_optionBlockUri;
	}
	
	public function setMultiple($multiple) {
		if( is_string($multiple) ) {
			$this->setData('multiple', $multiple);
			return $this;
		}
		
		if( $multiple ) {
			$this->setData('multiple', self::ATTRIBUTE_VALUE_MULTIPLE);
			return $this;
		}
		
		$this->unsetData('multiple');
		return $this;
	}
	
	protected function _beforeToHtml() {
		parent::_beforeToHtml();
		
		if( $this->getMultiple() != self::ATTRIBUTE_VALUE_MULTIPLE ) {
			return $this;
		}
		
		$name = $this->getName();
		$nameEndsWith = substr($name, strlen($name) - 2);
		if( $nameEndsWith != '[]' ) {
			$this->setName($name . '[]');
		}
		
		return $this;
	}
}