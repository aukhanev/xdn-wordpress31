<?php

/**
 * Description of Text
 *
 * @author anton
 */
class Xedin_Html_Block_Element_Input_Textarea extends Xedin_Html_Block_Element_Input_Abstract {
	
	protected $_tagName = 'textarea';
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
	}
}