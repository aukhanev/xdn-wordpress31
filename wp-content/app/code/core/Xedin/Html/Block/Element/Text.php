<?php

/**
 * Description of Text
 *
 * @author anton
 */
class Xedin_Html_Block_Element_Text extends Xedin_Html_Block_Abstract {
	protected $_content;
	
	protected function _construct($layout = null, $attributes = array()) {
		parent::_construct($layout, $attributes);
		$this->setTemplate('html/element/text.php');
	}
	
	public function setContent($content) {
		$this->_content = $content;
		return $this;
	}
	
	public function getContent() {
		return $this->_content;
	}
	
	public function __toString() {
		return $this->toHtml();
	}
}