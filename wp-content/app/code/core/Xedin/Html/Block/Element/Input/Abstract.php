<?php

/**
 * Description of Abstract
 *
 * @author anton
 */
abstract class Xedin_Html_Block_Element_Input_Abstract extends Xedin_Html_Block_Element {
	
	protected $_tagName = 'input';
	protected $_labelFieldName = 'label';
}