<?php

/**
 * Description of Anchor
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Html_Block_Element_Anchor extends Xedin_Html_Block_Element {
	
	protected $_tagName = 'a';
}