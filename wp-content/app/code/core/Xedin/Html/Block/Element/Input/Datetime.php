<?php

/**
 * Description of Datetime
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Html_Block_Element_Input_Datetime extends Xedin_Html_Block_Element_Input_Text {
	
	protected $_pickerOptions = array(
					'timeFormat'	=>	'hh:mm:ss',
					'dateFormat'	=>	'yy-mm-dd'
				);
	
	protected function _toHtml() {
		$html = parent::_toHtml();
		$html .= $this->getLayout()->createBlock('clientside/ui_datetimepicker')->setElementSelector('"#' . $this->getId() . '"')
				->setPickerOptions($this->getPickerOptions())
				->toHtml();
		return $html;
	}
	
	public function setPickerOptions($name, $value = null) {
		if( is_null($value) && is_array($name) ) {
			$this->_pickerOptions = $name;
			return $this;
		}
		
		$this->_pickerOptions[$name] = $value;
		
		return $this;
	}
	
	public function hasPickerOptions($name = null) {
		if( is_null($name) ) {
			return !empty($this->_pickerOptions);
		}
		
		return isset($this->_pickerOptions);
	}
	
	public function addPickerOptions($name, $value = null) {
		if( is_null($value) && is_array($name) ) {
			foreach( $name as $_name => $_value ) {
				$this->addPickerOptions($_name, $_value);
			}
			
			return $this;
		}
		
		if( !$this->hasPickerOptions($name) ) {
			$this->setPickerOptions($name, $value);
		}
		
		return $this;
	}
	
	public function getPickerOptions($name = null, $default = null) {
		if( is_null($name) ) {
			return $this->_pickerOptions;
		}
		
		if( !$this->hasPickerOptions($name) ) {
			return $default;
		}
		
		return $this->_pickerOptions[$name];
	}
}