<?php

/**
 * Description of List
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Html_Block_List extends Xedin_Html_Block_Abstract {
	
	protected $_items;
	protected $_itemClass = 'Varien_Object';
	
	public function addItem($url, $label = null, $title = null, $prepare = true) {
		if( $prepare ) {
			$url = Hub::getUrl($url);
		}
		
		if( is_null($label) ) {
			$label = $url;
		}
		
		if( is_null($title) ) {
			$title = $label;
		}
		
		$itemClass = $this->getItemClass();
		$item = new $itemClass();
		$item->setData(array(
			'url'			=>	$url,
			'label'			=>	$label,
			'title'			=>	$title
		));
		
		$this->_items[] = $item;
		
		return $this;
	}
	
	public function getItems() {
		return $this->_items;
	}
	
	public function getItemClass() {
		return $this->_itemClass;
	}
	
	public function setItemClass($itemClass) {
		$this->_itemClass = $itemClass;
		return $this;
	}
	
	public function removeByAttribute($value, $attribute) {
		foreach( $this->getItems() as $_idx => $_item ) {
			if( isset($_item[$attribute]) && $_item[$attribute] == $value ) {
				unset($this->_items[$_idx]);
			}
		}
		
		return $this;
	}
}