<?php
/*
Plugin Name: Mage Modules
Description: Allows creating autoloadable classes and organizing them into modules Magento style! =)
Version: 0.1
Author: Xedin Unknown <xedin.unknown@gmail.com>
*/
$oldErrLvl = error_reporting(E_ALL);


/**
 * Tiny function to enhance functionality of ucwords
 *
 * Will capitalize first letters and convert separators if needed
 *
 * @param string $str
 * @param string $destSep
 * @param string $srcSep
 * @return string
 */
function uc_words($str, $destSep='_', $srcSep='_')
{
    return str_replace(' ', $destSep, ucwords(str_replace($srcSep, ' ', $str)));
}


define( 'MM_PLUGIN_ROOT', 'wp-content/plugins/' );
define( 'MM_ROOT', 'wp-content/plugins/' . dirname(plugin_basename(__FILE__)) . '/' );
define( 'MM_CODE_ROOT', '' );

/**
 * Determines if this plugin is the first in the list of active plugins, e.g.
 * at index 0. If not shifts it to the beginning of the plugin queue.
 */
function mm_loadFirst() {
	// ensure path to this file is via main wp plugin path
	$thisFilePath = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__);
	$thisPlugin = plugin_basename(trim($thisFilePath));
	$activePlugins = get_option('active_plugins');
	$thisPluginKey = array_search($thisPlugin, $activePlugins);
	if ($thisPluginKey) { // if it's 0 it's the first plugin already, no need to continue
		array_splice($activePlugins, $thisPluginKey, 1);
		array_unshift($activePlugins, $thisPlugin);
		update_option('active_plugins', $activePlugins);
	}
}

/**
 * Found in /wp-admin/includes/plugin.php ln. 536.
 * Runs after the list of active plugins has been saved.
 */
add_action("activated_plugin", "mm_loadFirst");


function mm_wordpressInit() {
    // Code to possibly intercept request
}
add_action('init', 'mm_wordpressInit');

function mm_addMetaboxes() {
    add_meta_box(
            'mage_modules', __('Mage Modules'), 'mm_getFieldHtml', 'post', 'advanced', 'default', array('textarea', 'layout_update', 'XML Layout Update')
    );
    add_meta_box(
            'mage_modules', __('Mage Modules'), 'mm_getFieldHtml', 'page', 'advanced', 'default', array('textarea', 'layout_update', 'XML Layout Update')
    );
}
add_action( 'add_meta_boxes', 'mm_addMetaboxes' );

function mm_getFieldHtml($post, $more) {
    $args = $more['args'];
    $fieldType = $args[0];
    $fieldName = $args[1];
    $fieldTitle = $args[2];
    $options = isset($args[3]) ? $args[3] : array();
?>
<div class="mage-modules-field">
    <label for="mage_modules_fields:<?php echo $fieldName ?>"><?php echo $fieldTitle ?></label><br />
    <?php switch($fieldType):
        case 'textarea': ?>
    <textarea name="mage_modules_fields[<?php echo $fieldName ?>]" id="mage_modules_fields:<?php echo $fieldName ?>" rows="20" cols="100"><?php echo get_post_meta($post->ID, $fieldName, true) ?>
    </textarea>
    <?php break; ?>
    <?php case 'select': ?>
    <select name="mage_modules_fields[<?php echo $fieldName ?>]" id="mage_modules_fields:<?php echo $fieldName ?>">
        <?php foreach( $options as $_value => $_label ): ?>
        <option value="<?php echo $_value ?>"><?php echo $_label ?></option>
        <?php endforeach ?>
    </select>
        <?php default: ?>
    <input type="<?php echo $fieldType ?>" name="mage_modules_fields[<?php echo $fieldName ?>]" id="mage_modules_fields:<?php echo $fieldName ?>" value="<?php echo get_post_meta($post->ID, $fieldName, true) ?>"/>
    <?php endswitch ?>
    <?php wp_nonce_field( plugin_basename( __FILE__ ), 'mage_modules_fields[mm_auth]' ); ?>
</div>    
<?php
}

function mm_savePostData($postId) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  {
        return;
    }
    
    if( !isset($_POST['mage_modules_fields']) ) {
        return;
    }
    
    if ( !wp_verify_nonce( $_POST['mage_modules_fields']['mm_auth'], plugin_basename( __FILE__ ) ) ) {
        return;
    }
    
    // Check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $postId ) ) {
            return;
        }
    }
    else {
        if ( !current_user_can( 'edit_post', $postId ) ) {
            return;
        }
    }
    
    foreach( $_POST['mage_modules_fields'] as $_name => $_value ) {
        if( $_name == 'mm_auth' ) {
            continue;
        }
        
        if( !update_post_meta($postId, $_name, $_value) ) {
            add_post_meta($postId, $_name, $_value, true);
        }
    }
}
add_action( 'save_post', 'mm_savePostData' );

require_once( ABSPATH . MM_PLUGIN_ROOT . dirname(plugin_basename(__FILE__)) . DIRECTORY_SEPARATOR . MM_CODE_ROOT  . '/Loader.php' );

Mage_Loader::setBasePath( ABSPATH . MM_PLUGIN_ROOT);
Mage_Loader::autoloadRegister();

try{
    /**
     * @var Xedin_Wordpress_Model_Post $model
     */
//    $model = Mage_Loader::getSingleton('xedin_wordpress/model_category')->load(15)->getTopParent();
}
catch( Exception $e ) {
    echo $e->getMessage();      
}
//exit();


error_reporting($oldErrLvl);
?>