<?php
/**
 * @version 0.1.2
 * + Added constant retrieval functionality. See $_constants, getConstant(),
 * normalizeConstantName(), addConstant, setConstant(), unsConstant(),
 * hasConstant().
 * This allows to override existing global, local and class
 * constants without overwriting values of actual PHP constant.
 * This method uses the registry, so it is always possible to retrieve local
 * constants even if the use of getConstant() is not advised. See REG_PREFIX_CONSTANT.
 * 
 * 0.1.1
 * + Added the getModelUri() method. The model URI passed to getModel() no longer has to contain "model_". Duh!
 * 
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 */

class Mage_Loader {
    protected static $_registry;
    private static $_singletons = array();
    private static $_classNames = array();

    private static $_basePath;
    private static $_fileExtension = 'php';
    
    private static $_layout;
	
	private static $_constants = array();
    
    const URI_SEPARATOR = '/';
    const URI_PREFIX_BLOCK = 'block_';
    const REG_PREFIX_SINGLETON = '_singleton';
	const REG_PREFIX_CONSTANT = '_constant';
    
    const URL_TYPE_BASE = 'base';
    const URL_TYPE_THEME = 'theme';
    const URL_TYPE_INCLUDES = 'includes';
    const URL_TYPE_CONTENT = 'content';
    const URL_TYPE_UPLOADS = 'uploads';
    
    const PATH_TYPE_BASE = 'base';
    const PATH_TYPE_THEME = 'theme';
    const PATH_TYPE_INCLUDES = 'includes';
    const PATH_TYPE_CONTENT = 'content';
    const PATH_TYPE_UPLOADS = 'uploads';

    public static function register($key, $value, $graceful = true) {
        if( is_null($value) ) {
            self::unregister($key);
            return true;
        }
        
        if( isset(self::$_registry[$key]) ) {
            if( $graceful ) {
                return false;
            }
            
            throw new Exception('Could not register key "' . $key . '": already exists.');
        }
        self::$_registry[$key] = $value;
    }
    
    public static function unregister($key, $graceful = true) {
        if( !isset(self::$_registry[$key]) ) {
            if( $graceful ) {
                return false;
            }
            
            throw new Exception('Could not unregister key "' . $key . '": no such key found.');
        }
        
        unset(self::$_registry[$key]);
    }
    
    public static function registry($key, $default = null) {
        if( isset(self::$_registry[$key]) ) {
            return self::$_registry[$key];
        }
        return $default;
    }
    
    public static function setFileExtension($extension) {
        self::$_fileExtension = $extension;
    }

    public static function getFileExtension() {
        return self::$_fileExtension;
    }

    public static function getBasePath() {
        return self::$_basePath;
    }

    public static function setBasePath($basePath) {
        self::$_basePath = $basePath;
    }

    public static function getClassName($classIdentifier) {
        if( !isset(self::$_classNames[$classIdentifier]) ) {
            $className = strtolower( str_ireplace( array('_', '/'), ' ', $classIdentifier ) );
            $className = ucwords($className);
            $className = str_ireplace( ' ', '_', $className );
            self::$_classNames[$classIdentifier] = $className;
        }
        return self::$_classNames[$classIdentifier];
    }

    public static function getFilePath($classIdentifier) {
        $className = self::getClassName($classIdentifier);
        
        
        
        $filePath = str_ireplace( '_', '/', $className ) . '.' . self::$_fileExtension;

        return self::$_basePath . $filePath;
    }

    public static function getSingleton($classIdentifier) {
        $newClassIdentifier = self::REG_PREFIX_SINGLETON . self::URI_SEPARATOR . $classIdentifier;
        if( !self::registry($newClassIdentifier) ) {
            self::register($newClassIdentifier, self::getModel($classIdentifier));
        }
        return self::registry($newClassIdentifier);
    }
    
    /**
     *
     * @todo Refactor legacy code that contains "model_".
     * @param string $uri Something of format "module/class_name"
     */
    public static function getModelUri($uri) {
        $uri = explode('/', $uri);
        if( strpos($uri[1], 'model_') !== 0 ) {
            $uri[1] = 'model_' . $uri[1];
        }
        $uri = implode('/', $uri);
        return self::getClassName($uri);
    }

    public static function getModel($classIdentifier, $params = array()) {
        return self::getInstance(self::getModelUri($classIdentifier), $params);
    }
    
    public static function getInstance($classIdentifier) {
        self::loadClass($classIdentifier);
        if( !class_exists(self::getClassName($classIdentifier)) ) {
			throw new Exception('Class for identifier "' . $classIdentifier . '" does not exist');
            return null;
        }

        $className = self::getClassName($classIdentifier);
        
        $args = func_get_args();
        array_shift($args);
        
        $instance = null;
        if( count($args) ) {
            $instance = (new $className($args[0]));
        }
        else {
            $instance = (new $className());
        }
        return $instance;
    }

    public static function loadClass($classIdentifier, $silent = true) {
        $classFilePath = self::getFilePath($classIdentifier);
        if( !file_exists( $classFilePath ) ) {
            if( $silent ) {
                return false;
            }
            
            throw new Exception('No class "' . $classIdentifier . '" found at "' . $classFilePath . '"');
        }
        try {
            require_once( $classFilePath );
        }
        catch( Exception $e ) {
            echo get_class($this) . ': ' . $e->getMessage() . "\n" . print_r( debug_backtrace(), true );
        }
    }

    public static function exception($namespace = '', $message = '') {
        $className = 'Exception';
        if( !empty($namespace) ) {
            $className = $namespace . '_' . $className;
        }

        return new $className($message);
    }
    
    public static function autoloadRegister() {
        spl_autoload_register(__CLASS__ . '::loadClass');
        return true;
    }
    
    public function getBlock($blockUri, $attributes = array()) {
        if( stripos(self::URI_SEPARATOR, $blockUri) === false ) {
            $uriParts = explode(self::URI_SEPARATOR, $blockUri);
            $uriParts[1] = self::URI_PREFIX_BLOCK . $uriParts[1];
            $blockUri = implode(self::URI_SEPARATOR, $uriParts);
        }
//        echo 'get block "' . $blockUri . '"' . "\n";
        return self::getInstance($blockUri, $attributes);
    }
    
    public static function getLayout($source = null) {
        if( !self::registry('_layout') ) {
            self::register('_layout', self::getModel('xedin_wordpress/layout',  $source));
        }
        return self::registry('_layout');
    }
    
    public static function getFrontController($options = array()) {
        if( !self::registry('_frontController') ) {
            self::register('_frontController', new Xedin_Core_Controller_Front($options));
        }
        
        return self::registry('_frontController');
    }
    
    public static function getControllerPath($uri) {
        if(stripos($uri, '/') ) {
            $uriParts = explode('/', $uri);
            
            $module = $uriParts[0];
            $controller = $uriParts[1];
            
            /**
             * @todo Switch to config-dependent algorythm
             */
            $module = 'xedin_' . $module;
            $module = str_ireplace('_', ' ', $module);
            $module = ucwords($module);
            $module = str_ireplace(' ', '/', $module);
            
            $controller = str_ireplace('_', ' ', $controller);
            $controller = ucwords($controller);
            $controller = 'controllers/' . $controller . 'Controller';
            $controller = str_ireplace(' ', '/', $controller);
            
            $path = $module . '/' . $controller . '.php';
            return self::$_basePath . $path;
        }
    }
    
    public static function getControllerClassName($uri) {
        /**
         * @todo Switch to config-dependent algorythm
         */
        $uri = 'xedin_' . $uri;
        $className = self::getClassName($uri) . 'Controller';
        return $className;
    }
    
    public static function getControllerInstance($uri) {
        $filePath = self::getControllerPath($uri);
        $className = self::getControllerClassName($uri);
        
        if( !file_exists($filePath) ) {
            throw new Exception('No controller fount at ' . $filePath . '.');
        }
        
        require_once($filePath);
        if( !class_exists($className) ) {
            throw new Exception('Class ' . $className . ' not found in ' . $filePath);
        }
        
        return new $className();
    }
    
    public static function getUrl($url = '', $type = self::URL_TYPE_BASE, $absolute = false) {
        switch( $type ) {
            case self::URL_TYPE_THEME:
                return  ( $absolute ? self::getUrl() : '' ) . 'wp-content' . get_raw_theme_root(get_template()) . DIRECTORY_SEPARATOR . get_template() . DIRECTORY_SEPARATOR . $url;
                break;
            
            case self::URL_TYPE_INCLUDES:
                return ( $absolute ? self::getUrl() : '' ) . WPINC . DIRECTORY_SEPARATOR . $url;
                break;
            
            case self::URL_TYPE_CONTENT:
                return ( $absolute ? self::getUrl() : '' ) . 'wp-content' . DIRECTORY_SEPARATOR . $url;
                
            case self::URL_TYPE_UPLOADS:
                return self::getUrl( 'uploads/' . $url, self::URL_TYPE_CONTENT, $absolute);
                
            default:
            case self::URL_TYPE_BASE:
                return get_bloginfo('wpurl') . DIRECTORY_SEPARATOR . $url;
                break;
        }
    }
    
    public function getPath($path='', $type = self::PATH_TYPE_BASE) {
        switch( $type ) {
            case self::PATH_TYPE_THEME:
                return self::getPath() . 'wp-content' . get_raw_theme_root(get_template()) . DIRECTORY_SEPARATOR . get_template() . DIRECTORY_SEPARATOR . $path;
                break;
            
            case self::PATH_TYPE_INCLUDES:
                return self::$_basePath . WPINC . DIRECTORY_SEPARATOR . $path;
                break;
            
            case self::PATH_TYPE_CONTENT:
                return self::getPath() . 'wp-content' . DIRECTORY_SEPARATOR . '/' . $path;
                
            case self::PATH_TYPE_UPLOADS:
                return self::getPath('uploads/' . $path, self::PATH_TYPE_CONTENT);
            
            case self::PATH_TYPE_BASE:
            default:
                return realpath('./') . '/' . $path;
                break;
        }
    }
	
	/**
	 * Gets the value of a constant.
	 * If the second parameter is omitted, gets a class constant of Mage_Loader.
	 * If the second parameter is a boolean false, gets a constant from the global namespace.
	 * If the second parameter is a string and is a model URI (contains the model URI separator), retrieves the constant of the class that the URI resolves to.
	 * If the second parameter is an object, and has a method called 'getConstant', retrieves the return value of that method.
	 * If the second parameter is an object, and does not have a method called 'getConstant', retrieves the value of the constant of the class of that object.
	 * 
	 * @see Mage_Loader::URI_SEPARATOR
	 * @param string $constantName Name of the constant to retrieve.
	 * @param string|object $class Either a class instance, or a class name, or a model URI.
	 * @param null|mixed The value to return if the constant is not eventually found.
	 * @return mixed Value of the retrieved constant.
	 */
	public static function getConstant($constantName, $class = null, $default = null) {
		$constantName = self::normalizeConstantName($constantName);
		
		if( $class === false ) {
			if( !defined($constantName) ) {
				return $default;
			}
			
			return constant( $constantName );
		}
		
		if( is_null($class) ) {
			$class = get_called_class();
			
			if( !is_null(self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName)) ) {
				return self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName);
			}
		}
		
		if( is_string($class) && (strpos($class, self::URI_SEPARATOR) !== false) ) {
			$class = self::getClassName(self::getModelUri($class));
		}
		
		if( is_object($class) ) {
			if( method_exists($class, 'getConstant') ) {
				return $class->getConstant($constantName);
			}
			$class = get_class($class);
		}
		
		
		$constantName = $class . '::' . $constantName;
		if( !defined($constantName) ) {
			return $default;
		}
		
		return constant($constantName);
	}
	
	/**
	 * Normalizes the given constant name.
	 * 
	 * @param string $constantName The constant name to normalized.
	 * @return string A valid constant name.
	 */
	public static function normalizeConstantName($constantName) {
		$constantName = str_ireplace('-', '_', $constantName);
		$constantName = strtoupper($constantName);
		return $constantName;
	}
	
	/**
	 * Whether or not a constant with the given name exists.
	 * See getConstant() for the places which will be checked.
	 * 
	 * @param string $constantName Name of the constant to check for.
	 * @param string|object $class See $class parameter of getConstant() for allowed values.
	 * @return boolean True if the constant exists, false otherwise. 
	 */
	public static function hasConstant($constantName, $class = null) {
		return is_null( self::getConstant($constantName, $class, null) );
	}
	
	/**
	 * Whether or not a constant exists internally to this class.
	 * 
	 * @param string $constantName Name of the constant to check for.
	 * @return boolean True if a constant with the given name exists; false otherwise.
	 */
	public static function hasOwnConstant($constantName) {
		$constantName = self::normalizeConstantName($constantName);
		return is_null(self::registry(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $constantName));
	}
	
	/**
	 * Sets a value for a constant.
	 * 
	 * @param string $name The name of the constant to set the value for.
	 * @param mixed $value The value to set to the constant.
	 */
	public static function setConstant($name, $value) {
		$name = self::normalizeConstantName($name);
		self::register(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $name, $value);
	}
	
	/**
	 * Sets a value for a constant if it doesn't already exist.
	 * 
	 * @param string $name The name of the constant to set the value for.
	 * @param mixed $value The value to give to the constant.
	 */
	public static function addConstant($name, $value) {
		if( !self::hasOwnConstant($name) ) {
			self::setConstant($name, $value);
		}
	}
	
	public static function unsConstant($name) {
		if( self::hasOwnConstant($name) ) {
			$name = self::normalizeConstantName($name);
			self::unregister(self::REG_PREFIX_CONSTANT . self::URI_SEPARATOR . $name);
		}
	}
}