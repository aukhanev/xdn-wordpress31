<?php

return array(
    'modules'           =>  array(
        'Xedin_Core'			=> array(
            'version'				=>  '0.1.0',
            'isActive'				=>  1
        ),
        'Xedin_Competitions'    => array(
            'version'				=>  '0.1.0',
            'isActive'				=>  0
        ),
        'Xedin_Cms'				=> array(
            'version'				=>  '0.1.0',
            'isActive'				=>  0
        ),
		'Xedin_Adminhtml'		=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		),
		'Xedin_Facebook'		=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		),
		'Xedin_Eav'				=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		),
		'Xedin_Clientside'		=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		),
		'Xedin_Crypt'		=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		),
		'Xedin_B2b'		=>	array(
			'version'				=>	'0.1.0',
			'isActive'				=>	0
		)
    )
);

?>