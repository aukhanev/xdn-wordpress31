<?php

return array(
	'global'			=>	array(
	
		'connections'       =>  array(
			/*
			 * Another connection for reading may be specified. If not, the
			 * connection for writing will be also used for reading.
			 */
			'core_write'        =>  array(
				
				// Test
				'username'              =>  'wp31-test',
				'password'              =>  'wp31-test',
				'host'                  =>  'localhost',
				'dbname'                =>  'wp31-test'
				
				// Live
//				'username'              =>  '572887_thepoint',
//				'password'              =>  'T1gershark',
//				'host'                  =>  'mysql51-007.wc2.dfw1.stabletransit.com',
//				'dbname'                =>  '572887_thepoint'
			)
		),

		'routers'                   =>  array(
			'adminhtml'                 =>  array(
				'frontName'                 =>  'admin',
				'module'                    =>  'Xedin_Adminhtml'
			),

			'competitions'						=> array(
				'frontName'					=>	'', // Default module
				'module'					=>	'Xedin_Cms'
			)
		),
		
		'uri'			=>	array(
			'segments'			=> array(
				/*
				 * Values that will be given to unspecified segments.
				 */
				'default'			=>	array(
					'controller'			=>	'Index',
					'action'				=>	'index'
				),
				
				'map'				=>	array(
					'admin'				=>	array(
						'rule'				=>	'%^/admin(/((?!login).)*(/\w*)?)?$%',
							'segments'		=>	array(
								'_0'			=>	'module',
								'_1'			=>	'admin_module',
								'_2'			=>	'model',
								'_3'			=>	'rest_action'
							)
					),
					
					/* Names of handles don't matter. However, the order does:
					 * First matched, first served.
//					 */
					'default'			=>	array(
						/*
						 * Must include delimiters. The actual expression must 
						 * begin with a slash.
						 */
						'rule'			=>	'!/\w*(/\w*(/\w*)?)?!',
							/*
							* Because segments may be skipped and/or specified in
							* any order, the tag name must be the segment index.
							* But because XML does not allow numbers as tag names,
							* prefix them with an underscore.
							*/
							'segments'		=>	array(
								'_0'			=>	'module',
								'_1'			=>	'controller',
								'_2'			=>	'action'
							)
					)
				)
			),
			
			'base_unsecure'		=>	'http://wordpress31.test/',
			'base_secure'		=>	'http://wordpress31.test/',
			
//			'base_unsecure'		=>	'http://direct.digitalmarketing.com.mt/clients/thepoint/04168/',
//			'base_secure'		=>	'https://direct.digitalmarketing.com.mt/clients/thepoint/04168/',
			
//			'base_unsecure'		=>	'http://www.digitalmarketing.com.mt/clients/thepoint/04168/',
//			'base_secure'		=>	'https://www.digitalmarketing.com.mt/clients/thepoint/04168/',
			
//			'skin_unsecure'		=>	'http://direct.digitalmarketing.com.mt/clients/thepoint/04168/',
//			'skin_secure'		=>	'https://direct.digitalmarketing.com.mt/clients/thepoint/04168/',
			
			'skin_unsecure'		=>	'http://wordpress31.test/',
			'skin_secure'		=>	'http://wordpress31.test/',
			
			/*
			 * Name of the controller to redirect to when page not found.
			 */
			'_404_controller'	=>	'404',
			
			/*
			 * Name of the module, whose controllers are considered to be admin.
			 */
			'admin_module_name'	=>	'adminhtml'
		),
		
		'session'			=>	array(
			'cookie'			=>	array(
				'domain'			=>	'wordpress31.test',
//				'domain'			=>	'.digitalmarketing.com.mt',
//				'path'				=>	'/',
				'path'				=>	'/',
				'expire'			=>	0,
				
			)
			
		),
		
		'design'		=>	array(
			'theme'			=>	array(
				'default'		=>	'base'
			),
			'template'		=>	array(
				'default'		=>	'default'
			)
		)
	)
);