<?php
/*
Plugin Name: WPXDN Test
Description: A test plugin for the WPXDN framework
Version: 0.1.0
Author: Xedin Unknown
*/

class Xedin_Wordpress_Plugin_Test extends Xedin_Wordpress_Model_Plugin_Abstract {
	
	protected function _run() {
		parent::_run();
		
//		Hub::dispatchEvent('my_other_test_event', array('my_additional_arg' => '12321654353fddxzc'), $this);
	}
}

class Xedin_Wordpress_Observer_Test extends Xedin_Wordpress_Model_Event_Observer_Abstract {
	
	/**
	 * @param Xedin_Core_Model_Event $event 
	 */
	public function observeTest($event) {
//		echo 'Some other head output';
//		var_dump($event->getCaller());
	}
}

//$wpxdn = Hub::getSingleton('wordpress/wordpress')
//		->getPlugin('wpxdn_test')
//		->run(__FILE__);


//var_dump($wpxdnTest->getDependencies());
