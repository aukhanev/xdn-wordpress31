<?php
/*
Plugin Name: XDN Framework for Wordpress
Description: A modification of the XDN framework for use with WordPress
Author: Xedin Unknown <xedin.unknown+wpxdn@gmail.com>
Version:0.1
*/

/**
 * Determines if this plugin is the first in the list of active plugins, e.g.
 * at index 0. If not shifts it to the beginning of the plugin queue.
 */
function xdn_loadFirst() {
	// ensure path to this file is via main wp plugin path
	$thisFilePath = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__);
	$thisPlugin = plugin_basename(trim($thisFilePath));
	$activePlugins = get_option('active_plugins');
	$thisPluginKey = array_search($thisPlugin, $activePlugins);
	if ($thisPluginKey) { // if it's 0 it's the first plugin already, no need to continue
		array_splice($activePlugins, $thisPluginKey, 1);
		array_unshift($activePlugins, $thisPlugin);
		update_option('active_plugins', $activePlugins);
	}
}

/**
 * Found in /wp-admin/includes/plugin.php ln. 536.
 * Runs after the list of active plugins has been saved.
 */
add_action("activated_plugin", "xdn_loadFirst");
//do_action();

function xdn_afterPluginRow() {
	echo '<tr><td colspan="3">';
//	$configModules = Hub::getSingleton('core/config_instance_module')->getCollection()->load();
//	Hub::getConfig()->getConfigs(dirname(__FILE__) . DS . 'etc', Xedin_Config::CONFIG_TYPE_XML);
//	echo '<pre>';
//	var_dump(Hub::getConfig()->getValue());
//	echo '</pre>';
//	var_dump($configModules);
//	$block = Hub::getBlock('core/modules');
//	echo $block->toHtml();
?>

<?php
	echo '</td></tr>';
}

add_action('after_plugin_row', 'xdn_afterPluginRow');

#===============================================================================
#   INITIALIZATION
#===============================================================================
define( 'DS', DIRECTORY_SEPARATOR );
define( 'PS', PATH_SEPARATOR );

define( 'PATH_ROOT', trim(ABSPATH, ' /') );

define( 'PATH_INCLUDES', PATH_ROOT . DS . 'includes' . DS );
define( 'PATH_APP', PATH_ROOT . DS . 'wp-content' . DS . 'app' . DS );
//define( 'PATH_CONFIG', PATH_INCLUDES . DS . 'config.php' );

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
$hubPath = PATH_APP . DS . 'Hub.php';
require_once $hubPath;

$originalIncludePath = get_include_path();
$includePath = explode( PS, $originalIncludePath );
$includePath[] = PATH_ROOT . DS . 'wp-includes' . DS . 'library';
$includePath[] = PATH_APP . 'code' . DS . 'local';
$includePath[] = PATH_APP . 'code' . DS . 'community';
$includePath[] = PATH_APP . 'code' . DS . 'core';
$includePath = implode( PS, $includePath );

set_include_path( $includePath );

require_once( 'Loader.php' );
Loader::isSuppressingClassExistsAutoloadWarning(true);
Loader::init();

Hub::isDebugMode(true);
Hub::run(array(
	'config_type'			=>	'xml'
));

#===============================================================================
#	Plugin Code
#===============================================================================

class Xedin_Wordpress_Plugin_Wpxdn_Framework extends Xedin_Wordpress_Model_Plugin_Abstract {
	
	protected $_wordpressEventPrefix = 'wp_';
	
	protected function _run() {
		parent::_run();
		
		// Include all observers
		$this->includeDirectory('Observer/*/');
		$this->getWordpress()->addAction( 'all', array($this, 'handleAllWpActions') );
	}
	
	/**
	 * A callback for the wordpress 'all' action.
	 * This dispatches a standard XDN event.
	 * The event code is lowercased and has dashes ("-") converted to underscores ("_").
	 * The caller visible in handlers is a singleton of Xedin_Wordpress_Model_Wordpress.
	 * @param $tag The tag of the action being handled.
	 */
	public function handleAllWpActions() {
		$args = func_get_args();
		$tag = array_shift($args);
		$tag = strtolower($tag);
		$tag = str_replace(array('-', '/', '.'), '_', $tag);
//		var_dump($tag);
		Hub::dispatchEvent($this->getWordpressEventPrefix($tag), $args, $this->getWordpress());
		
		return $this;
	}
	
	/**
	 * Returns a prefix used for wordpress-generated events.
	 * If the first parameter is given, it will be prefexed.
	 *  
	 * @param string|null $eventCode The event code to prefix. Default: empty string ('')/
	 * @return string The prefix or prefixed event code.
	 */
	public function getWordpressEventPrefix($eventCode = '') {
		return $this->_wordpressEventPrefix . $eventCode;
	}
	
	public function afterThisPluginRow() {
		$block = Hub::getBlock('core/modules');
		echo '<tr><td></td><td colspan="2">' . $block->toHtml() . '</td></tr>';
	}
}

$wpxdn = Hub::getSingleton('wordpress/wordpress')
		->getPlugin('wpxdn_framework')
		->run(__FILE__);
