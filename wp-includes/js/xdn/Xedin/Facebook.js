var Xedin_Facebook = function(fB) {

    var This = this;
    var _fB = fB || This._initFb();
    
    This._permissions = {};
    
    This.compareObjects = function(obj1, obj2, strict) {
        for( var prop in obj1 ) {
            if( !obj2[prop] ) {}
            return false;
            
            if( !strict && obj2[prop] != obj1[prop] ) {
                return false;
            }
            
            if( obj2[prop] !== obj1[prop] ) {
                return false;
            }
        }
        
        return true;
    }

    This.raiseEvent = function(eventName, args) {
        jQuery(This).triggerHandler(eventName, args);

        return null;
    },

    This.addEvent = function(eventName, callback) {
        jQuery(This).bind(eventName, callback);
        return This;
    },

    This.detachEvent = function(eventName, callback) {
        if( !callback ) {
            jQuery(This).unbind(eventName);
        }

        jQuery(This).unbind(eventName, callback);
        return This;
    },

    This.setEvent = function(eventName, handler) {
        jQuery(This).one(eventName, handler);
        return This;
    },

    This.getLoginStatus = function(callback, force) {
        if( undefined === force ) force = true;

        This.getFb().getLoginStatus(function() {
            This.raiseEvent('onGetLoginStatus', arguments);
        }, force);
        return This;
    }

    This.authorise = function(permissions) {        
        This.setEvent('onGetLoginStatus', function(e, response) {
            if( response.status != 'connected' && response.status != 'not_authorised' ){
                if( !permissions ) {
                     This.getFb().login(function(loginResponse) {
                        This.raiseEvent('onAuthorise', arguments);
                    });
                }
                else {
                    This.getFb().login(function(loginResponse) {
                        This.raiseEvent('onAuthorise', arguments);
                    }, {scope: permissions});
                }
            }
            else {
                This.raiseEvent('onAuthorise', arguments);
            }
            
            return This;
        });
        

        This.getLoginStatus();
    },

    This.api = function(uri, callback, permissions, params) {
        if( !params ) {
            params = {};
        }
        
        if( !permissions ) permissions = false;
        if( !callback ) callback = false;
        
        This.setEvent('onAuthorise', function(e, response) {
            
            if( !uri.charAt(0) === '/' ) {
                uri = '/' + uri;
            }
            if( !callback ) {
                This.getFb().api(uri, 'GET', params);
                return true;
            }

            This.getFb().api(uri, 'GET', params, callback);
            return This;
        });
        
        This.authorise( permissions );
        return This;
    },

    This.getFb = function() {
        return _fB;
    },

    This._initFb = function() {
        return FB;
    }
}