
/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
var Xedin_Competition_Fortunewheel_Animator = null;
(function($){
	Xedin_Competition_Fortunewheel_Animator = {
		_frameInterval: 150,
		_wheelSelector: null,
		_defaultAnimationDuration: 3000,
		_frameWidth: 627,
		_maxFrames: 16,
		_isAnimationInProgress: false,
		_animationProperties: {
		},
		
		_currentFrame: 0,
		_seeker: 0,
		_rounds: null,
		_framesToAnimate: null,
		_timer: null,
		_finishCallback: null,
		
		init: function(wheelSelector) {
			this._wheelSelector = wheelSelector;
			$.easing.frames = this._myFrameEasing;
			return this;
		},
		
		setFrameInterval: function(interval) {
			this._frameInterval = interval;
			return this;
		},
		
		getFrameInterval: function() {
			return this._frameInterval;
		},
		
		setFrameWidth: function(width) {
			this._frameWidth = width;
			return this;
		},
		
		getFrameWidth: function() {
			return this._frameWidth;
		},
		
		getWheelSelector: function() {
			return this._wheelSelector;
		},
		
		isAnimationInProgress: function() {
			return this._isAnimationInProgress;
		},
		
//		animate: function(sectors) {
//			if( this.isAnimationInProgress() ) {
//				return this;
//			}
//			
//			var formerInterval = jQuery.fx.interval;
//			jQuery.fx.interval = this.getFrameInterval();
//			this._prepareAnimationProperties(sectors);
//			console.log('sectors: ' + sectors);
//			console.log(this.getAnimationProperties());
//			var This = this;
//			$(This.getWheelSelector()).css('background-position-x', 0)
//			.animate(This.getAnimationProperties(), {
//				duration: This._defaultAnimationDuration,
//				easing: 'frames',
//				complete: This._onAnimationEnd,
//				step: This._onEachFrame
//			});
//			this._isAnimationInProgress = true;
//			jQuery.fx.interval = formerInterval;
//			return this;
//		},

		animate: function(sectors, callback) {
			this._currentFrame = 0;
			this._seeker = 0;
			this._framesToAnimate = this.getRandomRoundsFrameCount(sectors, 2, 5);
			if( callback ) {
				this._finishCallback = callback;
			}
			this._timer = window.setTimeout(this._myFrameEasing, this._frameInterval);
			this._isAnimationInProgress = true;
			Math.floor((Math.random()*10)+1);
		},
		
		stop: function() {
			window.clearTimeout(this._timer);
			this._isAnimationInProgress = false;
			this._finishCallback.call();
			return this;
		},
		
		getRandomRoundsFrameCount: function(sectors, from, to) {
			var rounds = Math.floor((Math.random()*to)+from);
			var extraFrames = rounds * (this._maxFrames);
			return sectors + extraFrames;
		},
		
//		_myFrameEasing: function(t, millisecondsSince, startValue, endValue, totalDuration) {
//			return Xedin_Competition_Fortunewheel_Animator._currentFrame * Xedin_Competition_Fortunewheel_Animator._frameWidth;
//		},
		
		_myFrameEasing: function() {
			This = Xedin_Competition_Fortunewheel_Animator;
			$(This.getWheelSelector()).css('background-position', (-This._frameWidth * This._currentFrame) + 'px 0' );
			This._currentFrame += 1;
			if( This._currentFrame >= (This._maxFrames) ) {
				This._currentFrame = 0;
			}
			
			This._seeker += 1;
			if( This._seeker == This._framesToAnimate+1 ) {
				This.stop();
			}
			
			if( This._isAnimationInProgress ) {
				var interval = (This._framesToAnimate - This._seeker ) <= 4 ? 400 : This._frameInterval;
				setTimeout(This._myFrameEasing, interval);
			}
			
			return this;
		},
		
		setAnimationProperty: function(property, value) {
			this._animationProperties[property] = value;
			return this;
		},
		
		unsetAnimationProperty: function(property) {
			if( this._animationProperties.hasOwnProperty(property) ) {
				delete this._animationProperties[property];
			}
			
			return this;
		},
		
		getAnimationProperties: function(prepare) {
			if( prepare ) {
				this._prepareAnimationProperties();
			}
			return this._animationProperties;
		},
		
		_prepareAnimationProperties: function(sectors) {
			var finalX = sectors * this.getFrameWidth();
			this.setAnimationProperty('background-position', -finalX + 'px 0px');
			this._defaultAnimationDuration = this._frameInterval * sectors;
			return this;
		},
		
		_onAnimationEnd: function() {
			Xedin_Competition_Fortunewheel_Animator._isAnimationInProgress = false;
			This._currentFrame = 0;
		}
		
//		_onEachFrame: function(now, fx) {
//			This._currentFrame += 1;
//			console.log('frame ' + this._currentFrame + ': ' + now + 'px');
//		}
	};
}(jQuery))

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.0
 */
var Xedin_Competition_Fortunewheel = null;
(function($) {
	Xedin_Competition_Fortunewheel = {
		_baseUrl: null,
		_defaultRequestDataType: 'json',
//		_defaultRequestDataType: null,
		_defaultRequestType: 'spin',
		_responseHandlers: {
		},
		_lastResponse: null,
		_animator: null,
		_finishCallback: null,
				
		init: function(baseUrl, animator) {
			this._baseUrl = baseUrl;
			this._animator = animator || Xedin_Competition_Fortunewheel_Animator.init('#fortune-wheel');
			this.addResponseHandler('spin', this.animateWheel);
			
			return this;
		},
		
		_executeResponseHandler: function(type, response) {
			if( this._responseHandlers.hasOwnProperty(type) ) {
				this._responseHandlers[type].call(this, response);
			}
			
			return this;
		},
		
		_receiveResponse: function(response, status, jqXHR) {
			var This = Xedin_Competition_Fortunewheel;
			This._lastResponse = response;
			This._executeResponseHandler(response.type, response);
//			This._trace(response);
			return This;
		},
		
		_sendRequest: function(data, dataType) {
			if( !dataType ) {
				dataType = this._defaultRequestDataType;
			}
			
			$.post(this._baseUrl, data, this._receiveResponse, dataType);
		},
		
		_trace: function(data) {
			console.log(data);
		},
		
		_getNextSector: function() {
			this._sendRequest(this._prepareRequestData());
		},
		
		_prepareRequestData: function(data, type) {
			type = this._fillIfEmpty(type, this._defaultRequestType);
			data = this._fillIfEmpty(data, {});
			
			var requestData = {};
			requestData.data = data;
			requestData.type = type;
			
			return requestData;
		},
		
		_fillIfEmpty: function(data, alt) {
			if( undefined === data ) {
				return alt;
			}
			
			return data;
		},
		
		_loadMap: function() {
			
		},
		
		spin: function(callback) {
			if( callback ) {
				this._finishCallback = callback;
			}
			this._getNextSector();
		},
		
		setResponseHandler: function(type, handler) {
			this._responseHandlers[type] = handler;
			return this;
		},
		
		
		addResponseHandler: function(type, handler) {
			if( !this._responseHandlers.hasOwnProperty(type) ) {
				this.setResponseHandler(type, handler);
			}
			
			return this;
		},
		
		removeResponseHandler: function(type) {
			if( this._responseHandlers.hasOwnProperty(type) ) {
				delete this._responseHandlers['type'];
			}
			
			return this;
		},
		
		getAnimator: function() {
			return this._animator;
		},
		
		setAnimator: function(animator) {
			this._animator = animator;
			return this;
		},
		
		animateWheel: function(sectors) {
			if( typeof(sectors) == 'object' ) {
				sectors = sectors.sector.sort_order;
			}
			
			sectors = parseInt(sectors);
			this.getAnimator().animate(sectors, this._finishSpin);
		},
		
		getLastResponse: function() {
			return this._lastResponse;
		},
		
		_finishSpin: function() {
			var This = Xedin_Competition_Fortunewheel;
			$('.leaderboard').replaceWith(This.getLastResponse().html);
			This._finishCallback.call(This, This.getLastResponse());
		}
	}
}(jQuery) )

