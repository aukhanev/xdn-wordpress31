var Xedin_Competition_Abstract = null;
(function($) {
	Xedin_Competition_Abstract = {
		
		This: null,
		_mainSelector: null,
		_mainBlock: null,
		_baseUrl: null,
		_key: null,

		_executeGameAction: function(data, status, jqXHR, callback) {
			if( data.error ) {
				console.log(error);
				return This;
			}
			
			This.getMainBlock().html(data);
			FB.XFBML.parse();
			
			if( callback ) {
				callback();
			}
			
			return This;
		},

		init: function(mainSelector) {
			This = this;
			if( mainSelector ) {
				This.mainSelector = mainSelector;
			}
			return This;
		},

		executeGameAction:	function(gameId, data, callback) {
			var $mainBlock = This.getMainBlock();
			$mainBlock.empty();
			
			if( !data ) {
				data = {};
			}
			
			This.sendProtectedRequest( This.getBaseUrl() +'/code/' + gameId, data, This._executeGameAction, callback );
			return This;
		},
		
		
		
		getMainBlock: function() {
			if( !This._mainBlock ) {
				This._mainBlock = $(This.mainSelector);
			}
			
			return This._mainBlock;
		},
		
		setBaseUrl: function(baseUrl) {
			This._baseUrl = baseUrl;
			return This;
		},
		
		getBaseUrl: function() {
			return This._baseUrl;
		},
		
		setKey: function(key) {
			This._key = key;
			return This;
		},
		
		getKey: function() {
			return This._key;
		},
		
		takeAuthorisedAction: function(action) {
			if( !FB ) { return false; }

			var fbPermissions = "email";

			FB.getLoginStatus(function(response) {
				if( response.status != "connected" && response.status != "not_authorised" ) {
					FB.login(function(loginResponse) {
						if( loginResponse.status == "connected" ) {
							action();
						}
					}, {scope: fbPermissions});
					return;
				}
				action();
			});
		},
		
		sendProtectedRequest: function(url, data, callback, key) {
			if( !key ) {
				key = This.getKey();
			}
			
			if( key ) {
				data['request_key'] = key;
			}
			
			$.post(url, data, callback);
			return This;
		}
	}
}(jQuery))