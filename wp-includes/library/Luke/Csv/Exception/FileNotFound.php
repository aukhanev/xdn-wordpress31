<?php
/**
 * File Not Found Exception
 * 
 * Please read the LICENSE file
 * 
 * @package 	PHP CSV Utilities
 * @subpackage  Exceptions
 * @copyright 	(c) 2010 Luke Visinoni <luke.visinoni@gmail.com>
 * @author 		Luke Visinoni <luke.visinoni@gmail.com>
 * @license 	GNU Lesser General Public License
 * @version 	$Id: FileNotFound.php 81 2010-04-22 02:24:16Z luke.visinoni $
 */
class Luke_Csv_Exception_FileNotFound extends Luke_Csv_Exception{}
