<?php

class Object {
    protected $_data;

    /**
     * Get all data, or data at the specified key
     * @param null|string $key The key of the data slot to retreive
     * @return mixed|array|null The data at specified key if found; null otherwise.
     */
    public function getData($key = null) {
        if( is_null($key) ) {
            return $this->_data;
        }

        if( isset($this->_data[$key]) ) {
            return $this->_data[$key];
        }

        return null;
    }

    /**
     * If key is specified, sets the data at that key.
     * Otherwise, replaces the current data with the given.
     * @param mixed|array $value All a part of data to set.
     * @param null|string $key The key of the data to set.
     * @return Object This instance.
     */
    public function setData($value, $key = null) {
        if( is_null($key) ) {
            $this->_data = $value;
        }

        $this->_data[$key] = $value;
        return $this;
    }

    /**
     * Adds the given data to the existing.
     * Any existing keys will be overwritten.
     * @param array $values An array of values to add to the current data.
     * @return Object This instance.
     */
    public function addData($values) {
        if( is_array($values) )  {
            foreach( $values as $key => $_value ) {
                $this->setData( $_value, $key );
            }
        }

        return $this;
    }

    /**
     * Checks whether this object has the requested data.
     * If $key is specified, will check at that key.
     * Otherwise, will check if at least any data exists.
     * @param string|null $key The key of this data to check for.
     * @return bool Whether or not the requested data exists in this object.
     */
    public function hasData($key = null) {
        if( is_null($key) ) {
            return empty($this->_data);
        }

        return isset($this->_data[$key]);
    }
}

?>