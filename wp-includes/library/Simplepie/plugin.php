<?php

/*
    Plugin Name: SimplePie Rss
    Description: This is a slight modification of the easy to use API that handles all of the dirty work when it comes to fetching, caching, parsing, normalizing data structures between RSS and Atom formats, handling character encoding translation, and sanitizing the resulting data.
    Version: 0.1
    Author: Xedin Unknown 
    Author URI: https://plus.google.com/116166066110848708831
    Plugin URI: http://simplepie.org/
 
    This is a slight modification to the SimplePie PHP framework, made by Xedin Unknown. The author of the modifications
    is not responsible for any misunderstanding of the purpose of this plugin, and has no intension of distributing it
    as a standalone and/or commercial product. THIS PLUGIN IS INDENDED TO USE WITH THE "MAGE PLUGINS" AND "XEDIN COMPONENTS"
    PLUGINS ONLY.
    
    For a reference to the framework, visit http://simplepie.org/.
    For information on modifications, e-mail xedin.unknown@gmail.com.
*/
?>
