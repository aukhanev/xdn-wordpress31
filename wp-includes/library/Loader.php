<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.2
 * + Added isFunctionInCallStack() function.
 * + Added isSuppressingClassExistsAutoloadWarning() function.
 * * autoload() function can now check if called by class_exists(). See isSuppressingClassExistsAutoloadWarning().
 */
class Loader {

	protected static $_isSuppressingClassExistsAutoloadWarning = false;

    public static function autoload($className) {
		if( self::isSuppressingClassExistsAutoloadWarning() && self::isFunctionInCallStack('class_exists') ) {
			return @Hub::loadClass($className, !Hub::isDebugMode());
		}
        return Hub::loadClass($className, !Hub::isDebugMode());
    }

    public static function init() {
        spl_autoload_register( __CLASS__ . '::autoload' );
    }
	
	/**
	 *
	 * @param string $functionName Name of the function to check for
	 * @param int $limit How many steps in stack to check. 0 is unlimited.
	 * @return boolean True if function is in call stack; false otherwise.
	 */
	public static function isFunctionInCallStack($functionName, $limit = 0) {
		$functionName = trim($functionName);
		$limit = (int)$limit;
		$callStack = debug_backtrace();
		$i = 0;
		foreach( $callStack  as $_idx => $_call ) {
			if( $_call['function'] == $functionName ) {
				return true;
			}
			
			if( $limit && ($limit - 1) == $i ) {
				return false;
			}
		}
		
		return false;
	}
	
	public static function isSuppressingClassExistsAutoloadWarning($isSuppressing = null) {
		if( is_null($isSuppressing) ) {
			return self::$_isSuppressingClassExistsAutoloadWarning;
		}
		
		self::$_isSuppressingClassExistsAutoloadWarning = (bool)$isSuppressing;
		return null;
	}
}