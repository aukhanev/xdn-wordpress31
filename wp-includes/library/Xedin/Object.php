<?php

/**
 * An OO-wrapper for Varien_Object.
 *
 * @version 0.1.2
 * + Added hasId(), which returns whether or not the ID key, dependent on the ID field name, is set.
 * 
 * version 0.1.1
 * + Passes constructor arguments to parent constructor
 * + If first constructor argument is an instance of Varien_Object, turns that argument into an array containing the instance's data.
 * 
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Object extends Varien_Object
{
//	public function setData($name, $value = null) {
//		if( !strpos($name, '/') ) {
//			return parent::setData($name, $value);
//		}
//		
//		$data = $this->_data;
//		$keys = explode('/', $name);
//		$i = 0;
//		foreach( $keys as $_idx => $_key ) {
//			$isLast = $i == count($keys - 1);
//			
//			if( $isLast ) {
//				$data[$_key] = $value;
//				break;
//			}
//			
//			if( isset($data[$_key]) ) {
//				if( !is_array($data[$_key]) ) {
//					$data[$_key] = array($data[$key]);
//				}
//			}
//			else {
//				$data[$_key] = array();
//			}
//			
//			$data =& $data[$_key];
//			
//			$i++;
//		}
//	}
	
	public function __construct() {
		$args = func_get_args();
		if( isset($args[0]) && $args[0] instanceof Varien_Object ) {
			$obj = $args[0];
			$args[0] = $obj->getData();
		}
		
		call_user_func_array(array('parent', '__construct'), $args);
	}
	
    public function setData($key, $value=null)
    {
        if( $key instanceof Varien_Object ) {
			$key = $key->getData();
		}
		
		parent::setData($key, $value);
		return $this;
    }
	
	public function hasId() {
		if ( $idFieldName = $this->getIdFieldName() ) {
			return $this->hasData($idFieldName);
		}
		
		return $this->hasData('id');
	}
	
	public static function getCalledClass() {
		if( function_exists('get_called_class') ) {
			return get_called_class();
		}
		$backtrace = debug_backtrace();
		return $backtrace[count($backtrace)-1]['class'];
	}
}