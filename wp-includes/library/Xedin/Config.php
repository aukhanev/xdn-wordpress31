<?php

/**
 * @author Xedin Unknown <xedin.unknown@gmail.com>
 * @version 0.1.2
 * * XML Configs now retrieved as canonical array if destination is an array.
 * * getConfigs() now returns the returned config, after mergin it with the existing data.
 * 
 * version 0.1.1
 * + Inclusion algorithm moved to _getIncludeConfigs();
 * + Added _getArrayValue() - unified algorithm for internal value retrieval.
 * + Added getTypeExtension() function.
 * + Added getConfigs() function.
 */
class Xedin_Config {
    protected static $_data;

    protected $_modelClassNames = array();
	protected $_options = array();

    const CONFIG_TYPE_INCLUDE = 'include';
    const CONFIG_TYPE_ARRAY = 'array';
	const CONFIG_TYPE_XML = 'xml';
	
	const TYPE_EXTENSION_INCLUDE = '.php';
	const TYPE_EXTENSION_XML = '.xml';
	
	const NODE_DELIMITER = '/';

    public function init($options) {
        $defaultOptions = array(
            'etc_path'              =>  'etc',
            'config_path'           =>  'etc/config',
            'config_type'           =>  'include',
            'config_data'           =>  array()
        );

        $this->_options = array_merge( $defaultOptions, $options );

        switch( $this->_options['config_type'] ) {
            case self::CONFIG_TYPE_ARRAY:
                $this->getConfigs($this->_options['config_data']);
                break;

            default:
				$this->getConfigs(PATH_APP . $this->_options['etc_path'], $this->getOptions('config_type'));
				$path = PATH_APP . $this->_options['etc_path'] . DS . 'modules';
				$this->getConfigs($path, $this->_options['config_type']);
				foreach( $this->_getArrayValue('modules', self::$_data) as $_moduleName => $_moduleConfig ) {
					$moduleConfigsPath = $this->_getModuleConfigDir($_moduleName);
					if( $this->_getArrayValue('isActive', $_moduleConfig) && file_exists( $moduleConfigsPath ) ) {
						$this->getConfigs($moduleConfigsPath, $this->_options['config_type']);
					}
				}
        }

    }
	
	/**
	 * 
	 * @param type $type
	 * @return type 
	 */
	public function getTypeExtension($type = null) {
		if( empty($type) ) {
			$this->getOptions('config_type');
		}
		
		$extension = null;
		
		switch( $type ) {
			case self::CONFIG_TYPE_XML:
				$extension = self::TYPE_EXTENSION_XML;
			
			case self::CONFIG_TYPE_INCLUDE:
			default:
				$extension = self::TYPE_EXTENSION_INCLUDE;
		}
		
		return $extension;
	}
	
	/**
	 * Retrieves type configs using the method appropriate for the type, and merges them with existing data.
	 * @param string|array $path Path or array of folder names where to get the configs from. Trailing directory separator optional.
	 * @param string $type One of the CONFIG_TYPE_* constants.
	 * @return array|Varien_Simplexml_Config The config that was retrieved and merged.
	 */
	public function getConfigs($path, $type = self::CONFIG_TYPE_INCLUDE) {
		$path = trim($path, ' ' . DS);
		$return = $this;
		switch( $type ) {
            case self::CONFIG_TYPE_ARRAY:
				if( is_array($path) )
                self::$_data = $path;
				$return = $path;
                break;
				
			case self::CONFIG_TYPE_XML:
				$return = $this->_getXmlConfigs($path);
				break;
			
			case self::CONFIG_TYPE_INCLUDE:
			default:
				$return = $this->_getIncludeConfigs($path);
				
		}
		return $return;
	}
	
	/**
	 * @param string $path The absolute path from where to retrieve the configs.
	 * @return array The config that was retrieved and merged.
	 */
	protected function _getIncludeConfigs($path) {
		$configFiles = $this->_getConfigPaths($path, self::CONFIG_TYPE_INCLUDE);
		$newConfig = array();
		foreach( $configFiles as $_pathIdx => $_filePath ) {
			$newConfig = array_merge_recursive( $newConfig, require_once($_filePath) );
		}
		self::$_data = array_merge_recursive(self::$_data, $newConfig);
		return $newConfig;
	}
	
	/**
	 * @param string $path The absolute path from where to retrieve the configs.
	 * @return \Varien_Simplexml_Config The config that was retrieved and merged.
	 */
	protected function _getXmlConfigs($path) {
		$configFiles = $this->_getConfigPaths($path, self::CONFIG_TYPE_XML);
		$xml = new Varien_Simplexml_Config();
		$xml->loadString('<?xml version="1.0" encoding="UTF-8"?><config></config>');
		foreach( $configFiles as $_pathIdx => $_filePath ) {
			$newXml = new Varien_Simplexml_Config();
			$newXml->loadFile($_filePath);
			$xml->extend($newXml);
		}
		
		if( empty(self::$_data) ) {
			self::$_data = $xml;
		}
		elseif( self::$_data instanceof Varien_Simplexml_Config ) {
			/* @var self::$_data Varien_Simplexml_Config */
			self::$_data->extend($xml);
		}
		elseif( is_array(self::$_data) ) {
			self::$_data = array_merge_recursive(self::$_data, (array)$xml->getNode()->asCanonicalArray());
		}
		
		return $xml;
	}
	
	protected function _getConfigPaths($directoryPath, $type) {
		$expression = $directoryPath . DS;
		switch( $type ) {
			case self::CONFIG_TYPE_XML:
				$expression .= '*' . self::TYPE_EXTENSION_XML;
				break;
			case self::CONFIG_TYPE_INCLUDE:
			default:
				$expression .= '*' . self::TYPE_EXTENSION_INCLUDE;				
		}
		return glob($expression);
	}

	/**
	 *
	 * @param type $path
	 * @param type $data
	 * @return string|array|Varien_Simplexml_Element
	 */
    public function getValue($path = null, $data = null) {
		if( is_null($data) ) {
			$data = self::$_data;
		}
		
        if( is_null($path) ) {
            return self::$_data;
        }

		if( !is_array($path) ) {
			$path = explode( self::NODE_DELIMITER, $path );
		}


		return $this->_getArrayValue($path, self::$_data);
    }
	
	/**
	 * Get a value from the array or Varien_Simplexml_Element at a specified path.
	 * 
	 * @param string|array $path Either a string, where index/node names are separated with NODE_DELIMITER, or an array of index/node names.
	 * @param array|Varien_Simplexml_Element $array The array or Varien_Simplexml_Element, from which to get the value.
	 * @param int $level The current level of depth. For internal use only. 
	 */
	public function getArrayValue($path, $array) {
		return $this->_getArrayValue($path, $array, 0);
	}
	
	/**
	 * Get a value from the array or Varien_Simplexml_Element at a specified path.
	 * 
	 * @param string|array $path Either a string, where index/node names are separated with NODE_DELIMITER, or an array of index/node names.
	 * @param array|Varien_Simplexml_Element $array The array or Varien_Simplexml_Element, from which to get the value.
	 * @param int $level The current level of depth. For internal use only.
	 * @return Varien_Simplexml_Element|mixed The value at the specified path.
	 */
	protected function _getArrayValue($path, $array, $level = 0) {
		if( !is_array($path) ) {
			$path = trim($path, ' ' . self::NODE_DELIMITER);
			$path = explode(self::NODE_DELIMITER, $path);
		}
		
		$currentNodeName = array_shift($path);
//			var_dump($currentNodeName);
//			var_dump($path);
		
		$currentNodeValue = null;
		switch( true ) {
			case $array instanceof Varien_Simplexml_Config:
				$currentNodeValue = $array->getNode($currentNodeName);
//				$currentNodeValue = $currentNodeValue->hasChildren() ? $currentNodeValue->children() : $currentNodeValue;
				break;
			
			case $array instanceof Varien_Simplexml_Element:
				$currentNodeValue = $array->descend($currentNodeName);
//				$currentNodeValue = $currentNodeValue->hasChildren() ? $currentNodeValue->children() : $currentNodeValue;
				break;
			
			case is_array($array):
				$currentNodeValue = isset($array[$currentNodeName]) ? $array[$currentNodeName] : null;
				break;
		}

		// If current node index does not exist, return null
		if( !$currentNodeValue ) {
			return null;
		}
//		if( $array instanceof Varien_Simplexml_Config && !$array->getNode($currentNodeName) ) {
//			return null;
//		}
//		elseif( $array instanceof Varien_Simplexml_Element && !$array->descend($currentNodeName) ) {
//			return null;
//		}
//		elseif( is_array($array) && !isset($array[$currentNodeName]) ) {
//			return null;
//		}

		// If current index is last, return the value
		if( count($path) == 0  ) {
			if( $currentNodeValue instanceof Varien_Simplexml_Element ) {
				if( $currentNodeValue->children()->count() ) {
//					echo '[children]';
					$currentNodeValue = $currentNodeValue->children();
				}
				else {
//					echo '[nochildren]';
					$currentNodeValue = (string)$currentNodeValue;
				}
			}
			return $currentNodeValue;
//			if( $array instanceof Varien_Simplexml_Config ) {
//				echo '_getArrayValue: ' . "\n";
//				var_dump($array->getNode($currentNodeName));
//				return $array->getNode($currentNodeName)->hasChildren();
//			}
//			elseif( $array instanceof Varien_Simplexml_Element ) {
//				echo 'Children: ' . "\n";
//				var_dump($array->descend($currentNodeName));
//				return $array->descend($currentNodeName)->children();
//			}
//			return $array[$currentNodeName];
		}

		// If the current value is not and array and there's still more path, return null
		if( !is_array($currentNodeValue) && !$currentNodeValue->hasChildren() ) {
//			var_dump('Still more path');
//			var_dump($currentNodeValue->hasChildren());
			return null;
		}
//		if( $array instanceof Varien_Simplexml_Config && !$array->getNode($currentNodeName) ) {
//			return null;
//		}
//		elseif( $array instanceof Varien_Simplexml_Element && !$array->descend($currentNodeName) ) {
//			return null;
//		}
//		elseif( is_array($array) && !is_array($array[$currentNodeName]) ) {
//			return null;
//		}
//		
//		var_dump($array);
//		$nextNode = $array instanceof Varien_Simplexml_Config ? $array->getNode($currentNodeName) : $array[$currentNodeName];
		// If this is not the last index in path, and the value is an array
		return $this->_getArrayValue($path, $currentNodeValue, $level+1);
	}
	
	protected function _getNormalizedPath($path, $before = null, $after = null) {
		$args = func_get_args();
		foreach( $args as $idx => $_arg ) {
			if( !empty($_arg) && is_string($_arg) ) {
				$args[$idx] = explode(self::NODE_DELIMITER, $_arg);
			}
		}
		
		$path = $args[0];
		
		if( !empty($args[1]) ) {
			$path = array_merge($args[1], $path);
		}
		
		if( !empty($args[2]) ) {
			$path = array_merge($args[2], $path);
		}
		
		return $path;
	}

    public function getModelClassName($modelPath) {
        $modelPathArr = explode('/', $modelPath);
        $modelConfig = $this->getModelConfig($modelPath);
        $moduleName = $modelPathArr[0];
        $modelClassStart = $modelConfig['class'];
        if( !$modelClassStart ) {
            $modelClassStart = 'Xedin_' . $moduleName . '_Model';
        }

        return Hub::getClassName($modelClassStart . '_' . $modelPathArr[1]);
    }

    public function getHelperClassName($helperPath) {
        $modelPathArr = explode('/', $helperPath);
        $modelConfig = $this->getHelperConfig($helperPath);
        $moduleName = $modelPathArr[0];
        $modelClassStart = $modelConfig['class'];
        if( !$modelClassStart ) {
            $modelClassStart = 'Xedin_' . $moduleName . '_Helper';
        }

        return Hub::getClassName($modelClassStart . '_' . $modelPathArr[1]);
    }

    public function getResourceModelClassName($modelPath) {
        $modelPathArr = explode('/', $modelPath);
        $moduleName = $modelPathArr[0];

        $modelConfig = $this->getModelConfig($modelPath);
        $resourceModelName = $modelConfig['resourceModel'];
        if( !$resourceModelName ) {
			if( !isset($modelPathArr[1]) ) {
				throw Hub::exception('Xedin_Core', 'Model path incomplete.');
			}
			
            return $this->getModelClassName( $moduleName . '/' . 'resource_' . $modelPathArr[1] );
        }
        return Hub::getClassName( Hub::getConfig('global/models/' . $resourceModelName . '/class') . '_' . $modelPathArr[1] );
    }

    public function getModelConfig($modulePath) {
        $modelPathArr = explode( '/', $modulePath );
        $moduleName = $modelPathArr[0];
        return Hub::getConfig('global/models/' . $moduleName);
    }

    public function getHelperConfig($helperPath) {
        $modelPathArr = explode( '/', $helperPath );
        $moduleName = $modelPathArr[0];
        return Hub::getConfig('global/helpers/' . $moduleName);
    }

    public function getResourceModelConfig($modelPath) {
        $modelConfig = $this->getModelConfig($modelPath);
        return $this->getModelConfig( $modelConfig['resourceModel'] );
    }

    protected function _getModuleConfigDir($moduleName) {
        $relPath = str_ireplace( '_', DS, $moduleName ) . DS . 'etc';
        if( file_exists(PATH_APP . 'code' . DS . 'local' . DS . $relPath) ) {
            return PATH_APP . 'code' . DS . 'local' . DS . $relPath;
        }
        if( file_exists(PATH_APP . 'code' . DS . 'community' . DS . $relPath) ) {
            return PATH_APP . 'code' . DS . 'community' . DS . $relPath;
        }
        return PATH_APP . 'code' . DS . 'core' . DS . $relPath;
    }
	
	public function getOptions($path = null) {
		if( is_null($path) ) {
			return $this->_options;
		}
		
		return $this->_getArrayValue($path, $this->_options);
	}
}

?>