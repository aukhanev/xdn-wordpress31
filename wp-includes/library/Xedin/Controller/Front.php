<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 * * Fixed notFound() - now does 404; 
 */
class Xedin_Controller_Front extends Xedin_Controller_Abstract {

    protected $_currentController;
	
	protected $_is404 = false;
	
	const SUFFIX_CONTROLLER = 'Controller';
	const SUFFIX_ACTION = 'Action';
	const DEFAULT_PARAM_VALUE = 'default';
	
	const XML_PATH_URI_404_CONTROLLER = 'global/uri/_404_controller';

    public function dispatch() {
		$this->_beforeDispatch();
		
        
		$controllerModule = $this->getActualModuleName();
		
		$xmlPathModule = 'modules/' . $controllerModule;
		$xmlPathModuleIsActive = $xmlPathModule . '/isActive';
		if( !Hub::getConfig($xmlPathModule) || !Hub::getConfig($xmlPathModuleIsActive) ) {
			$this->_404();
			return $this;
		}
		
		$controllerName = ucfirst( $this->getControllerName() ) . self::SUFFIX_CONTROLLER;
        $controllerPath = ucfirst( str_ireplace( '_', '/', $controllerModule ) ) . DS . 'controllers' . DS . ucfirst($controllerName) . '.php';
		include_once $controllerPath;
		
		$controllerClassName = $controllerModule . '_' . $controllerName;
		if( !class_exists($controllerClassName) ) {
//			exit('3');
			$this->_404();
			return $this;
		}
		
        $this->_currentController = new $controllerClassName;
        $controllerMethod = $this->getActionName() . self::SUFFIX_ACTION;
		
		if( !method_exists($controllerClassName, $controllerMethod) ) {
//			exit('4');
			$this->_404();
			return $this;
		}
		
        $this->getCurrentController()->$controllerMethod();
		$this->_afterDispatch();
		
		return $this;
    }
	
	/**
	 * Start handling a 404 case.
	 * Attempt to dispatch a 404 controller before sending 404.
	 * 
	 * @todo If 404 request is coming from within a controller,
	 * this should reset all the previous params and output.
	 * Perhaps, using 
	 * @see self::XML_PATH_URI_404_CONTROLLER
	 * @return Xedin_Controller_Front
	 */
	protected function _404() {
		if( !$this->_is404 ){
			// Attempt to dispatch a 404 controller
			$this->setControllerName(Hub::getConfig(self::XML_PATH_URI_404_CONTROLLER));
//			var_dump(Hub::getConfig()->getValue());
			$defaultSegments = $this->_getDefaultUriSegments();
//			var_dump($defaultSegments);
			$this->setActionName($defaultSegments['action']);
			$this->_is404 = true;
			return $this->dispatch();
		}
		
		header('Not found', true, 404);
		echo('The page which you are looking for does not exist.');
		exit();
	}
	
	public function notFound() {
		$this->_404();
	}
	
	public function is404() {
		return (bool)$this->_is404;
	}
    
    public function getNormalizedRequestPath($delimiter = self::XML_SEGMENT_SEPARATOR) {
		$normalizedString = $this->getModuleName();
		$controller = $this->getControllerName();
		$action = $this->getActionName();
		$normalizedString .= $delimiter . $controller . $delimiter . $action;
		
		return strtolower($normalizedString);
    }
}