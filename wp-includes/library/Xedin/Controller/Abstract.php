<?php

/**
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 * @version 0.1.1
 * + Fixed errors related to retreiving config values 
 */
abstract class Xedin_Controller_Abstract extends Zend_Controller_Request_Abstract {

	/** @var Xedin_Controller_Request_Http */
    protected $_request;

    protected $_originalSegments;
	protected $_segments;
	
	protected $_sectionKeys = array();
	protected $_isExplicitKeyOverrides = false;
	
	protected $_originalModule;
	protected $_originalController;
	protected $_actualModule;
	

    protected $_isRewritten = false;
	protected $_isAdmin;

	/** @var Xedin_Controller_Abstract */
    protected $_currentController;
	
	protected $_urlMap;
	

    const XML_PATH_ROUTERS = 'global/routers';
	const XML_PATH_URI_SEGMENTS_DEFAULT = 'global/uri/segments/default';
	const XML_PATH_URI_SEGMENTS_MAP = 'global/uri/segments/map';
	const XML_PATH_URI_ADMIN_MODULE_NAME = 'global/uri/admin_module_name';
	
	const URL_SEGMENT_SEPARATOR = '/';
	const XML_SEGMENT_SEPARATOR = '_';
	const XML_NUMERIC_TAG_PREFIX = '_';

    public function __construct() {
		$this->_originalSegments = $this->parsePathInfo();
		$this->_segments = $this->getNormalizedSegments();
		
		$this->setSectionKeys( $this->getUrlMap() )
				->setParams( $this->_getNormalizedParams() );
		
		
		$this->_originalModule = $this->getModuleName();
		$this->_originalController = $this->getControllerName();

        $this->rewrite();
    }
	
	public function getUrlMap($index = null, $default = null) {
		if( !$this->_urlMap ) {
			$this->_urlMap = $this->_getUrlMap();
		}
		
		if( is_null($index) ) {
			return $this->_urlMap;
		}
		
		if( !isset($this->_urlMap[$index]) ) {
			return $default;
		}
		
		return $this->_urlMap[$index];
	}
	
	protected function _getUrlMap() {
		$originalMap = (array)Hub::getConfig(self::XML_PATH_URI_SEGMENTS_MAP);
		$urlMap = array();
		
		foreach( $originalMap as $_name => $_info ) {
			$_info = (array)$_info;
			if( !preg_match($_info['rule'], $this->getRequest()->getPathInfo()) ) {
				continue;
			}
			
			foreach( $_info['segments'] as $_index => $_segmentName ) {
				$_index = trim($_index, ' _');
				$urlMap[$_index] = $_segmentName;
			}
			return $urlMap;
		}
		
		
	}
	
	public function getNormalizedSegments($segment = null, $default = null) {
		if( !$this->_segments ) {
			$this->_segments = $this->_getNormalizedSegments();
		}
		
		if( is_null($segment) ) {
			return $this->_segments;
		}
		
		if( !isset($this->_segments[$segment]) ) {
			return $default;
		}
		
		return $this->_segments[$segment];
	}
	
	public function parsePathInfo($pathInfo = null) {
		if( is_null($pathInfo) ) {
			$pathInfo = $this->getRequest()->getPathInfo();
		}
		
		$pathInfo = trim($pathInfo, ' /' . self::XML_NUMERIC_TAG_PREFIX);
		
		return explode(self::URL_SEGMENT_SEPARATOR, $pathInfo);
	}
	
	public function getSectionKeys($index = null, $default = null) {
		if( is_null($index) ) {
			return $this->_sectionKeys;
		}
		
		if( !isset($this->_sectionKeys[$index]) ) {
			return $default;
		}
		
		return $this->_sectionKeys[$index];
	}
	
	public function hasSectionKeys($index = null) {
		if( is_null($index) ) {
			return (bool)count($this->getSectionKeys());
		}
		
		
		if( is_numeric($index) ) {
			$index = intval($index);
			return isset($this->_sectionKeys[$index]);
		}
		
		return in_array($index, $this->_sectionKeys);
	}
	
	/**
	 *
	 * @param type $index
	 * @param type $value
	 * @return \Xedin_Controller_Request_Http 
	 */
	public function setSectionKeys($index, $value = null) {
		if( is_null($value) && is_array($index) ) {
			foreach( $index as $_index => $_value ) {
		
				$this->setSectionKeys($_index, $_value);
			}
			
			return $this;
		}
		$index = intval($index);
		$this->_sectionKeys[$index] = $value;
		
		return $this;
	}
	
	/**
	 * Makes sure the URI segments are in the necessary order.
	 * Applies defaults.
	 * 
	 * @param type $segments
	 * @return type 
	 */
	protected function _getNormalizedSegments($segments = null) {
		if( is_null($segments) ) {
			$segments = $this->getOriginalSegments();
		}
		
		return $segments;
	}
	
	/**
	 * Parses a URL path (everything between base path and query) into an array
	 * according to the map specified in section keys. Example:
	 * 
	 * URL: "one/two/three/four/five/six/7"
	 * Result:
	 * array(5) {
	 *	["module"]=>
	 *  string(3) "one"
	 *  ["controller"]=>
	 *	string(3) "two"
	 *	["action"]=>
	 *	string(5) "three"
	 *	["four"]=>
	 *	string(4) "five"
	 *	["six"]=>
	 *	string(1) "7"
	 *	}
	 * 
	 * @param type $pathInfo
	 * @param type $default
	 * @return type 
	 */
	protected function _getNormalizedParams($pathInfo = null, $default = null) {
		if( is_null($pathInfo) ) {
			$pathInfo = $this->getRequest()->getPathInfo();
		}
		
		if( is_string($pathInfo) ) {
			$pathInfo = $this->parsePathInfo($pathInfo);
		}
		
		/* @var array $sections The normalized segments in key => value pairs */
		$sections = array();
		foreach( $pathInfo as $_index => $_segment ) {
			if( !$this->hasSectionKeys($_index) ) {
				$this->setSectionKeys($_index+1, $_segment);
				$sections[$this->getSectionKeys($_index+1)] = $default;
				continue;
			}
			$sections[(string)$this->getSectionKeys($_index)] = $_segment;
		}
		
		$sections = array_merge($this->_getDefaultUriSegments(), $sections);
		return $sections;
	}
	
	protected function _getDefaultUriSegments() {
//		var_dump(Hub::getConfig('global/uri'));
		return $defaultSegments = (array)Hub::getConfig(self::XML_PATH_URI_SEGMENTS_DEFAULT);
	}
	
	public function isExplicitKeyOverrides($overrides = null) {
		if( is_null($overrides) ) {
			return $this->_isExplicitKeyOverrides;
		}
		
		$this->_isExplicitKeyOverrides = (bool)$overrides;
	}

	/**
	 *
	 * @return Xedin_Controller_Request_Http
	 */
    public function getRequest() {
		if( !$this->_request ) {
			$this->_request = Hub::getLibrary('xedin_controller_request_http');
		}
        return $this->_request;
    }

	/**
	 * @todo The Xedin vendor is supposed to be default, but configurable.
	 * Right now, however, only controllers from the Xedin vendor modules will be dispatched.
	 * @return \Xedin_Controller_Abstract 
	 */
    public function rewrite($frontName = null) {
        $routers = (array)Hub::getConfig( self::XML_PATH_ROUTERS );
		$moduleName = (is_null($frontName)) ? $this->getModuleName() : $frontName;
        foreach( $routers as $toModule => $_router ) {
			$_router = (array)$_router;
            if( (string)$_router['frontName'] == $moduleName ) {
                $this->setModuleName($toModule);
                $this->_actualModule = $_router['module'];
                $this->_isRewritten = true;
                return;
            }
        }
		
        $this->_actualModule = 'Xedin_' . ucfirst( $this->getModuleName() );

        return $this;
    }
	
	public function isRewritten() {
		return $this->_isRewritten;
	}
	
	public function getActualModuleName() {
		return $this->_actualModule;
	}

    public function getOriginalSegments($index=null) {
        if( is_null($index) ) {
            return $this->_originalSegments;
        }

        if( !isset($this->_originalSegments[$index]) ) {
            return null;
        }

        return $this->_originalSegments[$index];
    }

	/**
	 * @return Xedin_Core_Controller_Abstract
	 */
    public function getCurrentController() {
        return $this->_currentController;
    }
	
	protected function _beforeDispatch() {
		
	}
	
	protected function _afterDispatch() {
		
	}

//    public abstract function dispatch();
	
	public function redirect($url = '', $prepare = true, $params = array()) {
		header('Location: ' . $this->getRedirectUrl($url, $prepare, $params));
		exit();
	}
	
	public function getRedirectUrl($url = '', $prepare = true, $params = array()) {
		if( $prepare && !is_array($url) ) {
			$sections = explode('/', $url);
		}
		else {
			$sections = $url;
		}
		
		
		$redirectUrlSegments = array();
		$sectionKeys = $this->getSectionKeys();
		$i = 0;
		foreach( $sectionKeys as $_idx => $_key ) {
			if( isset($sections[$_idx]) ) {
				if( preg_match('!(\*)!', $sections[$_idx]) ) {
					$redirectUrlSegments[$i] = preg_replace('!(\*)!', $sections[$_idx], $this->getParam($sectionKeys[$_idx]));
				}
				else {
					$redirectUrlSegments[$i] = $sections[$_idx];
				}
			}
			$i++;
		}
		
		$additionalParams = array();
		foreach( $params as $_key => $_value ) {
			$additionalParams[] = $_key;
			$additionalParams[] = $_value;
		}
		
		$redirectUrlSegments = array_merge($redirectUrlSegments, $additionalParams);
		$url = implode('/', $redirectUrlSegments);
		
		if( $prepare ) {
			$url = $this->getUrl($url);
		}
		
		return $url;
	}
	
	public function setParams(array $array) {
		parent::setParams($array);
		$this->getRequest()->setParams($array);
		return $this;
	}
	
	public function getUrl($path = '') {
		return Hub::getUrl($path);
	}
	
	/**
	 * If parameter specified and is not null, will set the request to be an admin request.
	 * If no parameter specified, returns whether or not this is an admin request.
	 * The first time this is called with no parameters, the controller will 
	 * autodetect whether or not it is handling an admin request.
	 * 
	 * @see self::XML_PATH_URI_ADMIN_MODULE_NAME
	 * @param null|bool $isAdmin Whether or not the request should be treated as an admin request.
	 * @return \Xedin_Controller_Abstract|bool Either whether or not an admin controller has been invoked, or this instance.
	 */
	public function isAdmin($isAdmin = null) {
		if( !is_null($isAdmin) ) {
			$this->_isAdmin = (bool)$isAdmin;
			return $this;
		}
		
		if( is_null($this->_isAdmin) ) {
			$isAdmin = $this->getModuleName() == $this->getAdminModuleName();
			$this->isAdmin($isAdmin);
		}
		
		return $this->_isAdmin;
	}
	
	public function getAdminModuleName() {
		return (string)Hub::getConfig(self::XML_PATH_URI_ADMIN_MODULE_NAME);
	}
}

?>