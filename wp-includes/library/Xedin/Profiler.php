<?php

/**
 * A padding around Varien_Profiler.
 * But not really. Actually, this is an instance wrapper that separates
 * multiple "profiles" through prefexing, and delegation to Varien_Profiler's
 * static methods. Lol. =P
 *
 * @version 0.1.0
 * @author Xedin Unknown <xedin.unknown+xdn@gmail.com>
 */
class Xedin_Profiler {
	
	protected $_profileId;
	
	const PROFILE_ID_SEPARATOR = '>>';
	
	public function __construct($profileId) {
		$this->_profileId = $profileId;
	}
	
	public function getProfileId() {
		return $this->_profileId;
	}
	
	public function reset($timerName) {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::reset($timerName);
		
		return $this;
	}
	
	public function resume($timerName) {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::resume($timerName);
		
		return $this;
	}
	
	public function start($timerName) {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::start($timerName);
		
		return $this;
	}
	
	public function pause($timerName) {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::pause($timerName);
		
		return $this;
	}
	
	public function stop($timerName) {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::stop($timerName);
		
		return $this;
	}
	
	public function fetch($timerName, $key = 'sum') {
		$timerName = $this->getTimerName($timerName);
		Varien_Profiler::fetch($timerName, $key);
		
		return $this;
	}	
	public function getTimerName($timerName) {
		return $this->getProfileId() . self::PROFILE_ID_SEPARATOR . $timerName;
	}
}